<?php

namespace App\Channels;

use Illuminate\Notifications\Notification;
use Illuminate\Bus\Queueable;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Channels\DatabaseChannel as IlluminateDatabaseChannel;

class DatabaseChannel extends IlluminateDatabaseChannel
{
     use Queueable;
    /**
    * Send the given notification.
    *
    * @param mixed $notifiable
    * @param \Illuminate\Notifications\Notification $notification
    * @return \Illuminate\Database\Eloquent\Model
    */
  
    public function send($notifiable, Notification $notification)
    {
        return $notifiable->routeNotificationFor('database')->create([
            'id'      => $notification->id,
            'type'    => get_class($notification),
            'notifiable_type'=>get_class($notification),
            'notifiable_id'=>$notification->notifiable_id ?? null,
            'data'    => $this->getData($notifiable, $notification),
            'read_at' => null,
            'created_at'=>Carbon::now(),
            'updated_at'=>Carbon::now(),
            'added_by'=>null,
             'company_id'=> $data['company_id']  ?? null,
        ]);
    }
}