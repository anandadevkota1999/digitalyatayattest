<?php
   
namespace App\Console\Commands;
   
use Illuminate\Console\Command;
use App\Rules\Nepali_Calendar as NepaliCalender;
use App\Models\Users\Driver;
use App\Models\Notification as Mynotification;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Admin\Transactions\GeneralShulka;
use App\Models\Admin\Transactions\GeneralShulkaKharcha;
use App\Models\Admin\Transactions\GeneralKharchaDummy;
use App\Models\Admin\Transactions\JournalEntryOfGeneralShulka;
use App\Models\Admin\Transactions\FiscalYear;
use App\Models\Admin\Transactions\AccountPriceDeclaration;
use App\Models\Admin\Transactions\VehicleFixedAccount;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;

class AddFixedTopicsMonthly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'addfixedpaymenttopicmonthly:cron';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update fixed vehicle account monthly';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //system log
        \Log::info("Cron is working fine!");
     DB::beginTransaction();
       $getfixedtopics=AccountPriceDeclaration::where('payment_option','fixed')->get();
       $get=[];
       foreach($getfixedtopics as $fixedtopics){
           $paymenttopic=$fixedtopics->payment_topic;
           $paymentprice=$fixedtopics->payment_price;
           $expirydate=$fixedtopics->expiry_date;
           $paymenttype=$fixedtopics->payment_type;
           $parsedate=Carbon::parse($expirydate);
           $addonemonth=$parsedate->addDays(30)->toDateString();
           $today=Carbon::now()->toDateString();
           $countanyshulka=$fixedtopics->where('payment_topic',$paymenttopic)->where('payment_option','fixed')->where('payment_type',$paymenttype)->whereDate('expiry_date','>=',$today)->count('id'); 
      //  return $this->sendResponse($countanyshulka,'data that is of one month to the expiry date is added  successfully!!');
         if($countanyshulka<1){
           if($today==$addonemonth&&$paymenttype=$paymenttype){
               //get price from fixed account of specific topic
             $paymenttopic=$fixedtopics->payment_topic;
             if($paymenttopic==$paymenttopic){
            $shulka=VehicleFixedAccount::where('payment_topic',$paymenttopic)->first();
            if(!is_null($shulka)){
            $shulkaprice=$shulka->payment_price;
            //then add the price to the vehicle aacount
            $model=new AccountPriceDeclaration();
            $model->bus_no=$fixedtopics->bus_no;
            $model->payment_topic=$fixedtopics->payment_topic;
            $model->payment_option=$fixedtopics->payment_option;
            $model->payment_price=$shulkaprice;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$addonemonth;
            $model->company_id=$fixedtopics->company_id;
            $model->added_by=$fixedtopics->added_by;
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$fixedtopics->fiscal_year;
            $model->save();  
            $get[]=['datas'=>$model];
            }
             }
           }  
         }
      /*   if($countanyshulka>1){
               return $this->sendError('data has already been added this month on this topic!!'); 
         } */
         $get[]=['datas'=>$countanyshulka];
       } 
       DB::commit(); 
        return $response = \Response::json($showupdateddays, 200);
    }
}