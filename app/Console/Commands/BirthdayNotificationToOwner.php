<?php
   
namespace App\Console\Commands;
   
use Illuminate\Console\Command;
use App\Rules\Nepali_Calendar as NepaliCalender;
use App\Models\Users\busOwner;
use App\Models\Notification as Mynotification;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;

class BirthdayNotificationToOwner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'birthdayowner:cron';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Birthday reminder to owners';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //system log
        \Log::info("Cron is working fine!");
         //notification contents:
           
     //server key
            define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
          //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',]; 
            //get all driver data:
            $getalldata=busOwner::get();
            $get=[];
            $getbirthdate=[];
            //using calender to convert date in english
            $callcalender=new NepaliCalender();
            //looping for sending notification as per their birthday: 
            foreach($getalldata as $selectedonly){
            //get birthday    
           
            //get name,device_token and birthdate
            $get[]=[
            $getname=$selectedonly->owner_name,
            $gettoken=$selectedonly->device_token,
            $getdate=$selectedonly->owner_dob,
                ];
        //start of:today nepali date section....... 
           $nepalidate=Carbon::now()->toDateString();
           $correctformatfornepali=str_replace('-', ',',$nepalidate);
           $nepalidatearray=explode (",", $correctformatfornepali);
           //using converter
           $nyear=$nepalidatearray[0];
           $nmonth=$nepalidatearray[1];
           $nday=$nepalidatearray[2];
           $convertonepali= $callcalender->eng_to_nep($nyear,$nmonth,$nday);
            $nslice =array_shift($convertonepali);
            //implding to string format
           $ntostring=implode(",",$convertonepali);
           //then,nepali month and day value
          $nmonthandday = str_replace( ',', ' ', $ntostring );
        //end of today nepali date section......
          
            //for birthday::=> put in correct format:
            $correctformat= str_replace('-', ',',$getdate);
            //then,convert to array format
            $makeitarray = explode (",", $correctformat); 
          // return $this->sendResponse($makeitarray,'Notification successfully sent!!');
          //get year,month and day separately
            $year=$makeitarray[0];
            $month=$makeitarray[1];
            $trmonth=ltrim($month, '0');
            $day=$makeitarray[2];
            $trday=ltrim($day, '0');
            $nepalibirthdate=$trmonth.' '.$trday;
         //   return $this->sendResponse($nepalibirthdate,'Notification successfully sent!!');
        
     if($nmonthandday==$nepalibirthdate){
            //then notify::=> each driven via tokens:
            //sending request field:for whom(reciever) and for what(data):::for owner
             $notify=array('title'=>'Birthday Reminder','body'=>'HAPPY Birthday to you '.$getname.'!'.' ,Best wishes for you','sound' => 'default');
        $fields= json_encode(array( 'to'=> $gettoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
             $store->type='general';
            $store->notifiable_id=$selectedonly->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Birthday Reminder';
            $store->body='HAPPY Birthday to you '.$getname.'!'.' ,Best wishes for you';
            $store->added_by='Backend';
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$selectedonly->company_id;
            $store->save();
            }
       
            }
          return $response = \Response::json($get, 200);

    }
}