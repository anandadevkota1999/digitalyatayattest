<?php
   
namespace App\Console\Commands;
   
use Illuminate\Console\Command;
use App\Rules\Nepali_Calendar as NepaliCalender;
use App\Models\Users\StaffInfo;
use App\Models\Notification as Mynotification;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;

class BirthdayNotificationToStaff extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'birthdaystaff:cron';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Birthday reminder to staff';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //system log
        \Log::info("Cron is working fine!");
        //notification contents:
            //server key
            define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
          //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',]; 
            //get all driver data:
            $getalldata=StaffInfo::get();
            $get=[];
            $getbirthdate=[];
            //using calender to convert date in english
            $callcalender=new NepaliCalender();
            //looping for sending notification as per their birthday: 
            foreach($getalldata as $selectedonly){
            //get birthday    
           
            //get name,device_token and birthdate
            $get[]=[
            $getname=$selectedonly->staff_name,
            $gettoken=$selectedonly->device_token,
            $getdate=$selectedonly->staff_dob,
                ];
            //for birthday::=> put in correct format:
            $correctformat= str_replace('-', ',',$getdate);
            //then,convert to array format
            $makeitarray = explode (",", $correctformat); 
           
            //get year,month and day separately
            $year=$makeitarray[0];
            $month=$makeitarray[1];
            $day=$makeitarray[2];
            
            //finally,convert into english date
            $getbirthdate= $callcalender->nep_to_eng($year,$month,$day);
           // return $this->sendResponse($getbirthdate,'Notification successfully sent!!');
            //slicing year to get month and day
            $slice =array_shift($getbirthdate);
            //implding to string format
           $tostring=implode(",",$getbirthdate);
           //then,month and day value
          $monthandday = str_replace( ',', ' ', $tostring );

        // return $this->sendResponse($monthandday,'Notification successfully sent!!');
            //get today english date
            $todayenglishdate=Carbon::now()->toDateString();
            //now putting today english date into correct format
            $todayenglishdateformat=str_replace('-', ',',$todayenglishdate);
            //then,convert today english date to array format
            $makeitarray = explode (",", $todayenglishdateformat); 
            //then,finally get month and year in array
             $emonths=$makeitarray[1];
             $emonth=ltrim($emonths, '0');
            $edays=$makeitarray[2];
            $eday=ltrim($edays, '0');
            $todayenglishmonthandday=$emonth.' '.$eday;
           //return $this->sendResponse($todayenglishmonthandday,'Notification successfully sent!!');
           //comparing today date with birthdate and notifying on that date:
           if($todayenglishmonthandday==$monthandday){
           //then notify::=> each driven via tokens:
            //sending request field:for whom(reciever) and for what(data):::for owner
             $notify=array('title'=>'Birthday Reminder','body'=>'HAPPY Birthday to you '.$getname.'!'.' ,Best wishes for you','sound' => 'default');
        $fields= json_encode(array( 'to'=> $gettoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
             $store->type='general';
            $store->notifiable_id=$selectedonly->staff_id;
            $store->notifiable_type=StaffInfo::class;
            $store->title='Birthday Reminder';
            $store->body='Birhtday of:'.$getname;
            $store->added_by='Backend';
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$selectedonly->company_id;
            $store->save();
            }
           } 
          return $response = \Response::json($get, 200);

    }
}