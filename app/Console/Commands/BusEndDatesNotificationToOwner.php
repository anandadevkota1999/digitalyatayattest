<?php
   
namespace App\Console\Commands;
use Illuminate\Console\Command;
use App\Rules\Nepali_Calendar as NepaliCalender;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\Users\busOwner;
use App\Models\Notification as Mynotification;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;   
class BusEndDatesNotificationToOwner extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'busenddates:cron';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Bus end dates reminder to owner';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("Cron is working fine!");
  //1st notify owner of Bus::
         // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
        
        //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
          //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',]; 
            //using calender-converter::
            $callcalender=new NepaliCalender();
        //get owner of bus:
        $getownerofbus=businfo::join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->select('bus_owner.owner_id','bus_owner.owner_name','bus_owner.owner_phone','bus_owner.device_token','bus_info.*')->get();
        $getinarray=[];
        $getbillbookedate=[];
        $getcheckpassedate=[];
        //looping for all owners of each bus
        foreach($getownerofbus as $ownerdatas){
            //getrelevent datas
            $getdataarray[]=[
                $busno=$ownerdatas->bus_no,
                $ownername=$ownerdatas->owner_name,
                $ownerdevicetoken=$ownerdatas->device_token,
                $billbookenddate=$ownerdatas->bus_billbook_enddate,
                $routepermitenddate=$ownerdatas->bus_routepermit_enddate,
                $checkpassenddate=$ownerdatas->bus_checkpass_enddate,
                $insuranceenddate=$ownerdatas->bus_insurance_enddate,
                ];
        //start of:today nepali date section....... 
           $nepalidate=Carbon::now()->toDateString();
           $correctformatfornepali=str_replace('-', ',',$nepalidate);
           $nepalidatearray=explode (",", $correctformatfornepali);
           //using converter
           $nyear=$nepalidatearray[0];
           $nmonth=$nepalidatearray[1];
           $nday=$nepalidatearray[2];
           $convertonepali= $callcalender->eng_to_nep($nyear,$nmonth,$nday);
            $nslice =array_shift($convertonepali);
            //implding to string format
           $ntostring=implode(",",$convertonepali);
           //then,nepali month and day value
          $nmonthandday = str_replace( ',', ' ', $ntostring );
        //end of today nepali date section......        
        //now,-------------------- for billbook enddate::::
          //put in correct format:
            $bcorrectformat= str_replace('-', ',',$billbookenddate);
            //then,convert to array format
            $bmakeitarray = explode (",", $bcorrectformat); 
            //get year,month and day separately
            $byear=$bmakeitarray[0];
            $bmonth=$bmakeitarray[1];
            $btrmonth=ltrim($bmonth, '0');
            $bday=$bmakeitarray[2];
            $btrday=ltrim($bday, '0');
            $bmonthandday=$btrmonth.' '.$btrday;
         
          //------------******* 7 day before the enddate ******----------- 
          $bwarnbmonthday=Carbon::parse($billbookenddate)->subDays('6');
          $bwdateonly=$bwarnbmonthday->toDateString();
          $bwarnformat=str_replace('-', ',',$bwdateonly);
          $bwmakeitarray = explode (",", $bwarnformat); 
            //then,finally get month and year in array
             $bwmonths=$bwmakeitarray[1];
             $bwmonth=ltrim($bwmonths, '0');
            $bwdays=$bwmakeitarray[2];
            $bwday=ltrim($bwdays, '0');
            $bwarnmonthandday=$bwmonth.' '.$bwday;
      
         
    //----------------end of billbok part-----------------------
    //now,-------------------- for routepermit enddate::::
     //put in correct format:
            $rcorrectformat= str_replace('-', ',',$routepermitenddate);
            //then,convert to array format
            $rmakeitarray = explode (",", $rcorrectformat); 
            //get year,month and day separately
            $ryear=$rmakeitarray[0];
            $rmonth=$rmakeitarray[1];
            $rtrmonth=ltrim($rmonth, '0');
            $rday=$rmakeitarray[2];
            $rtrday=ltrim($rday, '0');
            $rmonthandday=$rtrmonth.' '.$rtrday;
          //    return $this->sendResponse($rmonthandday,'Notification successfully sent!!'); 
         //------------******* 7 day before the enddate ******----------- 
          $rwarnbmonthday=Carbon::parse($routepermitenddate)->subDays('6');
          $rwdateonly=$rwarnbmonthday->toDateString();
           //now putting warning english date into correct format
            $rwarnformat=str_replace('-', ',',$rwdateonly);
            //then,convert today english date to array format
            $rwmakeitarray = explode (",", $rwarnformat); 
            //then,finally get month and year in array
             $rwmonths=$rwmakeitarray[1];
             $rwmonth=ltrim($rwmonths, '0');
             $rwdays=$rwmakeitarray[2];
             $rwday=ltrim($rwdays, '0');
            $rwarnmonthandday=$rwmonth.' '.$rwday;
      //   return $this->sendResponse($rwarnmonthandday,'Notification successfully sent!!');
    //----------------end of route permit part-----------------------
    //now,-------------------- for checkpass enddate::::
     //put in correct format:
            $chcorrectformat= str_replace('-', ',',$checkpassenddate);
            //then,convert to array format
            $chmakeitarray = explode (",", $chcorrectformat); 
            //get year,month and day separately
            $chyear=$chmakeitarray[0];
            $chmonth=$chmakeitarray[1];
            $ctrmonth=ltrim($chmonth, '0');
            $chday=$chmakeitarray[2];
            $ctrday=ltrim($chday, '0');
            $chmonthandday=$ctrmonth.' '.$ctrday;
          //   return $this->sendResponse($chmonthandday,'Notification successfully sent!!');  
         //------------******* 7 day before the enddate ******----------- 
          $cwarnbmonthday=Carbon::parse($checkpassenddate)->subDays('6');
          $cwdateonly=$cwarnbmonthday->toDateString();
           //now putting warning english date into correct format
            $cwarnformat=str_replace('-', ',',$cwdateonly);
            //then,convert today english date to array format
            $cwmakeitarray = explode (",", $cwarnformat); 
            //then,finally get month and year in array
             $cwmonths=$cwmakeitarray[1];
             $cwmonth=ltrim($cwmonths, '0');
             $cwdays=$cwmakeitarray[2];
             $cwday=ltrim($cwdays, '0');
            $cwarnmonthandday=$cwmonth.' '.$cwday;
      //    return $this->sendResponse($cwarnmonthandday,'Notification successfully sent!!');
     //----------------end of route permit part-----------------------   
    //finally,-------------------- for insurance enddate:::: 
    //put in correct format:
            $incorrectformat= str_replace('-', ',',$insuranceenddate);
            //then,convert to array format
            $inmakeitarray = explode (",", $incorrectformat); 
            //get year,month and day separately
            $inyear=$inmakeitarray[0];
            $inmonth=$inmakeitarray[1];
            $intrmonth=ltrim($inmonth, '0');
            $inday=$inmakeitarray[2];
            $intrday=ltrim($inday, '0');
            $inmonthandday=$intrmonth.' '.$intrday;
       //    return $this->sendResponse($inmonthandday,'Notification successfully sent!!');   
        //------------******* 7 day before the enddate ******----------- 
          $inwarnbmonthday=Carbon::parse($insuranceenddate)->subDays('6');
          $inwdateonly=$inwarnbmonthday->toDateString();
           //now putting warning english date into correct format
            $inwarnformat=str_replace('-', ',',$inwdateonly);
            //then,convert today english date to array format
            $inwmakeitarray = explode (",", $inwarnformat); 
            //then,finally get month and year in array
             $inwmonths=$inwmakeitarray[1];
             $inwmonth=ltrim($inwmonths, '0');
            $inwdays=$inwmakeitarray[2];
            $inwday=ltrim($inwdays, '0');
            $inwarnmonthandday=$inwmonth.' '.$inwday;
      //    return $this->sendResponse($inwarnmonthandday,'Notification successfully sent!!');
     //----------------end of insurance part-----------------------  
      //--------------------------------------------------------
      //checking multiple condition for end date cases:
      
      $addsixdaystonow=Carbon::now()->addDays('6')->toDateString();
    //  return $this->sendResponse($addsixdaystonow,'Notification successfully sent!!');
      //bilbook end date:
    if($nmonthandday==$bmonthandday){
    //sending request field:for whom(reciever) and for what(data):::for owner
    //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'Billbook date of  '.$busno.' is expired.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
         $store->body='Billbook date of  '.$busno.' is expired.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
       //     return $this->sendResponse($store,'Notification successfully sent!!');
          }
        //warning:before 6 day of billbook end date !     
    if($nmonthandday==$bwarnmonthandday){
        //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'Billbook date of '.$busno.' is on '.$addsixdaystonow.'.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
            $store->body='Billbook date of '.$busno.' is on '.$addsixdaystonow.'.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
           // return $this->sendResponse($store,'Notification successfully sent!!');

        }
    //for Route permit end date:    
    if($nmonthandday==$rmonthandday){
             //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'RoutePermit date of '.$busno.' is expired.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
            $store->body='RoutePermit date of '.$busno.' is expired.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
       //     return $this->sendResponse($store,'Notification successfully sent!!');
        }
     //warning:before 6 day of route permit end date !
    if($nmonthandday==$rwarnmonthandday){
         //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'RoutePermit date of '.$busno.' is on '.$addsixdaystonow.'.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
            $store->body='RoutePermit date of '.$busno.' is on '.$addsixdaystonow.'.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
           // return $this->sendResponse($store,'Notification successfully sent!!');

        }
    //for Check pass end date:
    if($nmonthandday==$chmonthandday){
      //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'Checkpass date of '.$busno.' is expired.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
            $store->body='Checkpass date of '.$busno.' is expired.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
       //     return $this->sendResponse($store,'Notification successfully sent!!');
        }
    //warning:before 6 day of checkpass end date !    
    if($nmonthandday==$cwarnmonthandday){
         //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'Checkpass date of '.$busno. ' is on '.$addsixdaystonow.'.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
            $store->body='Checkpass date of '.$busno. ' is on '.$addsixdaystonow.'.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
           // return $this->sendResponse($store,'Notification successfully sent!!');
    
        }
   //for insurance end date:    
    if($nmonthandday==$inmonthandday){
          //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'Insurance date of '.$busno.' is expired.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
            $store->body='Insurance date of '.$busno.' is expired.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
       //     return $this->sendResponse($store,'Notification successfully sent!!');    
        }
    //warning:before 6 day of insurance end date !    
    if($nmonthandday==$inwarnmonthandday){
         //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'Insurance date of '.$busno.' is on '.$addsixdaystonow.'.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
            $store->body='Insurance date of '.$busno.' is on '.$addsixdaystonow.'.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
           // return $this->sendResponse($store,'Notification successfully sent!!');
            }
        }
        //then, also include Admin as well
        //including admin as well  
// return $this->warnAdminOfEndDates();  
         return $response = \Response::json($getdataarray,200);
   
    }
}