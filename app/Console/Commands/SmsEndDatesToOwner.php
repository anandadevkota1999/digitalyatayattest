<?php
   
namespace App\Console\Commands;
   
use Illuminate\Console\Command;
use App\Rules\Nepali_Calendar as NepaliCalender;
use App\Models\Users\Driver;
use App\Models\Notification as Mynotification;
use Carbon\Carbon;
use App\Models\Admin\Sms;
use App\Models\Users\busOwner;
use App\Models\Users\businfo;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;

Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;

class SmsEndDatesToOwner extends Command
{
     /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'smsendatetoowner:cron';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'sms to owner of vehicle end dates.';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
     
     function sendSMS($content)
        {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://sms.cosmosnepal.net/api/v3/sms?");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$content);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);
       // $smstoken=LiBLVuY08EfqCMBF1432gS08XSQpCOxpsH2N;
        return $server_output;
        } 
    public function handle()
    {
       \DB::statement("SET SQL_MODE=''");
         //get owner of bus:
       $getownerofbus=businfo::join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->select('bus_owner.*','bus_info.*')->get();
       $dataarray=[];
       foreach($getownerofbus as $ownerdata){
        $token = 'LiBLVuY08EfqCMBF1432gS08XSQpCOxpsH2N';
        $ownerphone=$ownerdata->owner_phone; 
        $ownername=$ownerdata->owner_name; 
        $busno=$ownerdata->bus_no;
        $bustype=$ownerdata->bus_type;
        $billbookendate=$ownerdata->bus_billbook_enddate;
        $routepermitendate=$ownerdata->bus_routepermit_enddate;
        $checkpassendate=$ownerdata->bus_checkpass_enddate;
        $insuranceendate=$ownerdata->bus_insurance_enddate;
        
        //today date conversion to nepali
        $callcalender=new NepaliCalender();
       $todaydate=Carbon::now()->toDateString();
       $correctformatfornepali=str_replace('-', ',',$todaydate);
       $todaydatearray=explode (",", $correctformatfornepali);
       $tyear=$todaydatearray[0];
       $tmonth=$todaydatearray[1];
       $tday=$todaydatearray[2];
       $convertonepali= $callcalender->eng_to_nep($tyear,$tmonth,$tday);
       $todateformat=str_replace(',', '-',$convertonepali);
       $nslice =array_shift($convertonepali);
       //implding to string format
       $ntostring=implode(",",$convertonepali);
       //then,nepali month and day value
       $nmonthandday = str_replace( ',', ' ', $ntostring );
       $getremainingbalance=Sms::query()->orderBy('id','desc')->first();
       $to = $ownerphone;
        $sender = '1010';
     
      
       //------------*******billbook sms 3 day before the end date ****** 
          $bwarnbmonthday=Carbon::parse($billbookendate)->subDays('3');
          $bwdateonly=$bwarnbmonthday->toDateString();
          $bwarnformat=str_replace('-', ',',$bwdateonly);
          $bwmakeitarray = explode (",", $bwarnformat); 
            //then,finally get month and year in array
             $bwmonths=$bwmakeitarray[1];
             $bwmonth=ltrim($bwmonths, '0');
            $bwdays=$bwmakeitarray[2];
            $bwday=ltrim($bwdays, '0');
            $bwarnmonthandday=$bwmonth.' '.$bwday;
       
        if($nmonthandday==$bwarnmonthandday){
        $remainingbalance=$getremainingbalance->remaining_balance;
        //assuming deducted value in the sms
        $deductedvalue=1.5;
           $message = 'Dear, '.$ownername.' your '.$bustype.': '.$busno.'  bluebook end-date  is at: '.$billbookendate.', please pay in time!!, thank you.';
           // set post fields
        $content =[
        'token'=>$token,
        'to'=>$to,
        'sender'=>$sender,
        'message'=>$message,
        ];
        $newremainingbalance=$remainingbalance-$deductedvalue;
        if($newremainingbalance>=0){
        $send_with_content =$this->sendSMS($content);
        $datas = json_decode($send_with_content);
        if($datas->total_numbers==0){
         //   return $this->sendError('Message could not be sent!!');
            return $response = \Response::json('Message could not be sent!!', 200);
        }
        else{
        //store in db
        $sms=new Sms();
        $sms->sms_to=$to;
        $sms->receiver_name=$ownername;
        $sms->sms_message=$message;
        $sms->created_at=Carbon::now();
        $sms->updated_at=Carbon::now();
        $sms->company_id='100';
        $sms->added_by='system generated';
        $sms->initial_balance=$getremainingbalance->initial_balance;
        $sms->remaining_balance=$newremainingbalance;
        $sms->save();
          $dataarray=['msg sent to'=>$ownername];
      // return $this->sendResponse($datas,'msg sent!');
        }
            
        }
        
        else{
               //return $this->sendError('There is not enough balance in your account, please recharge soon!!');
                   return $response = \Response::json('There is not enough balance in your account, please recharge soon!!', 200);
        }  
        } 
        
          //------------*******route permit sms 3 day before the  end date ****** 
          $rwarnbmonthday=Carbon::parse($routepermitendate)->subDays('3');
          $rwdateonly=$rwarnbmonthday->toDateString();
          $rwarnformat=str_replace('-', ',',$rwdateonly);
          $rwmakeitarray = explode (",", $rwarnformat); 
            //then,finally get month and year in array
             $rwmonths=$rwmakeitarray[1];
             $rwmonth=ltrim($rwmonths, '0');
            $rwdays=$rwmakeitarray[2];
            $rwday=ltrim($rwdays, '0');
            $rwarnmonthandday=$rwmonth.' '.$rwday;
       
        if($nmonthandday==$rwarnmonthandday){
        $remainingbalance=$getremainingbalance->remaining_balance;
        //assuming deducted value in the sms
        $deductedvalue=1.5;
           $message = 'Dear, '.$ownername.' your '.$bustype.': '.$busno.' route permit end-date is at: '.$routepermitendate.', please pay in time!!, thank you.';
           // set post fields
        $content =[
        'token'=>$token,
        'to'=>$to,
        'sender'=>$sender,
        'message'=>$message,
        ];
        $newremainingbalance=$remainingbalance-$deductedvalue;
        if($newremainingbalance>=0){
        $send_with_content =$this->sendSMS($content);
        $datas = json_decode($send_with_content);
        if($datas->total_numbers==0){
           // return $this->sendError('Message could not be sent!!');
            return $response = \Response::json('Message could not be sent!!', 200);
        }
        else{
        //store in db
        $sms=new Sms();
        $sms->sms_to=$to;
        $sms->receiver_name=$ownername;
        $sms->sms_message=$message;
        $sms->created_at=Carbon::now();
        $sms->updated_at=Carbon::now();
        $sms->company_id='100';
        $sms->added_by='system generated';
        $sms->initial_balance=$getremainingbalance->initial_balance;
        $sms->remaining_balance=$newremainingbalance;
        $sms->save();
          $dataarray=['msg sent to'=>$ownername];
       //return $this->sendResponse($datas,'msg sent!');
        }
            
        }
        
        else{
            //   return $this->sendError('There is not enough balance in your account, please recharge soon!!');
               return $response = \Response::json('There is not enough balance in your account, please recharge soon!!', 200);  
        }  
        }
        
        //------------*******check pass sms 3 day before the end date ****** 
          $cwarnbmonthday=Carbon::parse($checkpassendate)->subDays('3');
          $cwdateonly=$cwarnbmonthday->toDateString();
          $cwarnformat=str_replace('-', ',',$cwdateonly);
          $cwmakeitarray = explode (",", $cwarnformat); 
            //then,finally get month and year in array
             $cwmonths=$cwmakeitarray[1];
             $cwmonth=ltrim($cwmonths, '0');
            $cwdays=$cwmakeitarray[2];
            $cwday=ltrim($cwdays, '0');
            $cwarnmonthandday=$cwmonth.' '.$cwday;
       
        if($nmonthandday==$cwarnmonthandday){
        $remainingbalance=$getremainingbalance->remaining_balance;
        //assuming deducted value in the sms
        $deductedvalue=1.5;
           $message = 'Dear, '.$ownername.' your '.$bustype.': '.$busno.' checkpass end-date is at: '.$checkpassendate.', please pay in time!!, thank you.';
           // set post fields
        $content =[
        'token'=>$token,
        'to'=>$to,
        'sender'=>$sender,
        'message'=>$message,
        ];
        $newremainingbalance=$remainingbalance-$deductedvalue;
        if($newremainingbalance>=0){
        $send_with_content =$this->sendSMS($content);
        $datas = json_decode($send_with_content);
        if($datas->total_numbers==0){
            //return $this->sendError('Message could not be sent!!');
            return $response = \Response::json('Message could not be sent!!', 200); 
        }
        else{
        //store in db
        $sms=new Sms();
        $sms->sms_to=$to;
        $sms->receiver_name=$ownername;
        $sms->sms_message=$message;
        $sms->created_at=Carbon::now();
        $sms->updated_at=Carbon::now();
        $sms->company_id='100';
        $sms->added_by='system generated';
        $sms->initial_balance=$getremainingbalance->initial_balance;
        $sms->remaining_balance=$newremainingbalance;
        $sms->save();
          $dataarray=['msg sent to'=>$ownername];
       //return $this->sendResponse($datas,'msg sent!');
        }
            
        }
        
        else{
            //   return $this->sendError('There is not enough balance in your account, please recharge soon!!');
            return $response = \Response::json('There is not enough balance in your account, please recharge soon!!', 200); 
               
        }  
        }
        
        //------------*******check pass sms 3 day before the end date ****** 
          $iwarnbmonthday=Carbon::parse($insuranceendate)->subDays('3');
          $iwdateonly=$iwarnbmonthday->toDateString();
          $iwarnformat=str_replace('-', ',',$iwdateonly);
          $iwmakeitarray = explode (",", $iwarnformat); 
            //then,finally get month and year in array
            $iwmonths=$iwmakeitarray[1];
            $iwmonth=ltrim($iwmonths, '0');
            $iwdays=$iwmakeitarray[2];
            $iwday=ltrim($iwdays, '0');
            $iwarnmonthandday=$iwmonth.' '.$iwday;
       
        if($nmonthandday==$iwarnmonthandday){
        $remainingbalance=$getremainingbalance->remaining_balance;
        //assuming deducted value in the sms
        $deductedvalue=1.5;
           $message = 'Dear, '.$ownername.' your '.$bustype.': '.$busno.' insurance end-date of is at: '.$insuranceendate.', please pay in time!!, thank you.';
           // set post fields
        $content =[
        'token'=>$token,
        'to'=>$to,
        'sender'=>$sender,
        'message'=>$message,
        ];
        $newremainingbalance=$remainingbalance-$deductedvalue;
        if($newremainingbalance>=0){
        $send_with_content =$this->sendSMS($content);
        $datas = json_decode($send_with_content);
        if($datas->total_numbers==0){
           // return $this->sendError('Message could not be sent!!');
            return $response = \Response::json('Message could not be sent!!', 200); 
        }
        else{
        //store in db
        $sms=new Sms();
        $sms->sms_to=$to;
        $sms->receiver_name=$ownername;
        $sms->sms_message=$message;
        $sms->created_at=Carbon::now();
        $sms->updated_at=Carbon::now();
        $sms->company_id='100';
        $sms->added_by='system generated';
        $sms->initial_balance=$getremainingbalance->initial_balance;
        $sms->remaining_balance=$newremainingbalance;
        $sms->save();
          $dataarray=['msg sent to'=>$ownername];
       //return $this->sendResponse($datas,'msg sent!');
        }
            
        }
        
        else{
           //    return $this->sendError('There is not enough balance in your account, please recharge soon!!');
               return $response = \Response::json('There is not enough balance in your account, please recharge soon!!', 200);
        }  
        }
       }
     //  return $this->sendResponse($dataarray,'Sms successfully sent!!');
      return $response = \Response::json($dataarray, 200);
    }
}