<?php
   
namespace App\Console\Commands;
   
use Illuminate\Console\Command;
use App\Rules\Nepali_Calendar as NepaliCalender;
use App\Models\Users\Driver;
use App\Models\Notification as Mynotification;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Models\Admin\Transactions\GeneralShulka;
use App\Models\Admin\Transactions\GeneralShulkaKharcha;
use App\Models\Admin\Transactions\GeneralKharchaDummy;
use App\Models\Admin\Transactions\JournalEntryOfGeneralShulka;
use App\Models\Admin\Transactions\FiscalYear;
use App\Models\Admin\Transactions\AccountPriceDeclaration;
use App\Models\Admin\Transactions\VehicleFixedAccount;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;

class UpdateFixedVehicleAccountMonthly extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updatefixedvehicleaccount:cron';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update fixed vehicle account monthly';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //system log
        \Log::info("Cron is working fine!");
       DB::beginTransaction();
        $fiscalyear=FiscalYear::OrderBy('id','DESC')->first();
        $fyear=$fiscalyear->fiscal_year;
       $showupdateddays=[];
       $getfixedexpirevalues=VehicleFixedAccount::whereDate('expiry_date','<',Carbon::now())->get();
       foreach($getfixedexpirevalues as $expiredvalues){
        $getexpirydate=$expiredvalues->expiry_date;
         $parsedate=Carbon::parse($getexpirydate);
         $addmonth=$parsedate->addDays(30);
         $expiredvalues->expiry_date=$addmonth;
         $expiredvalues->save();
          $showupdateddays[]=[
              'updated day'=>$expiredvalues,
              ];
       }
        DB::commit(); 
        return $response = \Response::json($showupdateddays, 200);
    }
}