<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Http\Controllers\Api\Admin\AllNotificationController;
class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
         Commands\BirthdayNotificationToDriver::class,
         Commands\BusEndDatesNotificationToOwner::class,
         Commands\BirthdayNotificationToOwner::class,
         Commands\BirthdayNotificationToStaff::class,
         Commands\UpdateFixedVehicleAccountMonthly::class,
         Commands\AddFixedTopicsMonthly::class,
         Commands\SmsEndDatesToOwner::class
         
         //addfixedpaymenttopicmonthly
     ];
protected function scheduleTimezone()
{
    return 'Asia/Kathmandu';
}

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->command('birthdaydriver:cron')
                ->everyMinute();
        $schedule->command('busenddates:cron')
               ->everyMinute();
       $schedule->command('birthdayowner:cron')
            ->everyMinute();
        $schedule->command('birthdaystaff:cron')
            ->everyMinute();    
        $schedule->command('updatefixedvehicleaccount:cron')
            ->everyMinute();        
       // $schedule->command('addfixedpaymenttopicmonthly:cron')
       //     ->everyMinute();
         $schedule->command('smsendatetoowner:cron')
            ->everyMinute();    
      //   $schedule->call('App\Http\Controllers\Api\Admin\AllNotificationController@NotificationOwnerUpdate')->everyMinute();
         
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
