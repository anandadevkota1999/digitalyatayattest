<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Users\busOwner;
use App\Models\Users\DummyBusOwner;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\DummyBusInfo;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Admin\adminInfo as admininfoResource;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class AdminController extends BaseController
{
 
   
      
    //for viewing the logged in Admin datas of all companies
    public function index(){
    	    $get_all = LoginModel::all();
    
        return $this->sendResponse(admininfoResource::collection($get_all), 'Authorized Admins retrieved successfully!!');
    }

//count Admin of specific company
public function countSpecificAdmins($ids){
        $owners = LoginModel::where('company_id',$ids)->count();
   return $this->sendResponse($owners, 'admins filtered successfully!!');   
}
//list Admin of specific company
public function listSpecificAdmins($ids){
        $owners = LoginModel::where('company_id',$ids)->get(['admin_name','admin_phone','admin_address','admin_email','admin_image','created_at','company_id']);
   return $this->sendResponse($owners, 'admins filtered successfully!!');   
}
//count admins of all companies    
public function countAdmins(){
  $total['admins'] = LoginModel::count('admin_id');
        return $this->sendResponse($total, 'Admins counted successfully!!');
 }


public function updateAdmin(Request $req, $id){
    try{
    $validator =Validator::make($req->all(),[
 'admin_phone'=>['required', 'digits:10'],
   'admin_name'=>'required',
   'company_id'=>'required',
   //'admin_image'=>'image|mimes:jpeg,png,jpg,gif,svg,jfif|max:2048',
  ]);
   if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }    
  if($req->hasFile('admin_image')){
       $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->admin_image->store($getcompany, 'local');
       $admin=LoginModel::findorFail($id);
      if($admin){
      $admin->admin_name=$req['admin_name'];
       $admin->admin_phone =$req['admin_phone'];
       $admin->admin_address=$req['admin_address'];
       $admin->admin_image=$req->admin_image->hashName();
       $admin->updated_at=Carbon::now();
       //rollback if any unintentional error occurs!!
         DB::transaction(function () use ($admin) { // Start the transaction
            
            $admin->save();
        
        }); // End transaction
      // $admin->save();       
       return $this->sendResponse($admin,'Admin Records are successfully updated!!');      
      }
     else{
       return $this->sendError('There are no Admin records to update!');
      }
   }
 else{
         $admin=LoginModel::findorFail($id);
         $admin->admin_name=$req['admin_name'];
         $admin->admin_address=$req['admin_address'];
         $admin->admin_phone =$req['admin_phone'];
         $admin->updated_at=Carbon::now();
          //rollback if any unintentional error occurs!!
         DB::transaction(function () use ($admin) { // Start the transaction
            
            $admin->save();
        
        }); // End transaction
        // $admin->save(); 
         return $this->sendResponse($admin,'Admin Records are successfully updated!!');
         }
             
    }
    catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
    
}


// *****_________________  For BUS OWNERS ______________

//listing owners of all companies
public function listOwner(){
    $getlist=busOwner::all();
    return $this->sendResponse($getlist, 'owners are listed!');
}
//listing owners of specific company
public function listOwnerSpecificCompany($ids){
       $owners = busOwner::where('company_id',$ids)->get();
   return $this->sendResponse($owners, 'owners filtered successfully!!');    
}
//show owner detail by phone
function showOwner($phone){
    $getdata=busOwner::where('owner_phone',$phone)->where('active_status','yes')->first();
    return $this->sendResponse($getdata,'owner records shown successfully!!');
}
 //count owners of companies     
public function countowner(){
     $total['owners'] = busOwner::count('owner_id');
        return $this->sendResponse($total, 'owners counted successfully!!');    
}

 //count owners of specific companies     
public function countownerSpecificCompany($getid){
     $total['owners'] = busOwner::where('company_id',$getid)->count('owner_id');
        return $this->sendResponse($total, 'owners counted successfully!!');    
}

// filter data by latest dates(15 days)
public function searchBusownerbyLatestDate($getid){
$last_15_days['latest owners'] = busOwner::where('company_id',$getid)->where('created_at','>=',Carbon::now()->subdays(15))->orderBy('created_at','DESC')->get(['owner_name','created_at']);
        return $this->sendResponse($last_15_days, 'owners filtered successfully!!');    
}

//filter latest owners of specific company
public function CountOwnerBySpecificCompany($ids){
    $last_15_days=busOwner::where('company_id',$ids)->where('created_at','>=',Carbon::now()->subdays(30))->count();
     return $this->sendResponse($last_15_days, 'owners counted successfully!!'); 
}

//filter data by specific dates!
public function filterbySpecificDate(){
$from = date('2020-04-02');
$to = date('2021-03-28');
$filter['filtered owners']=busOwner::whereBetween('created_at', [$from, $to])->get('owner_name');  
        return $this->sendResponse($filter, 'owners filtered successfully!!');    
}

//deleting bus owner records by shifting to the dummy:DummyBusOwner table
public function deleteBusOwner($id){
  
    try{
    //get owners record of a row by ID
    $getowner=busOwner::where('owner_id',$id)->first();
    //saving records row wise
    $owner_name=$getowner->owner_name;
    $owner_address=$getowner->owner_address;
    $owner_phone=$getowner->owner_phone;
    $owner_pin=$getowner->owner_pin;
    $owner_dob=$getowner->owner_dob; 
    $owner_gender=$getowner->owner_gender;
    $company_id=$getowner->company_id;
    $owner_citizenship_image=$getowner->owner_citizenship_image;
    $owner_image=$getowner->owner_image;
    $device_token=$getowner->device_token;
    $device_name=$getowner->device_name;
    $created_at=$getowner->created_at;
    $updated_at=$getowner->updated_at;
        
//now using dummy table to insert deleted records    
    $getdummy=new DummyBusOwner();
    $getdummy->owner_id=$id;
    $getdummy->owner_name=$owner_name;
    $getdummy->owner_address=$owner_address;
    $getdummy->owner_phone=$owner_phone;
    $getdummy->owner_pin=$owner_pin;
    $getdummy->owner_dob=$owner_dob;
    $getdummy->owner_gender=$owner_gender;
    $getdummy->company_id=$company_id;
    $getdummy->owner_citizenship_image=$owner_citizenship_image;
    $getdummy->owner_image=$owner_image;
    $getdummy->device_token=$device_token;
    $getdummy->device_name=$device_name;
    $getdummy->created_at=$created_at;
    $getdummy->updated_at=$updated_at;
    $getdummy->save();
       $getowner->delete();
 
    return $this->sendResponse($getdummy, 'bus owner records moved to dummy table successfully!!');
    }
     catch ( \Exception $ex ){
         $errorCode = $ex->errorInfo[1];
          if($errorCode == '1451'){
              return $this->sendError('You can not delete this record,It is connected to other data !!');
          }
      return $this->sendError(($ex->getMessage())); 
    }
    
    
}

//test
public function test(){
    
     $createvalues=LoginModel::create($input);
}


// *****_________________  end of  BUS OWNERS ______________

//------------------------------------------------------------------------------


// *****_________________  For BUS DRIVERS ______________

//count drivers of specific companies     
public function countDriverSpecificCompany($getid){
     $total['drivers'] = Driver::where('active_status','yes')->where('company_id',$getid)->where('active_status','yes')->count('driver_id');
        return $this->sendResponse($total, 'owners counted successfully!!');    
}

//listing owners of specific company
public function listDriverSpecificCompany($ids){
       $owners = Driver::where('company_id',$ids)->where('active_status','yes')->get();

   return $this->sendResponse($owners, 'owners filtered successfully!!');    
}


//showing specific driver informations
public function showDriverDetail($id){
 try{
 $showall=Driver::where('driver_id',$id)->where('active_status','yes')->first();
 return $this->sendResponse($showall,'owner details are successfully shown!');
 }
    catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
    
}

 //drivers of all companies   
public function countDriver(){
     $total['drivers'] = Driver::where('active_status','yes')->count('driver_id');
        return $this->sendResponse($total, 'drivers counted successfully!!');   
}

//filter data by latest dates(15 days)
public function searchDriverbyLatestDate(){
$last_15_days['latest drivers'] = Driver::where('created_at','>=',Carbon::now()->subdays(15))->get(['driver_id','created_at'])->count('driver_id');
        return $this->sendResponse($last_15_days, 'owners filtered successfully!!');    
}
//show in-active drivers:
function showInactiveDrivers($companyid){
    $getinactivedrivers=Driver::where('company_id',$companyid)->where('active_status','!=','yes')->get();
     return $this->sendResponse($getinactivedrivers, 'In-active drivers shown successfully!!'); 
}

// *****_________________  end of  BUS DRIVERS ______________

//------------------------------------------------------------------------------

// *****_________________  For BUS INFO ______________

//count bus of specific companies     
public function countBusSpecificCompany($getid){
     $total['buses'] = businfo::where('company_id',$getid)->count('bus_id');
        return $this->sendResponse($total, 'bus counted successfully!!');    
}

//listing buses  of specific company
public function listBusSpecificCompany($ids){
       $owners = businfo::where('bus_info.company_id',$ids)->where('bus_info.active_status','yes')->leftJoin('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->get();//join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->join('driver_info','bus_info.bus_no','driver_info.bus_no')->select('bus_info.*','bus_owner.owner_name','driver_info.driver_name')->get();
   return $this->sendResponse($owners, 'owners filtered successfully!!');    
}

//showing specific  informations of bus
public function showBusDetail($id){
 $showall=businfo::where('bus_id',$id)->where('active_status','yes')->first();
return $this->sendResponse($showall,'owner details are successfully shown!');   
    
}

public function showBusDetailByLatestDate($ids){
      $owners = businfo::where('company_id',$ids)->where('created_at','>=',Carbon::now()->subdays(30))->orderBy('created_at','DESC')->get();
   return $this->sendResponse($owners, 'owners filtered successfully!!');  
}


//   count buses of all companies  
 
public function countBus(){
     $total['buses'] = businfo::count('driver_id');
        return $this->sendResponse($total, 'drivers counted successfully!!');    
}

//------ for storing datas in database from frontend
public function busStore(Request $req){
    //input all data through http requests
    try{
     $input_the_data= $req->all();
       $validator =Validator::make($input_the_data,[
  	        'bus_id' => 'required',
            'bus_no' => 'required',
            'owner_phone'=>'required|numeric',
            'bus_billbook_no'=>'required',
            'bus_billbook_enddate'=>'required',
            'bus_routepermit_enddate'=>'required',
            'bus_checkpass_enddate'=>'required',
            'bus_insurance_enddate'=>'required',
            'bus_billbook_image'=>'required|mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
            'bus_routepermit_image'=>'required|mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
            'bus_insurance_image'=>'required|mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
            'bus_checkpass_image'=>'required|mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
            'company_id' => 'required',
            'file' => 'max:250000',
  ],[
      'bus_billbook_image.max'=>'25',
     'bus_insurance_image.max'=>'25',
      'bus_checkpass_image.max'=>'25',
      'bus_routepermit_image'=>'25',
      ]);
  if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
   if($req->hasFile('bus_billbook_image')&&$req->hasFile('bus_routepermit_image')&&$req->hasFile('bus_checkpass_image')&&$req->hasFile('bus_insurance_image')){
          $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->bus_billbook_image->store($getcompany, 'local');
       $req->bus_routepermit_image->store($getcompany, 'local');
       $req->bus_checkpass_image->store($getcompany, 'local');
       $req->bus_insurance_image->store($getcompany, 'local');
       //check if already exists
       $getbusinfo=businfo::where('bus_no',$req->input('bus_no'))->first();
       if($getbusinfo){
        if($getbusinfo->bus_no==$req->input('bus_no')){
            $updateactivestatus=businfo::where('bus_no',$req->input('bus_no'))->update(['bus_type'=>$req->input('bus_type'),'owner_phone'=>$req->input('owner_phone'),'bus_billbook_no'=>$req->input('bus_billbook_no'),'bus_billbook_enddate'=>$req->input('bus_billbook_enddate'),'bus_routepermit_enddate'=>$req['bus_routepermit_enddate'],
            'bus_checkpass_enddate'=>$req['bus_checkpass_enddate'],'bus_insurance_enddate'=>$req['bus_insurance_enddate'],'bus_billbook_image'=>$req->bus_billbook_image->hashName(),'bus_routepermit_image'=>$req->bus_routepermit_image->hashName()
            ,'bus_checkpass_image'=>$req->bus_checkpass_image->hashName(),'bus_insurance_image'=>$req->bus_insurance_image->hashName(),'updated_by'=>$req->input('updated_by'),'active_status'=>'yes']);
            return $this->sendResponse($updateactivestatus, 'stored successfully!!');
        }   
       } 
       if(is_null($getbusinfo)){
     //now save values in table
     $model=new businfo();
     $model->bus_id=$req->input('bus_id');
     $model->bus_no=$req->input('bus_no');
     $model->bus_type=$req->input('bus_type');
     $model->owner_phone=$req->input('owner_phone');
     $model->bus_billbook_no=$req->input('bus_billbook_no');
     $model->bus_billbook_enddate=$req->input('bus_billbook_enddate');
     $model->bus_routepermit_enddate=$req->input('bus_routepermit_enddate');
     $model->bus_checkpass_enddate=$req->input('bus_checkpass_enddate');
     $model->bus_insurance_enddate=$req->input('bus_insurance_enddate');
     $model->bus_billbook_image=$req->bus_billbook_image->hashName();
     $model->bus_routepermit_image=$req->bus_routepermit_image->hashName();
     $model->bus_checkpass_image=$req->bus_checkpass_image->hashName();
     $model->bus_insurance_image=$req->bus_insurance_image->hashName();
     $model->company_id=$req->input('company_id');
     $model->added_by=$req->input('added_by');
     $model->updated_by=$req->input('updated_by');
     $model->save();
     return $this->sendResponse($model, 'stored successfully!!');
       }
     } 
    }
      catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
}

//Update bus info records
public function updateBusInfo(Request $req, $id){
    try{
    $validator =Validator::make($req->all(),[
 // 'owner_phone'=>'required|min:10|max:14',
  'bus_no'=>'required',
   //'bus_billbook_image'=>'image|mimes:jpeg,png,jpg,gif,svg,jfif|max:2048',
   //'bus_routepermit_image'=>'image|mimes:jpeg,png,jpg,gif,svg,jfif|max:2048',
   //'bus_insurance_image'=>'image|mimes:jpeg,png,jpg,gif,svg,jfif|max:2048','bus_checkpass_image'=>'image|mimes:jpeg,png,jpg,gif,svg,jfif|max:2048',
   'company_id'=>'required',
   'bus_billbook_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
            'bus_routepermit_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
            'bus_insurance_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
            'bus_checkpass_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
   'file' => 'max:250000',
  ],[
      'bus_billbook_image.max'=>'25',
     'bus_insurance_image.max'=>'25',
      'bus_checkpass_image.max'=>'25',
      'bus_routepermit_image'=>'25',
      ]);
   if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        } 
        
        if($req->hasFile('bus_billbook_image')&&$req->hasFile('bus_routepermit_image')&&$req->hasFile('bus_checkpass_image')&&$req->hasFile('bus_insurance_image')){
       $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->bus_billbook_image->store($getcompany, 'local');
       $req->bus_routepermit_image->store($getcompany, 'local');
       $req->bus_checkpass_image->store($getcompany, 'local');
       $req->bus_insurance_image->store($getcompany, 'local');
        
 //***********saving the required values to the database!!**************
 $bus=businfo::findorFail($id)->where('active_status','yes');
 if($bus){
 $bus->bus_no=$req['bus_no'];
 $bus->bus_type=$req->input('bus_type');
 $bus->bus_billbook_no=$req['bus_billbook_no'];
 $bus->bus_billbook_enddate =$req['bus_billbook_enddate'];
 $bus->bus_routepermit_enddate=$req['bus_routepermit_enddate'];
 $bus->bus_checkpass_enddate=$req['bus_checkpass_enddate'];
 $bus->bus_insurance_enddate=$req['bus_insurance_enddate'];
 $bus->bus_billbook_image=$req->bus_billbook_image->hashName();
 $bus->bus_routepermit_image=$req->bus_routepermit_image->hashName();
 $bus->bus_checkpass_image=$req->bus_checkpass_image->hashName();
 $bus->bus_insurance_image=$req->bus_insurance_image->hashName();
 $bus->updated_at=Carbon::now();
  $bus->updated_by=$req['updated_by'];
   $bus->save();
  //now also connecting  bus detail with driver linked by bus no.
     $query=Driver::where( 'bus_no' , $id)->update( array( 'bus_no' =>$req['bus_no']));
    
  return $this->sendResponse($bus,'Bus Records are successfully updated!');
      }
      else{
  return $this->sendError('There are no Bus records to update!');
}
        }
        
        
      
        
        // update::for billbook image only
        else if($req->hasFile('bus_billbook_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->bus_billbook_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 $bus=businfo::findorFail($id);
 if($bus){
 $bus->bus_no=$req['bus_no'];
 $bus->bus_billbook_no=$req['bus_billbook_no'];
 $bus->bus_billbook_enddate =$req['bus_billbook_enddate'];
 $bus->bus_routepermit_enddate=$req['bus_routepermit_enddate'];
 $bus->bus_checkpass_enddate=$req['bus_checkpass_enddate'];
 $bus->bus_insurance_enddate=$req['bus_insurance_enddate'];
 $bus->bus_billbook_image=$req->bus_billbook_image->hashName();
 $bus->updated_at=Carbon::now();
 $bus->updated_by=$req['updated_by'];
 $bus->save();
 //now also connecting  bus detail with driver linked by bus no.
     $query=Driver::where( 'bus_no' , $id)->update( array( 'bus_no' =>$req['bus_no']));
    
  return $this->sendResponse($bus,'Bus Records are successfully updated!');
      }
      else{
  return $this->sendError('There are no Bus records to update!');
}
       
        }
        
          // update::for routepermit image only
          else if($req->hasFile('bus_routepermit_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->bus_routepermit_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 $bus=businfo::findorFail($id);
 if($bus){
 $bus->bus_no=$req['bus_no'];
 $bus->bus_billbook_no=$req['bus_billbook_no'];
 $bus->bus_billbook_enddate =$req['bus_billbook_enddate'];
 $bus->bus_routepermit_enddate=$req['bus_routepermit_enddate'];
 $bus->bus_checkpass_enddate=$req['bus_checkpass_enddate'];
 $bus->bus_insurance_enddate=$req['bus_insurance_enddate'];
 $bus->bus_routepermit_image=$req->bus_routepermit_image->hashName();
 $bus->updated_at=Carbon::now();
 $bus->updated_by=$req['updated_by'];
 $bus->save();
 //now also connecting  bus detail with driver linked by bus no.
     $query=Driver::where( 'bus_no' , $id)->update( array( 'bus_no' =>$req['bus_no']));
    
  return $this->sendResponse($bus,'Bus Records are successfully updated!');
 // return $this->sendResponse($bus,'Bus Records are successfully updated!');
      }
      else{
  return $this->sendError('There are no Bus records to update!');
}
       
        }
        
         // update::for checkpass image only
         else if($req->hasFile('bus_checkpass_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->bus_checkpass_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 $bus=businfo::findorFail($id);
 if($bus){
 $bus->bus_no=$req['bus_no'];
 $bus->bus_billbook_no=$req['bus_billbook_no'];
 $bus->bus_billbook_enddate =$req['bus_billbook_enddate'];
 $bus->bus_routepermit_enddate=$req['bus_routepermit_enddate'];
 $bus->bus_checkpass_enddate=$req['bus_checkpass_enddate'];
 $bus->bus_insurance_enddate=$req['bus_insurance_enddate'];
 $bus->bus_checkpass_image=$req->bus_checkpass_image->hashName();
 $bus->updated_at=Carbon::now();
 $bus->updated_by=$req['updated_by'];
  $bus->save();
 //now also connecting  bus detail with driver linked by bus no.
     $query=Driver::where( 'bus_no' , $id)->update( array( 'bus_no' =>$req['bus_no']));
    
  return $this->sendResponse($bus,'Bus Records are successfully updated!');
 // return $this->sendResponse($bus,'Bus Records are successfully updated!');
      }
      else{
  return $this->sendError('There are no Bus records to update!');
}
       
        }
        
         // update::for insurance image only
           else if($req->hasFile('bus_insurance_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->bus_insurance_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 $bus=businfo::findorFail($id);
 if($bus){
 $bus->bus_no=$req['bus_no'];
 $bus->bus_billbook_no=$req['bus_billbook_no'];
 $bus->bus_billbook_enddate =$req['bus_billbook_enddate'];
 $bus->bus_routepermit_enddate=$req['bus_routepermit_enddate'];
 $bus->bus_checkpass_enddate=$req['bus_checkpass_enddate'];
 $bus->bus_insurance_enddate=$req['bus_insurance_enddate'];
 $bus->bus_insurance_image=$req->bus_insurance_image->hashName();
 $bus->updated_at=Carbon::now();
 $bus->updated_by=$req['updated_by'];
 $bus->save();
 //now also connecting  bus detail with driver linked by bus no.
     $query=Driver::where( 'bus_no' , $id)->update( array( 'bus_no' =>$req['bus_no']));
    
  return $this->sendResponse($bus,'Bus Records are successfully updated!');
 // return $this->sendResponse($bus,'Bus Records are successfully updated!');
      }
      else{
  return $this->sendError('There are no Bus records to update!');
}
        }
    
          //if the bus etries : No need to input images so,
     else{
         
        //updating in db table
         $bus=businfo::findorFail($id);
         $bus->bus_no=$req['bus_no'];
         $bus->bus_billbook_no=$req['bus_billbook_no'];
         $bus->bus_billbook_enddate =$req['bus_billbook_enddate'];
         $bus->bus_routepermit_enddate=$req['bus_routepermit_enddate'];
         $bus->bus_checkpass_enddate=$req['bus_checkpass_enddate'];
         $bus->bus_insurance_enddate=$req['bus_insurance_enddate'];
         $bus->updated_at=Carbon::now();
         $bus->updated_by=$req['updated_by'];
       $bus->save();
         //now also connecting  bus detail with driver linked by bus no.
     $query=Driver::where( 'bus_no' , $id)->update( array( 'bus_no' =>$req['bus_no']));
  return $this->sendResponse($bus,'Bus Records are successfully updated!');
//    return $this->sendResponse( $bus,'the datas are now stored successfully!!');
           
     } 
        
    }
    catch ( \Exception $ex ){ 
     //    return $this->sendError(($ex->getMessage())); 
        $errorCode = $ex->errorInfo[1];
          if($errorCode == '1062'){
              return $this->sendError('The Given bus number is duplicate, please undo your process!!');
          } 
    }
 
}

//for displaying images
public function displayImage($filename,Request $req){
    $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $path=storage_path('app/'.$getcompany.'/'.$filename);
   //   return $this->sendResponse($getcompany,'image shown!');
   //    $path=storage_path('app/ChitwanYatayat/'.$filename);
    if (!File::exists($path)) {
    abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response; 

//return $this->sendResponse($images,'image shown!');

}

//for getting images
public function getSpecificImages($id){
    $user = businfo::select(['bus_billbook_image', 'bus_routepermit_image','bus_checkpass_image','bus_insurance_image'])->find($id);
   	return $this->sendResponse($user,'the images are successfully shown!'); 
}
//filter data by latest dates(15 days)
public function searchBusbyLatestDate(){
$last_15_days['latest drivers'] = businfo::where('created_at','>=',Carbon::now()->subdays(15))->get(['driver_id','created_at'])->count('driver_id');
        return $this->sendResponse($last_15_days, 'owners filtered successfully!!');    
}
// *****_________________  end of  BUS INFO ______________
//--------------------------------------------------------------------

// *****_________________  Start of STAFF: ______________
//staff of all companies
public function indexStaff(){
     	    $get_all = StaffInfo::all();
    
        return $this->sendResponse($get_all, 'staffs of all companies retrieved successfully!!');
}

//get staffs of specific company  
public function ListStaffSpecific($ids){
   $staffs = StaffInfo::where('company_id',$ids)->get();
   if (!$staffs->isEmpty()) { 
   return $this->sendResponse($staffs, 'staffs filtered successfully!!');     
   }
   else
       return $this->sendError('there are no any staffs of this company!');
    }
//showing specific staff informations
public function showStaffDetail($id){
 $showall=StaffInfo::findOrFail($id);
return $this->sendResponse($showall,'staff details are successfully shown!');   
    
}
 //count staff of all companies     
public function countStaff(){
     $total['Toatal staffs'] = StaffInfo::count('staff_id');
        return $this->sendResponse($total, 'staffs counted successfully!!');    
}

 //count owners of specific companies     
public function countStaffSpecificCompany($getid){
     $total= StaffInfo::where('company_id',$getid)->count('staff_id');
        return $this->sendResponse($total, 'owners counted successfully!!');    
}
// *****_________________  End of STAFF: ______________

// *****_________________  Start of General Section: ______________

public function countAllMembers($ids){
    $countadmin = LoginModel::where('company_id',$ids)->count();
    $countstaff=StaffInfo::where('company_id',$ids)->count();
    $countowner=busOwner::where('company_id',$ids)->count();
    $countdriver=Driver::where('company_id',$ids)->count();
    $getall= $countadmin+$countstaff+$countowner+$countdriver;
     return $this->sendResponse($getall,'All the members are added and counted successfully !!');
    
}
//reset owner by admin:
public function resetOwnerByAdmin($id){
    $getstatus=busOwner::where('owner_id',$id)->first(); 
   //logout state
   if(is_null($getstatus->device_token)){
   $defaultpin=123456;      
   $getstatus->update(['owner_pin'=>$defaultpin]);
   $getstatus->save();
   return $this->sendResponse($getstatus,'password has been reset successfully!!');
    }
    else{
    $getstatus->update(['device_token' => null,'owner_pin'=>123456]);
    $getstatus->save();
    return $this->sendResponse($getstatus,'successfully logged out!');  
        }
}
 /*
 //logout when forgot pasword
function setPasswordWhenForgot($id){
 $getloginstatus=busOwner::where('owner_id',$id)->first(); 
  if(is_null($getdriver->device_token)){
   $defaultpin=123456;      
   $getloginstatus->update(['owner_pin'=>$defaultpin]);
         return $this->sendResponse($getloginstatus,'password has been reset successfully!!');
    }
    else{
        return $this->sendError('user is already logged out');
    }
}

 */

//reset driver by admin:
public function resetDriverByAdmin($id){
     $getdriver=Driver::where('driver_id',$id)->first();
     if(is_null($getdriver->device_token)){
   $defaultpin=123456;      
   $getdriver->update(['driver_pin'=>$defaultpin]);
   $getdriver->save();
   return $this->sendResponse($getdriver,'password has been reset successfully!!');
     
     }
     else{
     $getdriver->update(['device_token' => null,'driver_pin'=>'123456']); 
     $getdriver->save();
    return $this->sendResponse($getdriver,'successfully logged out!');  
     }
}
//reset staff by admin
public function resetStaffByAdmin($id){
     $getdriver=StaffInfo::where('staff_id',$id)->first();
     if(is_null($getdriver->device_token)){
     $defaultpin=123456;      
   $getdriver->update(['staff_pin'=>$defaultpin]);
   $getdriver->save();
   return $this->sendResponse($getdriver,'password has been reset successfully!!');
    
     }
     else{
     $getdriver->update(['device_token' => null,'staff_pin'=>'123456']); 
     $getdriver->save();
     return $this->sendResponse($getdriver,'successfully logged out!'); 
     } 
}
//show every details for dashboard view ::admin,staff,owner,driver,new owner,total bus,totalmember
public function showEveryDashoboardComponent($ids){
    //count admin
      $cadmin = LoginModel::where('company_id',$ids)->count();
    //count staff
       $cstaff= StaffInfo::where('company_id',$ids)->count('staff_id');
    //count owner
      $cowner = busOwner::where('company_id',$ids)->count('owner_id');
    //count driver 
     $cdriver = Driver::where('active_status','yes')->where('company_id',$ids)->count('driver_id');
    //newly added bus owner
     $latestowner=busOwner::where('company_id',$ids)->where('created_at','>=',Carbon::now()->subdays(30))->count();
    //total bus
     $totalbus = businfo::where('active_status','yes')->where('company_id',$ids)->count('bus_id'); 
    //total members
    $totalmember=$cadmin+$cstaff+$cowner+$cdriver;
    $putinarray=['total_admin'=>$cadmin,'total_staff'=>$cstaff,'total_owner'=>$cowner,'total_driver'=>$cdriver,'latest_owner'=>$latestowner,'total_bus'=>$totalbus,'total_members'=>$totalmember
        ];
    return $this->sendResponse($putinarray,'successfully shown in dashboard!!');
}

}
