<?php 
 
namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Admin\AdminReset;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\BeforeLogin\companyList;
//use Illuminate\Support\Facades\Session;
use Cache;
use Session;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Hash;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Validator;
use Illuminate\Contracts\Auth\UserProvider;
//use Auth;
use App\Http\Resources\Admin\adminInfo as admininfoResource;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Log;
//use Illuminate\Support\Facades\Auth;



class AdminLoginController extends BaseController
{
    private static $token,$sendto;
   function __construct(Request $request){
       self::$token=rand(100000, 999999);
       self::$sendto=$request['admin_email'];
   }
   //for registering Admin users in the System
  public function adminRegister(Request $req){
     
     try{
         $validator = Validator::make($req->all(), [
             'admin_id'=>'required',
            'admin_name' => 'required',
             'admin_phone' => ['required', 'digits:10'],
             'admin_address'=>'required',
             'admin_email'=>'required|email',
            'password'=>'required|min:6',
            'admin_image'=>'required',
            'company_id'=>'required',
        ]);
   
        if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
   
   
   if($req->hasFile('admin_image')){
       $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
     //  $getcompanyname=companyList::where('company_id',$req['company_id'])->first('company_name');
         $currentimagedate = Carbon::now();
       $req->admin_image->store($getcompany, 'local');
       
  //  return $this->sendResponse($get,'company');
      $input = $req->all();
       $input['password'] = Hash::make($req['password']);
       $password=$input['password'];
       $input['admin_validity_period']=Carbon::now()->addDays(365);
       $getdefaultvalidity=$input['admin_validity_period'];
    $model=new LoginModel();
     $model->admin_id=$req->input('admin_id');
     $model->admin_name=$req->input('admin_name');
     $model->admin_phone=$req->input('admin_phone');
     $model->admin_address=$req->input('admin_address');
     $model->admin_email=$req->input('admin_email');
     $model->password=$password;
     $model->admin_image=$req->admin_image->hashName().$currentimagedate;
     $model->company_id=$req->input('company_id');
     $model->admin_validity_period=$getdefaultvalidity;
     $model->p_change_method=$req->input('p_change_method');
     $model->device_token=$req->input('device_token');
     $model->admin_type=$req->input('admin_type');
     $model->save();
     
     return $this->sendResponse($model,'Congrats!, admin has been registered successfully!!');  
}
}
 catch ( \Exception $ex ){
     
      return $this->sendError(($ex->getMessage())); 
    }
      //  return $this->sendResponse($success, 'Congrats!, User has been registered successfully!!');




  }



	//for Admin login after validation
 public function adminLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
           // 'admin_name' => 'required',
            'admin_email' => 'required|email',
            'password'=>'required|min:6',
            'company_id'=>'required',
        ],[
            
            ]);
       
        if($validator->fails()){
            
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);
        } 
     
  if (Auth::guard('admin')->attempt(['admin_email'=> $request->admin_email, 'password' => $request->password, 'company_id'=>$request->company_id])) {
       config(['auth.guards.api.provider' => 'admin_info']);  
       $admin=Auth::guard('admin')->user();
     $success['admin_id']=$admin->admin_id;
     $request['device_token']=$admin->device_token;
     // $id=LoginModel::find('admin_email');
      // $success['id']=LoginModel::first('admin_id');
       $success['token'] =  $admin->createToken('MyApp')->accessToken;
          Log::channel('adminlogin')->info('an admin '.$admin->admin_name.' logged in!'); 
   return $this->sendResponse($success,'token created successfully!!'); 
}
   else{
         Log::channel('adminlogin')->emergency('an admin with email: '.$request['admin_email'].' wrongfully attempted to log in!'); 
    return $this->sendError('provided credentials are wrong!');
  }
    }

//******************for viewing the logged in Admin datas::*********************

    public function index(){
    	    $get_all = LoginModel::all();
    
        return $this->sendResponse(admininfoResource::collection($get_all), 'Authorized Admins retrieved successfully!!');
    }

  public function show($id){
  if (Auth::guard('admin-api')->check()) {
  $showall=LoginModel::find($id);
  if(is_null($showall)){
  return $this->sendError('no values to show!!');
  }
  else{
  return $this->sendResponse($showall,'the values are successfully shown!');
  }
 }
 else {
  return $this->sendError('You are Unauthorized to show Admins.');    
 }
  }
  
  
  //for changing password
  
public function changePassword(Request $request,$id){
     $validator = Validator::make($request->all(), [
            'old_password' => 'required|min:6',
            'new_password'=>'required|min:6|different:old_password',
            'confirm_password'=>'required|same:new_password',
        ],
        [
        'new_pin.different'=>'New password should be different than old one!!'    
            ]);
        if($validator->fails()){
           $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string); 
        }
   if (Auth::guard('admin-api')->check()) {
  $showall=LoginModel::find($id);
  if(is_null($showall)){
  return $this->sendError('there is no admin to show!!');
  }
  else{
      $getpass=$showall->password;
   //   \Hash::check($request->oldpassword , $hashedPassword )
   if(\Hash::check($request->old_password,$getpass) ){
       $showall->password=bcrypt($request->new_password);
       $query=LoginModel::where( 'admin_id' , $showall->admin_id)->update( array( 'password' =>  $showall->password,'p_change_method'=>'c'));
       
       return $this->sendResponse($query,'admin password is successfully changed!');
   } 
   else{
         return $this->sendError('old password doesnt match'); 
   }
      
  }
       
   }
}
//for handling forget password things
public function mailForgetPassword(Request $request){
    $validator = Validator::make($request->all(), [
        'admin_email' => 'required|email|exists:admin_info',
        ]);
        if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
        //make token to send as otp
        $say='your new password is:';
      
     $gettoken=self::$token;
       $warn=' '.' '.' change your password Immediately to avoid security risk!!';
     //  $link=$request['site_url'];
       //$link= redirect()->away('https://www.digitalyatayat.com/#/login');
       $concat=$say . ' '.' ' . $gettoken.'  '.'  '.$warn;
      $sendto=$request['admin_email'];
       
       $showall=AdminReset::where('admin_email','=',$request['admin_email'])->first();
 
  if(is_null($showall)){
      
      
//send mail
        \Mail::raw($concat, function($message) use ($sendto,$gettoken) {
        // $otp=rand(100000, 999999);
        $message->subject('System Generated Password:')->to($sendto);
        });  
//put in db table        
         DB::table('admin_password_reset')->insert(
          ['admin_email' => $request->admin_email, 'token' => self::$token, 'created_at' => Carbon::now()]
      );
  return $this->sendResponse(self::$token,'Your New Password has been sent to your email address!!');
  }
     else{
      return   $this->sendError('Token is already sent to your Email,Please check your email address');
     }  
       
      
      /*    $data = [
          'email' =>self::$sendto,
          'content'=>'your new Password is: '.' '.self::$token,
          'subject'=>'Change your password'
        ];
         Mail::send('email', $data, function($message) use ($data) {
              $message->to($data['email'])->subject($data['subject']);
        }); */
    
    
   
 
}
//for updating password to db
public  function updateForgetPassword(Request $request){
    
    $validator = Validator::make($request->all(), [
        'admin_email' => 'required|email|exists:admin_info',
       'password' => 'required|exists:admin_password_reset,token',
        ]);
        if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }

      
        
//now updating in admin table
 $holdme=DB::table('admin_info')->where('admin_email',  $request['admin_email'])->update(
          ['password' => bcrypt($request['password']),'p_change_method'=>'f']); 
 
//then delete from reset table
    DB::table('admin_password_reset')->where(['admin_email'=> $request->admin_email])->delete();

  return $this->sendResponse(' ','Your  Password has been changed successfully!!');
    
    
}



}
