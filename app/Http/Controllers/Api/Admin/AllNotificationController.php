<?php

namespace App\Http\Controllers\Api\Admin;
use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Rules\Nepali_Calendar as NepaliCalender;
use App\Models\Users\busOwner;
use App\Models\Users\DummyBusOwner;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\Notification as Mynotification;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\DummyBusInfo;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Admin\adminInfo as admininfoResource;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class AllNotificationController extends BaseController
{
     private static $serverapikey;
   function __construct(){
       self::$serverapikey='AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql';
   }
    public function getcurl(){
        //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
          // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
        //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',];
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
          return $this->sendResponse($result,'NOtification successfully sent!!');   
            
    }
    public function sendNotificationToOwnerAndDriver (Request $request)
    {
        // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
        //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
        //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',];
     
        //notification contents:
            $notify=array('title'=>$request->title,'body'=>$request->body);
       
        //taking requests
        $getownerphone=$request['owner_phone'];
        $getbusid=$request['bus_id'];
        if($getownerphone){
            $getowner=busOwner::where('owner_phone',$getownerphone)->first();
            $ownerFcmToken=$getowner->device_token;
            //saving in notification table as well::
            $store=new Mynotification();
            $store->type=$request['type'];
            $store->notifiable_id=$getowner->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title=$request['title'];
            $store->body=$request['body'];
            $store->added_by=$request['added_by'];
             $store->created_at=Carbon::now();
            $store->company_id=$getowner->company_id;
            $store->save();
   //sending request field:for whom(reciever) and for what(data)
        $fields= json_encode(array('to'=>$ownerFcmToken,
        'notification'=>$notify));     
 
//!!!!using curl to send notification!!!!

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
          return $this->sendResponse($result,'NOtification successfully sent!!');   
            
            
            
            
            
        }
        else if($getbusid){
            $getdriver=businfo::where('bus_id',$getbusid)->join('driver_info', 'bus_info.bus_no', '=', 'driver_info.bus_no')->first();
           $driverFcmToken=$getdriver->device_token;
             //saving in notification table as well::
            $store=new Mynotification();
             $store->type=$request['type'];
            $store->notifiable_id=$getdriver->driver_id;
            $store->notifiable_type=Driver::class;
            $store->title=$request['title'];
            $store->body=$request['body'];
            $store->added_by=$request['added_by'];
            $store->created_at=Carbon::now();
            $store->company_id=$getdriver->company_id;
            $store->save();
           $ch = curl_init();  
        //sending request field:for whom(reciever) and for what(data)
        $fields= json_encode(array('to'=>$driverFcmToken,
        'notification'=>$notify));  
          
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
          return $this->sendResponse($result,'NOtification successfully sent!!');   
        }
        else{
         return   $this->sendError('Provided input is invalid!!');
        }
    }
 //getting notification by latest date of any kind   
public function getAlltypeNotification($notifiable_id){
    $getany=Mynotification::where('notifiable_id',$notifiable_id)->orderBy('created_at', 'desc')->first();
    if(is_null($getany)){
        return $this->sendError('there are no datas on this ID!!');
    }
    else{
         return $this->sendResponse($getany,'latest notification datas filtered successfully!!');
    }
    
}    

public function getAdmintypeNotification($company_id){
     $getany=Mynotification::where('company_id',$company_id)->where('notifiable_type','App\Models\Admin\LoginModel')->where('read_at',null)->orderBy('created_at', 'desc')->get();
   
     if(is_null($getany)){
        return $this->sendError('there are no datas on this ID!!');
    }
    else{
         return $this->sendResponse($getany,'all null type notification datas filtered successfully!!');
    }
}

public function getAllAdminNotification($company_id){
    $getall=Mynotification::where('company_id',$company_id)->where('notifiable_type','App\Models\Admin\LoginModel')->orderBy('created_at', 'desc')->get();
    if(is_null($getall)){
        return $this->sendError('there are no datas on this ID!!');
    }
    else{
         return $this->sendResponse($getall,'all admin notification datas filtered successfully!!');
    }
}

public function countAdmintypeUnreadNotification(Request $req,$company_id){
     $countunread=Mynotification::where('company_id',$company_id)->where('notifiable_type','App\Models\Admin\LoginModel')->where('read_at',null)->count();
     if(is_null($countunread)){
        return $this->sendError('there are no datas on this ID!!');
    }
    else{
         return $this->sendResponse($countunread,'unread notification datas filtered successfully!!');
    }
}

public function MarkAsReadSingleAdminNotification($id){
    $markasread=Mynotification::where('id',$id)->where('read_at',null)->update( array( 'read_at' => Carbon::now()));
     return $this->sendResponse('done!','notification status is updated!!');
    
}

public function markAsReadAllAdminNotification($company_id){
    
    $getnullnotification=Mynotification::where('company_id',$company_id)->where('read_at',null)->where('notifiable_type','App\Models\Admin\LoginModel')->get();
    $arrays=[];
    //return $this->sendResponse($getnullnotification,'latest notification datas filtered successfully!!');
   foreach($getnullnotification as $nullnotification){
       $get=$nullnotification->where('notifiable_type','App\Models\Admin\LoginModel')->update( array( 'read_at' => Carbon::now()));
       
   }
   return $this->sendResponse('done!','all notification status are updated !!');
}


//getting latest notification of android null status:
public function getAndroidNulltypeNotification($notifiable_id){
    $getnull=Mynotification::where('notifiable_id',$notifiable_id)->where('android_read_status',null)->orderBy('created_at', 'desc')->get();
    if(is_null($getnull)){
        return $this->sendError('there are no datas on this ID!!');
    }
    else{
         return $this->sendResponse($getnull,'latest notification datas filtered successfully!!');
    }
    
}    

//update andorid:read column:
public function updateAndroidNullNotification(Request $req,$notifiable_id){
    $modal=Mynotification::where( 'notifiable_id' , $notifiable_id)->update( array( 'android_read_status' =>  $req['read_status']));
    
     return $this->sendResponse($modal,'Read status updated successfully!!');
}
//get history of android notification
public function getHistryOfAnyNotificaion($notifiable_id){
    $getanyhistory=Mynotification::where('notifiable_id',$notifiable_id)->orderBy('created_at', 'desc')->get();
     if(is_null($getanyhistory)){
        return $this->sendError('there are no datas on this ID!!');
    }
    else{
         return $this->sendResponse($getanyhistory,'notification history filtered successfully!!');
    }
}    
 //notify update:for owner and driver
public function NotificationOwnerUpdate (Request $request,$id){
     // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
     //notification contents:
            $notify=array('title'=>$request->title,'body'=>$request->body);
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
    //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',];     
    $getownerdata=busOwner::where('owner_id',$id)->first();
  //get owner token
     $ownerFcmToken=$getownerdata->device_token;  
     
     //saving in notification table as well::
            $store=new Mynotification();
             $store->type=$request['type'];
            $store->notifiable_id=$getownerdata->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title=$request['title'];
            $store->body=$request['body'];
            $store->added_by=$request['added_by'];
             $store->created_at=Carbon::now();
            $store->company_id=$getownerdata->company_id;
            $store->save();
           $ch = curl_init();  
        
        //sending request field:for whom(reciever) and for what(data):::for owner
        $fields= json_encode(array( 'to'=> $ownerFcmToken,
        'notification'=>$notify));  
          
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
 ///         return $this->sendResponse($result,'NOtification successfully sent!!');   
//including driver as well  
 return $this->ownerupdateTodriver($request,$id) ;
  } 
//notify:ownerupdate: to driver
public function ownerupdateTodriver(Request $request,$id){
         //notification contents:
            $notify=array('title'=>$request->title,'body'=>$request->body);
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
          //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',];     
    $driverdata=busOwner::where('bus_owner.owner_id',$id)->join('bus_info', 'bus_owner.owner_phone', '=', 'bus_info.owner_phone')->join('driver_info', 'bus_info.bus_no', '=', 'driver_info.bus_no')->select('bus_owner.owner_name','bus_info.bus_no','driver_info.*')->get();
        //   return $this->sendResponse($driverdata,'NOtification successfully sent!!'); 
           $get=[];
           $result=[];
            foreach($driverdata as $selectedonly){
            //get birthday    
           
            //get name,device_token and birthdate
            $get[]=[
            $getdriverid=$selectedonly->driver_id,    
            $getname=$selectedonly->driver_name,
            $gettoken=$selectedonly->device_token,
            $getcompanyid=$selectedonly->company_id,
                ];
            
              //sending request field:for whom(reciever) and for what(data):::for owner
        $fields= json_encode(array( 'to'=> $gettoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
        //  return $this->sendResponse($result,'NOtification successfully sent!!');   
    
                 //saving in notification table as well::
            $store=new Mynotification();
             $store->type=$request['type'];
            $store->notifiable_id=$getdriverid;
            $store->notifiable_type=Driver::class;
            $store->title=$request['title'];
            $store->body=$request['body'];
            $store->added_by=$request['added_by'];
             $store->created_at=Carbon::now();
            $store->company_id=$getcompanyid;
            $store->save();
            }
    
      return $this->sendResponse($get,'Notification successfully sent!!');   

    }
    //notify update:owner and admin
public function driverUpdateToOwnerAndAdmin(Request $request,$id){
        //for owner at first....
         // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
     //notification contents:
            $notify=array('title'=>$request->title,'body'=>$request->body);
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
    //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',]; 
     $getownerdata=Driver::where('driver_info.driver_id',$id)->join('bus_info', 'driver_info.bus_no', '=', 'bus_info.bus_no')->join('bus_owner', 'bus_info.owner_phone', '=', 'bus_owner.owner_phone')->select('bus_owner.*')->first();
     if($getownerdata==null||$getownerdata->device_token==null){
          return $this->driverupdateToAdmin($request,$id);   
        
       }     
  //get owner token
     $ownerFcmToken=$getownerdata->device_token; 
     // return $this->sendResponse($ownerFcmToken,'NOtification successfully sent!!');  
     //saving in notification table as well::
            $store=new Mynotification();
             $store->type=$request['type'];
            $store->notifiable_id=$getownerdata->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title=$request['title'];
            $store->body=$request['body'];
            $store->added_by=$request['added_by'];
             $store->created_at=Carbon::now();
            $store->company_id=$getownerdata->company_id;
            $store->save();
           $ch = curl_init();  
        //sending request field:for whom(reciever) and for what(data):::for driver update
        $fields= json_encode(array( 'to'=> $ownerFcmToken,
        'notification'=>$notify));  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
     // Close connection
        curl_close($ch);
     // FCM response
    ///         return $this->sendResponse($result,'NOtification successfully sent!!');       
      //including admin as well  
   return $this->driverupdateToAdmin($request,$id);  
    }
    public function driverupdateToAdmin($request,$id){
     //notification contents:
            $notify=array('title'=>$request->title,'body'=>$request->body);
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
          //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',];     
          $getadmindata=Driver::where('driver_info.driver_id',$id)->join('admin_info', 'driver_info.company_id', '=', 'admin_info.company_id')->select('driver_info.driver_name','admin_info.*')->first();
        //  return $this->sendResponse($getadmindata,'NOtification successfully sent!!');
    //get driver token
    $adminFcmToken=$getadmindata->device_token;
   //  return $this->sendResponse($adminFcmToken,'NOtification successfully sent!!');
     //saving in notification table as well::
            $store=new Mynotification();
             $store->type=$request['type'];
            $store->notifiable_id=$getadmindata->admin_id;
            $store->notifiable_type=LoginModel::class;
            $store->title=$request['title'];
            $store->body=$request['admin_body'];
            $store->added_by=$request['added_by'];
             $store->created_at=Carbon::now();
            $store->company_id=$getadmindata->company_id;
            $store->save();
    //sending request field:for whom(reciever) and for what(data):::for owner
        $fields= json_encode(array( 'to'=> $adminFcmToken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
          return $this->sendResponse($result,'NOtification successfully sent!!');       
        
        
        
    }
    //notify:busupdate: to driver and admin
public function busUpdateToDriverAndAdmin(Request $request,$id){
         //for driver at first....
         // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
     //notification contents:
            $notify=array('title'=>$request->title,'body'=>$request->body);
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
    //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',]; 
     $getdriverdata=businfo::where('bus_info.bus_id',$id)->join('driver_info', 'bus_info.bus_no', '=', 'driver_info.bus_no')->select('bus_info.bus_no','driver_info.*')->first();
    // return $this->sendResponse($getdriverdata,'NOtification successfully sent!!'); 
     if($getdriverdata==null||$getdriverdata->device_token==null){
      //  return $this->sendResponse($getdriverdata,'NOtification successfully sent!!'); 
         return $this->busUpdateToAdmin($request,$id);
     }
    //get driver token
     $driverFcmToken=$getdriverdata->device_token;  
      //return $this->sendResponse($driverFcmToken,'NOtification successfully sent!!');  
     //saving in notification table as well::
            $store=new Mynotification();
             $store->type=$request['type'];
            $store->notifiable_id=$getdriverdata->driver_id;
            $store->notifiable_type=Driver::class;
            $store->title=$request['title'];
            $store->body=$request['body'];
            $store->added_by=$request['added_by'];
             $store->created_at=Carbon::now();
            $store->company_id=$getdriverdata->company_id;
            $store->save();
           $ch = curl_init();  
        
        //sending request field:for whom(reciever) and for what(data):::for driver update
        $fields= json_encode(array( 'to'=> $driverFcmToken,
        'notification'=>$notify));  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
 ///         return $this->sendResponse($result,'NOtification successfully sent!!');       
      //including admin as well  
 return $this->busUpdateToAdmin($request,$id);  

    }
 public function busUpdateToAdmin($request,$id){
        //notification contents:
            $notify=array('title'=>$request->title,'body'=>$request->body);
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
          //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',];     
          $getadmindata=businfo::where('bus_info.bus_id',$id)->join('admin_info', 'bus_info.company_id', '=', 'admin_info.company_id')->select('bus_info.bus_no','admin_info.*')->first();
        //  return $this->sendResponse($getadmindata,'NOtification successfully sent!!');
    //get admin token
    $adminFcmToken=$getadmindata->device_token;
     //saving in notification table as well::
            $store=new Mynotification();
             $store->type=$request['type'];
            $store->notifiable_id=$getadmindata->admin_id;
            $store->notifiable_type=LoginModel::class;
            $store->title=$request['title'];
            $store->body=$request['admin_body'];
            $store->added_by=$request['added_by'];
             $store->created_at=Carbon::now();
            $store->company_id=$getadmindata->company_id;
            $store->save();
    //sending request field:for whom(reciever) and for what(data):::for owner
        $fields= json_encode(array( 'to'=> $adminFcmToken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
          return $this->sendResponse($result,'NOtification successfully sent!!');       
    }
 //notify:owner update: to driver and admin
public function ownerUpdateToDriverAndAdmin(Request $request,$id){
         //for driver at first....
         // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
     //notification contents:
            $notify=array('title'=>$request->title,'body'=>$request->body,'sound' => 'default');
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
    //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',]; 
    $getdriverdata= busOwner::where('bus_owner.owner_id',$id)->join('bus_info', 'bus_owner.owner_phone', '=', 'bus_info.owner_phone')->join('driver_info', 'bus_info.bus_no', '=', 'driver_info.bus_no')->select('bus_owner.owner_phone','driver_info.*')->get();
    if($getdriverdata==null){
           return $this->ownerUpdateToAdmin($request,$id);  
        
       }
  //  return $this->sendResponse($getdriverdata,'NOtification successfully sent!!'); 
    //get multiple drivers token
    $get=[];
    foreach($getdriverdata as $getdata){
        
         $get[]=[
            $getdriverid=$getdata->driver_id,    
            $getname=$getdata->driver_name,
            $gettoken=$getdata->device_token,
            $getcompanyid=$getdata->company_id,
                ];
       // $gettokens=$getdata->device_token;
         //saving in notification table as well::
            $store=new Mynotification();
             $store->type=$request['type'];
            $store->notifiable_id=$getdriverid;
            $store->notifiable_type=Driver::class;
            $store->title=$request['title'];
            $store->body=$request['body'];
            $store->added_by=$request['added_by'];
             $store->created_at=Carbon::now();
            $store->company_id=$getcompanyid;
            $store->save();
     //       $driverFcmToken=$getdata->device_token;
       //     $tokenarray=array_map('intval', explode(',', $driverFcmToken));
       //      return $this->sendResponse($driverFcmToken,'NOtification successfully sent!!');  
                 
            $ch = curl_init();
            //sending request field:for whom(reciever) and for what(data):::for driver update
        $fields= json_encode(array( 'to'=> $gettoken,
        'notification'=>$notify));  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);
        // FCM response
       //   return $this->sendResponse($result,'NOtification successfully sent!!');  
    } 
             
      //including admin as well  
 return $this->ownerUpdateToAdmin($request,$id);  

    }
    public function ownerUpdateToAdmin($request,$id){
         //notification contents:
            $notify=array('title'=>$request->title,'body'=>$request->body,'sound' => 'default');
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
          //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',];     
          $getadmindata=busOwner::where('bus_owner.owner_id',$id)->join('admin_info', 'bus_owner.company_id', '=', 'admin_info.company_id')->select('bus_owner.owner_phone','admin_info.*')->first();
      //   return $this->sendResponse($getadmindata,'NOtification successfully sent!!');
    //get admin token
    $adminFcmToken=$getadmindata->device_token;
     //saving in notification table as well::
            $store=new Mynotification();
             $store->type=$request['type'];
            $store->notifiable_id=$getadmindata->admin_id;
            $store->notifiable_type=LoginModel::class;
            $store->title=$request['title'];
            $store->body=$request['admin_body'];
            $store->added_by=$request['added_by'];
             $store->created_at=Carbon::now();
            $store->company_id=$getadmindata->company_id;
            $store->created_at=Carbon::now();
            $store->save();
    //sending request field:for whom(reciever) and for what(data):::for owner
        $fields= json_encode(array( 'to'=> $adminFcmToken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
          return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');      
    }
    
//notify owner:of bus entry
public function notifyOwnerOfBusEntry(Request $request,$owner_phone){
    // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
     //notification contents:
            $notify=array('title'=>$request->title,'body'=>$request->body,'sound' => 'default');
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
    //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',];
    $getownerdata= businfo::where('bus_info.owner_phone',$owner_phone)->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->select('bus_info.bus_id','bus_info.bus_no','bus_owner.owner_id','bus_owner.owner_name','bus_owner.owner_phone','bus_owner.device_token')->first();
    $ownerFcmToken=$getownerdata->device_token;
    //sending request field:for whom(reciever) and for what(data):::for driver update
        $fields= json_encode(array( 'to'=> $ownerFcmToken,
        'notification'=>$notify));  
         $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
   //      return $this->sendResponse($result,'NOtification successfully sent!!');       
    
     //saving in notification table as well::
            $store=new Mynotification();
             $store->type=$request['type'];
            $store->notifiable_id=$getownerdata->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title=$request['title'];
            $store->body=$request['body'];
            $store->added_by=$request['added_by'];
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$getownerdata->company_id;
            $store->save();
  return $this->sendResponse($store,'NOtification successfully sent!!');
}
//notify driver and admin of driver update done by :owner
public function driverUpdateByOwnerToDriverAndAdmin(Request $request,$id){
      //for driver at first....
         // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
     //notification contents:
            $notify=array('title'=>$request->title,'body'=>$request->body,'sound' => 'default');
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
    //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',]; 
        $getdriverid=$request['driver_id'];    
   $getdriverofowner=busOwner::where('bus_owner.owner_id',$id)->join('bus_info','bus_owner.owner_phone','bus_info.owner_phone')->join('driver_info','bus_info.bus_no','driver_info.bus_no')->where('driver_info.driver_id',$getdriverid)->select('bus_owner.owner_id','bus_owner.owner_name','bus_info.bus_no','driver_info.driver_id','driver_info.driver_name','driver_info.device_token','driver_info.company_id')->first();
   
               $ch = curl_init();
            //sending request field:for whom(reciever) and for what(data):::for driver update
        $fields= json_encode(array( 'to'=> $getdriverofowner->device_token,
        'notification'=>$notify));  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);
        // FCM response
     //   return $this->sendResponse($result,'NOtification successfully sent!!');
        //saving in notification table as well::
            $store=new Mynotification();
             $store->type=$request['type'];
            $store->notifiable_id=$getdriverofowner->driver_id;
            $store->notifiable_type=Driver::class;
            $store->title=$request['title'];
            $store->body=$request['body'];
            $store->added_by=$request['added_by'];
             $store->created_at=Carbon::now();
            $store->company_id=$getdriverofowner->company_id;
            $store->save();  
             //  return $this->sendResponse($store,'NOtification successfully sent!!');
   //same as admin changing his/her profile 
   //including admin as well  
 return $this->ownerUpdateToAdmin($request,$id);  
}
    //birthday notification to driver of any company
public function birthdayNotificationToDriver(Request $request){
         //server key
            define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
          //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',]; 
            //get all driver data:
            $getalldata=Driver::get();
            $get=[];
            $getbirthdate=[];
            //using calender to convert date in english
            $callcalender=new NepaliCalender();
            //looping for sending notification as per their birthday: 
            foreach($getalldata as $selectedonly){
            //get birthday    
           
            //get name,device_token and birthdate
            $get[]=[
            $getname=$selectedonly->driver_name,
            $gettoken=$selectedonly->device_token,
            $getdate=$selectedonly->driver_dob,
                ];
                
            if(is_null($getdate)){
                
            }   
            else{
              //start of:today nepali date section....... 
           $nepalidate=Carbon::now()->toDateString();
           $correctformatfornepali=str_replace('-', ',',$nepalidate);
           $nepalidatearray=explode (",", $correctformatfornepali);
           //using converter
           $nyear=$nepalidatearray[0];
           $nmonth=$nepalidatearray[1];
           $nday=$nepalidatearray[2];
           $convertonepali= $callcalender->eng_to_nep($nyear,$nmonth,$nday);
            $nslice =array_shift($convertonepali);
            //implding to string format
           $ntostring=implode(",",$convertonepali);
           //then,nepali month and day value
          $nmonthandday = str_replace( ',', ' ', $ntostring );
        //end of today nepali date section......    
                
                
            //for birthday::=> put in correct format:
            $correctformat= str_replace('-', ',',$getdate);
            //then,convert to array format
            $makeitarray = explode (",", $correctformat); 
           
            //get year,month and day separately
            $year=$makeitarray[0];
            $month=$makeitarray[1];
            $day=$makeitarray[2];
          //then,finally get month and year in array
            $trmonth=ltrim($month, '0');
            $trday=ltrim($day, '0');
            $nepalibirthdate=$trmonth.' '.$trday;
                    //comparing today date with birthdate and notifying on that date:
         if($nmonthandday==$nepalibirthdate){
             //notification contents:
            $notify=array('title'=>'Birthday Reminder','body'=>'HAPPY Birthday to you '.$getname.'!'.' ,Best wishes for you','sound' => 'default');
           //then notify::=> each driven via tokens:
            //sending request field:for whom(reciever) and for what(data):::for owner
        $fields= json_encode(array( 'to'=> $gettoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
             $store->type='general';
            $store->notifiable_id=$selectedonly->driver_id;
            $store->notifiable_type=Driver::class;
            $store->title='Birthday Reminder';
            $store->body='Birhtday of:'.$getname;
            $store->added_by='Backend';
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$selectedonly->company_id;
            $store->save();
          
            }
            }
            } 
          return $response = \Response::json($get, 200);

    }
    //birthday notification to owner of any compamy
public function birthdayNotificationToOwner(){
       //notification contents:
    //server key
            define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
          //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',]; 
            //get all driver data:
            $getalldata=busOwner::get();
            $get=[];
            $getbirthdate=[];
            //using calender to convert date in english
            $callcalender=new NepaliCalender();
            //looping for sending notification as per their birthday: 
            foreach($getalldata as $selectedonly){
            //get birthday    
           
            //get name,device_token and birthdate
            $get[]=[
            $getname=$selectedonly->owner_name,
            $gettoken=$selectedonly->device_token,
            $getdate=$selectedonly->owner_dob,
                ];
        //start of:today nepali date section....... 
           $nepalidate=Carbon::now()->toDateString();
         //   return $this->sendResponse($nepalidate,'Notification successfully sent!!');
           $correctformatfornepali=str_replace('-', ',',$nepalidate);
           $nepalidatearray=explode (",", $correctformatfornepali);
           //using converter
           $nyear=$nepalidatearray[0];
           $nmonth=$nepalidatearray[1];
           $nday=$nepalidatearray[2];
           $convertonepali= $callcalender->eng_to_nep($nyear,$nmonth,$nday);
            $nslice =array_shift($convertonepali);
            //implding to string format
           $ntostring=implode(",",$convertonepali);
           //then,nepali month and day value
          $nmonthandday = str_replace(',', ' ', $ntostring );
        //end of today nepali date section......
          
            //for birthday::=> put in correct format:
            $correctformat= str_replace('-', ',',$getdate);
            return $this->sendResponse($correctformat,'Notification successfully sent!!'); 
            //then,convert to array format
            $makeitarray = explode (",", $correctformat); 
          // return $this->sendResponse($makeitarray,'Notification successfully sent!!');
          //get year,month and day separately
            $year=$makeitarray[0];
            $month=$makeitarray[1];
            $trmonth=ltrim($month, '0');
            $day=$makeitarray[2];
            $trday=ltrim($day, '0');
            $nepalibirthdate=$trmonth.' '.$trday;
         //   return $this->sendResponse($nepalibirthdate,'Notification successfully sent!!');
        
     if($nmonthandday==$nepalibirthdate){
            //then notify::=> each driven via tokens:
            //sending request field:for whom(reciever) and for what(data):::for owner
             $notify=array('title'=>'Birthday Reminder','body'=>'HAPPY Birthday to you '.$getname.'!'.' ,Best wishes for you','sound' => 'default');
        $fields= json_encode(array( 'to'=> $gettoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
             $store->type='general';
            $store->notifiable_id=$selectedonly->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Birthday Reminder';
            $store->body='HAPPY Birthday to you '.$getname.'!'.' ,Best wishes for you';
            $store->added_by='Backend';
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$selectedonly->company_id;
            $store->save();
            }
       
            }
          return $response = \Response::json($get, 200);

} 
//birthday notification to owner of any compamy
public function birthdayNotificationToStaff(){
       //notification contents:
            //server key
            define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
          //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',]; 
            //get all driver data:
            $getalldata=StaffInfo::get();
            $get=[];
            $getbirthdate=[];
            //using calender to convert date in english
            $callcalender=new NepaliCalender();
            //looping for sending notification as per their birthday: 
            foreach($getalldata as $selectedonly){
            //get birthday    
           
            //get name,device_token and birthdate
            $get[]=[
            $getname=$selectedonly->staff_name,
            $gettoken=$selectedonly->device_token,
            $getdate=$selectedonly->staff_dob,
                ];
            //for birthday::=> put in correct format:
            $correctformat= str_replace('-', ',',$getdate);
            //then,convert to array format
            $makeitarray = explode (",", $correctformat); 
           
            //get year,month and day separately
            $year=$makeitarray[0];
            $month=$makeitarray[1];
            $day=$makeitarray[2];
            
            //finally,convert into english date
            $getbirthdate= $callcalender->nep_to_eng($year,$month,$day);
           // return $this->sendResponse($getbirthdate,'Notification successfully sent!!');
            //slicing year to get month and day
            $slice =array_shift($getbirthdate);
            //implding to string format
           $tostring=implode(",",$getbirthdate);
           //then,month and day value
          $monthandday = str_replace( ',', ' ', $tostring );

        // return $this->sendResponse($monthandday,'Notification successfully sent!!');
            //get today english date
            $todayenglishdate=Carbon::now()->toDateString();
            //now putting today english date into correct format
            $todayenglishdateformat=str_replace('-', ',',$todayenglishdate);
            //then,convert today english date to array format
            $makeitarray = explode (",", $todayenglishdateformat); 
            //then,finally get month and year in array
             $emonths=$makeitarray[1];
             $emonth=ltrim($emonths, '0');
            $edays=$makeitarray[2];
            $eday=ltrim($edays, '0');
            $todayenglishmonthandday=$emonth.' '.$eday;
           //return $this->sendResponse($todayenglishmonthandday,'Notification successfully sent!!');
           //comparing today date with birthdate and notifying on that date:
           if($todayenglishmonthandday==$monthandday){
           //then notify::=> each driven via tokens:
            //sending request field:for whom(reciever) and for what(data):::for owner
             $notify=array('title'=>'Birthday Reminder','body'=>'HAPPY Birthday to you '.$getname.'!'.' ,Best wishes for you','sound' => 'default');
        $fields= json_encode(array( 'to'=> $gettoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
             $store->type='general';
            $store->notifiable_id=$selectedonly->staff_id;
            $store->notifiable_type=StaffInfo::class;
            $store->title='Birthday Reminder';
            $store->body='Birhtday of:'.$getname;
            $store->added_by='Backend';
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$selectedonly->company_id;
            $store->save();
            }
           } 
          return $response = \Response::json($get, 200);

} 
//sending notification to owner when routepermit ,checkpass,bill book,insurance ends(sending 6 day before the end date)
 public function notifyOwnerAndAdminOfEndDates(Request $request){
  //1st notify owner of Bus::
         // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
        
        //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
          //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',]; 
            //using calender-converter::
            $callcalender=new NepaliCalender();
        //get owner of bus:
        $getownerofbus=businfo::join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->select('bus_owner.owner_id','bus_owner.owner_name','bus_owner.owner_phone','bus_owner.device_token','bus_info.*')->get();
        $getinarray=[];
        $getbillbookedate=[];
        $getcheckpassedate=[];
        //looping for all owners of each bus
        foreach($getownerofbus as $ownerdatas){
            //getrelevent datas
            $getdataarray[]=[
                $busno=$ownerdatas->bus_no,
                $ownername=$ownerdatas->owner_name,
                $ownerdevicetoken=$ownerdatas->device_token,
                $billbookenddate=$ownerdatas->bus_billbook_enddate,
                $routepermitenddate=$ownerdatas->bus_routepermit_enddate,
                $checkpassenddate=$ownerdatas->bus_checkpass_enddate,
                $insuranceenddate=$ownerdatas->bus_insurance_enddate,
                ];
        //start of:today nepali date section....... 
           $nepalidate=Carbon::now()->toDateString();
           $correctformatfornepali=str_replace('-', ',',$nepalidate);
           $nepalidatearray=explode (",", $correctformatfornepali);
           //using converter
           $nyear=$nepalidatearray[0];
           $nmonth=$nepalidatearray[1];
           $nday=$nepalidatearray[2];
           $convertonepali= $callcalender->eng_to_nep($nyear,$nmonth,$nday);
            $nslice =array_shift($convertonepali);
            //implding to string format
           $ntostring=implode(",",$convertonepali);
           //then,nepali month and day value
          $nmonthandday = str_replace( ',', ' ', $ntostring );
        //end of today nepali date section......        
        //now,-------------------- for billbook enddate::::
          //put in correct format:
            $bcorrectformat= str_replace('-', ',',$billbookenddate);
            //then,convert to array format
            $bmakeitarray = explode (",", $bcorrectformat); 
            //get year,month and day separately
            $byear=$bmakeitarray[0];
            $bmonth=$bmakeitarray[1];
            $btrmonth=ltrim($bmonth, '0');
            $bday=$bmakeitarray[2];
            $btrday=ltrim($bday, '0');
            $bmonthandday=$btrmonth.' '.$btrday;
         
          //------------******* 7 day before the enddate ******----------- 
          $bwarnbmonthday=Carbon::parse($billbookenddate)->subDays('6');
          $bwdateonly=$bwarnbmonthday->toDateString();
          $bwarnformat=str_replace('-', ',',$bwdateonly);
          $bwmakeitarray = explode (",", $bwarnformat); 
            //then,finally get month and year in array
             $bwmonths=$bwmakeitarray[1];
             $bwmonth=ltrim($bwmonths, '0');
            $bwdays=$bwmakeitarray[2];
            $bwday=ltrim($bwdays, '0');
            $bwarnmonthandday=$bwmonth.' '.$bwday;
      
         
    //----------------end of billbok part-----------------------
    //now,-------------------- for routepermit enddate::::
     //put in correct format:
            $rcorrectformat= str_replace('-', ',',$routepermitenddate);
            //then,convert to array format
            $rmakeitarray = explode (",", $rcorrectformat); 
            //get year,month and day separately
            $ryear=$rmakeitarray[0];
            $rmonth=$rmakeitarray[1];
            $rtrmonth=ltrim($rmonth, '0');
            $rday=$rmakeitarray[2];
            $rtrday=ltrim($rday, '0');
            $rmonthandday=$rtrmonth.' '.$rtrday;
          //    return $this->sendResponse($rmonthandday,'Notification successfully sent!!'); 
         //------------******* 7 day before the enddate ******----------- 
          $rwarnbmonthday=Carbon::parse($routepermitenddate)->subDays('6');
          $rwdateonly=$rwarnbmonthday->toDateString();
           //now putting warning english date into correct format
            $rwarnformat=str_replace('-', ',',$rwdateonly);
            //then,convert today english date to array format
            $rwmakeitarray = explode (",", $rwarnformat); 
            //then,finally get month and year in array
             $rwmonths=$rwmakeitarray[1];
             $rwmonth=ltrim($rwmonths, '0');
             $rwdays=$rwmakeitarray[2];
             $rwday=ltrim($rwdays, '0');
            $rwarnmonthandday=$rwmonth.' '.$rwday;
      //   return $this->sendResponse($rwarnmonthandday,'Notification successfully sent!!');
    //----------------end of route permit part-----------------------
    //now,-------------------- for checkpass enddate::::
     //put in correct format:
            $chcorrectformat= str_replace('-', ',',$checkpassenddate);
            //then,convert to array format
            $chmakeitarray = explode (",", $chcorrectformat); 
            //get year,month and day separately
            $chyear=$chmakeitarray[0];
            $chmonth=$chmakeitarray[1];
            $ctrmonth=ltrim($chmonth, '0');
            $chday=$chmakeitarray[2];
            $ctrday=ltrim($chday, '0');
            $chmonthandday=$ctrmonth.' '.$ctrday;
          //   return $this->sendResponse($chmonthandday,'Notification successfully sent!!');  
         //------------******* 7 day before the enddate ******----------- 
          $cwarnbmonthday=Carbon::parse($checkpassenddate)->subDays('6');
          $cwdateonly=$cwarnbmonthday->toDateString();
           //now putting warning english date into correct format
            $cwarnformat=str_replace('-', ',',$cwdateonly);
            //then,convert today english date to array format
            $cwmakeitarray = explode (",", $cwarnformat); 
            //then,finally get month and year in array
             $cwmonths=$cwmakeitarray[1];
             $cwmonth=ltrim($cwmonths, '0');
             $cwdays=$cwmakeitarray[2];
             $cwday=ltrim($cwdays, '0');
            $cwarnmonthandday=$cwmonth.' '.$cwday;
      //    return $this->sendResponse($cwarnmonthandday,'Notification successfully sent!!');
     //----------------end of route permit part-----------------------   
    //finally,-------------------- for insurance enddate:::: 
    //put in correct format:
            $incorrectformat= str_replace('-', ',',$insuranceenddate);
            //then,convert to array format
            $inmakeitarray = explode (",", $incorrectformat); 
            //get year,month and day separately
            $inyear=$inmakeitarray[0];
            $inmonth=$inmakeitarray[1];
            $intrmonth=ltrim($inmonth, '0');
            $inday=$inmakeitarray[2];
            $intrday=ltrim($inday, '0');
            $inmonthandday=$intrmonth.' '.$intrday;
       //    return $this->sendResponse($inmonthandday,'Notification successfully sent!!');   
        //------------******* 7 day before the enddate ******----------- 
          $inwarnbmonthday=Carbon::parse($insuranceenddate)->subDays('6');
          $inwdateonly=$inwarnbmonthday->toDateString();
           //now putting warning english date into correct format
            $inwarnformat=str_replace('-', ',',$inwdateonly);
            //then,convert today english date to array format
            $inwmakeitarray = explode (",", $inwarnformat); 
            //then,finally get month and year in array
             $inwmonths=$inwmakeitarray[1];
             $inwmonth=ltrim($inwmonths, '0');
            $inwdays=$inwmakeitarray[2];
            $inwday=ltrim($inwdays, '0');
            $inwarnmonthandday=$inwmonth.' '.$inwday;
      //    return $this->sendResponse($inwarnmonthandday,'Notification successfully sent!!');
     //----------------end of insurance part-----------------------  
      //--------------------------------------------------------
      //checking multiple condition for end date cases:
      
      $addsixdaystonow=Carbon::now()->addDays('6')->toDateString();
    //  return $this->sendResponse($addsixdaystonow,'Notification successfully sent!!');
      //bilbook end date:
    if($nmonthandday==$bmonthandday){
    //sending request field:for whom(reciever) and for what(data):::for owner
    //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'Billbook date of  '.$busno.' is expired.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
         $store->body='Billbook date of  '.$busno.' is expired.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
       //     return $this->sendResponse($store,'Notification successfully sent!!');
          }
        //warning:before 6 day of billbook end date !     
    if($nmonthandday==$bwarnmonthandday){
        //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'Billbook date of '.$busno.' is on '.$addsixdaystonow.'.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
            $store->body='Billbook date of '.$busno.' is on '.$addsixdaystonow.'.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
           // return $this->sendResponse($store,'Notification successfully sent!!');

        }
    //for Route permit end date:    
    if($nmonthandday==$rmonthandday){
             //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'RoutePermit date of '.$busno.' is expired.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
            $store->body='RoutePermit date of '.$busno.' is expired.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
       //     return $this->sendResponse($store,'Notification successfully sent!!');
        }
     //warning:before 6 day of route permit end date !
    if($nmonthandday==$rwarnmonthandday){
         //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'RoutePermit date of '.$busno.' is on '.$addsixdaystonow.'.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
            $store->body='RoutePermit date of '.$busno.' is on '.$addsixdaystonow.'.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
           // return $this->sendResponse($store,'Notification successfully sent!!');

        }
    //for Check pass end date:
    if($nmonthandday==$chmonthandday){
      //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'Checkpass date of '.$busno.' is expired.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
            $store->body='Checkpass date of '.$busno.' is expired.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
       //     return $this->sendResponse($store,'Notification successfully sent!!');
        }
    //warning:before 6 day of checkpass end date !    
    if($nmonthandday==$cwarnmonthandday){
         //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'Checkpass date of '.$busno. ' is on '.$addsixdaystonow.'.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
            $store->body='Checkpass date of '.$busno. ' is on '.$addsixdaystonow.'.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
           // return $this->sendResponse($store,'Notification successfully sent!!');
    
        }
   //for insurance end date:    
    if($nmonthandday==$inmonthandday){
          //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'Insurance date of '.$busno.' is expired.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
            $store->body='Insurance date of '.$busno.' is expired.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
       //     return $this->sendResponse($store,'Notification successfully sent!!');    
        }
    //warning:before 6 day of insurance end date !    
    if($nmonthandday==$inwarnmonthandday){
         //notification contents:
        $notify=array('title'=>'Dear,'.' '.$ownername,'body'=>'Insurance date of '.$busno.' is on '.$addsixdaystonow.'.please pay in time!','sound' => 'default');
        $fields= json_encode(array( 'to'=> $ownerdevicetoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
     // Execute post
        $result = curl_exec($ch);
       if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
         // Close connection
        curl_close($ch);
       // FCM response
         // return $this->sendResponse('sent to multiple driver and an admin','Notification successfully sent!!');
           //saving in notification table as well::
            $store=new Mynotification();
            $store->type='general';
            $store->notifiable_id=$ownerdatas->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Reminder';
            $store->body='Insurance date of '.$busno.' is on '.$addsixdaystonow.'.please pay in time!';
            $store->added_by=$ownername;
            $store->created_at=Carbon::now();
            $store->updated_at=Carbon::now();
            $store->company_id=$ownerdatas->company_id;
            $store->save();
           // return $this->sendResponse($store,'Notification successfully sent!!');
            }
        }
        //then, also include Admin as well
        //including admin as well  
// return $this->warnAdminOfEndDates();  
         return $response = \Response::json($getdataarray,200);
   
    }
    //notify staff by staff id: done by admin :
    public function staffUpdateByAdmin(Request $request,$staff_id){
        //server api key
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
        $getserverapikey=self::$serverapikey;
        //notification contents:
            $notify=array('title'=>$request->title,'body'=>$request->body);
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
    //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key='.GOOGLE_API_KEY,
            'Content-Type: application/json',];     
    $getstaffdata=StaffInfo::where('staff_id',$staff_id)->first();
     //saving in notification table as well::
            $store=new Mynotification();
             $store->type='general';
            $store->notifiable_id=$getstaffdata->staff_id;
            $store->notifiable_type=StaffInfo::class;
           $store->title=$request['title'];
            $store->body=$request['body'];
            $store->added_by=$request['added_by'];
            $store->company_id=$getstaffdata->company_id;
            $store->save();
  //get staff token
     $staffFcmToken=$getstaffdata->device_token;
     if(is_null($staffFcmToken)){
         return $this->sendError('Deveic token not configured yet!!');
     }
     //initiating curl:
          $ch = curl_init();  
       //sending request field:for whom(reciever) and for what(data):::for owner
        $fields= json_encode(array( 'to'=> $staffFcmToken,
        'notification'=>$notify));  
          
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
          return $this->sendResponse($result,'NOtification successfully sent!!');
          
         
      
    }
    public function staffUpdateToAdmin(Request $request,$staff_id){
         //server api key
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
        //$getserverapikey=self::$serverapikey;
        //notification contents:
            $notify=array('title'=>$request->title,'body'=>$request->body);
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
          //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',];     
         //   $get=StaffInfo::where('staff_info.staff_id',$staff_id)->get();
    //  $getadmindata=busOwner::where('bus_owner.owner_id',$id)->join('admin_info', 'bus_owner.company_id', '=', 'admin_info.company_id')->select('bus_owner.owner_phone','admin_info.*')->first();
    
          $getadmindata=StaffInfo::where('staff_info.staff_id',$staff_id)->join('admin_info', 'staff_info.company_id', '=', 'admin_info.company_id')->select('staff_info.staff_name','admin_info.*')->first();
    //get staff token
    $staffFcmToken=$getadmindata->device_token;
     
    //sending request field:for whom(reciever) and for what(data):::for owner
        $fields= json_encode(array( 'to'=> $staffFcmToken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);
     // FCM response
   //       return $this->sendResponse($result,'NOtification successfully sent!!');       
       
  
//saving in notification table as well::
            $store=new Mynotification();
             $store->type=$request['type'];
            $store->notifiable_id=$getadmindata->admin_id;
            $store->notifiable_type=LoginModel::class;
            $store->title=$request['title'];
            $store->body=$request['admin_body'];
            $store->added_by=$request['added_by'];
             $store->created_at=Carbon::now();
            $store->company_id=$getadmindata->company_id;
            $store->save();
           return $this->sendResponse($store,'NOtification successfully sent!!');           
        

    }
    
    //-----------------------------------Transaction Notification--------------------------------
    
    
    //array chunk for spliting array that prevents it from getting bigger than 1000 list item ;;;;    array_chunk($userList,900);
    //  Example:to use calender
//  $cal = new Nepali_Calendar();
//  print_r ($cal->eng_to_nep(2008,11,23));
//  print_r($cal->nep_to_eng(2065,8,8));
    
}