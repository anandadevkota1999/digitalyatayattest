<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Admin\Sms;
use App\Models\Admin\Route;
use App\Models\Admin\VehicleRoute;
use App\Models\Admin\DriverBehaviourDescription as DriverBehaviour;
use App\Models\Users\busOwner;
use App\Models\Users\DummyBusOwner;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\DummyBusInfo;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Admin\adminInfo as admininfoResource;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class DriverBehaviourController extends BaseController
{
 function setDriverBehaviour(Request $req){
      $validator = Validator::make($req->all(), [
            'driver_id' => 'required',
             'company_id' => 'required',
             'behaviour_description'=>'required',
             'added_by'=>'required',
        ] );
    if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
    if($req['behaviour_status']=='normal'){
    $model=new DriverBehaviour();
    $model->driver_id=$req['driver_id'];
    $model->behaviour_description=$req['behaviour_description'];
    $model->added_by=$req['added_by'];
    $model->company_id=$req['company_id'];
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->behaviour_status=$req['behaviour_status'];
    $model->behaviour_rate=$req['behaviour_rate'];
    $model->save();
    return $this->sendResponse($model,'driver behaviour set successfully!!');   
 }
 if($req['behaviour_status']=='blacklisted'){
    //rfemoving from active driver
    $blacklistfromcompany=Driver::where('driver_id',$req['driver_id'])->where('active_status','yes')->update(['active_status'=>'no']);
    $model=new DriverBehaviour();
    $model->driver_id=$req['driver_id'];
    $model->behaviour_description=$req['behaviour_description'];
    $model->added_by=$req['added_by'];
    $model->company_id=$req['company_id'];
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->behaviour_status=$req['behaviour_status'];
    $model->behaviour_rate=$req['behaviour_rate'];
    $model->save();
    return $this->sendResponse($model,'driver behaviour set successfully!!');
 }
     
 }
 function viewDriverBehaviour($companyid){
     $getdriverbehaviour=Driver::join('driver_behaviour_description','driver_info.driver_id','driver_behaviour_description.driver_id')->where('driver_info.active_status','yes')->where('driver_info.company_id',$companyid)->where('driver_behaviour_description.behaviour_status','!=','blacklisted')->orderBy('id', 'desc')->get();
     return $this->SendResponse($getdriverbehaviour,'driver with defined behaviour of the company is shown successfully!!');
 }
 function showBlackListDriver($companyid){
   $getdriverbehaviour=Driver::join('driver_behaviour_description','driver_info.driver_id','driver_behaviour_description.driver_id')->where('driver_info.active_status','no')->where('driver_info.company_id',$companyid)->where('driver_behaviour_description.behaviour_status','=','blacklisted')->orderBy('id', 'desc')->get();   
     return $this->SendResponse($getdriverbehaviour,'driver with defined behaviour of the company is shown successfully!!');
     
 }
}