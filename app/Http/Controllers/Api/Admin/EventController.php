<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Events\EventCreated;
use App\Models\EventImage;
use App\Models\Admin\LoginModel;
use App\Models\Users\busOwner;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Rules\Nepali_Calendar as NepaliCalender;

class EventController extends BaseController
{
    public function CreateEvent(Request $req){
    $validator = Validator::make($req->all(), [
             'event_topic' => 'required',
             'event_content' => 'required',
             'event_for'=>'required',
             'added_by'=>'required',
             'priority'=>'required',
             'company_id'=>'required'
        ],[
            'event_topic.required' => 'event topic is required!!',
            'event_content.required' => 'event content is required!!',
            'event_for.required'=>'targeted audience is required!!',
            'added_by.required'=>'event adder is required!!',
            'priority.required'=>'event priority is required!!',
            'company_id.required'=>'company id is required!!'
            ]);
    if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
    try{
     if($req['event_for']=='owner'){
      if($req->hasFile('image')){
        $reqimage=$req->file('image');
        $model=new Event();
        $model->event_topic=$req['event_topic'];
        $model->event_content=$req['event_content'];
        $model->priority=$req['priority'];
        $model->event_for='owner';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();

        foreach ($reqimage as $image) {
            $filename=$image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $getmewsid=Event::orderBy('id','desc')->first();
            $id=$getmewsid->id;
            $models=new EventImage();
            $models->event_id=$id;
            $models->image=$has;
            $models->added_by=$req['added_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
         }
           $events=$model;
            event(new EventCreated($events));
          return $this->SendResponse($events,'Event sent to Vehicle Owners successfully!!');  
             } 
        else{
        $model=new Event();
        $model->event_topic=$req['event_topic'];
        $model->event_content=$req['event_content'];
        $model->priority=$req['priority'];
        $model->event_for='owner';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();
        $events=$model;
      //dispatch event
      event(new EventCreated($events));
      return $this->SendResponse($events,'Event sent to Vehicle Owners successfully!!');  
        }     
       }
     if($req['event_for']=='driver'){
      if($req->hasFile('image')){
        $reqimage=$req->file('image');
         //in the news section
        $model=new Event();
        $model->event_topic=$req['event_topic'];
        $model->event_content=$req['event_content'];
        $model->priority=$req['priority'];
        $model->event_for='driver';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();

        foreach ($reqimage as $image) {
            $filename=$image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $getmewsid=Event::orderBy('id','desc')->first();
            $id=$getmewsid->id;
            $models=new EventImage();
            $models->event_id=$id;
            $models->image=$has;
            $models->added_by=$req['added_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
         }
           $events=$model;
            event(new EventCreated($events));
          return $this->SendResponse($events,'Event sent to Vehicle Owners successfully!!');  
             } 
        else{
        $model=new Event();
        $model->event_topic=$req['event_topic'];
        $model->event_content=$req['event_content'];
        $model->priority=$req['priority'];
        $model->event_for='driver';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();
        $events=$model;
      //dispatch event
      event(new EventCreated($events));
      return $this->SendResponse($events,'Event sent to Vehicle Owners successfully!!');  
        }     
       }  
     if($req['event_for']=='staff'){
      if($req->hasFile('image')){
        $reqimage=$req->file('image');
         //in the news section
        $model=new Event();
        $model->event_topic=$req['event_topic'];
        $model->event_content=$req['event_content'];
        $model->priority=$req['priority'];
        $model->event_for='staff';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();

        foreach ($reqimage as $image) {
            $filename=$image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $getmewsid=Event::orderBy('id','desc')->first();
            $id=$getmewsid->id;
            $models=new EventImage();
            $models->event_id=$id;
            $models->image=$has;
            $models->added_by=$req['added_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
         }
           $events=$model;
            event(new EventCreated($events));
          return $this->SendResponse($events,'Event sent to Vehicle Owners successfully!!');  
             } 
        else{
        $model=new Event();
        $model->event_topic=$req['event_topic'];
        $model->event_content=$req['event_content'];
        $model->priority=$req['priority'];
        $model->event_for='staff';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();
        $events=$model;
      //dispatch event
      event(new EventCreated($events));
      return $this->SendResponse($events,'Event sent to Vehicle Owners successfully!!');  
        }     
       }
     if($req['event_for']=='all'){
      if($req->hasFile('image')){
        $reqimage=$req->file('image');
         //in the news section
        $model=new Event();
        $model->event_topic=$req['event_topic'];
        $model->event_content=$req['event_content'];
        $model->priority=$req['priority'];
        $model->event_for='all';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();

        foreach ($reqimage as $image) {
            $filename=$image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $getmewsid=Event::orderBy('id','desc')->first();
            $id=$getmewsid->id;
            $models=new EventImage();
            $models->event_id=$id;
            $models->image=$has;
            $models->added_by=$req['added_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
         }
           $events=$model;
            event(new EventCreated($events));
          return $this->SendResponse($events,'Event sent to Vehicle Owners successfully!!');  
             } 
        else{
        $model=new Event();
        $model->event_topic=$req['event_topic'];
        $model->event_content=$req['event_content'];
        $model->priority=$req['priority'];
        $model->event_for='all';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();
        $events=$model;
      //dispatch event
      event(new EventCreated($events));
      return $this->SendResponse($events,'Event sent to Vehicle Owners successfully!!');  
        }     
       }  
           }
    catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }  
    }
    function getEvents($companyid){
        $getdata=Event::with('images')->where('event_info.company_id',$companyid)->orderBy('id','desc')->get();
        return $this->SendResponse($getdata,'company event history shown successfully!!');
        
    }
    function showEventImage($eventid){
        $getimage=EventImage::where('event_id',$eventid)->get();
        return $this->SendResponse($getimage,'company event images shown successfully!!');
        
    }
    function deleteImageFromStorage($imageid){
        $all=companyList::where('company_id','100')->first();
        $getcompanyname=$all->company_name;
        $getdata=EventImage::where('id',$imageid)->first();
        if(is_null($getdata)){
             return $this->SendError('There is no image exists!'); 
        }
        $filename=$getdata->image;
        if(Storage::disk('local')->exists($getcompanyname.'/'.$filename)) {
          $path = Storage::disk('local')->path($getcompanyname.'/'.$filename);  
          Storage::disk('local')->delete($getcompanyname.'/'.$filename);
         $getdata->delete();
         return $this->SendResponse('done!', 'file deleted successfully!!');
        }
        else{
            return $this->SendError('image not found!');
        }
    }
    function showEventDetail($eventid){
    $getdata=Event::with('images')->where('id',$eventid)->first();
         return $this->SendResponse($getdata, 'Event detail shown successfully!!');
    }
    function updateEventWithImage(Request $req,$eventid){
     $validator = Validator::make($req->all(), [
             'event_topic' => 'required',
             'event_content' => 'required',
             'event_for'=>'required',
             'updated_by'=>'required',
             'priority'=>'required',
             'company_id'=>'required'],[
            'event_topic.required' => 'event topic is required!!',
            'event_content.required' => 'event content is required!!',
            'event_for.required'=>'targeted audience is required!!',
            'updated_by.required'=>'event updater is required!!',
            'priority.required'=>'event priority is required!!',
            'company_id.required'=>'company id is required!!'
            ]);
     if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
     if($req['event_for']=='owner'){  
       if($req->hasFile('image')){
         $reqimage=$req->file('image');
         $update=Event::where('id',$eventid)->update([
             'event_topic'=>$req['event_topic'],
             'event_content'=>$req['event_content'],
             'priority'=>$req['priority'],
             'event_for'=>'owner',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
             foreach($reqimage as $image){
            $filename=$image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $models=new EventImage();
            $models->event_id=$eventid;
            $models->image=$has;
            $models->added_by=$req['updated_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
             }
     return $this->SendResponse('done', 'updated!!');
           
       }
      else{
         $update=Event::where('id',$eventid)->update([
             'event_topic'=>$req['event_topic'],
             'event_content'=>$req['event_content'],
             'priority'=>$req['priority'],
             'event_for'=>'owner',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
        return $this->SendResponse('done', 'updated!!');     
      }
         
     }
     if($req['event_for']=='driver'){  
       if($req->hasFile('image')){
         $reqimage=$req->file('image');
         $update=Event::where('id',$eventid)->update([
             'event_topic'=>$req['event_topic'],
             'event_content'=>$req['event_content'],
             'priority'=>$req['priority'],
             'event_for'=>'driver',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
             foreach($reqimage as $image){
            $filename=$image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $models=new EventImage();
            $models->event_id=$eventid;
            $models->image=$has;
            $models->added_by=$req['updated_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
             }
     return $this->SendResponse('done', 'updated!!');
           
       }
      else{
         $update=Event::where('id',$eventid)->update([
             'event_topic'=>$req['event_topic'],
             'event_content'=>$req['event_content'],
             'priority'=>$req['priority'],
             'event_for'=>'driver',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
        return $this->SendResponse('done', 'updated!!');     
      }
         
     }
     if($req['event_for']=='staff'){  
       if($req->hasFile('image')){
         $reqimage=$req->file('image');
         $update=Event::where('id',$eventid)->update([
             'event_topic'=>$req['event_topic'],
             'event_content'=>$req['event_content'],
             'priority'=>$req['priority'],
             'event_for'=>'staff',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
             foreach($reqimage as $image){
            $filename=$image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $models=new EventImage();
            $models->event_id=$eventid;
            $models->image=$has;
            $models->added_by=$req['updated_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
             }
     return $this->SendResponse('done', 'updated!!');
           
       }
      else{
         $update=Event::where('id',$eventid)->update([
             'event_topic'=>$req['event_topic'],
             'event_content'=>$req['event_content'],
             'priority'=>$req['priority'],
             'event_for'=>'staff',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
        return $this->SendResponse('done', 'updated!!');     
      }
         
     }
     if($req['event_for']=='all'){  
       if($req->hasFile('image')){
         $reqimage=$req->file('image');
         $update=Event::where('id',$eventid)->update([
             'event_topic'=>$req['event_topic'],
             'event_content'=>$req['event_content'],
             'priority'=>$req['priority'],
             'event_for'=>'all',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
             foreach($reqimage as $image){
            $filename=$image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $models=new EventImage();
            $models->event_id=$eventid;
            $models->image=$has;
            $models->added_by=$req['updated_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
             }
     return $this->SendResponse('done', 'updated!!');
           
       }
      else{
         $update=Event::where('id',$eventid)->update([
             'event_topic'=>$req['event_topic'],
             'event_content'=>$req['event_content'],
             'priority'=>$req['priority'],
             'event_for'=>'all',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
        return $this->SendResponse('done', 'updated!!');     
      }
         
     }
    }
    function deleteEventAndImage($eventid){
     $getdata=Event::with('images')->where('id',$eventid)->first();
     if(is_null($getdata)){
         return $this->SendError('Event does not exists!!');
     }
     else{
     $filename=$getdata['images'];
     $all=companyList::where('company_id','100')->first();
     $getcompanyname=$all->company_name;
     foreach($filename as $image){
       $name=$image->image;
       $file=$getcompanyname.'/'.$name;
       if(Storage::disk('local')->exists($file)) {
          $path = Storage::disk('local')->path($file);  
          Storage::disk('local')->delete($file);
       $image->delete();
       }
     }
      $getdata->delete();
      return $this->SendResponse('done!', 'Event detail shown successfully!!');
      
    }
        
    }
    
}