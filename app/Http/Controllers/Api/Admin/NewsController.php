<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\News;
use App\Events\NewsCreated;
use App\Models\NewsImage;
use App\Models\Admin\LoginModel;
use App\Models\Users\busOwner;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Rules\Nepali_Calendar as NepaliCalender;

class NewsController extends BaseController
{
    public function CreateNews(Request $req){
  
    $validator = Validator::make($req->all(), [
             'topic' => 'required',
             'text_content' => 'required',
             'news_for'=>'required',
             'added_by'=>'required',
             'priority'=>'required',
             'company_id'=>'required'
        ],[
            'topic.required' => 'news topic is required!!',
            'text_content.required' => 'news content is required!!',
            'news_for.required'=>'targeted audience is required!!',
            'added_by.required'=>'news adder is required!!',
            'priority.required'=>'news priority is required!!',
            'company_id.required'=>'company id is required!!'
            ]);
    if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
    try{
     if($req['news_for']=='owner'){
      if($req->hasFile('image')){
        $reqimage=$req->file('image');
         //in the news section
        $model=new News();
        $model->topic=$req['topic'];
        $model->text_content=$req['text_content'];
        $model->priority=$req['priority'];
        $model->news_for='owner';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();

        foreach ($reqimage as $image) {
            $filename=$image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $getmewsid=News::orderBy('id','desc')->first();
            $id=$getmewsid->id;
            $models=new NewsImage();
            $models->news_id=$id;
            $models->image=$has;
            $models->added_by=$req['added_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
         }
           $news=$model;
            event(new NewsCreated($news));
        return $this->SendResponse($news,'News sent to Drivers successfully!!');  
             } 
        else{
        $model=new News();
        $model->topic=$req['topic'];
        $model->text_content=$req['text_content'];
        $model->priority=$req['priority'];
        $model->news_for='owner';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();
        $news=$model;
      //dispatch event
      event(new NewsCreated($news));
      return $this->SendResponse($news,'News sent to Vehicle Owners successfully!!');  
        }     
       }
     if($req['news_for']=='driver'){
       if($req->hasFile('image')){
       $reqimage=$req->file('image');
         //in the news section
        $model=new News();
        $model->topic=$req['topic'];
        $model->text_content=$req['text_content'];
        $model->priority=$req['priority'];
        $model->news_for='driver';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();

        foreach ($reqimage as $image) {
            $filename=$image->getClientOriginalName();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $getmewsid=News::orderBy('id','desc')->first();
            $id=$getmewsid->id;
            $models=new NewsImage();
            $models->news_id=$id;
            $models->image=$has;
            $models->added_by=$req['added_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
         }
           $news=$model;
            event(new NewsCreated($news));
          return $this->SendResponse($news,'News sent to Drivers successfully!!');  
       } 
        else{
        $model=new News();
        $model->topic=$req['topic'];
        $model->text_content=$req['text_content'];
        $model->priority=$req['priority'];
        $model->news_for='driver';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();
        $news=$model;
      //dispatch event
      event(new NewsCreated($news));
      return $this->SendResponse($news,'News sent to Drivers successfully!!'); 
        }     
       }
     if($req['news_for']=='staff'){
       if($req->hasFile('image')){
      $reqimage=$req->file('image');
         //in the news section
        $model=new News();
        $model->topic=$req['topic'];
        $model->text_content=$req['text_content'];
        $model->priority=$req['priority'];
        $model->news_for='staff';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();

        foreach ($reqimage as $image) {
            $filename=$image->getClientOriginalName();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $getmewsid=News::orderBy('id','desc')->first();
            $id=$getmewsid->id;
            $models=new NewsImage();
            $models->news_id=$id;
            $models->image=$has;
            $models->added_by=$req['added_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
         }
           $news=$model;
            event(new NewsCreated($news));
          return $this->SendResponse($news,'News sent to Staffs successfully!!');  

       } 
        else{
        $model=new News();
        $model->topic=$req['topic'];
        $model->text_content=$req['text_content'];
        $model->priority=$req['priority'];
        $model->news_for='staff';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();
        $news=$model;
      //dispatch event
      event(new NewsCreated($news));
      return $this->SendResponse($news,'News sent to Staffs successfully!!'); 
        }     
       } 
     if($req['news_for']=='all'){
      if($req->hasFile('image')){
         $reqimage=$req->file('image');
         //in the news section
        $model=new News();
        $model->topic=$req['topic'];
        $model->text_content=$req['text_content'];
        $model->priority=$req['priority'];
        $model->news_for='all';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();

        foreach ($reqimage as $image) {
            $filename=$image->getClientOriginalName();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $getmewsid=News::orderBy('id','desc')->first();
            $id=$getmewsid->id;
            $models=new NewsImage();
            $models->news_id=$id;
            $models->image=$has;
            $models->added_by=$req['added_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
         }
           $news=$model;
            event(new NewsCreated($news));
          return $this->SendResponse($news,'News sent to all successfully!!');  
       } 
        else{
        $model=new News();
        $model->topic=$req['topic'];
        $model->text_content=$req['text_content'];
        $model->priority=$req['priority'];
        $model->news_for='all';
        $model->added_by=$req['added_by'];
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->company_id=$req['company_id'];
        $model->save();
        $news=$model;
      //dispatch event
      event(new NewsCreated($news));
      return $this->SendResponse($news,'News sent to all successfully!!');  
        }     
       }  
    }
    catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }  
    
    }
    function getNews($companyid){
        $getdata=News::with('images')->where('news_info.company_id',$companyid)->orderBy('id','desc')->get();
       // leftJoin('news_image','news_info.id','news_image.news_id')->select('news_info.id as news_id','news_info.topic','news_info.text_content','news_info.created_at','news_image.image as news_image')->get();
        return $this->SendResponse($getdata,'company news history shown successfully!!');
        
    }
    function shownewsImage($newsid){
        $getimage=NewsImage::where('news_id',$newsid)->get();
        return $this->SendResponse($getimage,'company news history shown successfully!!');
        
    }
    function deleteImageFromStorage($imageid){
        $all=companyList::where('company_id','100')->first();
        $getcompanyname=$all->company_name;
        $getdata=NewsImage::where('id',$imageid)->first();
        if(is_null($getdata)){
             return $this->SendError('There is no image exists!'); 
        }
        $filename=$getdata->image;
        if(Storage::disk('local')->exists($getcompanyname.'/'.$filename)) {
          $path = Storage::disk('local')->path($getcompanyname.'/'.$filename);  
          Storage::disk('local')->delete($getcompanyname.'/'.$filename);
         $getdata->delete();
         return $this->SendResponse('done!', 'file deleted successfully!!');
        }
        else{
            return $this->SendError('image not found!');
        }
    }
    function showNewsDetail($newsid){
    $getdata=News::with('images')->where('id',$newsid)->first();
         return $this->SendResponse($getdata, 'News detail shown successfully!!');
    }
    function updateNewsWithImage(Request $req,$newsid){
     $validator = Validator::make($req->all(), [
             'topic' => 'required',
             'text_content' => 'required',
             'news_for'=>'required',
             'updated_by'=>'required',
             'priority'=>'required',
             'company_id'=>'required'],[
            'topic.required' => 'news topic is required!!',
            'text_content.required' => 'news content is required!!',
            'news_for.required'=>'targeted audience is required!!',
            'updated_by.required'=>'news updater is required!!',
            'priority.required'=>'news priority is required!!',
            'company_id.required'=>'company id is required!!'
            ]);
     if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
     if($req['news_for']=='owner'){  
       if($req->hasFile('image')){
         $reqimage=$req->file('image');
         $update=News::where('id',$newsid)->update([
             'topic'=>$req['topic'],
             'text_content'=>$req['text_content'],
             'priority'=>$req['priority'],
             'news_for'=>'owner',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
             foreach($reqimage as $image){
            $filename=$image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $models=new NewsImage();
            $models->news_id=$newsid;
            $models->image=$has;
            $models->added_by=$req['updated_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
             }
     return $this->SendResponse('done', 'updated!!');
           
       }
      else{
         $update=News::where('id',$newsid)->update([
             'topic'=>$req['topic'],
             'text_content'=>$req['text_content'],
             'priority'=>$req['priority'],
             'news_for'=>'owner',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
        return $this->SendResponse('done', 'updated!!');     
      }
         
     }
     if($req['news_for']=='driver'){  
       if($req->hasFile('image')){
         $reqimage=$req->file('image');
         $update=News::where('id',$newsid)->update([
             'topic'=>$req['topic'],
             'text_content'=>$req['text_content'],
             'priority'=>$req['priority'],
             'news_for'=>'driver',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
             foreach($reqimage as $image){
            $filename=$image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $models=new NewsImage();
            $models->news_id=$newsid;
            $models->image=$has;
            $models->added_by=$req['updated_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
             }
     return $this->SendResponse('done', 'updated!!');
           
       }
       else{
           $update=News::where('id',$newsid)->update([
             'topic'=>$req['topic'],
             'text_content'=>$req['text_content'],
             'priority'=>$req['priority'],
             'news_for'=>'driver',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
         return $this->SendResponse('done', 'updated!!');     
       }
      }
     if($req['news_for']=='staff'){  
       if($req->hasFile('image')){
         $reqimage=$req->file('image');
         $update=News::where('id',$newsid)->update([
             'topic'=>$req['topic'],
             'text_content'=>$req['text_content'],
             'priority'=>$req['priority'],
             'news_for'=>'staff',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
             foreach($reqimage as $image){
            $filename=$image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $models=new NewsImage();
            $models->news_id=$newsid;
            $models->image=$has;
            $models->added_by=$req['updated_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
             }
     return $this->SendResponse('done', 'updated!!');
           
       }
       else{
            $update=News::where('id',$newsid)->update([
             'topic'=>$req['topic'],
             'text_content'=>$req['text_content'],
             'priority'=>$req['priority'],
             'news_for'=>'staff',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
        return $this->SendResponse('done', 'updated!!');     
       }
      }
     if($req['news_for']=='all'){  
       if($req->hasFile('image')){
         $reqimage=$req->file('image');
         $update=News::where('id',$newsid)->update([
             'topic'=>$req['topic'],
             'text_content'=>$req['text_content'],
             'priority'=>$req['priority'],
             'news_for'=>'all',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
             foreach($reqimage as $image){
            $filename=$image->getClientOriginalName();
            $extension = $image->getClientOriginalExtension();
            $has=uniqid().'.'.File::extension($filename);
            $all=companyList::where('company_id',$req['company_id'])->first();
            $getcompany=$all->company_name;
            $image->storeAs($getcompany,$has, 'local');
            $models=new NewsImage();
            $models->news_id=$newsid;
            $models->image=$has;
            $models->added_by=$req['updated_by'];
            $models->created_at=Carbon::now();
            $models->updated_at=Carbon::now();
            $models->company_id=$req['company_id'];
            $models->save();
             }
     return $this->SendResponse('done', 'updated!!');
           
       }
       else{
           $update=News::where('id',$newsid)->update([
             'topic'=>$req['topic'],
             'text_content'=>$req['text_content'],
             'priority'=>$req['priority'],
             'news_for'=>'all',
             'updated_by'=>$req['updated_by'],
             'updated_at'=>Carbon::now(),
             'company_id'=>$req['company_id']
             ]);
        return $this->SendResponse('done', 'updated!!');     
       }
      } 
    }
    function deleteNewsAndImage($newsid){
     $getdata=News::with('images')->where('id',$newsid)->first();
     if(is_null($getdata)){
         return $this->SendError('News does not exists!!');
     }
     else{
     $filename=$getdata['images'];
     $all=companyList::where('company_id','100')->first();
     $getcompanyname=$all->company_name;
     foreach($filename as $image){
       $name=$image->image;
       $file=$getcompanyname.'/'.$name;
       if(Storage::disk('local')->exists($file)) {
          $path = Storage::disk('local')->path($file);  
          Storage::disk('local')->delete($file);
       $image->delete();
       }
     }
      $getdata->delete();
      return $this->SendResponse('done!', 'News detail shown successfully!!');
      
    }
        
    }
    
}