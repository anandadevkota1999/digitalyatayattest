<?php

namespace App\Http\Controllers\Api\Admin\Transactions;

use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Admin\Transactions\GeneralShulka;
use App\Models\Admin\Transactions\AccountPriceDeclaration;
use App\Models\Admin\Transactions\VehicleFixedAccount;
use App\Models\Admin\Transactions\DurghatanaFund;
use App\Models\Admin\Transactions\DurghatanaKharcha;
use App\Models\Admin\Transactions\DurghatanaKharchaDummy;
use App\Models\Admin\Transactions\JournalEntryOfGeneralShulka;
use App\Models\Admin\Transactions\JournalEntryOfDurghatanaFund;
use App\Models\Admin\Transactions\FiscalYear;
use App\Models\Users\busOwner;
use App\Models\Users\DummyBusOwner;
use App\Models\Notification as Mynotification;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\DummyBusInfo;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Admin\adminInfo as admininfoResource;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class DurghatanaFundController extends BaseController
{
 //prepare bill for durghatana fund by incrementing
 public function makeBillnoDurghatana($company){
     $initial=DurghatanaFund::where('company_id',$company)->orderBy('bill_no', 'DESC')->first('bill_no');
     if(empty($initial)){
        $setbillno=['bill_no'=>1];
        return $this->sendResponse($setbillno,'this is initial bill no.');
     }
    /*  if($initial==null){
          $updatetoshulkatable=GeneralShulka::where( 'company_id' , $company)->update( array( 'bill_no' =>  1));
          
     } */
     else{
     $newbillno=$initial->bill_no+1;
     $setbillagain=['bill_no'=>$newbillno];
    return $this->sendResponse($setbillagain,'data with new bill no.'); 
    
     }
 }
 //store durghatana fund as well as prepare journal entry instantly
 public function storeDurghatanaFundAndJournal(Request $req){
    // Start transaction!
    DB::beginTransaction();
     $input=$req->all();
      $getinarray=[];
     $fiscalyear=FiscalYear::OrderBy('id','DESC')->first();
        $fenddate=$fiscalyear->fiscal_year_end_date;
        $endadateformat=Carbon::createFromFormat('Y-m-d H:i:s', $fenddate);
       // $fenddatecorrectformat=$fenddate->toDateTimeString();
        $getpaymentdate=Carbon::now();
        $paymentformat=Carbon::createFromFormat('Y-m-d H:i:s', $getpaymentdate);
        $paymentformatendofday=Carbon::parse($paymentformat)->endOfDay();
       $getfyear=$fiscalyear->fiscal_year;
      //check for fiscal year
     if($endadateformat->gt($paymentformatendofday)){
        $sumdurghatanajokhimshulka=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $summembershipshulka=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumnibedanshulka=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumanudaanshulka=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumsattadartashulka=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumdurghatananirixedshulka=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumpeskifarchyotshulka=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumanyaamdani=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumreserveshulka=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumjagedakoshshulka=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumayamemershipshulka=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        
            try{
     $validator =Validator::make($input,[
  	       // 'bill_payment_date' => 'required',
            'bus_no' => 'required',
            'member_name'=>'required',
            'durghatana_jokhim_shulka'=>'required',
            'membership_shulka'=>'required',
            'nibedan_shulka'=>'required',
            'anudaan_shulka'=>'required',
            'sattadarta_shulka'=>'required',
            'durghatana_nirichhyad_and_nyunikarad_shulka'=>'required',
            'peski_farchyut'=>'required',
            'anya_aamdani'=>'required',
            'reserve_shulka'=>'required',
            'jageda_kosh'=>'required',
            'naya_membership_shulka'=>'required',
            'bill_no'=>'required|unique:durghatana_fund'
  ]);
  if($validator->fails()){
            DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);
         }
        //checking if all input values are zero
        if($req->input('durghatana_jokhim_shulka')==0&&$req->input('membership_shulka')==0&&$req->input('nibedan_shulka')==0&&$req->input('anudaan_shulka')==0&&$req->input('sattadarta_shulka')==0&&$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')==0
        &&$req->input('peski_farchyut')==0&&$req->input('anya_aamdani')==0&&$req->input('reserve_shulka')==0&&$req->input('jageda_kosh')==0&&$req->input('naya_membership_shulka')==0)
        {
             DB::rollback();
             return $this->sendError('please pay atleast on a topic!');
        }
    else{
        $model=new DurghatanaFund();
        $model->bill_no=$req->input('bill_no');
        $model->bill_payment_date=$paymentformat;
        $model->bus_no=$req->input('bus_no');
        $model->member_name=$req->input('member_name');
      //  number_format((float)$foo, 2, '.', '');
     if($sumdurghatanajokhimshulka<$req->input('durghatana_jokhim_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumdurghatanajokhimshulka)&&$sumdurghatanajokhimshulka>=$req->input('durghatana_jokhim_shulka')){
        $model->durghatana_jokhim_shulka=number_format((float)$req->input('durghatana_jokhim_shulka'), 2, '.', '');
               }
     if($summembershipshulka<$req->input('membership_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($summembershipshulka)&&$summembershipshulka>=$req->input('membership_shulka')){
        $model->membership_shulka=number_format((float)$req->input('membership_shulka'), 2, '.', '');
               }
     if($sumnibedanshulka<$req->input('nibedan_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumnibedanshulka)&&$sumnibedanshulka>=$req->input('nibedan_shulka')){
        $model->nibedan_shulka=number_format((float)$req->input('nibedan_shulka'), 2, '.', '');
               }           
     if($sumanudaanshulka<$req->input('anudaan_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumanudaanshulka)&&$sumanudaanshulka>=$req->input('anudaan_shulka')){
        $model->anudaan_shulka=number_format((float)$req->input('anudaan_shulka'), 2, '.', '');
               }            
     if($sumsattadartashulka<$req->input('sattadarta_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumsattadartashulka)&&$sumsattadartashulka>=$req->input('sattadarta_shulka')){
        $model->sattadarta_shulka=number_format((float)$req->input('sattadarta_shulka'), 2, '.', '');
               }            
     if($sumdurghatananirixedshulka<$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumdurghatananirixedshulka)&&$sumdurghatananirixedshulka>=$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
        $model->durghatana_nirichhyad_and_nyunikarad_shulka=number_format((float)$req->input('durghatana_nirichhyad_and_nyunikarad_shulka'), 2, '.', '');
               }        
     if($sumpeskifarchyotshulka<$req->input('peski_farchyut')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumpeskifarchyotshulka)&&$sumpeskifarchyotshulka>=$req->input('peski_farchyut')){
        $model->peski_farchyut=number_format((float)$req->input('peski_farchyut'), 2, '.', '');               }        
     if($sumanyaamdani<$req->input('anya_aamdani')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumanyaamdani)&&$sumanyaamdani>=$req->input('anya_aamdani')){
        $model->anya_aamdani=number_format((float)$req->input('anya_aamdani'), 2, '.', '');
        }        
     if($sumreserveshulka<$req->input('reserve_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumreserveshulka)&&$sumreserveshulka>=$req->input('reserve_shulka')){
        $model->reserve_shulka=number_format((float)$req->input('reserve_shulka'), 2, '.', '');
        }                    
     if($sumjagedakoshshulka<$req->input('jageda_kosh')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumjagedakoshshulka)&&$sumjagedakoshshulka>=$req->input('jageda_kosh')){
        $model->jageda_kosh=number_format((float)$req->input('jageda_kosh'), 2, '.', '');
        }                    
     if($sumayamemershipshulka<$req->input('naya_membership_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumayamemershipshulka)&&$sumayamemershipshulka>=$req->input('naya_membership_shulka')){
        $model->naya_membership_shulka=number_format((float)$req->input('naya_membership_shulka'), 2, '.', '');
        }                        
        $model->durghatana_jokhim_shulka_remarks=$req->input('durghatana_jokhim_shulka_remarks');
        $model->membership_shulka_remarks=$req->input('membership_shulka_remarks');
        $model->nibedan_shulka_remarks=$req->input('nibedan_shulka_remarks');
        $model->anudaan_shulka_remarks=$req->input('anudaan_shulka_remarks');
        $model->sattadarta_shulka_remarks=$req->input('sattadarta_shulka_remarks');
        $model->durghatana_nirichhyad_and_nyunikarad_shulka_remarks=$req->input('durghatana_nirichhyad_and_nyunikarad_shulka_remarks');
        $model->peski_farchyut_remarks=$req->input('peski_farchyut_remarks');
        $model->anya_aamdani_remarks=$req->input('anya_aamdani_remarks');
        $model->reserve_shulka_remarks=$req->input('reserve_shulka_remarks');
        $model->jageda_kosh_remarks=$req->input('jageda_kosh_remarks');
        $model->naya_membership_shulka_remarks=$req->input('naya_membership_shulka_remarks');
        $model->added_by=$req->input('added_by');
        $model->company_id=$req->input('company_id');
        $model->updated_by=$req->input('updated_by');
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->total_amount=$req->input('total_amount');
        $model->payment_type=$req->input('payment_type');
        $model->fiscal_year=$getfyear;
        $model->save();
         }
    //------------------Sending notification to respective owner as well----------------------
    // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
        //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
        //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',];
     
        //notification contents:
          $notify=array('title'=>'Durghatana Fund Payment','body'=>'Your Durghatana Fund of rupees '.$model->total_amount.' has been paid.');
    $getownerdata=DurghatanaFund::where('durghatana_fund.bus_no',$req['bus_no'])->join('bus_info','durghatana_fund.bus_no','bus_info.bus_no')->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->select('bus_info.bus_no','bus_owner.owner_id','bus_owner.owner_name','bus_owner.owner_phone','bus_owner.device_token','bus_owner.company_id')->first();
 //   return $this->sendResponse($getownerdata,'general shulka stored successfully!!');    
  $ownerFcmToken=$getownerdata->device_token;
  // return $this->sendResponse($ownerFcmToken,'general shulka stored successfully!!'); 
    //saving in notification table as well::
            $store=new Mynotification();
            $store->type='Durghatana Fund Payment';
            $store->notifiable_id=$getownerdata->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Durghatana Fund Payment by admin';
         //   $store->body='Vehicle Owner '.'member name'.' '.'has paid total of '.'total amount'.' rupees.';
            $store->body='Your Durghatana Fund of rupees '.$model->total_amount.' has been paid by company admin.';
            $store->added_by=$req['added_by'];
            $store->created_at=Carbon::now();
            $store->company_id=$getownerdata->company_id;
            $store->save();
    //sending request field:for whom(reciever) and for what(data):::for owner
        $fields= json_encode(array( 'to'=> $ownerFcmToken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
            DB::rollback();
        }        

        // Close connection
        curl_close($ch);

        // FCM response
        //  return $this->sendResponse($result,'NOtification successfully sent!!');
       
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
         
       
//again store to journal entry of durghatana fund
try{
     $validator =Validator::make($input,[
  	       // 'bill_payment_date' => 'required',
          //  'date' => 'required',
            'bill_no'=>'required',
          //  'debit_particular'=>'required',
        //    'credit_particular'=>'required',
         //  'debit_amount'=>'required',
          //  'credit_amount'=>'required',
          //  'total_amount'=>'required',
            'added_by'=>'required',
            'company_id'=>'required',
  ]);
  if($validator->fails()){
            DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
    try{
    if($req->input('durghatana_jokhim_shulka')>0&&!is_null($sumdurghatanajokhimshulka)&&$sumdurghatanajokhimshulka<$req->input('durghatana_jokhim_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('durghatana_jokhim_shulka')>0&&!is_null($sumdurghatanajokhimshulka)&&$sumdurghatanajokhimshulka>=$req->input('durghatana_jokhim_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumdurghatanajokhimshulka-$req->input('durghatana_jokhim_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('bus_no',$req['bus_no'])->where('payment_type','durghatana_shulka')->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumdurghatanajokhimshulka&&$sumdurghatanajokhimshulka==$req->input('durghatana_jokhim_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumdurghatanajokhimshulka&&$sumdurghatanajokhimshulka>$req->input('durghatana_jokhim_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('durghatana_jokhim_shulka');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$req->input('durghatana_jokhim_shulka');
                $model->credit_amount=$req->input('durghatana_jokhim_shulka');
                $model->total_amount=$req->input('durghatana_jokhim_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding durghatana jokhim shulka a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='durghatana_jokhim_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumdurghatanajokhimshulka==$req->input('durghatana_jokhim_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumdurghatanajokhimshulka>$req->input('durghatana_jokhim_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('durghatana_jokhim_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding durghatana jokhim shulka a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding durghatana jokhim shulka a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='durghatana_jokhim_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{
    if($req->input('membership_shulka')>0&&!is_null($summembershipshulka)&&$summembershipshulka<$req->input('membership_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('membership_shulka')>0&&!is_null($summembershipshulka)&&$summembershipshulka>=$req->input('membership_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$summembershipshulka-$req->input('membership_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$summembershipshulka&&$summembershipshulka==$req->input('membership_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$summembershipshulka&&$summembershipshulka>$req->input('membership_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('membership_shulka');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$req->input('membership_shulka');
                $model->credit_amount=$req->input('membership_shulka');
                $model->total_amount=$req->input('membership_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding membership shulka a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='membership_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($summembershipshulka==$req->input('membership_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($summembershipshulka>$req->input('membership_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('membership_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding membership shulka a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding membership shulka a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='membership_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
      }
       catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{  
    if($req->input('nibedan_shulka')>0&&!is_null($sumnibedanshulka)&&$sumnibedanshulka<$req->input('nibedan_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('nibedan_shulka')>0&&!is_null($sumnibedanshulka)&&$sumnibedanshulka>=$req->input('nibedan_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumnibedanshulka-$req->input('nibedan_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumnibedanshulka&&$sumnibedanshulka==$req->input('nibedan_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumnibedanshulka&&$sumnibedanshulka>$req->input('nibedan_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('nibedan_shulka');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$req->input('nibedan_shulka');
                $model->credit_amount=$req->input('nibedan_shulka');
                $model->total_amount=$req->input('nibedan_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding nibedan shulka a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='nibedan_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumnibedanshulka==$req->input('nibedan_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumnibedanshulka>$req->input('nibedan_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('nibedan_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding nibedan shulka a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding nibedan shulka a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='nibedan_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
      }
        catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{  
    if($req->input('anudaan_shulka')>0&&!is_null($sumanudaanshulka)&&$sumanudaanshulka<$req->input('anudaan_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('anudaan_shulka')>0&&!is_null($sumanudaanshulka)&&$sumanudaanshulka>=$req->input('anudaan_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumanudaanshulka-$req->input('anudaan_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumanudaanshulka&&$sumanudaanshulka==$req->input('anudaan_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumanudaanshulka&&$sumanudaanshulka>$req->input('anudaan_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('anudaan_shulka');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$req->input('anudaan_shulka');
                $model->credit_amount=$req->input('anudaan_shulka');
                $model->total_amount=$req->input('anudaan_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding anudaan shulka a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='anudaan_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumanudaanshulka==$req->input('anudaan_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumanudaanshulka>$req->input('anudaan_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('anudaan_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding anudaan shulka a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding anudaan shulka a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='anudaan_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
      }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
     try{ 
    if($req->input('sattadarta_shulka')>0&&!is_null($sumsattadartashulka)&&$sumsattadartashulka<$req->input('sattadarta_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('sattadarta_shulka')>0&&!is_null($sumsattadartashulka)&&$sumsattadartashulka>=$req->input('sattadarta_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumsattadartashulka-$req->input('sattadarta_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumsattadartashulka&&$sumsattadartashulka==$req->input('sattadarta_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumsattadartashulka&&$sumsattadartashulka>$req->input('sattadarta_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('sattadarta_shulka');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$req->input('sattadarta_shulka');
                $model->credit_amount=$req->input('sattadarta_shulka');
                $model->total_amount=$req->input('sattadarta_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding satta darta shulka a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='sattadarta_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumsattadartashulka==$req->input('sattadarta_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumsattadartashulka>$req->input('sattadarta_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('sattadarta_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding satta darta shulka a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding satta darta shulka a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='sattadarta_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }     
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
   try{
    if($req->input('durghatana_nirichhyad_and_nyunikarad_shulka')>0&&!is_null($sumdurghatananirixedshulka)&&$sumdurghatananirixedshulka<$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('durghatana_nirichhyad_and_nyunikarad_shulka')>0&&!is_null($sumdurghatananirixedshulka)&&$sumdurghatananirixedshulka>=$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumdurghatananirixedshulka-$req->input('durghatana_nirichhyad_and_nyunikarad_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumdurghatananirixedshulka&&$sumdurghatananirixedshulka==$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumdurghatananirixedshulka&&$sumdurghatananirixedshulka>$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('durghatana_nirichhyad_and_nyunikarad_shulka');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$req->input('durghatana_nirichhyad_and_nyunikarad_shulka');
                $model->credit_amount=$req->input('durghatana_nirichhyad_and_nyunikarad_shulka');
                $model->total_amount=$req->input('durghatana_nirichhyad_and_nyunikarad_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding durghatana nirichhyad and nyunikarad shulka a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='durghatana_nirichhyad_and_nyunikarad_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumdurghatananirixedshulka==$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumdurghatananirixedshulka>$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('durghatana_nirichhyad_and_nyunikarad_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding durghatana nirichhyad and nyunikarad shulka a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding durghatana nirichhyad and nyunikarad shulka a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='durghatana_nirichhyad_and_nyunikarad_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
   }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('peski_farchyut')>0&&!is_null($sumpeskifarchyotshulka)&&$sumpeskifarchyotshulka<$req->input('peski_farchyut')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('peski_farchyut')>0&&!is_null($sumpeskifarchyotshulka)&&$sumpeskifarchyotshulka>=$req->input('peski_farchyut')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumpeskifarchyotshulka-$req->input('peski_farchyut');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumpeskifarchyotshulka&&$sumpeskifarchyotshulka==$req->input('peski_farchyut')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumpeskifarchyotshulka&&$sumpeskifarchyotshulka>$req->input('peski_farchyut')){
       $subtractedvalue=$getpaymentprice-$req->input('peski_farchyut');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$req->input('peski_farchyut');
                $model->credit_amount=$req->input('peski_farchyut');
                $model->total_amount=$req->input('peski_farchyut');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding peski farchyut a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='peski_farchyut';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumpeskifarchyotshulka==$req->input('peski_farchyut')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumpeskifarchyotshulka>$req->input('peski_farchyut')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('peski_farchyut');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding peski farchyut a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding peski farchyut a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='peski_farchyut';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }

      }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('anya_aamdani')>0&&!is_null($sumanyaamdani)&&$sumanyaamdani<$req->input('anya_aamdani')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('anya_aamdani')>0&&!is_null($sumanyaamdani)&&$sumanyaamdani>=$req->input('anya_aamdani')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumanyaamdani-$req->input('anya_aamdani');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumanyaamdani&&$sumanyaamdani==$req->input('anya_aamdani')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumanyaamdani&&$sumanyaamdani>$req->input('anya_aamdani')){
       $subtractedvalue=$getpaymentprice-$req->input('anya_aamdani');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$req->input('anya_aamdani');
                $model->credit_amount=$req->input('anya_aamdani');
                $model->total_amount=$req->input('anya_aamdani');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding anya aamdani a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='anya_aamdani';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumanyaamdani==$req->input('anya_aamdani')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumanyaamdani>$req->input('anya_aamdani')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('anya_aamdani');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding anya aamdani a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding anya aamdani a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='anya_aamdani';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
       catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('reserve_shulka')>0&&!is_null($sumreserveshulka)&&$sumreserveshulka<$req->input('reserve_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('reserve_shulka')>0&&!is_null($sumreserveshulka)&&$sumreserveshulka>=$req->input('reserve_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumreserveshulka-$req->input('reserve_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumreserveshulka&&$sumreserveshulka==$req->input('reserve_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumreserveshulka&&$sumreserveshulka>$req->input('reserve_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('reserve_shulka');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$req->input('reserve_shulka');
                $model->credit_amount=$req->input('reserve_shulka');
                $model->total_amount=$req->input('reserve_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding reserve shulka a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='reserve_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumreserveshulka==$req->input('reserve_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumreserveshulka>$req->input('reserve_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('reserve_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding reserve shulka a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding reserve shulka a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='reserve_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('jageda_kosh')>0&&!is_null($sumjagedakoshshulka)&&$sumjagedakoshshulka<$req->input('jageda_kosh')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('jageda_kosh')>0&&!is_null($sumjagedakoshshulka)&&$sumjagedakoshshulka>=$req->input('jageda_kosh')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumjagedakoshshulka-$req->input('jageda_kosh');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumjagedakoshshulka&&$sumjagedakoshshulka==$req->input('jageda_kosh')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumjagedakoshshulka&&$sumjagedakoshshulka>$req->input('jageda_kosh')){
       $subtractedvalue=$getpaymentprice-$req->input('jageda_kosh');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$req->input('jageda_kosh');
                $model->credit_amount=$req->input('jageda_kosh');
                $model->total_amount=$req->input('jageda_kosh');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding jageda kosh a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='jageda_kosh';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumjagedakoshshulka==$req->input('jageda_kosh')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumjagedakoshshulka>$req->input('jageda_kosh')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('jageda_kosh');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding jageda kosh a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding jageda kosh a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='jageda_kosh';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
     }