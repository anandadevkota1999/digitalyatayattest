<?php

namespace App\Http\Controllers\Api\Admin\Transactions;

use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Admin\Transactions\GeneralShulka;
use App\Models\Admin\Transactions\GeneralShulkaKharcha;
use App\Models\Admin\Transactions\GeneralKharchaDummy;
use App\Models\Admin\Transactions\JournalEntryOfGeneralShulka;
use App\Models\Admin\Transactions\FiscalYear;
use App\Models\Admin\Transactions\AccountPriceDeclaration;
use App\Models\Admin\Transactions\VehicleFixedAccount;
use App\Models\Users\busOwner;
use App\Models\Users\DummyBusOwner;
use App\Models\Notification as Mynotification;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\DummyBusInfo;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Admin\adminInfo as admininfoResource;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class GeneralShulkaController extends BaseController
{
 //prepare bill by incrementing
 public function makeBillno($company){
     $initial=GeneralShulka::where('company_id',$company)->orderBy('bill_no', 'DESC')->first('bill_no');
     if(empty($initial)){
        $setbillno=['bill_no'=>1];
        return $this->sendResponse($setbillno,'this is initial bill no.');
     }
    /*  if($initial==null){
          $updatetoshulkatable=GeneralShulka::where( 'company_id' , $company)->update( array( 'bill_no' =>  1));
          
     } */
     else{
     $newbillno=$initial->bill_no+1;
     $setbillagain=['bill_no'=>$newbillno];
    return $this->sendResponse($setbillagain,'data with new bill no.'); 
    
     }
 }
 //prepare bill for general shulka kharcha
 public function makeBillGeneralShulkaKharcha($company){
     $initial=GeneralShulkaKharcha::where('company_id',$company)->orderBy('bill_no', 'DESC')->first('bill_no');
     if(empty($initial)){
        $setbillno=['bill_no'=>1];
        return $this->sendResponse($setbillno,'this is initial bill no.');
     }
    /*  if($initial==null){
          $updatetoshulkatable=GeneralShulka::where( 'company_id' , $company)->update( array( 'bill_no' =>  1));
          
     } */
     else{
     $newbillno=$initial->bill_no+1;
     $setbillagain=['bill_no'=>$newbillno];
    return $this->sendResponse($setbillagain,'data with new bill no.'); 
    
     }
 }
 //search bus no and get: bus and it's owner details
 public function getBusandOwnerdetailbyBusno($busno){
     $result=businfo::where('bus_no',$busno)->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->select('bus_info.*','bus_owner.*')->first();
      return $this->sendResponse($result,'bus detail and owner detail of given busno is shown successfully!');  
 }
  //store shulkas as well as prepare journal entry instantly
 public function storeGeneralShulkaAndJournal(Request $req){
    // Start transaction!
    DB::beginTransaction();
     $input=$req->all();
     $getinarray=[];
     $fiscalyear=FiscalYear::OrderBy('id','DESC')->first();
        $fenddate=$fiscalyear->fiscal_year_end_date;
        $endadateformat=Carbon::createFromFormat('Y-m-d H:i:s', $fenddate);
       // $fenddatecorrectformat=$fenddate->toDateTimeString();
        $getpaymentdate=Carbon::now();
        $paymentformat=Carbon::createFromFormat('Y-m-d H:i:s', $getpaymentdate);
        $paymentformatendofday=Carbon::parse($paymentformat)->endOfDay();
     //   return $this->sendResponse($paymentformat,'general shulka stored successfully!!'); 
       $getfyear=$fiscalyear->fiscal_year;
      //check for fiscal year
     if($endadateformat->gt($paymentformatendofday)){
         $summaasikshulka=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('bus_no',$req['bus_no'])->where('payment_type','general_shulka')->sum('payment_price');
        //  return $this->sendResponse($summaasikshulka,'shulka stored successfully!!'); 
         $sumkharidbikrishulka=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price'); 
         $sumnibedanshulka=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        //   return $this->sendResponse($sumnibedanshulka,'shulka stored successfully!!'); 
         $summmemberrenewshulka=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumnewmembershulka=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumnewbusregistershulka=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumjariwanashulka=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumquesattapattashulka=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumidentitycardshulka=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumsifaarishshulka=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumvawanbuildshulka=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumstaffupadaanshulka=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumsharesavingshulka=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumstickershulka=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumothershulka=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumphotocopyshulka=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumpeskifarchyotshulka=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumsattadartashulka=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
            try{
   
     $validator =Validator::make($input,[
  	       // 'bill_payment_date' => 'required',
            'bus_no' => 'required',
            'member_name'=>'required',
            'maasik_shulka'=>'required',
            'kharid_bikri_darta_sulka'=>'required',
            'nibedan_shulka'=>'required',
            'membership_nabikarad_shulka'=>'required',
            'naya_membership_shulka'=>'required',
            'naya_bus_register_shulka'=>'required',
            'jariwana_shulka'=>'required',
            'queue_shatta_patta_shulka'=>'required',
            'identitycard_shulka'=>'required',
            'sifaarish_shulka'=>'required',
            'vawan_build_fund'=>'required',
            'staff_upadaan_fund'=>'required',
            'share_saving_shulka'=>'required',
            'sticker_shulka'=>'required',
            'photocopy_fee'=>'required',
            'peski_farchyot_shulka'=>'required',
            'shatta_darta_shulka'=>'required',
            'bill_no'=>'required|unique:general_shulka'
  ]);
  if($validator->fails()){
            DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);
         }
        //checking if all input values are zero
        if($req->input('maasik_shulka')==0&&$req->input('kharid_bikri_darta_sulka')==0&&$req->input('nibedan_shulka')==0&&$req->input('membership_nabikarad_shulka')==0&&$req->input('naya_membership_shulka')==0&&$req->input('naya_bus_register_shulka')==0&&$req->input('jariwana_shulka')==0&&$req->input('queue_shatta_patta_shulka')==0
        &&$req->input('identitycard_shulka')==0&&$req->input('sifaarish_shulka')==0&&$req->input('vawan_build_fund')==0&&$req->input('staff_upadaan_fund')==0&&$req->input('share_saving_shulka')==0&&$req->input('sticker_shulka')==0&&$req->input('other_shulka')==0
        &&$req->input('photocopy_fee')==0&&$req->input('peski_farchyot_shulka')==0&&$req->input('shatta_darta_shulka')==0)
        {
             DB::rollback();
             return $this->sendError('please pay atleast on a topic!');
        }

         else{
         //check the payment to be done 
        // $sumkharidbikrishulka=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->sum('payment_price');
         
        
        $model=new GeneralShulka();
        $model->bill_no=$req->input('bill_no');
        $model->bill_payment_date=$paymentformat;
        $model->bus_no=$req->input('bus_no');
        $model->member_name=$req->input('member_name');
      //  number_format((float)$foo, 2, '.', '');
     if($summaasikshulka<$req->input('maasik_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($summaasikshulka)&&$summaasikshulka>=$req->input('maasik_shulka')){
        $model->maasik_shulka=number_format((float)$req->input('maasik_shulka'), 2, '.', ''); 
               }
     if($sumkharidbikrishulka<$req->input('kharid_bikri_darta_sulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumkharidbikrishulka)&&$sumkharidbikrishulka>=$req->input('kharid_bikri_darta_sulka')){
        $model->kharid_bikri_darta_sulka=number_format((float)$req->input('kharid_bikri_darta_sulka'), 2, '.', '');
      }
     if($sumnibedanshulka<$req->input('nibedan_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumnibedanshulka)&&$sumnibedanshulka>=$req->input('nibedan_shulka')){
      $model->nibedan_shulka=number_format((float)$req->input('nibedan_shulka'), 2, '.', '');
      }
     if($summmemberrenewshulka<$req->input('membership_nabikarad_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($summmemberrenewshulka)&&$summmemberrenewshulka>=$req->input('membership_nabikarad_shulka')){
      $model->membership_nabikarad_shulka=number_format((float)$req->input('membership_nabikarad_shulka'), 2, '.', '');
      }
     if($sumnewmembershulka<$req->input('naya_membership_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumnewmembershulka)&&$sumnewmembershulka>=$req->input('naya_membership_shulka')){
      $model->naya_membership_shulka=number_format((float)$req->input('naya_membership_shulka'), 2, '.', '');
      }
     if($sumnewbusregistershulka<$req->input('naya_bus_register_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumnewbusregistershulka)&&$sumnewbusregistershulka>=$req->input('naya_bus_register_shulka')){
      $model->naya_bus_register_shulka=number_format((float)$req->input('naya_bus_register_shulka'), 2, '.', '');
      }
     if($sumjariwanashulka<$req->input('jariwana_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumjariwanashulka)&&$sumjariwanashulka>=$req->input('jariwana_shulka')){
      $model->jariwana_shulka=number_format((float)$req->input('jariwana_shulka'), 2, '.', '');
      }
     if($sumquesattapattashulka<$req->input('queue_shatta_patta_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumquesattapattashulka)&&$sumquesattapattashulka>=$req->input('queue_shatta_patta_shulka')){
      $model->queue_shatta_patta_shulka=number_format((float)$req->input('queue_shatta_patta_shulka'), 2, '.', '');
      }
     if($sumidentitycardshulka<$req->input('identitycard_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumidentitycardshulka)&&$sumidentitycardshulka>=$req->input('identitycard_shulka')){
      $model->identitycard_shulka=number_format((float)$req->input('identitycard_shulka'), 2, '.', '');
      }
     if($sumsifaarishshulka<$req->input('sifaarish_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumsifaarishshulka)&&$sumsifaarishshulka>=$req->input('sifaarish_shulka')){
      $model->sifaarish_shulka=number_format((float)$req->input('sifaarish_shulka'), 2, '.', '');
      }
     if($sumvawanbuildshulka<$req->input('vawan_build_fund')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumvawanbuildshulka)&&$sumvawanbuildshulka>=$req->input('vawan_build_fund')){
      $model->vawan_build_fund=number_format((float)$req->input('vawan_build_fund'), 2, '.', '');
      }
     if($sumstaffupadaanshulka<$req->input('staff_upadaan_fund')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumstaffupadaanshulka)&&$sumstaffupadaanshulka>=$req->input('staff_upadaan_fund')){
      $model->staff_upadaan_fund=number_format((float)$req->input('staff_upadaan_fund'), 2, '.', '');
      }
     if($sumsharesavingshulka<$req->input('share_saving_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumsharesavingshulka)&&$sumsharesavingshulka>=$req->input('share_saving_shulka')){
      $model->share_saving_shulka=number_format((float)$req->input('share_saving_shulka'), 2, '.', '');
      }
     if($sumstickershulka<$req->input('sticker_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumstickershulka)&&$sumstickershulka>=$req->input('sticker_shulka')){
      $model->sticker_shulka=number_format((float)$req->input('sticker_shulka'), 2, '.', '');
      }
     if($sumothershulka<$req->input('other_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     } 
     if(!is_null($sumothershulka)&&$sumothershulka>=$req->input('other_shulka')){
      $model->other_shulka=number_format((float)$req->input('other_shulka'), 2, '.', '');
      }
     if($sumphotocopyshulka<$req->input('photocopy_fee')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumphotocopyshulka)&&$sumphotocopyshulka>=$req->input('photocopy_fee')){
      $model->photocopy_fee=number_format((float)$req->input('photocopy_fee'), 2, '.', '');
      }
     if($sumpeskifarchyotshulka<$req->input('peski_farchyot_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumpeskifarchyotshulka)&&$sumpeskifarchyotshulka>=$req->input('peski_farchyot_shulka')){
      $model->peski_farchyot_shulka=number_format((float)$req->input('peski_farchyot_shulka'), 2, '.', '');
      }
     if($sumsattadartashulka<$req->input('shatta_darta_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumsattadartashulka)&&$sumsattadartashulka>=$req->input('shatta_darta_shulka')){
      $model->shatta_darta_shulka=number_format((float)$req->input('shatta_darta_shulka'), 2, '.', '');
      }
        $model->maasik_shulka_remarks=$req->input('maasik_shulka_remarks');
        $model->kharid_bikri_darta_sulka_remarks=$req->input('kharid_bikri_darta_sulka_remarks');
        $model->nibedan_shulka_remarks=$req->input('nibedan_shulka_remarks');
        $model->membership_nabikarad_shulka_remarks=$req->input('membership_nabikarad_shulka_remarks');
        $model->naya_membership_shulka_remarks=$req->input('naya_membership_shulka_remarks');
        $model->naya_bus_register_shulka_remarks=$req->input('naya_bus_register_shulka_remarks');
        $model->jariwana_shulka_remarks=$req->input('jariwana_shulka_remarks');
        $model->queue_shatta_patta_shulka_remarks=$req->input('queue_shatta_patta_shulka_remarks');
        $model->identitycard_shulka_remarks=$req->input('identitycard_shulka_remarks');
        $model->sifaarish_shulka_remarks=$req->input('sifaarish_shulka_remarks');
        $model->vawan_build_fund_remarks=$req->input('vawan_build_fund_remarks');
        $model->staff_upadaan_fund_remarks=$req->input('staff_upadaan_fund_remarks');
        $model->share_saving_shulka_remarks=$req->input('share_saving_shulka_remarks');
        $model->sticker_shulka_remarks=$req->input('sticker_shulka_remarks');
        $model->other_shulka_remarks=$req->input('other_shulka_remarks');
        $model->photocopy_fee_remarks=$req->input('photocopy_fee_remarks');
        $model->peski_farchyot_shulka_remarks=$req->input('peski_farchyot_shulka_remarks');
        $model->shatta_darta_shulka_remarks=$req->input('shatta_darta_shulka_remarks');
        $model->added_by=$req->input('added_by');
        $model->company_id=$req->input('company_id');
        $model->updated_by=$req->input('updated_by');
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->total_amount=$req->input('total_amount');
        $model->payment_type=$req->input('payment_type');
        $model->fiscal_year=$getfyear;
        $model->save();
         }
    //------------------Sending notification to respective owner as well----------------------
    // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
        //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
        //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',];
     
        //notification contents:
          $notify=array('title'=>'General Shulka Payment','body'=>'Your General Shulka of rupees '.$model->total_amount.' has been paid.');
        //    $notify=array('title'=>'General Shulka Payment','body'=>'Vehicle Owner '.'has paid total of rupees.');
    $getownerdata=GeneralShulka::where('general_shulka.bus_no',$req['bus_no'])->join('bus_info','general_shulka.bus_no','bus_info.bus_no')->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->select('bus_info.bus_no','bus_owner.owner_id','bus_owner.owner_name','bus_owner.owner_phone','bus_owner.device_token','bus_owner.company_id')->first();
 //   return $this->sendResponse($getownerdata,'general shulka stored successfully!!');    
  $ownerFcmToken=$getownerdata->device_token;
  // return $this->sendResponse($ownerFcmToken,'general shulka stored successfully!!'); 
    //saving in notification table as well::
            $store=new Mynotification();
            $store->type='General Shulka Payment';
            $store->notifiable_id=$getownerdata->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title='Owner Shulka Paid by admin';
         //   $store->body='Vehicle Owner '.'member name'.' '.'has paid total of '.'total amount'.' rupees.';
            $store->body='Vehicle Owner '.$model->member_name.' '.'has paid total of '.$model->total_amount.' rupees.';
            $store->added_by=$req['added_by'];
            $store->created_at=Carbon::now();
            $store->company_id=$getownerdata->company_id;
            $store->save();
    //sending request field:for whom(reciever) and for what(data):::for owner
        $fields= json_encode(array( 'to'=> $ownerFcmToken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
            DB::rollback();
        }        

        // Close connection
        curl_close($ch);

        // FCM response
        //  return $this->sendResponse($result,'NOtification successfully sent!!');
       
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }

//again store to journal entry of general shulka
try{
     $validator =Validator::make($input,[
  	       // 'bill_payment_date' => 'required',
          //  'date' => 'required',
            'bill_no'=>'required',
          //  'debit_particular'=>'required',
        //    'credit_particular'=>'required',
         //  'debit_amount'=>'required',
          //  'credit_amount'=>'required',
          //  'total_amount'=>'required',
            'added_by'=>'required',
            'company_id'=>'required',
  ]);
  if($validator->fails()){
            DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
   try{
       if($req->input('maasik_shulka')>0&&!is_null($summaasikshulka)&&$summaasikshulka<$req->input('maasik_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('maasik_shulka')>0&&!is_null($summaasikshulka)&&$summaasikshulka>=$req->input('maasik_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$summaasikshulka-$req->input('maasik_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$summaasikshulka&&$summaasikshulka==$req->input('maasik_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$summaasikshulka&&$summaasikshulka>$req->input('maasik_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('maasik_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$req->input('maasik_shulka');
                $model->credit_amount=$req->input('maasik_shulka');
                $model->total_amount=$req->input('maasik_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding maasik shulka a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='maasik_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($summaasikshulka==$req->input('maasik_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($summaasikshulka>$req->input('maasik_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('maasik_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding maasik shulka a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding maasik shulka a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='maasik_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{
       if($req->input('kharid_bikri_darta_sulka')>0&&!is_null($sumkharidbikrishulka)&&$sumkharidbikrishulka<$req->input('kharid_bikri_darta_sulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('kharid_bikri_darta_sulka')>0&&!is_null($sumkharidbikrishulka)&&$sumkharidbikrishulka>=$req->input('kharid_bikri_darta_sulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumkharidbikrishulka-$req->input('kharid_bikri_darta_sulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumkharidbikrishulka&&$sumkharidbikrishulka==$req->input('kharid_bikri_darta_sulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumkharidbikrishulka&&$sumkharidbikrishulka>$req->input('kharid_bikri_darta_sulka')){
       $subtractedvalue=$getpaymentprice-$req->input('kharid_bikri_darta_sulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$req->input('kharid_bikri_darta_sulka');
                $model->credit_amount=$req->input('kharid_bikri_darta_sulka');
                $model->total_amount=$req->input('kharid_bikri_darta_sulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding kharid bikri darta shulka a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='kharid_bikri_darta_sulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumkharidbikrishulka==$req->input('kharid_bikri_darta_sulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumkharidbikrishulka>$req->input('kharid_bikri_darta_sulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('kharid_bikri_darta_sulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding kharid bikri darta shulka a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding kharid bikri darta shulka a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='kharid_bikri_darta_sulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
    }
       catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{  
      if($req->input('nibedan_shulka')>0&&!is_null($sumnibedanshulka)&&$sumnibedanshulka<$req->input('nibedan_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('nibedan_shulka')>0&&!is_null($sumnibedanshulka)&&$sumnibedanshulka>=$req->input('nibedan_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumnibedanshulka-$req->input('nibedan_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumnibedanshulka&&$sumnibedanshulka==$req->input('nibedan_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumnibedanshulka&&$sumnibedanshulka>$req->input('nibedan_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('nibedan_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$req->input('nibedan_shulka');
                $model->credit_amount=$req->input('nibedan_shulka');
                $model->total_amount=$req->input('nibedan_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding nibedan shulka a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='nibedan_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumnibedanshulka==$req->input('nibedan_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumnibedanshulka>$req->input('nibedan_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('nibedan_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding nibedan shulka a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding nibedan shulka a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='nibedan_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }

      }
        catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{  
     if($req->input('membership_nabikarad_shulka')>0&&!is_null($summmemberrenewshulka)&&$summmemberrenewshulka<$req->input('membership_nabikarad_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('membership_nabikarad_shulka')>0&&!is_null($summmemberrenewshulka)&&$summmemberrenewshulka>=$req->input('membership_nabikarad_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$summmemberrenewshulka-$req->input('membership_nabikarad_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$summmemberrenewshulka&&$summmemberrenewshulka==$req->input('membership_nabikarad_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$summmemberrenewshulka&&$summmemberrenewshulka>$req->input('membership_nabikarad_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('membership_nabikarad_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$req->input('membership_nabikarad_shulka');
                $model->credit_amount=$req->input('membership_nabikarad_shulka');
                $model->total_amount=$req->input('membership_nabikarad_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding membership nabikarad shulka a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='membership_nabikarad_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($summmemberrenewshulka==$req->input('membership_nabikarad_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($summmemberrenewshulka>$req->input('membership_nabikarad_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('membership_nabikarad_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding membership nabikarad shulka a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding membership nabikarad shulka a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='membership_nabikarad_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
      }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
     try{ 
    if($req->input('naya_membership_shulka')>0&&!is_null($sumnewmembershulka)&&$sumnewmembershulka<$req->input('naya_membership_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('naya_membership_shulka')>0&&!is_null($sumnewmembershulka)&&$sumnewmembershulka>=$req->input('naya_membership_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumnewmembershulka-$req->input('naya_membership_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumnewmembershulka&&$sumnewmembershulka==$req->input('naya_membership_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumnewmembershulka&&$sumnewmembershulka>$req->input('naya_membership_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('naya_membership_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$req->input('naya_membership_shulka');
                $model->credit_amount=$req->input('naya_membership_shulka');
                $model->total_amount=$req->input('naya_membership_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding naya membership shulka a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='naya_membership_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumnewmembershulka==$req->input('naya_membership_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumnewmembershulka>$req->input('naya_membership_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('naya_membership_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding naya membership shulka a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding naya membership shulka a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='naya_membership_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }     
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
   try{
  if($req->input('naya_bus_register_shulka')>0&&!is_null($sumnewbusregistershulka)&&$sumnewbusregistershulka<$req->input('naya_bus_register_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('naya_bus_register_shulka')>0&&!is_null($sumnewbusregistershulka)&&$sumnewbusregistershulka>=$req->input('naya_bus_register_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumnewbusregistershulka-$req->input('naya_bus_register_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumnewbusregistershulka&&$sumnewbusregistershulka==$req->input('naya_bus_register_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumnewbusregistershulka&&$sumnewbusregistershulka>$req->input('naya_bus_register_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('naya_bus_register_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$req->input('naya_bus_register_shulka');
                $model->credit_amount=$req->input('naya_bus_register_shulka');
                $model->total_amount=$req->input('naya_bus_register_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding naya bus register shulka a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='naya_bus_register_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumnewbusregistershulka==$req->input('naya_bus_register_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumnewbusregistershulka>$req->input('naya_bus_register_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('naya_bus_register_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding naya bus register shulka a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding naya bus register shulka a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='naya_bus_register_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
   }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('jariwana_shulka')>0&&!is_null($sumjariwanashulka)&&$sumjariwanashulka<$req->input('jariwana_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('jariwana_shulka')>0&&!is_null($sumjariwanashulka)&&$sumjariwanashulka>=$req->input('jariwana_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumjariwanashulka-$req->input('jariwana_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumjariwanashulka&&$sumjariwanashulka==$req->input('jariwana_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumjariwanashulka&&$sumjariwanashulka>$req->input('jariwana_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('jariwana_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$req->input('jariwana_shulka');
                $model->credit_amount=$req->input('jariwana_shulka');
                $model->total_amount=$req->input('jariwana_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding jariwana shulka a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='jariwana_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumjariwanashulka==$req->input('jariwana_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumjariwanashulka>$req->input('jariwana_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('jariwana_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding jariwana shulka a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding jariwana shulka a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='jariwana_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
      }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
     if($req->input('queue_shatta_patta_shulka')>0&&!is_null($sumquesattapattashulka)&&$sumquesattapattashulka<$req->input('queue_shatta_patta_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('queue_shatta_patta_shulka')>0&&!is_null($sumquesattapattashulka)&&$sumquesattapattashulka>=$req->input('queue_shatta_patta_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumquesattapattashulka-$req->input('queue_shatta_patta_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumquesattapattashulka&&$sumquesattapattashulka==$req->input('queue_shatta_patta_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumquesattapattashulka&&$sumquesattapattashulka>$req->input('queue_shatta_patta_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('queue_shatta_patta_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$req->input('queue_shatta_patta_shulka');
                $model->credit_amount=$req->input('queue_shatta_patta_shulka');
                $model->total_amount=$req->input('queue_shatta_patta_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding queue shatta patta shulka a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='queue_shatta_patta_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumquesattapattashulka==$req->input('queue_shatta_patta_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumquesattapattashulka>$req->input('queue_shatta_patta_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('queue_shatta_patta_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding queue shatta patta shulka a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding queue shatta patta shulka a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='queue_shatta_patta_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
       catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('identitycard_shulka')>0&&!is_null($sumidentitycardshulka)&&$sumidentitycardshulka<$req->input('identitycard_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('identitycard_shulka')>0&&!is_null($sumidentitycardshulka)&&$sumidentitycardshulka>=$req->input('identitycard_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumidentitycardshulka-$req->input('identitycard_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumidentitycardshulka&&$sumidentitycardshulka==$req->input('identitycard_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumidentitycardshulka&&$sumidentitycardshulka>$req->input('identitycard_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('identitycard_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$req->input('identitycard_shulka');
                $model->credit_amount=$req->input('identitycard_shulka');
                $model->total_amount=$req->input('identitycard_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding identitycard shulka a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='identitycard_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumidentitycardshulka==$req->input('identitycard_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumidentitycardshulka>$req->input('identitycard_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('identitycard_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding identitycard shulka a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding identitycard shulka a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='identitycard_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
   if($req->input('sifaarish_shulka')>0&&!is_null($sumsifaarishshulka)&&$sumsifaarishshulka<$req->input('sifaarish_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('sifaarish_shulka')>0&&!is_null($sumsifaarishshulka)&&$sumsifaarishshulka>=$req->input('sifaarish_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumsifaarishshulka-$req->input('sifaarish_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumsifaarishshulka&&$sumsifaarishshulka==$req->input('sifaarish_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumsifaarishshulka&&$sumsifaarishshulka>$req->input('sifaarish_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('sifaarish_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$req->input('sifaarish_shulka');
                $model->credit_amount=$req->input('sifaarish_shulka');
                $model->total_amount=$req->input('sifaarish_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding sifaarish shulka a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='sifaarish_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumsifaarishshulka==$req->input('sifaarish_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumsifaarishshulka>$req->input('sifaarish_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('sifaarish_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding sifaarish shulka a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding sifaarish shulka a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='sifaarish_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }     
    }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('vawan_build_fund')>0&&!is_null($sumvawanbuildshulka)&&$sumvawanbuildshulka<$req->input('vawan_build_fund')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('vawan_build_fund')>0&&!is_null($sumvawanbuildshulka)&&$sumvawanbuildshulka>=$req->input('vawan_build_fund')){
  //  return $this->SendResponse('ok','done');
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumvawanbuildshulka-$req->input('vawan_build_fund');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumvawanbuildshulka&&$sumvawanbuildshulka==$req->input('vawan_build_fund')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumvawanbuildshulka&&$sumvawanbuildshulka>$req->input('vawan_build_fund')){
       $subtractedvalue=$getpaymentprice-$req->input('vawan_build_fund');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$req->input('vawan_build_fund');
                $model->credit_amount=$req->input('vawan_build_fund');
                $model->total_amount=$req->input('vawan_build_fund');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding vawan build fund a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='vawan_build_fund';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumvawanbuildshulka==$req->input('vawan_build_fund')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumvawanbuildshulka>$req->input('vawan_build_fund')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('vawan_build_fund');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding vawan build fund a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding vawan build fund a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='vawan_build_fund';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }     
    }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('staff_upadaan_fund')>0&&!is_null($sumstaffupadaanshulka)&&$sumstaffupadaanshulka<$req->input('staff_upadaan_fund')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
    if($req->input('staff_upadaan_fund')>0&&!is_null($sumstaffupadaanshulka)&&$sumstaffupadaanshulka>=$req->input('staff_upadaan_fund')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumstaffupadaanshulka-$req->input('staff_upadaan_fund');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumstaffupadaanshulka&&$sumstaffupadaanshulka==$req->input('staff_upadaan_fund')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumstaffupadaanshulka&&$sumstaffupadaanshulka>$req->input('staff_upadaan_fund')){
       $subtractedvalue=$getpaymentprice-$req->input('staff_upadaan_fund');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$req->input('staff_upadaan_fund');
                $model->credit_amount=$req->input('staff_upadaan_fund');
                $model->total_amount=$req->input('staff_upadaan_fund');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding staff upadaan fund a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='staff_upadaan_fund';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumstaffupadaanshulka==$req->input('staff_upadaan_fund')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumstaffupadaanshulka>$req->input('staff_upadaan_fund')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('staff_upadaan_fund');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding staff upadaan fund a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding staff upadaan fund a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='staff_upadaan_fund';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }     
    }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('share_saving_shulka')>0&&!is_null($sumsharesavingshulka)&&$sumsharesavingshulka<$req->input('share_saving_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
    if($req->input('share_saving_shulka')>0&&!is_null($sumsharesavingshulka)&&$sumsharesavingshulka>=$req->input('share_saving_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumsharesavingshulka-$req->input('share_saving_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumsharesavingshulka&&$sumsharesavingshulka==$req->input('share_saving_shulka')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumsharesavingshulka&&$sumsharesavingshulka>$req->input('share_saving_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('share_saving_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$req->input('share_saving_shulka');
                $model->credit_amount=$req->input('share_saving_shulka');
                $model->total_amount=$req->input('share_saving_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding share saving shulka a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='share_saving_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumsharesavingshulka==$req->input('share_saving_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumsharesavingshulka>$req->input('share_saving_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('share_saving_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding share saving shulka a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding share saving shulka a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='share_saving_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('sticker_shulka')>0&&!is_null($sumstickershulka)&&$sumstickershulka<$req->input('sticker_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
    if($req->input('sticker_shulka')>0&&!is_null($sumstickershulka)&&$sumstickershulka>=$req->input('sticker_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumstickershulka-$req->input('sticker_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumstickershulka&&$sumstickershulka==$req->input('sticker_shulka')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumstickershulka&&$sumstickershulka>$req->input('sticker_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('sticker_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$req->input('sticker_shulka');
                $model->credit_amount=$req->input('sticker_shulka');
                $model->total_amount=$req->input('sticker_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding sticker shulka a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='sticker_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumstickershulka==$req->input('sticker_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumstickershulka>$req->input('sticker_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('sticker_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding sticker shulka a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding sticker shulka a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='sticker_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
     if($req->input('other_shulka')>0&&!is_null($sumothershulka)&&$sumothershulka<$req->input('other_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
    if($req->input('other_shulka')>0&&!is_null($sumothershulka)&&$sumothershulka>=$req->input('other_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumothershulka-$req->input('other_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumothershulka&&$sumothershulka==$req->input('other_shulka')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumothershulka&&$sumothershulka>$req->input('other_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('other_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$req->input('other_shulka');
                $model->credit_amount=$req->input('other_shulka');
                $model->total_amount=$req->input('other_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding other shulka a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='other_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumothershulka==$req->input('other_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumothershulka>$req->input('other_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('other_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding other shulka a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding other shulka a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='other_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
        
    }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('photocopy_fee')>0&&!is_null($sumphotocopyshulka)&&$sumphotocopyshulka<$req->input('photocopy_fee')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
    if($req->input('photocopy_fee')>0&&!is_null($sumphotocopyshulka)&&$sumphotocopyshulka>=$req->input('photocopy_fee')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumphotocopyshulka-$req->input('photocopy_fee');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumphotocopyshulka&&$sumphotocopyshulka==$req->input('photocopy_fee')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumphotocopyshulka&&$sumphotocopyshulka>$req->input('photocopy_fee')){
       $subtractedvalue=$getpaymentprice-$req->input('photocopy_fee');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$req->input('photocopy_fee');
                $model->credit_amount=$req->input('photocopy_fee');
                $model->total_amount=$req->input('photocopy_fee');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding photocopy shulka a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='photocopy_fee';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumphotocopyshulka==$req->input('photocopy_fee')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumphotocopyshulka>$req->input('photocopy_fee')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('photocopy_fee');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding photocopy shulka a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding photocopy shulka a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='photocopy_fee';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('peski_farchyot_shulka')>0&&!is_null($sumpeskifarchyotshulka)&&$sumpeskifarchyotshulka<$req->input('peski_farchyot_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
    if($req->input('peski_farchyot_shulka')>0&&!is_null($sumpeskifarchyotshulka)&&$sumpeskifarchyotshulka>=$req->input('peski_farchyot_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumpeskifarchyotshulka-$req->input('peski_farchyot_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumpeskifarchyotshulka&&$sumpeskifarchyotshulka==$req->input('peski_farchyot_shulka')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumpeskifarchyotshulka&&$sumpeskifarchyotshulka>$req->input('peski_farchyot_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('peski_farchyot_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$req->input('peski_farchyot_shulka');
                $model->credit_amount=$req->input('peski_farchyot_shulka');
                $model->total_amount=$req->input('peski_farchyot_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding peski farchyot shulka a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='peski_farchyot_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumpeskifarchyotshulka==$req->input('peski_farchyot_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumpeskifarchyotshulka>$req->input('peski_farchyot_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('peski_farchyot_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding peski farchyot shulka a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding peski farchyot shulka a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='peski_farchyot_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
    }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('shatta_darta_shulka')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular=$req->input('payment_type').' a/c';
    $model->credit_particular='shatta darta shulka a/c';
    $model->debit_amount=$req->input('shatta_darta_shulka');
    $model->credit_amount=$req->input('shatta_darta_shulka');
    $model->total_amount=$req->input('shatta_darta_shulka');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
    if($req->input('shatta_darta_shulka')>0&&!is_null($sumsattadartashulka)&&$sumsattadartashulka<$req->input('shatta_darta_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
    if($req->input('shatta_darta_shulka')>0&&!is_null($sumsattadartashulka)&&$sumsattadartashulka>=$req->input('shatta_darta_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumsattadartashulka-$req->input('shatta_darta_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumsattadartashulka&&$sumsattadartashulka==$req->input('shatta_darta_shulka')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumsattadartashulka&&$sumsattadartashulka>$req->input('shatta_darta_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('shatta_darta_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$req->input('shatta_darta_shulka');
                $model->credit_amount=$req->input('shatta_darta_shulka');
                $model->total_amount=$req->input('shatta_darta_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding shatta darta shulka a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='shatta_darta_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumsattadartashulka==$req->input('shatta_darta_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumsattadartashulka>$req->input('shatta_darta_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('shatta_darta_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding shatta darta shulka a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$req->input('bill_no');
                $model->debit_particular='outstanding shatta darta shulka a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='shatta_darta_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
}

   catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   } 
      }
      //if not the fiscal year
 else{
            return $this->sendError('New fiscal year has not been setup yet!!');
        }
// If we reach here, then
// data is valid and working.
//now Commit the queries!
DB::commit();
 return $this->sendResponse($getinarray,'general shulka stored successfully!!');
 
     
 }
 //show general payment history of company 
 public function showPaymentHistory($companyid){
     $getdata=GeneralShulka::where('general_shulka.company_id',$companyid)->select('general_shulka.*')->orderBy('bill_id', 'DESC')->get();
        $get=[];
       // $getownername=[];
         $getadminname=[];
    foreach($getdata as $data){     
    $getaddedsingle=$data->added_by;
     if(ctype_digit(strval($getaddedsingle))){
    $getadminname=GeneralShulka::where('general_shulka.company_id',$companyid)->join('bus_info','general_shulka.bus_no','bus_info.bus_no')->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->join('admin_info','general_shulka.added_by','admin_info.admin_id')->select('admin_info.admin_name')->first();    
    $minimizetoname=json_decode($getadminname);
    $getvalue=$minimizetoname->admin_name; 
    $data['added_by']=$getvalue;
    $get[]=[$data
          ];     
         
     }
        else{
        //  $getownername=GeneralShulka::join('bus_info','general_shulka.bus_no','bus_info.bus_no')->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->where('general_shulka.company_id',$companyid)->select('bus_owner.owner_name')->first();
        $getaddedby=GeneralShulka::where('company_id',100)->select('member_name')->orderBy('bill_id', 'DESC')->first();
          $toname=json_decode($getaddedby);
          $getvalues=$toname->member_name;  
           $data['added_by']=$getvalues;
           $get[]=[$data
          ];     
    
        }
    }
    return $this->sendResponse($get,'Transaction history of a company is shown successfully!!');
 }
//show sum of due payment  of bus
public function showGeneralShulkaDuePaymentofBus($busno){
     \DB::statement("SET SQL_MODE=''");
    $getdue=AccountPriceDeclaration::where('bus_no',$busno)->where('payment_type','general_shulka')->groupBy('acount_price_declaration.payment_topic')->groupBy('acount_price_declaration.payment_type')->get();
    $getinarray=[];
    foreach($getdue as $dues){
        $paymenttopic=$dues->payment_topic;
        $paymentdueprice=$dues->payment_price;
        $paymenttype=$dues->payment_type;
        $getsum=$dues->where('payment_topic',$paymenttopic)->where('payment_type','general_shulka')->where('bus_no',$busno)->sum('payment_price');
        $getinarray[]=[
            'payment_topic'=>$paymenttopic,
        'rate'=>$getsum    
            ];
    }
     return $this->sendResponse($getinarray,'Sum of Due of indivisual payment topic is shown successfully!!');
}

 //--general shulka part ends here! ----------------------
 
 //---------------------****** Genneral Shulka Kharcha part starts here *******----------------------------------------
 //store general kharcha in dummy at first to insure verification process
 public function DummyStoreGeneralKharcha(Request $req){
       // Start transaction!
    DB::beginTransaction();
     $input=$req->all();
     $fiscalyear=FiscalYear::OrderBy('id','DESC')->first();
        $fenddate=$fiscalyear->fiscal_year_end_date;
        $endadateformat=Carbon::createFromFormat('Y-m-d H:i:s', $fenddate);
       // $fenddatecorrectformat=$fenddate->toDateTimeString();
        $getpaymentdate=Carbon::now();
        $paymentformat=Carbon::createFromFormat('Y-m-d H:i:s', $getpaymentdate);
        $paymentformatendofday=Carbon::parse($paymentformat)->endOfDay();
     //   return $this->sendResponse($paymentformat,'general shulka stored successfully!!'); 
       $getfyear=$fiscalyear->fiscal_year;
  if($endadateformat->gt($paymentformatendofday)){
            try{
   
     $validator =Validator::make($input,[
  	        'masalanda_kharcha'=>'required',
            'chhapai_kharcha'=>'required',
            'bidhyut_kharcha'=>'required',
            'telephonebill_kharcha'=>'required',
            'dailytour_bhatta_kharcha'=>'required',
            'marmatsambhaar_kharcha'=>'required',
            'khaja_office_kharcha'=>'required',
            'talab_kharcha'=>'required',
            'bahiri_khaja_kharcha'=>'required',
            'staff_maasik_bhatta_kharcha'=>'required',
            'kosadaxya_maasik_bhatta_kharcha'=>'required',
            'staff_sanchayakosh_kharcha'=>'required',
            'housefare_kharcha'=>'required',
            'baithak_bhatta_kharcha'=>'required',
            'aarthiksahayog_kharcha'=>'required',
            'bibidh_kharcha'=>'required',
            'sanchaar_bhatta'=>'required',
            'bank_nagad_jamma'=>'required',
            'indhan_kharcha'=>'required',
          //  'bill_no'=>'required|unique:general_shulka_kharcha'
  ]);
  if($validator->fails()){
            DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);
         }
        //checking if all input values are zero
        if($req->input('masalanda_kharcha')==0&&$req->input('chhapai_kharcha')==0&&$req->input('bidhyut_kharcha')==0&&$req->input('telephonebill_kharcha')==0&&$req->input('dailytour_bhatta_kharcha')==0&&$req->input('marmatsambhaar_kharcha')==0
        &&$req->input('khaja_office_kharcha')==0&&$req->input('talab_kharcha')==0&&$req->input('bahiri_khaja_kharcha')==0&&$req->input('staff_maasik_bhatta_kharcha')==0&&$req->input('kosadaxya_maasik_bhatta_kharcha')==0&&$req->input('staff_sanchayakosh_kharcha')==0&&$req->input('housefare_kharcha')==0
        &&$req->input('baithak_bhatta_kharcha')==0&&$req->input('aarthiksahayog_kharcha')==0&&$req->input('bibidh_kharcha')==0&&$req->input('bank_nagad_jamma')==0&&$req->input('sanchaar_bhatta')==0&&$req->input('indhan_kharcha')==0)
        {
             DB::rollback();
             return $this->sendError('please pay atleast on a topic!');
        }

         else{
        $model=new GeneralKharchaDummy();
        //$model->bill_no=$req->input('bill_no');
        $model->bill_payment_date=$paymentformat;
        $model->kharcha_received_by=$req->input('kharcha_received_by');
        $model->kharcha_recipient_address=$req->input('kharcha_recipient_address');
        $model->masalanda_kharcha=number_format((float)$req->input('masalanda_kharcha'), 2, '.', '');
        $model->chhapai_kharcha=number_format((float)$req->input('chhapai_kharcha'), 2, '.', '');
        $model->bidhyut_kharcha=number_format((float)$req->input('bidhyut_kharcha'), 2, '.', '');
        $model->telephonebill_kharcha=number_format((float)$req->input('telephonebill_kharcha'), 2, '.', '');
        $model->dailytour_bhatta_kharcha=number_format((float)$req->input('dailytour_bhatta_kharcha'), 2, '.', '');
        $model->marmatsambhaar_kharcha=number_format((float)$req->input('marmatsambhaar_kharcha'), 2, '.', '');
        $model->khaja_office_kharcha=number_format((float)$req->input('khaja_office_kharcha'), 2, '.', '');
        $model->talab_kharcha=number_format((float)$req->input('talab_kharcha'), 2, '.', '');
        $model->bahiri_khaja_kharcha=number_format((float)$req->input('bahiri_khaja_kharcha'), 2, '.', '');
        $model->staff_maasik_bhatta_kharcha=number_format((float)$req->input('staff_maasik_bhatta_kharcha'), 2, '.', '');
        $model->kosadaxya_maasik_bhatta_kharcha=number_format((float)$req->input('kosadaxya_maasik_bhatta_kharcha'), 2, '.', '');
        $model->staff_sanchayakosh_kharcha=number_format((float)$req->input('staff_sanchayakosh_kharcha'), 2, '.', '');
        $model->housefare_kharcha=number_format((float)$req->input('housefare_kharcha'), 2, '.', '');
        $model->baithak_bhatta_kharcha=number_format((float)$req->input('baithak_bhatta_kharcha'), 2, '.', '');
        $model->aarthiksahayog_kharcha=number_format((float)$req->input('aarthiksahayog_kharcha'), 2, '.', '');
        $model->bibidh_kharcha=number_format((float)$req->input('bibidh_kharcha'), 2, '.', '');
        $model->sanchaar_bhatta=number_format((float)$req->input('sanchaar_bhatta'), 2, '.', '');
        $model->bank_nagad_jamma=number_format((float)$req->input('bank_nagad_jamma'), 2, '.', '');
        $model->indhan_kharcha=number_format((float)$req->input('indhan_kharcha'), 2, '.', '');
        $model->added_by=$req->input('added_by');
        $model->company_id=$req->input('company_id');
        $model->updated_by=$req->input('updated_by');
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->total_amount=$req->input('total_amount');
        $model->payment_type=$req->input('payment_type');
        $model->fiscal_year=$getfyear;
        $model->cheque_no=$req->input('cheque_no');
        $model->save();
         }
          //------------------Sending notification to respective owner as well----------------------
    // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
        //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
        //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',];
     
        //notification contents:
          $notify=array('title'=>'General Kharcha Payment Request','body'=>'General Kharcha of rupees '.$model->total_amount.' is requested to pay, please verify this.');
    $getstaffdata=StaffInfo::where('staff_info.company_id',$req['company_id'])->join('staff_roles','staff_info.staff_id','staff_roles.staff_id')->join('roles','staff_roles.role_id','roles.role_id')->where('roles.role_id',11)->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_phone','staff_info.device_token','staff_info.company_id','roles.*')->get();
           $get=[];
         //  $result=[];
            foreach($getstaffdata as $selectedonly){
            $get[]=[
            $getstaffid=$selectedonly->staff_id,    
            $getname=$selectedonly->staff_name,
            $gettoken=$selectedonly->device_token,
            $getcompanyid=$selectedonly->company_id,
                ];
    //saving in notification table as well::
            $store=new Mynotification();
            $store->type='General kharcha Payment';
            $store->notifiable_id=$getstaffid;
            $store->notifiable_type=StaffInfo::class;
            $store->title='General Kharcha Payment Request';
            $store->body='General kharcha of total '.$model->total_amount.' '.' rupees has been requested to you for verification. please verify this asap!';
            $store->added_by=$req['added_by'];
            $store->created_at=Carbon::now();
            $store->company_id=$getcompanyid;
            $store->save();
    //sending request field:for whom(reciever) and for what(data):::for owner
        $fields= json_encode(array( 'to'=> $gettoken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
            DB::rollback();
        }        

        // Close connection
        curl_close($ch);

        // FCM response
        //  return $this->sendResponse($result,'NOtification successfully sent!!');
        }
                
            }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
  
   }
}
      //if not the fiscal year
 else{
            return $this->sendError('New fiscal year has not been setup yet!!');
        }
// If we reach here, then
// data is valid and working.
//now Commit the queries!
DB::commit();
 return $this->sendResponse($getinarray,'general shulka stored successfully!!');


 }
 //show general kharcha payment request made by admin
 public function ShowDummyGShulka($companyid){
        $getdata=GeneralKharchaDummy::where('company_id',$companyid)->get();
     return $this->sendResponse($getdata,'Payment request by admin is shown successfully!');    
    }
//delete the request made by admin from dummy:
public function deleteDummyGShulka($id){
        $getdata=GeneralKharchaDummy::where('id',$id)->first();
        $del=$getdata->delete();
        return $this->sendResponse('done','Payment request is deleted successfully!');    
}
//show general kharcha of today of company
public function getGKharchaToday($companyid){
 $get=GeneralShulkaKharcha::where('company_id',$companyid)->whereDate('bill_payment_date', Carbon::today())->get(); 
 return $this->sendResponse($get,'general kharcha of today is shown successfully!');
}
//store general kharcha and prepare journal as well instantly
public function   storeGeneralShulkaKharchaAndJournal(Request $req){
    // Start transaction!
    DB::beginTransaction();
     $input=$req->all();
     $fiscalyear=FiscalYear::OrderBy('id','DESC')->first();
        $fenddate=$fiscalyear->fiscal_year_end_date;
        $endadateformat=Carbon::createFromFormat('Y-m-d H:i:s', $fenddate);
       // $fenddatecorrectformat=$fenddate->toDateTimeString();
        $getpaymentdate=Carbon::now();
        $paymentformat=Carbon::createFromFormat('Y-m-d H:i:s', $getpaymentdate);
        $paymentformatendofday=Carbon::parse($paymentformat)->endOfDay();
     //   return $this->sendResponse($paymentformat,'general shulka stored successfully!!'); 
       $getfyear=$fiscalyear->fiscal_year;
  if($endadateformat->gt($paymentformatendofday)){
            try{
   
     $validator =Validator::make($input,[
  	        'masalanda_kharcha'=>'required',
            'chhapai_kharcha'=>'required',
            'bidhyut_kharcha'=>'required',
            'telephonebill_kharcha'=>'required',
            'dailytour_bhatta_kharcha'=>'required',
            'marmatsambhaar_kharcha'=>'required',
            'khaja_office_kharcha'=>'required',
            'talab_kharcha'=>'required',
            'bahiri_khaja_kharcha'=>'required',
            'staff_maasik_bhatta_kharcha'=>'required',
            'kosadaxya_maasik_bhatta_kharcha'=>'required',
            'staff_sanchayakosh_kharcha'=>'required',
            'housefare_kharcha'=>'required',
            'baithak_bhatta_kharcha'=>'required',
            'aarthiksahayog_kharcha'=>'required',
            'bibidh_kharcha'=>'required',
            'sanchaar_bhatta'=>'required',
            'bank_nagad_jamma'=>'required',
            'indhan_kharcha'=>'required',
            'bill_no'=>'required|unique:general_shulka_kharcha'
  ]);
  if($validator->fails()){
            DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);
         }
        //checking if all input values are zero
        if($req->input('masalanda_kharcha')==0&&$req->input('chhapai_kharcha')==0&&$req->input('bidhyut_kharcha')==0&&$req->input('telephonebill_kharcha')==0&&$req->input('dailytour_bhatta_kharcha')==0&&$req->input('marmatsambhaar_kharcha')==0
        &&$req->input('khaja_office_kharcha')==0&&$req->input('talab_kharcha')==0&&$req->input('bahiri_khaja_kharcha')==0&&$req->input('staff_maasik_bhatta_kharcha')==0&&$req->input('kosadaxya_maasik_bhatta_kharcha')==0&&$req->input('staff_sanchayakosh_kharcha')==0&&$req->input('housefare_kharcha')==0
        &&$req->input('baithak_bhatta_kharcha')==0&&$req->input('aarthiksahayog_kharcha')==0&&$req->input('bibidh_kharcha')==0&&$req->input('bank_nagad_jamma')==0&&$req->input('sanchaar_bhatta')==0&&$req->input('indhan_kharcha')==0)
        {
             DB::rollback();
             return $this->sendError('please pay atleast on a topic!');
        }

         else{
        $model=new GeneralShulkaKharcha();
        $model->bill_no=$req->input('bill_no');
        $model->bill_payment_date=$paymentformat;
        $model->kharcha_received_by=$req->input('kharcha_received_by');
        $model->kharcha_recipient_address=$req->input('kharcha_recipient_address');
        $model->masalanda_kharcha=number_format((float)$req->input('masalanda_kharcha'), 2, '.', '');
        $model->chhapai_kharcha=number_format((float)$req->input('chhapai_kharcha'), 2, '.', '');
        $model->bidhyut_kharcha=number_format((float)$req->input('bidhyut_kharcha'), 2, '.', '');
        $model->telephonebill_kharcha=number_format((float)$req->input('telephonebill_kharcha'), 2, '.', '');
        $model->dailytour_bhatta_kharcha=number_format((float)$req->input('dailytour_bhatta_kharcha'), 2, '.', '');
        $model->marmatsambhaar_kharcha=number_format((float)$req->input('marmatsambhaar_kharcha'), 2, '.', '');
        $model->khaja_office_kharcha=number_format((float)$req->input('khaja_office_kharcha'), 2, '.', '');
        $model->talab_kharcha=number_format((float)$req->input('talab_kharcha'), 2, '.', '');
        $model->bahiri_khaja_kharcha=number_format((float)$req->input('bahiri_khaja_kharcha'), 2, '.', '');
        $model->staff_maasik_bhatta_kharcha=number_format((float)$req->input('staff_maasik_bhatta_kharcha'), 2, '.', '');
        $model->kosadaxya_maasik_bhatta_kharcha=number_format((float)$req->input('kosadaxya_maasik_bhatta_kharcha'), 2, '.', '');
        $model->staff_sanchayakosh_kharcha=number_format((float)$req->input('staff_sanchayakosh_kharcha'), 2, '.', '');
        $model->housefare_kharcha=number_format((float)$req->input('housefare_kharcha'), 2, '.', '');
        $model->baithak_bhatta_kharcha=number_format((float)$req->input('baithak_bhatta_kharcha'), 2, '.', '');
        $model->aarthiksahayog_kharcha=number_format((float)$req->input('aarthiksahayog_kharcha'), 2, '.', '');
        $model->bibidh_kharcha=number_format((float)$req->input('bibidh_kharcha'), 2, '.', '');
        $model->sanchaar_bhatta=number_format((float)$req->input('sanchaar_bhatta'), 2, '.', '');
        $model->bank_nagad_jamma=number_format((float)$req->input('bank_nagad_jamma'), 2, '.', '');
        $model->indhan_kharcha=number_format((float)$req->input('indhan_kharcha'), 2, '.', '');
        $model->added_by=$req->input('added_by');
        $model->company_id=$req->input('company_id');
        $model->updated_by=$req->input('updated_by');
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->total_amount=$req->input('total_amount');
        $model->payment_type=$req->input('payment_type');
        $model->fiscal_year=$getfyear;
        $model->cheque_no=$req->input('cheque_no');
        $model->save();
         }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
  
   }
         
       
//again store to journal entry of general shulka
try{
     $validator =Validator::make($input,[
  	       // 'bill_payment_date' => 'required',
          //  'date' => 'required',
            'bill_no'=>'required',
          //  'debit_particular'=>'required',
        //    'credit_particular'=>'required',
         //  'debit_amount'=>'required',
          //  'credit_amount'=>'required',
          //  'total_amount'=>'required',
            'added_by'=>'required',
            'company_id'=>'required',
  ]);
  if($validator->fails()){
            DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
        try{
    if($req->input('masalanda_kharcha')>0) {
    $model=new JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='masalanda kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('masalanda_kharcha');
    $model->credit_amount=$req->input('masalanda_kharcha');
    $model->total_amount=$req->input('masalanda_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;
    $model->save();
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{
    if($req->input('chhapai_kharcha')>0)
    {
    $model=new JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='chhapai kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('chhapai_kharcha');
    $model->credit_amount=$req->input('chhapai_kharcha');
    $model->total_amount=$req->input('chhapai_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
      }
       catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{  
    if($req->input('bidhyut_kharcha')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='bidhyut kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('bidhyut_kharcha');
    $model->credit_amount=$req->input('bidhyut_kharcha');
    $model->total_amount=$req->input('bidhyut_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
      }
        catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{  
    if($req->input('telephonebill_kharcha')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='telephonebill kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('telephonebill_kharcha');
    $model->credit_amount=$req->input('telephonebill_kharcha');
    $model->total_amount=$req->input('telephonebill_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
      }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
     try{ 
    if($req->input('dailytour_bhatta_kharcha')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='dailytour bhatta kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('dailytour_bhatta_kharcha');
    $model->credit_amount=$req->input('dailytour_bhatta_kharcha');
    $model->total_amount=$req->input('dailytour_bhatta_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
         
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
   try{
    if($req->input('marmatsambhaar_kharcha')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='marmatsambhaar kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('marmatsambhaar_kharcha');
    $model->credit_amount=$req->input('marmatsambhaar_kharcha');
    $model->total_amount=$req->input('marmatsambhaar_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
   }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('khaja_office_kharcha')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='khaja office kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('khaja_office_kharcha');
    $model->credit_amount=$req->input('khaja_office_kharcha');
    $model->total_amount=$req->input('khaja_office_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
      }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
     if($req->input('talab_kharcha')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='talab kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('talab_kharcha');
    $model->credit_amount=$req->input('talab_kharcha');
    $model->total_amount=$req->input('talab_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
     }
       catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('bahiri_khaja_kharcha')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='bahiri khaja kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('bahiri_khaja_kharcha');
    $model->credit_amount=$req->input('bahiri_khaja_kharcha');
    $model->total_amount=$req->input('bahiri_khaja_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
     }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('staff_maasik_bhatta_kharcha')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='staff maasik bhatta kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('staff_maasik_bhatta_kharcha');
    $model->credit_amount=$req->input('staff_maasik_bhatta_kharcha');
    $model->total_amount=$req->input('staff_maasik_bhatta_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
        
    }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
     if($req->input('kosadaxya_maasik_bhatta_kharcha')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='kosadaxya maasik bhatta kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('kosadaxya_maasik_bhatta_kharcha');
    $model->credit_amount=$req->input('kosadaxya_maasik_bhatta_kharcha');
    $model->total_amount=$req->input('kosadaxya_maasik_bhatta_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
    }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
     if($req->input('staff_sanchayakosh_kharcha')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='staff sanchayakosh kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('staff_sanchayakosh_kharcha');
    $model->credit_amount=$req->input('staff_sanchayakosh_kharcha');
    $model->total_amount=$req->input('staff_sanchayakosh_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
    }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('housefare_kharcha')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='housefare kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('housefare_kharcha');
    $model->credit_amount=$req->input('housefare_kharcha');
    $model->total_amount=$req->input('housefare_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
     if($req->input('baithak_bhatta_kharcha')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='baithak kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('baithak_bhatta_kharcha');
    $model->credit_amount=$req->input('baithak_bhatta_kharcha');
    $model->total_amount=$req->input('baithak_bhatta_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
     if($req->input('aarthiksahayog_kharcha')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='aarthiksahayog kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('aarthiksahayog_kharcha');
    $model->credit_amount=$req->input('aarthiksahayog_kharcha');
    $model->total_amount=$req->input('aarthiksahayog_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
      if($req->input('bibidh_kharcha')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='bibidh kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('bibidh_kharcha');
    $model->credit_amount=$req->input('bibidh_kharcha');
    $model->total_amount=$req->input('bibidh_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
     if($req->input('sanchaar_bhatta')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='sanchaar bhatta a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('sanchaar_bhatta');
    $model->credit_amount=$req->input('sanchaar_bhatta');
    $model->total_amount=$req->input('sanchaar_bhatta');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('bank_nagad_jamma')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='bank nagad jamma a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('bank_nagad_jamma');
    $model->credit_amount=$req->input('bank_nagad_jamma');
    $model->total_amount=$req->input('bank_nagad_jamma');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
   try{
    if($req->input('indhan_kharcha')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$req->input('bill_no');
    $model->debit_particular='indhan kharcha a/c';
    $model->credit_particular=$req->input('payment_type').' a/c';
    $model->debit_amount=$req->input('indhan_kharcha');
    $model->credit_amount=$req->input('indhan_kharcha');
    $model->total_amount=$req->input('indhan_kharcha');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
   

}
   catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   } 
      }
      //if not the fiscal year
 else{
            return $this->sendError('New fiscal year has not been setup yet!!');
        }
// If we reach here, then
// data is valid and working.
//now Commit the queries!
DB::commit();
 return $this->sendResponse($getinarray,'general shulka stored successfully!!');

     
     
     
     
     
 }
//show general payment history of company
public function showGeneralshulkaKharchaPaymentHistory($companyid){
     $getdata=GeneralShulkaKharcha::where('general_shulka_kharcha.company_id',$companyid)->select('general_shulka_kharcha.*')->orderBy('id', 'DESC')->get();
           return $this->sendResponse($getdata,'General kharcha history of a company is shown successfully!!');
 }
//show journal history of general (shulka and kharcha) of current fiscal year of a company
public function journalHistoryofGeneralshulka($companyid){
    $getcurrentfiscal=FiscalYear::orderBy('id', 'DESC')->first();
    $fyear=$getcurrentfiscal->fiscal_year;
    $get=JournalEntryOfGeneralShulka::where('company_id',$companyid)->where('fiscal_year',$fyear)->orderBy('id', 'DESC')->get();
     return $this->sendResponse($get,'Journal history of General shulka is shown successfully!!');
}
//practice session part------ 
 //using transaction
 public function storeGshulkaandShulkaVoucherandJournal(){
   // Start transaction!
DB::beginTransaction();

try{
     
     $input=$req->all();
     $validator =Validator::make($input,[
  	       // 'bill_payment_date' => 'required',
            'bus_no' => 'required',
            'member_name'=>'required',
            'maasik_shulka'=>'required',
            
  ]);
  if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
        
        $model=new GeneralShulka();
        $model->bill_payment_date=Carbon::now();
        $model->bus_no=$req->input('bus_no');
        $model->member_name=$req->input('member_name');
        $model->maasik_shulka=$req->input('maasik_shulka');
        $model->kharid_bikri_darta_sulka=$req->input('kharid_bikri_darta_sulka');
        $model->nibedan_shulka=$req->input('nibedan_shulka');
        $model->membership_nabikarad_shulka=$req->input('membership_nabikarad_shulka');
        $model->naya_membership_shulka=$req->input('naya_membership_shulka');
        $model->naya_bus_register_shulka=$req->input('naya_bus_register_shulka');
        $model->jariwana_shulka=$req->input('jariwana_shulka');
        $model->queue_shatta_patta_shulka=$req->input('queue_shatta_patta_shulka');
        $model->identitycard_shulka=$req->input('identitycard_shulka');
        $model->sifaarish_shulka=$req->input('sifaarish_shulka');
        $model->staff_utpaadan_fund=$req->input('staff_utpaadan_fund');
        $model->share_saving_shulka=$req->input('share_saving_shulka');
        $model->utility_stricker_shulka=$req->input('utility_stricker_shulka');
        $model->photocopy_fee=$req->input('photocopy_fee');
        $model->added_by=$req->input('added_by');
        $model->company_id=$req->input('company_id');
        $model->save();
        return $this->sendResponse($model,'general shulka stored successfully!!');
        
 }
   catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
catch(ValidationException $e)
{
    // Rollback and then redirect
    // back to form with errors
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
} 

try {
    // Validate, then create if valid
    $newUser = User::create([
        'username' => Input::get('username'),
        'account_id' => $newAcct->id
    ]);
} 
catch(ValidationException $e)
{
    // Rollback and then redirect
    // back to form with errors
    DB::rollback();
    return Redirect::to('/form')
        ->withErrors( $e->getErrors() )
        ->withInput();
}
catch(\Exception $e)
{
    DB::rollback();
    throw $e;
}

// If we reach here, then
// data is valid and working.
// Commit the queries!
DB::commit();
 }
 public function deleteGShulka($busno){
     $get=GeneralShulka::where('bus_no',$busno)->first();
     if(is_null($get)){
         return $this->sendError('there is no bus of such id!');
     }
     else{
     $getpaymentdate=$get->bill_payment_date;
     $takentime=Carbon::parse($getpaymentdate)->addMinutes(3)->toDateTimeString();
    // $addsometime=Carbon::now()->addMinutes(1)->toDateTimeString();
     $timenow=Carbon::now()->toDateTimeString();
    // return $this->sendResponse($addsometime,'general shulka deleted successfully!!');
     if($timenow<=$takentime){
         $get->delete();
          return $this->sendResponse('done','general shulka deleted successfully!!');
     }
     else{
         return $this->sendError('you can not undo the transation now!');
     }
         
    }
 }
 public function UpdateGShulka(Request $req,$busno){
    $updateme=GeneralShulka::where('bus_no',$busno)->update(array('maasik_shulka',$req['maasik_shulka']));
    return $this->sendResponse($updateme,'general shulka updated successfully!!');
} 
 
    
}