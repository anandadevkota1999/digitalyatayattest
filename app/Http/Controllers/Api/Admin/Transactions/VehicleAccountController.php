<?php

namespace App\Http\Controllers\Api\Admin\Transactions;
use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Admin\Transactions\GeneralShulka;
use App\Models\Admin\Transactions\GeneralShulkaKharcha;
use App\Models\Admin\Transactions\GeneralKharchaDummy;
use App\Models\Admin\Transactions\JournalEntryOfGeneralShulka;
use App\Models\Admin\Transactions\FiscalYear;
use App\Models\Admin\Transactions\AccountPriceDeclaration;
use App\Models\Admin\Transactions\VehicleFixedAccount;
use App\Rules\Nepali_Calendar as NepaliCalender;
use App\Models\Users\busOwner;
use App\Models\Users\DummyBusOwner;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\Notification as Mynotification;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\DummyBusInfo;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Admin\adminInfo as admininfoResource;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class VehicleAccountController extends BaseController
{
   //store pricing of vehicles
   public function SetVehiclePricing(Request $req){
        DB::beginTransaction();
        $fiscalyear=FiscalYear::OrderBy('id','DESC')->first();
        $fyear=$fiscalyear->fiscal_year;
        $input=$req->all();
         // storing in fixed account table for fixed type
        if($req->input('payment_option')=='fixed'){
        $getpervdata=VehicleFixedAccount::where('bus_no',$req['bus_no'])->where('payment_topic',$req['payment_topic'])->where('payment_type',$req['payment_type'])->first();
        // return $this->sendResponse($getpervdata,'price for the vehicle is declared successfully!!');
        if(is_null($getpervdata)){
              //for both fixed and not fixed
        try{
        $validator =Validator::make($input,[
  	        'bus_no' => 'required',
  	        'payment_topic' => 'required',
  	        'payment_option' => 'required',
  	        'payment_price' => 'required',
  	        'payment_type' => 'required',
  	        
     ]);
  if($validator->fails()){
             DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);
            
         }
        $model=new AccountPriceDeclaration();
        $model->bus_no=$req->input('bus_no');
        $model->payment_topic=$req['payment_topic'];
        $model->payment_option=$req->input('payment_option');
        $model->payment_price=$req->input('payment_price');
        $model->payment_type=$req['payment_type'];
        $model->expiry_date=$req['expiry_date'];
        $model->company_id=$req->input('company_id');
        $model->added_by=$req->input('added_by');
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->fiscal_year=$fyear;
        $model->save();     
        }
           catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }
    //storing in fixed account
            try{
        $validator =Validator::make($input,[
  	        'bus_no' => 'required',
  	        'payment_topic' => 'required',
  	        'payment_price' => 'required',
  	        'payment_type' => 'required',
   ]);
  if($validator->fails()){
             DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);
         }
        
        $model=new VehicleFixedAccount();
        $model->bus_no=$req->input('bus_no');
        $model->payment_topic=$req['payment_topic'];
        $model->payment_price=$req->input('payment_price');
        $model->payment_type=$req['payment_type'];
        $model->expiry_date=$req['expiry_date'];
        $model->company_id=$req->input('company_id');
        $model->added_by=$req->input('added_by');
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->fiscal_year=$fyear;
        $model->save();     
         }
           catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }  
        }
        else{
                 return $this->sendError('You have already set the fix value, go to edit section to change the value on the topic!');
     /*   $busno=$getpervdata->bus_no;
        $paymenttopic=$getpervdata->payment_topic;
        $paymenttype=$getpervdata->payment_type;
       $countentries=AccountPriceDeclaration::where('payment_option','fixed')->where('')->count();
       if($countentries>1){}
        if($req->input('payment_option')=='fixed'&&$req['bus_no']==$busno&&$req['payment_topic']==$paymenttopic&&$req['payment_type']==$paymenttype){
    
          DB::commit(); 
             return $this->sendResponse('done','fix value has been already declared!');
         }
         try{
        $validator =Validator::make($input,[
  	        'bus_no' => 'required',
  	        'payment_topic' => 'required',
  	        'payment_price' => 'required',
  	        'payment_type' => 'required',
   ]);
  if($validator->fails()){
             DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);
         }
        
        $model=new VehicleFixedAccount();
        $model->bus_no=$req->input('bus_no');
        $model->payment_topic=$req['payment_topic'];
        $model->payment_price=$req->input('payment_price');
        $model->payment_type=$req['payment_type'];
        $model->expiry_date=$req['expiry_date'];
        $model->company_id=$req->input('company_id');
        $model->added_by=$req->input('added_by');
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->fiscal_year=$fyear;
        $model->save();     
         }
           catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }   */
        }
            
        }
        //for not-fixed type
        if($req->input('payment_option')!='fixed'){
             //for both fixed and not fixed
        try{
        $validator =Validator::make($input,[
  	        'bus_no' => 'required',
  	        'payment_topic' => 'required',
  	        'payment_option' => 'required',
  	        'payment_price' => 'required',
  	        'payment_type' => 'required',
  	        
     ]);
  if($validator->fails()){
             DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);
            
         }
        $model=new AccountPriceDeclaration();
        $model->bus_no=$req->input('bus_no');
        $model->payment_topic=$req['payment_topic'];
        $model->payment_option=$req->input('payment_option');
        $model->payment_price=$req->input('payment_price');
        $model->payment_type=$req['payment_type'];
        $model->expiry_date=$req['expiry_date'];
        $model->company_id=$req->input('company_id');
        $model->added_by=$req->input('added_by');
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->fiscal_year=$fyear;
        $model->save();     
        }
           catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }

        }
    //notify vehicle owner of set topic with end date
        $getownerofbus=businfo::where('bus_info.bus_no',$req['bus_no'])->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->first();   
    //return $this->sendResponse($getownerofbus,'Price for the vehicle is declared successfully!!');
         // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
     //notification contents:
            $notify=array('title'=>$req['payment_topic'].' price declared','body'=>'dear '.$getownerofbus->owner_name.' your '.$req['payment_topic'].' payment is on '.$req['expiry_date'].' and payment price is '.$req['payment_price']);
      //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
    //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',]; 
        //get owner token
        $ownerFcmToken=$getownerofbus->device_token; 
        try{
        $store=new Mynotification();
             $store->type='Vehicle payment declaration';
            $store->notifiable_id=$getownerofbus->owner_id;
            $store->notifiable_type=busOwner::class;
            $store->title=$req['payment_topic'].' price declared';
            $store->body='dear '.$getownerofbus->owner_name.' your '.$req['payment_topic'].' payment is on '.$req['expiry_date'].' and payment price is '.$req['payment_price'];
            $store->added_by=$req['added_by'];
             $store->created_at=Carbon::now();
            $store->company_id=$getownerofbus->company_id;
            $store->save();
        }
        catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
        catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }
         $ch = curl_init();  
        //sending request field:for whom(reciever) and for what(data):::for driver update
        $fields= json_encode(array( 'to'=> $ownerFcmToken,
        'notification'=>$notify));  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
     // Close connection
        curl_close($ch);

   
   
     DB::commit(); 
    return $this->sendResponse('done','Price for the vehicle is declared successfully!!');
   
   }
   //update fixed payment price monthly
   public function updateFixedaccountperiodically(){
        DB::beginTransaction();
        $fiscalyear=FiscalYear::OrderBy('id','DESC')->first();
        $fyear=$fiscalyear->fiscal_year;
       $showupdateddays=[];
       $getfixedexpirevalues=VehicleFixedAccount::whereDate('expiry_date','<',Carbon::now())->get();
       foreach($getfixedexpirevalues as $expiredvalues){
        $getexpirydate=$expiredvalues->expiry_date;
         $parsedate=Carbon::parse($getexpirydate);
         $addmonth=$parsedate->addDays(30);
         $expiredvalues->expiry_date=$addmonth;
         $expiredvalues->save();
          $showupdateddays[]=[
              'updated day'=>$expiredvalues,
              ];
       }
        DB::commit(); 
       return $this->sendResponse($showupdateddays,'expired days has been updated successfully!!');
   }
   //add fixed amount to the specific topic monthly
   public function addFixedAmountOnSpecificTopicPayment(){
       DB::beginTransaction();
       $getfixedtopics=AccountPriceDeclaration::where('payment_option','fixed')->get();
       $get=[];
       foreach($getfixedtopics as $fixedtopics){
           $paymenttopic=$fixedtopics->payment_topic;
           $paymentprice=$fixedtopics->payment_price;
           $expirydate=$fixedtopics->expiry_date;
           $paymenttype=$fixedtopics->payment_type;
           $parsedate=Carbon::parse($expirydate);
           $addonemonth=$parsedate->addDays(30)->toDateString();
           $today=Carbon::now()->toDateString();
           $countanyshulka=$fixedtopics->where('payment_topic',$paymenttopic)->where('payment_option','fixed')->where('payment_type',$paymenttype)->whereDate('expiry_date','>=',$today)->count('id'); 
      //  return $this->sendResponse($countanyshulka,'data that is of one month to the expiry date is added  successfully!!');
         if($countanyshulka<1){
           if($today==$addonemonth&&$paymenttype=$paymenttype){
               //get price from fixed account of specific topic
             $paymenttopic=$fixedtopics->payment_topic;
             if($paymenttopic==$paymenttopic){
            $shulka=VehicleFixedAccount::where('payment_topic',$paymenttopic)->first();
            if(!is_null($shulka)){
            $shulkaprice=$shulka->payment_price;
            //then add the price to the vehicle aacount
            $model=new AccountPriceDeclaration();
            $model->bus_no=$fixedtopics->bus_no;
            $model->payment_topic=$fixedtopics->payment_topic;
            $model->payment_option=$fixedtopics->payment_option;
            $model->payment_price=$shulkaprice;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$addonemonth;
            $model->company_id=$fixedtopics->company_id;
            $model->added_by=$fixedtopics->added_by;
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$fixedtopics->fiscal_year;
            $model->save();  
            $get[]=['datas'=>$model];
            }
             }
           }  
         }
      /*   if($countanyshulka>1){
               return $this->sendError('data has already been added this month on this topic!!'); 
         } */
         $get[]=['datas'=>$countanyshulka];
       } 
       DB::commit(); 
       return $this->sendResponse($get,'data that is of one month to the expiry date is added  successfully!!');
   }
   
    
}