<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Admin\Sms;
use App\Models\Admin\Route;
use App\Models\Admin\VehicleRoute;
use App\Models\Admin\VehicleOwnershipTransfer as OwnershipTransfer;
use App\Models\Users\busOwner;
use App\Models\Users\DummyBusOwner;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\DummyBusInfo;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Admin\adminInfo as admininfoResource;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class VehicleOwnershipTransferController extends BaseController
{
    //change ownership
  function shiftOwnership(Request $req){
        DB::beginTransaction();
      try{
         $validator = Validator::make($req->all(), [
            'old_owner_id' => 'required',
             'new_owner_id' => 'required',
             'bus_id'=>'required',
             'company_id'=>'required',
             'trasfer_date'=>'required',
        ],[
          //   'route_number.unique'=>'Route already exists,please try another one!!',
            ]
            );
        if($validator->fails()){
                DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
        $inputdatas=$req->all();
       // $inputdatas['trasfer_date']=Carbon::now()->toDateTimeString();
        $inputdatas['created_at']=Carbon::now()->toDateTimeString();
        $inputdatas['updated_at']=Carbon::now()->toDateTimeString();
        $storenow=OwnershipTransfer::create($inputdatas);
        $getownerphone=busOwner::where('owner_id',$req['new_owner_id'])->first('owner_phone');
        $newownerphone=$getownerphone->owner_phone;
        //updating bus table for ownership change
        $updateownerphone=businfo::where('bus_id',$req['bus_id'])->update([
            'owner_phone'=>$newownerphone,'updated_at'=>Carbon::now(),'added_by'=>$req['added_by']
            ]);
        DB::commit();
         return $this->sendResponse($storenow,'Vehicle ownership transferred successfully!!'); 
  }
   catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
      }
  }
  //show ownership transfer records of company
  function showOwnershipRecords($companyid){
      $showdata=OwnershipTransfer::where('vehicle_ownership_transfer.company_id',$companyid)->join('bus_owner as old_owner','vehicle_ownership_transfer.old_owner_id','old_owner.owner_id')->join('bus_owner as new_owner','vehicle_ownership_transfer.new_owner_id','new_owner.owner_id')->join('bus_info','vehicle_ownership_transfer.bus_id','bus_info.bus_id')->select('vehicle_ownership_transfer.*','old_owner.owner_name as old_owner_name','old_owner.owner_phone as old_owner_phone','new_owner.owner_name as new_owner_name','new_owner.owner_phone as new_owner_phone','bus_info.bus_id','bus_info.bus_no')->orderBy('vehicle_ownership_transfer.id','desc')->get();
  return $this->sendResponse($showdata,'Vehicle ownership transferred successfully!!'); 
  }
  //update ownership in case of mistaken
  function updateOwnershipIfMistaken($busid,Request $req){
       DB::beginTransaction();
       try{
            $validator = Validator::make($req->all(), [
            'old_owner_id' => 'required',
             'new_owner_id' => 'required',
            // 'bus_id'=>'required',
             'company_id'=>'required',
             'trasfer_date'=>'required',
        ],[
          //   'route_number.unique'=>'Route already exists,please try another one!!',
            ]
            );
        if($validator->fails()){
                DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
        //checking if old owner is same:
        $getoldownerdetail=OwnershipTransfer::where('bus_id',$busid)->orderBy('id','DESC')->first();
        $getoldownerid=$getoldownerdetail->old_owner_id;
        if($req['new_owner_id']==$getoldownerid){
        DB::rollback();
        return $this->sendError('Duplicate ownership, Can not transfer ownership to same person!!!');           
        }
        if($getoldownerid==$req['old_owner_id']){
        $getlatestonly=OwnershipTransfer::where('bus_id',$busid)->where('old_owner_id',$getoldownerid)->orderBy('id','DESC')->first();
        $updateownership=$getlatestonly->update([
            'new_owner_id'=>$req['new_owner_id'],'trasfer_date'=>$req['trasfer_date'],'updated_at'=>Carbon::now(),'added_by'=>$req['added_by']
            ]);
      $getownerphone=busOwner::where('owner_id',$req['new_owner_id'])->first('owner_phone');
        $newownerphone=$getownerphone->owner_phone;
        //also,updating bus table for ownership change
        $updateownerphone=businfo::where('bus_id',$req['bus_id'])->update([
            'owner_phone'=>$newownerphone,'updated_at'=>Carbon::now(),'added_by'=>$req['added_by']
            ]);
        DB::commit();
         return $this->sendResponse($updateownership,'Vehicle ownership transferred successfully!!'); 

       }
       else {
             DB::rollback();
       return $this->sendError('Selected owner is not matching to the old owner information!!!');      
       }
       }
        catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
      }
  }
  
  
}