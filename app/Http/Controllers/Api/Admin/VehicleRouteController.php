<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Admin\Sms;
use App\Models\Admin\Route;
use App\Models\Admin\VehicleRoute;
use App\Models\Users\busOwner;
use App\Models\Users\DummyBusOwner;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\DummyBusInfo;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Admin\adminInfo as admininfoResource;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class VehicleRouteController extends BaseController
{
    function createRoutes(Request $req){
         $validator = Validator::make($req->all(), [
            'route_number' => 'required|unique:route_info',
             'company_id' => 'required',
             'route_from'=>'required',
             'route_to'=>'required',
        ],[
             'route_number.unique'=>'Route already exists,please try another one!!',
            ]
            );
        if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
        $inputdatas=$req->all();
        $inputdatas['created_at']=Carbon::now()->toDateTimeString();;
        $inputdatas['updated_at']=Carbon::now()->toDateTimeString();;
         $storenow=Route::create($inputdatas);
         return $this->sendResponse($storenow,'New ROute has been created successfully!!');  

    }
    function SetVehicleRoute(Request $req){
        try{
         $validator = Validator::make($req->all(), [
            'route_id' => 'required',
             'bus_id' => 'required|unique:vehicle_route_info',
             'company_id'=>'required',
        ],['bus_id.unique'=>'Already assigned to a route,please reomve this vehicle from other route first!']
        );
        if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
        $checkforvehicleassignment=Route::join('vehicle_route_info','route_info.id','vehicle_route_info.route_id')->where('vehicle_route_info.route_id',$req['route_id'])->where('vehicle_route_info.bus_id',$req['bus_id'])->select('route_info.id','vehicle_route_info.*')->first();
     //     return $this->sendResponse($checkforvehicleassignment,'Vehicle route has been declared successfully!!');
      if(is_null($checkforvehicleassignment)){
        $inputdatas=$req->all();
        $inputdatas['created_at']=Carbon::now()->toDateTimeString();;
        $inputdatas['updated_at']=Carbon::now()->toDateTimeString();;
         $storenow=VehicleRoute::create($inputdatas);
         return $this->sendResponse($storenow,'Vehicle route has been declared successfully!!'); 
      }
         else{
            return $this->sendError('vehicle is already registered in the given route!'); 
         }
        }
        catch ( \Exception $ex ){
         $errorCode = $ex->errorInfo[1];
          if($errorCode == '1452'){
              return $this->sendError('There is no any route of such given id!!');
          }
        return $this->sendError(($ex->getMessage())); 
    }
    }
    function getAllRoute($companyid){
        $getall=Route::where('company_id',$companyid)->orderBy('route_info.route_number','ASC')->get();
        return $this->sendResponse($getall,'all routes is shown successfully!!');
    }
    function deleteRoute(Request $req,$routeid){
        $checkforvehicleassignment=Route::join('vehicle_route_info','route_info.id','vehicle_route_info.route_id')->where('route_info.id',$routeid)->select('route_info.id','vehicle_route_info.*')->first();
        if(is_null($checkforvehicleassignment)){
        $redytodelete=Route::where('route_info.id',$routeid)->first();
        $redytodelete->delete();
        return $this->sendResponse($checkforvehicleassignment,'route deleted successfully!!'); 
        }
       else{
           return $this->sendError('Selected Route is already assoiciated with vehicles!!');
       }
    }
    function editRoute(Request $req,$id){
        try{
        $updatesingleroute=Route::where('id',$id)->update(['route_from'=>$req->input('route_from'),'route_to'=>$req->input('route_to'),'route_name'=>$req->input('route_name'),'route_number'=>$req->input('route_number')
        ,'route_type'=>$req->input('route_type'),'added_by'=>$req->input('added_by'),'updated_at'=>Carbon::now()]);
        $getdata=Route::where('id',$id)->first();
     return $this->sendResponse($getdata,'Route updated successfully!!'); 
        }
        catch ( \Exception $ex ){ 
         $errorCode = $ex->errorInfo[1];
          if($errorCode == '1062'){
              return $this->sendError('Given route number is already declared please choose new one!!');
          }
      return $this->sendError(($ex->getMessage())); 
    }
           }
    function getVehicleWithRouteDetail($companyid){
        $getallvehiclesofroutes=VehicleRoute::where('vehicle_route_info.company_id',$companyid)->join('route_info','vehicle_route_info.route_id','route_info.id')->join('bus_info','vehicle_route_info.bus_id','bus_info.bus_id')->join('bus_owner','bus_owner.owner_phone','bus_info.owner_phone')->select('vehicle_route_info.id','vehicle_route_info.route_id','route_info.route_name','route_info.route_number','bus_info.bus_no','bus_info.bus_type','bus_owner.owner_name','bus_owner.owner_phone')->orderBy('route_info.route_number', 'asc')->get();
    return $this->sendResponse($getallvehiclesofroutes,'vehicles with routes shown successfully!!');
    }
    function getIndivisualVehicleRouteDetail($routeno){
    $getdetails=VehicleRoute::where('route_info.route_number',$routeno)->join('route_info','vehicle_route_info.route_id','route_info.id')->join('bus_info','vehicle_route_info.bus_id','bus_info.bus_id')->join('bus_owner','bus_owner.owner_phone','bus_info.owner_phone')->select('vehicle_route_info.id','vehicle_route_info.route_id','route_info.route_name','route_info.route_number','bus_info.bus_no','bus_info.bus_type','bus_owner.owner_name','bus_owner.owner_phone')->orderBy('route_info.route_number', 'asc')->get();    
   if(is_null($getdetails)){
      return $this->sendResponse('null','no data found in this route!!'); 
   }
    else{
        return $this->sendResponse($getdetails,'vehicles of different routes shown successfully!!');
    }
    }
    function deleteVehiclefromRoute($id){
        $getdata=VehicleRoute::where('id',$id)->first();
        if(is_null($getdata)){
        return $this->sendError('records does not exist on this id!!');    
        }
        else{
        $getdata->delete();
        return $this->sendResponse($getdata,'vehicles from the route deleted successfully!!');
        }
    }
    function ShowVehicleswithoutRoute($companyid){
    $getrightonly=businfo::where('bus_info.company_id',$companyid)->leftJoin('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->leftJoin('vehicle_route_info','bus_info.bus_id','vehicle_route_info.bus_id')->where('bus_info.active_status','yes')->whereNull('vehicle_route_info.bus_id')->select('bus_info.bus_id','bus_info.bus_no','bus_info.bus_type','bus_owner.owner_name','bus_owner.owner_phone','bus_owner.owner_address')->get();
   return $this->sendResponse($getrightonly,'vehicles shown successfully!!');
    }
     function showAndCountVehiclesofRoute($routeno){
          \DB::statement("SET SQL_MODE=''");
    $getdetails=VehicleRoute::where('route_info.route_number',$routeno)->join('route_info','vehicle_route_info.route_id','route_info.id')->join('bus_info','vehicle_route_info.bus_id','bus_info.bus_id')->groupBy('vehicle_route_info.route_id')->select('route_info.*',DB::raw('group_concat(bus_info.bus_no) as bus_no'),DB::raw('COUNT(vehicle_route_info.bus_id) as total_bus'))->get();
     return $this->sendResponse($getdetails,'vehicles shown successfully!!');
        
    }
    function showDifferentRouteOfCompany($companyid){
         \DB::statement("SET SQL_MODE=''");
       $getdetails=VehicleRoute::where('vehicle_route_info.company_id',$companyid)->join('route_info','vehicle_route_info.route_id','route_info.id')->join('bus_info','vehicle_route_info.bus_id','bus_info.bus_id')->groupBy('vehicle_route_info.route_id')->join('bus_owner','bus_owner.owner_phone','bus_info.owner_phone')->select('vehicle_route_info.id','vehicle_route_info.route_id','route_info.route_name','route_info.route_number',DB::raw('group_concat(bus_info.bus_no) as bus_no'))->orderBy('route_info.route_number', 'asc')->get();    
   if(is_null($getdetails)){
      return $this->sendError('There are no vehicles assinged to any routes!!'); 
   }
    else{
        return $this->sendResponse($getdetails,'vehicles of different routes shown successfully!!');
    }
   
    }
}