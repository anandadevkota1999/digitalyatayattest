<?php

namespace App\Http\Controllers\Api\Admin;

use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Admin\Sms;
use App\Models\Admin\VehicleRoute;
use App\Models\Users\busOwner;
use App\Models\Users\DummyBusOwner;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\DummyBusInfo;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Admin\adminInfo as admininfoResource;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Rules\Nepali_Calendar as NepaliCalender;

class smsController extends BaseController
{
    //declaring price value for the company
    function SetInitialPriceValue(Request $req){
        $sms=new Sms();
        $sms->sms_to=$req['to'];
        $sms->receiver_name=$req['receiver_name'];
        $sms->sms_message='for initial entry to set sms balance for company!';
        $sms->created_at=Carbon::now();
        $sms->updated_at=Carbon::now();
        $sms->company_id=$req['company_id'];
        $sms->added_by=$req['added_by'];
        $sms->initial_balance=$req['initial_balance'];
        $sms->remaining_balance=$req['remaining_balance'];
        $sms->save();
       return $this->sendResponse($sms,'Initial balance has been setup for the company successfully!!!');

    }
     function sendSMS($content)
        {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,"http://sms.cosmosnepal.net/api/v3/sms?");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$content);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);
       // $smstoken=LiBLVuY08EfqCMBF1432gS08XSQpCOxpsH2N;
        return $server_output;
        }
        //send sms to vehicle owner
    function smsTosingleVehicleOwner($phone,Request $req){
        $token = 'LiBLVuY08EfqCMBF1432gS08XSQpCOxpsH2N';
        $to = $phone;
        $sender    = '1010';
        $message = $req['message'];
      // set post fields
        $content =[
        'token'=>$token,
        'to'=>$to,
        'sender'=>$sender,
        'message'=>$message,
        ];
        $getremainingbalance=Sms::query()->orderBy('id','desc')->first('remaining_balance');
        $remainingbalance=$getremainingbalance->remaining_balance;
        //assuming deducted value in the sms
        $deductedvalue=1.5;
        $newremainingbalance=$remainingbalance-$deductedvalue;
        if($newremainingbalance>0){
        $send_with_content =$this->sendSMS($content);
        $datas = json_decode($send_with_content);
        if($datas->total_numbers==0){
            return $this->sendError('Message could not be sent!!');
        }
        else{
        $getcurrentbalance=$datas->current_balance;
        //store in db
        $sms=new Sms();
        $sms->sms_to=$to;
        $sms->receiver_name=$req['receiver_name'];
        $sms->sms_message=$message;
        $sms->created_at=Carbon::now();
        $sms->updated_at=Carbon::now();
        $sms->company_id=$req['company_id'];
        $sms->added_by=$req['added_by'];
        $sms->initial_balance='5';
        $sms->remaining_balance=$newremainingbalance;
        $sms->save();
     //return \Response::json($send_with_content, 200);
       return $this->sendResponse($datas,'msg sent!');
            
        }
            
        }
        
        else{
               return $this->sendError('There is not enough balance in your account, please recharge soon!!'); 
        } 
        
    }
        //send sms to 
    function bulkSmsToDifferentMembers(Request $req,$ids){
        $token = 'LiBLVuY08EfqCMBF1432gS08XSQpCOxpsH2N';
        $sender    = '1010';
        $message=$req['message'];
         $getcurrentbalance=$datas->current_balance;
        $getinitialbalance=Sms::query()->orderBy('id','desc')->first();
        $initialbalance=$getinitialbalance->initial_balance;
  
        switch ($ids) {
             //bulk sms to owner
            case 'owner':
                $getownersofcompany=busOwner::where('company_id',$req['company_id'])->get('owner_phone');
                $countallowners=busOwner::where('company_id',$req['company_id'])->count();
                $getremainingbalance=Sms::query()->orderBy('id','desc')->first('remaining_balance');
                $remainingbalance=$getremainingbalance->remaining_balance;
                //assuming deducted value in the sms
                $deductedvalue=1.5*$countallowners;
                $newremainingbalance=$remainingbalance-$deductedvalue;
                if($newremainingbalance>0){
                $a=$getownersofcompany->toArray();
               //  return $this->sendResponse($a,'msg sent!'); 
                $b=implode(",", array_column($a, "owner_phone"));
                // return $this->sendResponse($b,'msg sent!');  
                $to=$b;
                if(empty($to)){
             return $this->sendError('No vehicle owner records in the system!!'); 
         }
         $content =[
        'token'=>$token,
        'to'=>$to,
        'sender'=>$sender,
        'message'=>$message,
        ];
        $send_with_content =$this->sendSMS($content);
        $datas = json_decode($send_with_content);
        if($datas->total_numbers==0){
            return $this->sendError('Message could not be sent!!');
        }
        else{
              
             //   return $this->sendResponse($datas,'msg sent!');    
              
                 
        //store in db
        $sms=new Sms();
        $sms->sms_to='1234567890';
        $sms->receiver_name='bulk owner';
        $sms->sms_message=$message;
        $sms->created_at=Carbon::now();
        $sms->updated_at=Carbon::now();
        $sms->company_id=$req['company_id'];
        $sms->added_by=$req['added_by'];
        $sms->sms_type='bulk';
        $sms->initial_balance=$initialbalance;
        $sms->remaining_balance=$newremainingbalance;
        $sms->save();
        return $this->sendResponse($datas,'bulk owner msg sent!');    
        }
                }
                 else{
               return $this->sendError('There is not enough balance in your account, please recharge soon!!'); 
        } 
         break;
        case 'staff':
         $getstaffofcompany=StaffInfo::where('company_id',$req['company_id'])->get('staff_phone');
         $a=$getstaffofcompany->toArray();
         $b=implode(",", array_column($a, "staff_phone"));
         $to=$b;
         $countallstaff=StaffInfo::where('company_id',$req['company_id'])->count();
         $getremainingbalance=Sms::query()->orderBy('id','desc')->first('remaining_balance');
         $remainingbalance=$getremainingbalance->remaining_balance;
         //assuming deducted value in the sms
         $deductedvalue=1.5*$countallstaff;
         $newremainingbalance=$remainingbalance-$deductedvalue;
         if($newremainingbalance>0){
                
         if(empty($to)){
             return $this->sendError('No Staff records in the system!!'); 
         }
         $content =[
        'token'=>$token,
        'to'=>$to,
        'sender'=>$sender,
        'message'=>$message,
        ];
        $send_with_content =$this->sendSMS($content);
        $datas = json_decode($send_with_content);
        if($datas->total_numbers==0){
            return $this->sendError('Message could not be sent!!');
        }
        else{
        $getcurrentbalance=$datas->current_balance;
        //store in db
        $sms=new Sms();
        $sms->sms_to='1234567891';
        $sms->receiver_name='bulk staff';
        $sms->sms_message=$message;
        $sms->created_at=Carbon::now();
        $sms->updated_at=Carbon::now();
        $sms->company_id=$req['company_id'];
        $sms->added_by=$req['added_by'];
        $sms->sms_type='bulk';
        $sms->initial_balance=$initialbalance;
        $sms->remaining_balance=$newremainingbalance;
        $sms->save();
        return $this->sendResponse($datas,'bulk staff msg sent!');    
        }
                }
         else{
               return $this->sendError('There is not enough balance in your account, please recharge soon!!'); 
        } 
         break;
        case 'driver':
         $getdriverofcompany=Driver::where('company_id',$req['company_id'])->get('driver_phone');
         $a=$getdriverofcompany->toArray();
         $b=implode(",", array_column($a, "driver_phone"));
         $to=$b;
         $countalldriver=Driver::where('company_id',$req['company_id'])->count();
         $getremainingbalance=Sms::query()->orderBy('id','desc')->first('remaining_balance');
         $remainingbalance=$getremainingbalance->remaining_balance;
         //assuming deducted value in the sms
         $deductedvalue=1.5*$countalldriver;
         $newremainingbalance=$remainingbalance-$deductedvalue;
         if($newremainingbalance>0){

          if(empty($to)){
             return $this->sendError('No driver records in the system!!'); 
         }
       // return $this->sendResponse($to,'bulk driver msg sent!');  
         $content =[
        'token'=>$token,
        'to'=>$to,
        'sender'=>$sender,
        'message'=>$message,
        ];
        $send_with_content =$this->sendSMS($content);
        $datas = json_decode($send_with_content);
       // return $this->sendResponse($datas,'bulk driver msg sent!'); 
        if($datas->total_numbers==0){
            return $this->sendError('Message could not be sent!!');
        }
        else{
        $getcurrentbalance=$datas->current_balance;
        //store in db
        $sms=new Sms();
        $sms->sms_to='1234567892';
        $sms->receiver_name='bulk driver';
        $sms->sms_message=$message;
        $sms->created_at=Carbon::now();
        $sms->updated_at=Carbon::now();
        $sms->company_id=$req['company_id'];
        $sms->added_by=$req['added_by'];
        $sms->sms_type='bulk';
        $sms->initial_balance=$initialbalance;
        $sms->remaining_balance=$newremainingbalance;
        $sms->save();
        return $this->sendResponse($datas,'bulk driver msg sent!');    
        }
         }
         else{
               return $this->sendError('There is not enough balance in your account, please recharge soon!!'); 
        } 
         
         break; 
        case 'all':
         $getstaffofcompany=StaffInfo::where('company_id',$req['company_id'])->get('staff_phone');
         $arrstaff=$getstaffofcompany->toArray();
         $implodestaff=implode(",", array_column($arrstaff, "staff_phone"));
         $getownersofcompany=busOwner::where('company_id',$req['company_id'])->get('owner_phone');
         $arrowner=$getownersofcompany->toArray();
         $implodeowner=implode(",", array_column($arrowner, "owner_phone"));
         $getdriverofcompany=Driver::where('company_id',$req['company_id'])->get('driver_phone');
         $arrdriver=$getdriverofcompany->toArray();
         $implodedriver=implode(",", array_column($arrdriver, "driver_phone"));
         $to=$implodestaff.','.$implodeowner.','.$implodedriver;
         $countalldriver=Driver::where('company_id',$req['company_id'])->count();
         $countallstaff=StaffInfo::where('company_id',$req['company_id'])->count();
         $countallowners=busOwner::where('company_id',$req['company_id'])->count();
         $countall=$countalldriver+$countallstaff+$countallowners;
         $getremainingbalance=Sms::query()->orderBy('id','desc')->first('remaining_balance');
         $remainingbalance=$getremainingbalance->remaining_balance;
         //assuming deducted value in the sms
         $deductedvalue=1.5*$countall;
         $newremainingbalance=$remainingbalance-$deductedvalue;
         if($newremainingbalance>0){
         if(empty($to)){
             return $this->sendError('No valid phone numbers in the system!!'); 
         }
         //return $this->sendResponse($to,'bulk all msg sent!');
         $content =[
        'token'=>$token,
        'to'=>$to,
        'sender'=>$sender,
        'message'=>$message,
        ];
        $send_with_content =$this->sendSMS($content);
        $datas = json_decode($send_with_content);
        if($datas->total_numbers==0){
            return $this->sendError('Message could not be sent!!');
        }
        else{
        $getcurrentbalance=$datas->current_balance;
        //store in db
        $sms=new Sms();
        $sms->sms_to='1234567893';
        $sms->receiver_name='bulk all';
        $sms->sms_message=$message;
        $sms->created_at=Carbon::now();
        $sms->updated_at=Carbon::now();
        $sms->company_id=$req['company_id'];
        $sms->added_by=$req['added_by'];
        $sms->sms_type='bulk';
        $sms->initial_balance=$initialbalance;
        $sms->remaining_balance=$newremainingbalance;
        $sms->save();
        return $this->sendResponse($datas,'bulk all msg sent!');    
        }
         }
          else{
               return $this->sendError('There is not enough balance in your account, please recharge soon!!'); 
        }
        
        break;
         }
    }
    //send sms of end dates to owners
    function smsEndDateOwner(){
         //get owner of bus:
       $getownerofbus=businfo::join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->select('bus_owner.*','bus_info.*')->get();
       $dataarray=[];
       foreach($getownerofbus as $ownerdata){
        $token = 'LiBLVuY08EfqCMBF1432gS08XSQpCOxpsH2N';
        
        $ownerphone=$ownerdata->owner_phone; 
        $ownername=$ownerdata->owner_name; 
        $busno=$ownerdata->bus_no;
        $bustype=$ownerdata->bus_type;
        $billbookendate=$ownerdata->bus_billbook_enddate;
        $routepermitendate=$ownerdata->bus_routepermit_enddate;
        $checkpassendate=$ownerdata->bus_checkpass_enddate;
        $insuranceendate=$ownerdata->bus_insurance_enddate;
        
        //today date conversion to nepali
        $callcalender=new NepaliCalender();
       $todaydate=Carbon::now()->toDateString();
       $correctformatfornepali=str_replace('-', ',',$todaydate);
       $todaydatearray=explode (",", $correctformatfornepali);
       $tyear=$todaydatearray[0];
       $tmonth=$todaydatearray[1];
       $tday=$todaydatearray[2];
       $convertonepali= $callcalender->eng_to_nep($tyear,$tmonth,$tday);
       $todateformat=str_replace(',', '-',$convertonepali);
       $nslice =array_shift($convertonepali);
       //implding to string format
       $ntostring=implode(",",$convertonepali);
       //then,nepali month and day value
       $nmonthandday = str_replace( ',', ' ', $ntostring );
       $getremainingbalance=Sms::query()->orderBy('id','desc')->first();
       $to = $ownerphone;
        $sender = '1010';
     
      
       //------------*******billbook sms 3 day before the end date ****** 
          $bwarnbmonthday=Carbon::parse($billbookendate)->subDays('3');
          $bwdateonly=$bwarnbmonthday->toDateString();
          $bwarnformat=str_replace('-', ',',$bwdateonly);
          $bwmakeitarray = explode (",", $bwarnformat); 
            //then,finally get month and year in array
             $bwmonths=$bwmakeitarray[1];
             $bwmonth=ltrim($bwmonths, '0');
            $bwdays=$bwmakeitarray[2];
            $bwday=ltrim($bwdays, '0');
            $bwarnmonthandday=$bwmonth.' '.$bwday;
       
        if($nmonthandday==$bwarnmonthandday){
        $remainingbalance=$getremainingbalance->remaining_balance;
        //assuming deducted value in the sms
        $deductedvalue=1.5;
           $message = 'Dear, '.$ownername.' your '.$bustype.': '.$busno.'  bluebook end-date  is at: '.$billbookendate.', please pay in time!!, thank you.';
           // set post fields
        $content =[
        'token'=>$token,
        'to'=>$to,
        'sender'=>$sender,
        'message'=>$message,
        ];
        $newremainingbalance=$remainingbalance-$deductedvalue;
        if($newremainingbalance>=0){
        $send_with_content =$this->sendSMS($content);
        $datas = json_decode($send_with_content);
        if($datas->total_numbers==0){
            return $this->sendError('Message could not be sent!!');
        }
        else{
        //store in db
        $sms=new Sms();
        $sms->sms_to=$to;
        $sms->receiver_name=$ownername;
        $sms->sms_message=$message;
        $sms->created_at=Carbon::now();
        $sms->updated_at=Carbon::now();
        $sms->company_id='100';
        $sms->added_by='system generated';
        $sms->initial_balance=$getremainingbalance->initial_balance;
        $sms->remaining_balance=$newremainingbalance;
        $sms->save();
          $dataarray=['msg sent to'=>$ownername];
      // return $this->sendResponse($datas,'msg sent!');
        }
            
        }
        
        else{
               return $this->sendError('There is not enough balance in your account, please recharge soon!!'); 
        }  
        } 
        
          //------------*******route permit sms 3 day before the  end date ****** 
          $rwarnbmonthday=Carbon::parse($routepermitendate)->subDays('3');
          $rwdateonly=$rwarnbmonthday->toDateString();
          $rwarnformat=str_replace('-', ',',$rwdateonly);
          $rwmakeitarray = explode (",", $rwarnformat); 
            //then,finally get month and year in array
             $rwmonths=$rwmakeitarray[1];
             $rwmonth=ltrim($rwmonths, '0');
            $rwdays=$rwmakeitarray[2];
            $rwday=ltrim($rwdays, '0');
            $rwarnmonthandday=$rwmonth.' '.$rwday;
       
        if($nmonthandday==$rwarnmonthandday){
        $remainingbalance=$getremainingbalance->remaining_balance;
        //assuming deducted value in the sms
        $deductedvalue=1.5;
           $message = 'Dear, '.$ownername.' your '.$bustype.': '.$busno.' route permit end-date is at: '.$routepermitendate.', please pay in time!!, thank you.';
           // set post fields
        $content =[
        'token'=>$token,
        'to'=>$to,
        'sender'=>$sender,
        'message'=>$message,
        ];
        $newremainingbalance=$remainingbalance-$deductedvalue;
        if($newremainingbalance>=0){
        $send_with_content =$this->sendSMS($content);
        $datas = json_decode($send_with_content);
        if($datas->total_numbers==0){
            return $this->sendError('Message could not be sent!!');
        }
        else{
        //store in db
        $sms=new Sms();
        $sms->sms_to=$to;
        $sms->receiver_name=$ownername;
        $sms->sms_message=$message;
        $sms->created_at=Carbon::now();
        $sms->updated_at=Carbon::now();
        $sms->company_id='100';
        $sms->added_by='system generated';
        $sms->initial_balance=$getremainingbalance->initial_balance;
        $sms->remaining_balance=$newremainingbalance;
        $sms->save();
          $dataarray=['msg sent to'=>$ownername];
       //return $this->sendResponse($datas,'msg sent!');
        }
            
        }
        
        else{
               return $this->sendError('There is not enough balance in your account, please recharge soon!!'); 
        }  
        }
        
        //------------*******check pass sms 3 day before the end date ****** 
          $cwarnbmonthday=Carbon::parse($checkpassendate)->subDays('3');
          $cwdateonly=$cwarnbmonthday->toDateString();
          $cwarnformat=str_replace('-', ',',$cwdateonly);
          $cwmakeitarray = explode (",", $cwarnformat); 
            //then,finally get month and year in array
             $cwmonths=$cwmakeitarray[1];
             $cwmonth=ltrim($cwmonths, '0');
            $cwdays=$cwmakeitarray[2];
            $cwday=ltrim($cwdays, '0');
            $cwarnmonthandday=$cwmonth.' '.$cwday;
       
        if($nmonthandday==$cwarnmonthandday){
        $remainingbalance=$getremainingbalance->remaining_balance;
        //assuming deducted value in the sms
        $deductedvalue=1.5;
           $message = 'Dear, '.$ownername.' your '.$bustype.': '.$busno.' checkpass end-date is at: '.$checkpassendate.', please pay in time!!, thank you.';
           // set post fields
        $content =[
        'token'=>$token,
        'to'=>$to,
        'sender'=>$sender,
        'message'=>$message,
        ];
        $newremainingbalance=$remainingbalance-$deductedvalue;
        if($newremainingbalance>=0){
        $send_with_content =$this->sendSMS($content);
        $datas = json_decode($send_with_content);
        if($datas->total_numbers==0){
            return $this->sendError('Message could not be sent!!');
        }
        else{
        //store in db
        $sms=new Sms();
        $sms->sms_to=$to;
        $sms->receiver_name=$ownername;
        $sms->sms_message=$message;
        $sms->created_at=Carbon::now();
        $sms->updated_at=Carbon::now();
        $sms->company_id='100';
        $sms->added_by='system generated';
        $sms->initial_balance=$getremainingbalance->initial_balance;
        $sms->remaining_balance=$newremainingbalance;
        $sms->save();
          $dataarray=['msg sent to'=>$ownername];
       //return $this->sendResponse($datas,'msg sent!');
        }
            
        }
        
        else{
               return $this->sendError('There is not enough balance in your account, please recharge soon!!'); 
        }  
        }
        
        //------------*******check pass sms 3 day before the end date ****** 
          $iwarnbmonthday=Carbon::parse($insuranceendate)->subDays('3');
          $iwdateonly=$iwarnbmonthday->toDateString();
          $iwarnformat=str_replace('-', ',',$iwdateonly);
          $iwmakeitarray = explode (",", $iwarnformat); 
            //then,finally get month and year in array
            $iwmonths=$iwmakeitarray[1];
            $iwmonth=ltrim($iwmonths, '0');
            $iwdays=$iwmakeitarray[2];
            $iwday=ltrim($iwdays, '0');
            $iwarnmonthandday=$iwmonth.' '.$iwday;
       
        if($nmonthandday==$iwarnmonthandday){
        $remainingbalance=$getremainingbalance->remaining_balance;
        //assuming deducted value in the sms
        $deductedvalue=1.5;
           $message = 'Dear, '.$ownername.' your '.$bustype.': '.$busno.' insurance end-date of is at: '.$insuranceendate.', please pay in time!!, thank you.';
           // set post fields
        $content =[
        'token'=>$token,
        'to'=>$to,
        'sender'=>$sender,
        'message'=>$message,
        ];
        $newremainingbalance=$remainingbalance-$deductedvalue;
        if($newremainingbalance>=0){
        $send_with_content =$this->sendSMS($content);
        $datas = json_decode($send_with_content);
        if($datas->total_numbers==0){
            return $this->sendError('Message could not be sent!!');
        }
        else{
        //store in db
        $sms=new Sms();
        $sms->sms_to=$to;
        $sms->receiver_name=$ownername;
        $sms->sms_message=$message;
        $sms->created_at=Carbon::now();
        $sms->updated_at=Carbon::now();
        $sms->company_id='100';
        $sms->added_by='system generated';
        $sms->initial_balance=$getremainingbalance->initial_balance;
        $sms->remaining_balance=$newremainingbalance;
        $sms->save();
          $dataarray=['msg sent to'=>$ownername];
       //return $this->sendResponse($datas,'msg sent!');
        }
            
        }
        
        else{
               return $this->sendError('There is not enough balance in your account, please recharge soon!!'); 
        }  
        }
       }
       return $this->sendResponse($dataarray,'Sms successfully sent!!');
    
        
    }
    //bulk sms to owner: routewise
    function bulkSmsRouteWise(Request $req,$routeno){
        $token = 'LiBLVuY08EfqCMBF1432gS08XSQpCOxpsH2N';
        $sender    = '1010';
        $message=$req['message'];
        \DB::statement("SET SQL_MODE=''");
        $getdata=VehicleRoute::join('route_info','vehicle_route_info.route_id','route_info.id')->join('bus_info','vehicle_route_info.bus_id','bus_info.bus_id')
        ->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->groupBy('bus_owner.owner_phone')->where('route_info.route_number',$routeno)
        ->select('route_info.route_number','bus_owner.owner_phone')->get('bus_owner.owner_phone');
       $countowners=VehicleRoute::join('route_info','vehicle_route_info.route_id','route_info.id')->join('bus_info','vehicle_route_info.bus_id','bus_info.bus_id')
        ->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->groupBy('bus_owner.owner_phone')
        ->where('route_info.route_number',$routeno)->get('bus_owner.owner_phone')->count('bus_owner.owner_phone');
        $getremainingbalance=Sms::query()->orderBy('id','desc')->first('remaining_balance');
        $remainingbalance=$getremainingbalance->remaining_balance;
        //assuming deducted value in the sms
        $deductedvalue=1.5*$countowners;
        $newremainingbalance=$remainingbalance-$deductedvalue;
        if($newremainingbalance>0){
            $a=$getdata->toArray();
                $b=implode(",", array_column($a, "owner_phone"));
               
            $to=$b;
          //   return $this->sendResponse($b,'bulk owner message sent successfully!'); 
                if(empty($to)){
             return $this->sendError('No vehicle owner records in this route!!'); 
         }
        $content =['token'=>$token,'to'=>$to,'sender'=>$sender,'message'=>$message,];
        $send_with_content =$this->sendSMS($content);
        $datas = json_decode($send_with_content);
            
          if($datas->total_numbers==0){
            return $this->sendError('Message could not be sent!!');
        }
        else{
        $getcurrentbalance=$datas->current_balance;
        //store in db
        $getinitialbalance=Sms::query()->orderBy('id','desc')->first();
        $initialbalance=$getinitialbalance->initial_balance;
        $sms=new Sms();
        $sms->sms_to='1234567890';
        $sms->receiver_name='bulk owner sms';
        $sms->sms_message=$message.'of Route:'.$routeno;
        $sms->created_at=Carbon::now();
        $sms->updated_at=Carbon::now();
        $sms->company_id=$req['company_id'];
        $sms->added_by=$req['added_by'];
        $sms->sms_type='bulk';
        $sms->initial_balance=$initialbalance;
        $sms->remaining_balance=$newremainingbalance;
        $sms->save();
        return $this->sendResponse($datas,'bulk sms to the owner of Route:'.$routeno .' sent successfully!');    
        }
        }
        else{
               return $this->sendError('There is not enough balance in your account, please recharge soon!!');    
        }
        return $this->sendResponse($count,'msg sent!');  
    }
    //sms history
    function smsHistory($companyid){
    $getdata=Sms::where('company_id',$companyid)->orderBy('id','desc')->get();
    return $this->SendResponse($getdata,'Sms history retrieved successfully!!');
    }
}