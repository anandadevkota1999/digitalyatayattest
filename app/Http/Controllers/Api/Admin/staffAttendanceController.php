<?php 
 
namespace App\Http\Controllers\Api\Admin;

use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Admin\StaffAttendance;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\BeforeLogin\companyList;
//use Illuminate\Support\Facades\Session;
//use Cache;
use App\Http\Controllers\Api\baseController as BaseController;
//use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Validator;
//use Illuminate\Contracts\Auth\UserProvider;
//use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
Use Exception;
use Illuminate\Database\QueryException;
//use Illuminate\Support\Facades\Auth;



class staffAttendanceController extends BaseController
{
    public function RegisterAttendance(Request $req){
         try{
             $input=$req->all();
         $validator = Validator::make($input, [
             'staff_id'=>'required',
             'arrival_time' => '',
             'departure_time' => '',
             'attendance_status'=>'',
             'attendance_remarks'=>'',
             'company_id'=>'required',
        ]);
   
        if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
        $input['arrival_time']=Carbon::now()->toDateTimeString();
        //->setHour(10)
        $input['departure_time']=Carbon::now()->addHours(6)->toDateTimeString();
        $input['created_at']=Carbon::now()->toDateTimeString();
        $input['updated_at']=Carbon::now()->toDateTimeString();
         $createvalues=StaffAttendance::create($input);
         return $this->sendResponse($createvalues,'staff attendance has been registered!');
    }
    catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
    
    
    }
    //adding staff details at first when butten is clicked
    public function StoreAtClick($company_id){
 $getstaff=StaffInfo::whereIn('staff_id', StaffInfo::get('staff_id'))->where('company_id',$company_id)->get();
 //initiate array
 //$getfromstaff=[];
    foreach($getstaff as $data){
  $getfromstaff=  ['staff_id' => $data->staff_id,
  'created_at'=>Carbon::now('Asia/Kathmandu'),
  'attendance_status'=>'A','company_id'=>$company_id,'arrival_time'=>Carbon::now('Asia/Kathmandu') ];
  $b=StaffAttendance::create($getfromstaff);
   }
  
  return $this->sendResponse($getfromstaff,'done!!!');
    }
    
    //filtering attendace of company by date:
    public function filterAttendanceByDate($company_id){
      $query=StaffAttendance::where('staff_attendance.company_id',$company_id)->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->whereDate('staff_attendance.created_at', Carbon::today())->get();
      if(!$query->isEmpty()){
          return $this->sendResponse($query,'data filtered successfully!!!');
           }
      else{
          return $this->sendError('Have a look: There are no attendance records of your company for this day!');
     }
    }
    
    public function setCompanyArAndDepTime(Request $request,$id){
            $input=$request->all();
         $validator = Validator::make($input, [
             'arrival_time' => 'required',
             'departure_time' => 'required',
             'company_id'=>'',
        ]);
   
        if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
        $values=companyList::find($id);
        $values->arrival_time=$input['arrival_time'];
        $values->departure_time=$input['departure_time'];
        $values->save();
         return $this->sendResponse($values,'values are updated successfully into the company!!');
    }
    
    public function updateAttendance(Request $request,$id){
        
         $validator = Validator::make($request->all(), [
             'arrival_time' => 'required',
             'departure_time' => 'required',
             'company_id'=>'',
        ]);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
         $values=StaffAttendance::find($id);
         $values->arrival_time=$request['arrival_time'];
         $values->departure_time=$request['departure_time'];
         $values->attendance_status=$request['attendance_status'];
         $values->attendance_remarks=$request['attendance_remarks'];
         $values->updated_by=$request['updated_by'];
         $values->updated_at=Carbon::now()->toDateTimeString();
         $values->save();
         return $this->sendResponse($values,'values are updated successfully into the company!!');
    }
    
    
    
      //get staffs attendance of specific company  
     public function getAttendenceInfo($ids){
             $get=StaffInfo::where('staff_attendance.company_id',$ids)->join('staff_attendance', 'staff_info.staff_id', '=', 'staff_attendance.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->orderBy('id', 'desc')->get();
        return $this->sendResponse($get,'values are updated successfully into the company!!');
    }
    //**************for  showing attendance of staffs ********************
    public function showStaffAtendanceInfo($ids){
   $combidedquery=StaffInfo::where('id',$ids)->join('staff_attendance', 'staff_info.staff_id', '=', 'staff_attendance.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->first();
   if(is_null($combidedquery)){
        return $this->sendError( 'there is no attendance record of such ID!!'); 
   }
   else{
   return $this->sendResponse($combidedquery, 'Attendance Record listed successfully!!'); 
   }  
    }
    
    //update attendance status:h,l,a,p
    public function updateAttendaceStatus(Request $req,$staff_id,$created_at){
          $getstatus=StaffAttendance::where('staff_id',$staff_id)->whereDate('staff_attendance.created_at', Carbon::today())->first();
        //  $getstatus->created_at= Carbon::createFromformat('Y-m-d H:i:s', $getstatus->created_at)->format('Y-m-d');
      //  return $this->sendResponse($getstatus, 'attendance filtered successfully!!');
        if(is_null($getstatus)){
         return $this->sendError('Attendance has not been started yet today!');   
        }
        else{
         $getstatus->attendance_status=$req['attendance_status'];
         $getstatus->arrival_time=$req['arrival_time'];
         $getstatus->departure_time=$req['departure_time'];
         $getstatus->updated_at=Carbon::parse('Asia/Kathmandu');
         $getstatus->updated_by=$req['updated_by'];
         $getstatus->attendance_remarks=$req['attendance_remarks'];
         $getstatus->save();
        return $this->sendResponse($getstatus, 'attendance filtered successfully!!'); 
    }
        
    }
    
    //get attenedance of staffs of a company
 public function filterAttendancebySpecificDate(Request $req,$company_id){
   $validator = Validator::make($req->all(), [
             'arrival_date' => 'required|date',
             'company_id'=>'',
        ]);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
 $getdate=$req['arrival_date'];
 
 //get (23:59:59)a day
 $comparewith=Carbon::parse($getdate)->endOfDay();

 $query=StaffAttendance::where('staff_attendance.company_id',$company_id)->whereBetween('arrival_time',[$getdate,$comparewith])->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->get();
return $this->sendResponse($query, 'attendance filtered successfully!!'); 
}
    
//get attendance of specific detail staffs
public function getAttendanceDetailOfStaff($ids){
   $get=StaffAttendance::where('staff_attendance.staff_id',$ids)->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->orderBy('staff_attendance.arrival_time', 'desc')->get();
 return $this->sendResponse($get, 'attendance filtered successfully!!'); 
    
}
    
//filter data by specific dates!
public function filterbySpecificDate(Request $req,$company_id){
     $validator = Validator::make($req->all(), [
             'from' => 'required|date',
             'to'=>'required|date',
        ]);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
$from = $req['from'];
$to = $req['to'];
$comparewith=Carbon::parse($to)->endOfDay();
$filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.company_id',$company_id)->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->get();  
        return $this->sendResponse($filter, 'owners filtered successfully!!');    
}
    //showing present staffs in company of current day
    public function showByAttendanceStatusOfCurrentDay(Request $req,$company_id){
        
         if($req['attendance_status']=='A'){
              $filter=StaffAttendance::where('staff_attendance.company_id',$company_id)->where('staff_attendance.attendance_status','A')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->whereDate('staff_attendance.arrival_time', Carbon::today())->get();
               if(!$filter->isEmpty()) { 
               return $this->sendResponse($filter, 'Absent status filtered successfully!!');    
              }
               else{
                   return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
               }
     
         }
       else  if($req['attendance_status']=='P'){
           $filter=StaffAttendance::where('staff_attendance.company_id',$company_id)->where('staff_attendance.attendance_status','P')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->whereDate('staff_attendance.arrival_time', Carbon::today())->get();
        if(!$filter->isEmpty()) { 
               return $this->sendResponse($filter, 'Present status filtered successfully!!');    
              }
               else{
                   return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
               }  
         }
           
      else if($req['attendance_status']=='L'){
            $filter=StaffAttendance::where('staff_attendance.company_id',$company_id)->where('staff_attendance.attendance_status','L')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->whereDate('staff_attendance.arrival_time', Carbon::today())->get();
       if(!$filter->isEmpty()) { 
               return $this->sendResponse($filter, 'staff on Leave filtered successfully!!');    
              }
               else{
                   return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
               }
 
       }
       else{
            return $this->sendError('undefinable Entry!!!'); 
       }
      
          
    }
    //filtering attendace of company :from and to(duration)
    public function AttendanceStatusOfSpecificDuration(Request $req,$company_id)
    {
         $validator = Validator::make($req->all(), [
             'from' => 'required|date',
             'to'=>'required|date',
        ]);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
     $from = $req['from'];
     $to = $req['to'];
     $comparewith=Carbon::parse($to)->endOfDay();
     //checking for present
         if($req['attendance_status']=='P'){
              $filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.company_id',$company_id)->where('staff_attendance.attendance_status','P')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->get();
              if(!$filter->isEmpty()) { 
               return $this->sendResponse($filter, 'Present status filtered successfully!!');    
              }
               else{
                   return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
               }
     
         }
         //checking for absentees
         else  if($req['attendance_status']=='A'){
              $filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.company_id',$company_id)->where('staff_attendance.attendance_status','A')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->get();
          if(!$filter->isEmpty()) { 
               return $this->sendResponse($filter, 'Absent status filtered successfully!!');    
              }
               else{
                   return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
               }      
         
          }
          //checking for:on leave staffs
         else  if($req['attendance_status']=='L'){
              $filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.company_id',$company_id)->where('staff_attendance.attendance_status','L')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->get();
               if(!$filter->isEmpty()) { 
               return $this->sendResponse($filter, 'staff on Leave filtered successfully!!');    
              }
               else{
                   return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
               }
     
         }
         else{
              return $this->sendError('undefinable Entry!!!'); 
         }
        
    }
    //for filtering staff status of current day
    public function staffAttendanceStatusOfCurrentDay(Request $req,$staff_id){
        if($req['attendance_status']=='A'){
              $filter=StaffAttendance::where('staff_attendance.staff_id',$staff_id)->where('staff_attendance.attendance_status','A')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_info.staff_image','staff_attendance.*')->whereDate('staff_attendance.arrival_time', Carbon::today())->get();
               if(!$filter->isEmpty()) { 
               return $this->sendResponse($filter, 'Absent status filtered successfully!!');    
              }
               else{
                   return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
               }
     
         }
       else  if($req['attendance_status']=='P'){
           $filter=StaffAttendance::where('staff_attendance.staff_id',$staff_id)->where('staff_attendance.attendance_status','P')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_info.staff_image','staff_attendance.*')->whereDate('staff_attendance.arrival_time', Carbon::today())->get();
        if(!$filter->isEmpty()) { 
               return $this->sendResponse($filter, 'Present status filtered successfully!!');    
              }
               else{
                   return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
               }  
         }
           
      else if($req['attendance_status']=='L'){
            $filter=StaffAttendance::where('staff_attendance.staff_id',$staff_id)->where('staff_attendance.attendance_status','L')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_info.staff_image','staff_attendance.*')->whereDate('staff_attendance.arrival_time', Carbon::today())->get();
       if(!$filter->isEmpty()) { 
               return $this->sendResponse($filter, 'staff on Leave filtered successfully!!');    
              }
               else{
                   return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
               }
       }
       else{
            return $this->sendError('undefinable Entry!!!'); 
       }
    }
 //filtering indivisual staff status:from specific duration(from and to)
    public function staffAttendanceStatusOfSpecificDuration(Request $req,$staff_id){
         $validator = Validator::make($req->all(), [
             'from' => 'required|date',
             'to'=>'required|date',
        ]);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
     $from = $req['from'];
     $to = $req['to'];
     $comparewith=Carbon::parse($to)->endOfDay();
     //checking for present
         if($req['attendance_status']=='P'){
              $filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.staff_id',$staff_id)->where('staff_attendance.attendance_status','P')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_info.staff_image','staff_attendance.*')->get();
              if(!$filter->isEmpty()) { 
               return $this->sendResponse($filter, 'Present status filtered successfully!!');    
              }
               else{
                   return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
               }
     
         }
         //checking for absentees
         else  if($req['attendance_status']=='A'){
              $filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.staff_id',$staff_id)->where('staff_attendance.attendance_status','A')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_info.staff_image','staff_attendance.*')->get();
          if(!$filter->isEmpty()) { 
               return $this->sendResponse($filter, 'Absent status filtered successfully!!');    
              }
               else{
                   return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
               }      
         
          }
          //checking for:on leave staffs
         else  if($req['attendance_status']=='L'){
              $filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.staff_id',$staff_id)->where('staff_attendance.attendance_status','L')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_info.staff_image','staff_attendance.*')->get();
               if(!$filter->isEmpty()) { 
               return $this->sendResponse($filter, 'staff on Leave filtered successfully!!');    
              }
               else{
                   return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
               }
     
         }
         else{
              return $this->sendError('undefinable Entry!!!'); 
         }
        

    }
    //update departure time and remarks
    public function updateDepAndRemarks(Request $req,$id){
          $validator = Validator::make($req->all(), [
             'departure_time' => 'date',
        ]);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
        $query=StaffAttendance::find($id);
        if($req['departure_time']){
         $query->departure_time=$req['departure_time'];
         $query->save();
         return $this->sendResponse($query, 'Departure time and Remarks updated successfully!!'); 
        }
        else if($req['attendance_remarks']){
           $query->attendance_remarks=$req['attendance_remarks'];
        $query->save();
        return $this->sendResponse($query, 'Departure time and Remarks updated successfully!!'); 
          }
        else{
            return $this->sendError('make sure you have correct input!!'); 
        }
         }
    
   //showing present staffs in company of current day
    public function countByAttendanceStatusOfCurrentDay(Request $req,$company_id){
        
         if($req['attendance_status']=='A'){
              $filter=StaffAttendance::where('staff_attendance.company_id',$company_id)->where('staff_attendance.attendance_status','A')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->whereDate('staff_attendance.arrival_time', Carbon::today())->count();
               if(is_null($filter)){
          return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
     }
              else{
                   return $this->sendResponse($filter, 'staff on absent filtered successfully!!');
               } 
     
         }
       else  if($req['attendance_status']=='P'){
           $filter=StaffAttendance::where('staff_attendance.company_id',$company_id)->where('staff_attendance.attendance_status','P')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->whereDate('staff_attendance.arrival_time', Carbon::today())->count();
       if(is_null($filter)){
          return $this->sendResponse('No datas found', 'Present status filtered successfully!!'); 
     }
              else{
                   return $this->sendResponse($filter, 'staff on present filtered successfully!!');
               } 
         }
           
      else if($req['attendance_status']=='L'){
            $filter=StaffAttendance::where('staff_attendance.company_id',$company_id)->where('staff_attendance.attendance_status','L')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->whereDate('staff_attendance.arrival_time', Carbon::today())->count();
                  
     if(is_null($filter)){
          return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
     }
              else{
                   return $this->sendResponse($filter, 'staff on Leave filtered successfully!!');
               } 
 
       }
       else{
            return $this->sendError('undefinable Entry!!!'); 
       }
      
          
    }
    
     //count attendace of company :from and to(duration)
    public function CountAttendanceStatusOfSpecificDuration(Request $req,$company_id)
    {
         $validator = Validator::make($req->all(), [
             'from' => 'required|date',
             'to'=>'required|date',
        ]);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
     $from = $req['from'];
     $to = $req['to'];
     $comparewith=Carbon::parse($to)->endOfDay();
     //checking for present
         if($req['attendance_status']=='P'){
              $filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.company_id',$company_id)->where('staff_attendance.attendance_status','P')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->count();
             if(is_null($filter)){
          return $this->sendResponse('No datas found', 'Present status filtered successfully!!'); 
     }
              else{
                   return $this->sendResponse($filter, 'staff on present filtered successfully!!');
               } 
     
         }
         //checking for absentees
         else  if($req['attendance_status']=='A'){
              $filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.company_id',$company_id)->where('staff_attendance.attendance_status','A')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->count();
                 if(is_null($filter)){
          return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
     }
              else{
                   return $this->sendResponse($filter, 'staff on absent filtered successfully!!');
               }     
         
          }
          //checking for:on leave staffs
         else  if($req['attendance_status']=='L'){
              $filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.company_id',$company_id)->where('staff_attendance.attendance_status','L')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->count();
              if(is_null($filter)){
          return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
     }
              else{
                   return $this->sendResponse($filter, 'staff on Leave filtered successfully!!');
               } 
     
         }
         else{
              return $this->sendError('undefinable Entry!!!'); 
         }
        
    }
    
      //for counting staff  of current day by: status
    public function CountStaffAttendanceStatusOfCurrentDay(Request $req,$staff_id){
        if($req['attendance_status']=='A'){
              $filter=StaffAttendance::where('staff_attendance.staff_id',$staff_id)->where('staff_attendance.attendance_status','A')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_info.staff_image','staff_attendance.*')->whereDate('staff_attendance.arrival_time', Carbon::today())->count();
                 if(is_null($filter)){
          return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
     }
              else{
                   return $this->sendResponse($filter, 'staff on absent filtered successfully!!');
               }     
         
         }
       else  if($req['attendance_status']=='P'){
           $filter=StaffAttendance::where('staff_attendance.staff_id',$staff_id)->where('staff_attendance.attendance_status','P')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_info.staff_image','staff_attendance.*')->whereDate('staff_attendance.arrival_time', Carbon::today())->count();
         if(is_null($filter)){
          return $this->sendResponse('No datas found', 'Present status filtered successfully!!'); 
     }
              else{
                   return $this->sendResponse($filter, 'staff on present filtered successfully!!');
               }  
         }
           
      else if($req['attendance_status']=='L'){
            $filter=StaffAttendance::where('staff_attendance.staff_id',$staff_id)->where('staff_attendance.attendance_status','L')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_info.staff_image','staff_attendance.*')->whereDate('staff_attendance.arrival_time', Carbon::today())->count();
        if(is_null($filter)){
          return $this->sendResponse('No datas found', 'Leave status filtered successfully!!'); 
     }
              else{
                   return $this->sendResponse($filter, 'staff on Leave filtered successfully!!');
               } 
       }
       else{
            return $this->sendError('undefinable Entry!!!'); 
       }
    }
    
    //counting indivisual staff status:from specific duration(from and to)
    public function countStaffAttendanceStatusOfSpecificDuration(Request $req,$staff_id){
         $validator = Validator::make($req->all(), [
             'from' => 'required|date',
             'to'=>'required|date',
        ]);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
     $from = $req['from'];
     $to = $req['to'];
     $comparewith=Carbon::parse($to)->endOfDay();
     //checking for present
         if($req['attendance_status']=='P'){
              $filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.staff_id',$staff_id)->where('staff_attendance.attendance_status','P')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_info.staff_image','staff_attendance.*')->count();
              if(is_null($filter)){
          return $this->sendResponse('No datas found', 'Present status filtered successfully!!'); 
     }
              else{
                   return $this->sendResponse($filter, 'staff on present filtered successfully!!');
               }  
     
         }
         //checking for absentees
         else  if($req['attendance_status']=='A'){
              $filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.staff_id',$staff_id)->where('staff_attendance.attendance_status','A')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_info.staff_image','staff_attendance.*')->count();
                 if(is_null($filter)){
          return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
     }
              else{
                   return $this->sendResponse($filter, 'staff on absent filtered successfully!!');
               }      
         
          }
          //checking for:on leave staffs
         else  if($req['attendance_status']=='L'){
              $filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.staff_id',$staff_id)->where('staff_attendance.attendance_status','L')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_info.staff_image','staff_attendance.*')->count();
             if(is_null($filter)){
          return $this->sendResponse('No datas found', 'Leave status filtered successfully!!'); 
     }
              else{
                   return $this->sendResponse($filter, 'staff on Leave filtered successfully!!');
               } 
     
         }
         else{
              return $this->sendError('undefinable Entry!!!'); 
         }
        

    }
    
    
}