<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\BeforeLogin\companyList;
use App\Models\BeforeLogin\DummyCompanyList;
use App\Models\Admin\LoginModel;
use App\Models\Users\busOwner;
use App\Models\Users\DummyBusOwner;
use App\Models\Users\Driver;
use App\Models\Users\DummyDriver;
use App\Models\Users\businfo;
use App\Models\Users\DummyBusInfo;
use App\Models\Users\StaffInfo;
use App\Models\Users\DummyStaffInfo;
use App\Models\Users\StaffRole;
use App\Models\Users\DummyStaffRole;
use App\Models\Admin\Role;
use App\Models\Notification;
use App\Models\Admin\StaffAttendance;
use App\Http\Controllers\Api\baseController as BaseController;
use Hash;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Validator;
use Auth;
use App\Http\Resources\BeforeLogin\companylist as companylistResource;

class BeforeLoginController extends BaseController
{
   //for working with huge volume of data
   /* 
   Public function processUsers()
{
User::chunk(500, function($chunked_collection){
//do some stuff with $chunked_collection
});
}
   */
   function CompanyDetail(){
    $get=companyList::where('company_id','100')->first();
    return $this->SendResponse($get,'company detail shown successfully!!');
}
//sample Demo yatayat pvt ltd
   
   function backupDb(){
       $filename = "backup-" .'sample_Demo_yatayat_pvt_ltd'. Carbon::now()->format('Y-m-d') . ".gz";
       $command = "mysqldump --user=" . env('DB_USERNAME') ." --password=" . env('DB_PASSWORD') . " --host=" . env('DB_HOST') . " " . env('DB_DATABASE') . "  | gzip > " . storage_path() . "/app/backup/" . $filename;
       $returnVar = NULL;
       $output  = NULL;
       exec($command, $output, $returnVar);
     return $this->SendResponse($command,'backup successfull');
   }
   
   function getDataFromChitwanYatayat(){
       $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://backend.chitwanyatayat.com.np/api/companylist",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 600,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET"
        ));
         $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return $err;
        }
        else {
            return $response;
        }
   }
    
     //to get values from the database through api
     public function store(Request $req){
         try{
         $validator = Validator::make($req->all(), [
             'company_id'=>'required',
            'company_name' => 'required',
             'company_address' => 'required',
             'company_phone'=>'required',
             'company_email'=>'required|email',
            'company_image'=>'required|image|mimes:jpeg,png,jpg,gif,svg,jfif',
      //      'company_pan_image'=>'image|mimes:jpeg,png,jpg,gif,svg,jfif',
        ]);
   
        if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
         if($req->hasFile('company_image')&&$req->hasFile('company_pan_image')){
         $getcompanyname=$req['company_name'];
         $req->company_image->store($getcompanyname, 'local');
         $req->company_pan_image->store($getcompanyname, 'local');
     $model=new companyList();
     $model->company_id=$req->input('company_id');
     $model->company_name=$req->input('company_name');
     $model->company_address=$req->input('company_address');
     $model->company_phone=$req->input('company_phone');
     $model->company_email=$req->input('company_email');
     $model->company_image=$req->company_image->hashName();
     $model->company_pan_no=$req->input('company_pan_no');
     $model->compny_english_establish_date=$req->input('compny_english_establish_date');
     $model->company_nepali_establish_date=$req->input('company_nepali_establish_date');
     $model->company_nepali_address=$req->input('company_nepali_address');
     $model->company_nepali_phone_no=$req->input('company_nepali_phone_no');
     $model->company_nepali_name=$req->input('company_nepali_name');
    // $model->company_pan_image=$req->company_pan_image->hashName();
     $model->created_at=Carbon::now();
     $model->updated_at=Carbon::now();
     $model->save();
    return $this->sendResponse($model, 'stored successfully!!');
    
     } 
      if($req->hasFile('company_image')){
        $getcompanyname=$req['company_name'];
        $req->company_image->store($getcompanyname, 'local');  
     $model=new companyList();
     $model->company_id=$req->input('company_id');
     $model->company_name=$req->input('company_name');
     $model->company_address=$req->input('company_address');
     $model->company_phone=$req->input('company_phone');
     $model->company_email=$req->input('company_email');
     $model->company_image=$req->company_image->hashName();
     $model->company_pan_no=$req->input('company_pan_no');
     $model->compny_english_establish_date=$req->input('compny_english_establish_date');
     $model->company_nepali_establish_date=$req->input('company_nepali_establish_date');
     $model->company_nepali_address=$req->input('company_nepali_address');
     $model->company_nepali_phone_no=$req->input('company_nepali_phone_no');
     $model->company_nepali_name=$req->input('company_nepali_name');
     $model->company_pan_image=$req->company_pan_image;
     $model->created_at=Carbon::now();
     $model->updated_at=Carbon::now();
     $model->save();
    return $this->sendResponse($model, 'stored successfully!!');

          
      }
    }
      catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }

    
     }
    public function retriever(){
    	$getall=companyList::all();
    if(is_null($getall)){
      return $this->sendError('there are no any values left!');
    }
    //$array=[];
    /*$array=[
        'companies'=>$getall,
        'route'=>'https://yatayatbackend.digitalyatayat.com/'
        ]; */
    return $this->sendResponse($getall, 'values are retrieved successfully!');
    }


    //for showing or displaying datas from the database through api
public function listOwnerSpecificCompany($ids){
       $owners = busOwner::where('company_id',$ids)->get();
   return $this->sendResponse($owners, 'owners filtered successfully!!');    
}


public function show($id){
   $showall=companyList::findOrFail($id);
return $this->sendResponse($showall,'owner details are successfully shown!');   
}

//for updating values in database through api

public function updateCompany(Request $req,$id){
       try{
    $validator =Validator::make($req->all(),[
  // 'company_id'=>'required',
   //'admin_image'=>'image|mimes:jpeg,png,jpg,gif,svg,jfif|max:2048',
  ]);
   if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }    
  $all=companyList::where('company_id',$id)->first();
  if($req->hasFile('company_image')&&$req->hasFile('company_pan_image')){
       $getcompany=$all->company_name;
       $req->company_image->store($getcompany, 'local');
       $req->company_pan_image->store($getcompany, 'local');
       $admin=companyList::findorFail($id);
       $updatecompany=companyList::where('company_id',$id)->update([
           'company_name'=>$req['company_name'],
           'company_phone'=>$req['company_phone'],
           'company_address'=>$req['company_address'],
           'company_email'=>$req['company_email'],
           'company_image'=>$req->company_image->hashName(),
           'company_pan_no'=>$req['company_pan_no'],
           'compny_english_establish_date'=>$req['compny_english_establish_date'],
           'company_nepali_address'=>$req['company_nepali_address'],
           'company_nepali_phone_no'=>$req['company_nepali_phone_no'],
           'company_nepali_name'=>$req['company_nepali_name'],
           'company_pan_image'=>$req->company_pan_image->hashName(),
           'updated_at'=>Carbon::now()
           ]);
       return $this->sendResponse($updatecompany,'Company Records are successfully updated!!');            
      }
 else if($req->hasFile('company_image')){
  $getcompany=$all->company_name;
       $req->company_image->store($getcompany, 'local');
       $admin=companyList::findorFail($id);
       $updatecompany=companyList::where('company_id',$id)->update([
           'company_name'=>$req['company_name'],
           'company_phone'=>$req['company_phone'],
           'company_address'=>$req['company_address'],
           'company_email'=>$req['company_email'],
           'company_image'=>$req->company_image->hashName(),
           'company_pan_no'=>$req['company_pan_no'],
           'compny_english_establish_date'=>$req['compny_english_establish_date'],
           'company_nepali_address'=>$req['company_nepali_address'],
           'company_nepali_phone_no'=>$req['company_nepali_phone_no'],
           'company_nepali_name'=>$req['company_nepali_name'],
           'updated_at'=>Carbon::now()
           ]);
       return $this->sendResponse($updatecompany,'Company Records are successfully updated!!');            
 
 }
 else if($req->hasFile('company_pan_image')){
  $getcompany=$all->company_name;
       $req->company_pan_image->store($getcompany, 'local');
       $admin=companyList::findorFail($id);
    $updatecompany=companyList::where('company_id',$id)->update([
           'company_name'=>$req['company_name'],
           'company_phone'=>$req['company_phone'],
           'company_address'=>$req['company_address'],
           'company_email'=>$req['company_email'],
           'company_pan_image'=>$req->company_pan_image->hashName(),
           'company_pan_no'=>$req['company_pan_no'],
           'compny_english_establish_date'=>$req['compny_english_establish_date'],
           'company_nepali_address'=>$req['company_nepali_address'],
           'company_nepali_phone_no'=>$req['company_nepali_phone_no'],
           'company_nepali_name'=>$req['company_nepali_name'],
           'updated_at'=>Carbon::now()
           ]);
       return $this->sendResponse($updatecompany,'Company Records are successfully updated!!');            
 }
 else{
     $updatecompany=companyList::where('company_id',$id)->update([
           'company_name'=>$req['company_name'],
           'company_phone'=>$req['company_phone'],
           'company_address'=>$req['company_address'],
           'company_email'=>$req['company_email'],
           'company_pan_no'=>$req['company_pan_no'],
           'compny_english_establish_date'=>$req['compny_english_establish_date'],
           'company_nepali_address'=>$req['company_nepali_address'],
           'company_nepali_phone_no'=>$req['company_nepali_phone_no'],
           'company_nepali_name'=>$req['company_nepali_name'],
           'updated_at'=>Carbon::now()
           ]);
       return $this->sendResponse($updatecompany,'Company Records are successfully updated!!');            
 }       
    }
    catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
    
}

//delete demo companies for demo only!!
public function deleteDemoCompany($id){
     $getcompany=companyList::where('company_id',$id)->first();
     if(is_null($getcompany)){
         return $this->sendError('company not found or already deleted!!');
     }
     else{
    //saving records row wise
    $company_name=$getcompany->company_name;
    $company_address=$getcompany->company_address;
    $company_phone=$getcompany->company_phone;
    $company_email=$getcompany->company_email;
    $company_image=$getcompany->company_image;
    $arrival_time=$getcompany->arrival_time;
    $departure_time=$getcompany->departure_time;
   
   $dummycompany=new DummyCompanyList(); 
   $dummycompany->company_id=$id;
   $dummycompany->company_name=$company_name;
   $dummycompany->company_address=$company_address;
   $dummycompany->company_phone=$company_phone;
   $dummycompany->company_email=$company_email;
   $dummycompany->company_image=$company_image;
   $dummycompany->arrival_time=$arrival_time;
   $dummycompany->departure_time=$departure_time;
   $dummycompany->created_at=Carbon::now();
   $dummycompany->save();
   
   $removeadmin=LoginModel::where('company_id',$id);
    if(is_null($removeadmin)){
        return $this->sendError('There is no admin of this company');
    }
    else{
        $removeadmin->delete();
    }
   $removedriver=Driver::where('company_id',$id);
   if(is_null($removedriver)){
        return $this->sendError('There is no driver of this company');
    }
    else{
        $removedriver->delete();
    }
    
   $removedummydriver=DummyDriver::where('company_id',$id);
    if(is_null($removedummydriver)){
        return $this->sendError('There is no dummy driver of this company');
    }
    else{
        $removedummydriver->delete();
    }
   $removebusdetail=businfo::where('company_id',$id);
    if(is_null($removebusdetail)){
        return $this->sendError('There is no bus of this company');
    }
    else{
        $removebusdetail->delete();
    }
   $removedummybusdetail=DummyBusInfo::where('company_id',$id);
    if(is_null($removedummybusdetail)){
        return $this->sendError('There is no dummy bus of this company');
    }
    else{
        $removedummybusdetail->delete();
    }
   
   $removebusowners=busOwner::where('company_id',$id);
    if(is_null($removebusowners)){
        return $this->sendError('There is no owner of this company');
    }
    else{
        $removebusowners->delete();
    }
   
   $removedummybusowners=DummyBusOwner::where('company_id',$id);
    if(is_null($removedummybusowners)){
        return $this->sendError('There is no dummy owner of this company');
    }
    else{
        $removedummybusowners->delete();
    }
   $removestaffs=StaffInfo::where('company_id',$id);
    if(is_null($removestaffs)){
        return $this->sendError('There is no staff of this company');
    }
    else{
        $removestaffs->delete();
    }
   $removedummystaffs=DummyStaffInfo::where('company_id',$id);
   if(is_null($removestaffs)){
        return $this->sendError('There is no dummy staff of this company');
    }
    else{
        $removestaffs->delete();
    }
   $removestaffroles=StaffRole::where('company_id',$id);
   if(is_null($removestaffroles)){
        return $this->sendError('There is no staff role of this company');
    }
    else{
        $removestaffroles->delete();
    }
   $removedummystaffroles=DummyStaffRole::where('company_id',$id);
   if(is_null($removedummystaffroles)){
        return $this->sendError('There is no dummy staff role of this company');
    }
    else{
        $removedummystaffroles->delete();
    }
   $removeroles=Role::where('company_id',$id);
    if(is_null($removeroles)){
        return $this->sendError('There is no  role of this company');
    }
    else{
        $removeroles->delete();
    }
   $removestaffattendance=StaffAttendance::where('company_id',$id);
     if(is_null($removestaffattendance)){
        return $this->sendError('There is no  attendance of this company');
    }
    else{
        $removestaffattendance->delete();
    }
    $removenotification=Notification::where('company_id',$id);
     if(is_null($removenotification)){
        return $this->sendError('There is no  notification of this company');
    }
    else{
        $removenotification->delete();
    }
   $getcompany->delete();
   return $this->sendResponse('done', 'all records associated with the company is deleted successfully!');
  
     }  
    
}

public function delete(companyList $values, $id){
  $values=companyList::find($id);
if(is_null($values)){
	return $this->sendError('there is no any data of such id!');
}
else {
	$values->delete();
	return $this->sendResponse('done!!', 'values deleted successfully!');
}
}



}
