<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Users\Driver;
use App\Models\Users\Conductor;
use App\Models\Users\businfo as VehicleInfo;
use App\Models\Notification as Mynotification;
use App\Models\BeforeLogin\companyList;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Api\baseController as BaseController;
use Carbon\Carbon;
//use Hash;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;


class ConductorController extends BaseController
{
    public function conductorRegister(Request $req){
  $input_the_data= $req->all();
     $validator =Validator::make($input_the_data,[
  	        'conductor_id' => 'required|unique:conductor_info',
            'conductor_name'=>'required',
            'conductor_phone'=>'unique:conductor_info',
            'conductor_citizenship_no'=>'unique:conductor_info',
            'conductor_address'=>'required',
            'conductor_pin'=>'required',
            'conductor_dob'=>'required',
            'conductor_gender'=>'required',
            'conductor_image'=>'required|mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
            'conductor_citizenship_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
            'company_id' => 'required',
              
            
  ],[
      'conductor_id.unique'=>'conductor id must be unique!',
      'conductor_phone.unique'=>'conductor phone must be unique!',
      'conductor_citizenship_no.unique'=>'conductor citizenship number must be unique!',
      'conductor_image.required'=>'conductor image is required!',
      'conductor_id.required'=>'conductor id is required!',
      'conductor_name.required'=>'conductor name is required!',
      'conductor_pin.required'=>'conductor pin is required!',
      'conductor_dob.required'=>'conductor dob is required!',
      'conductor_gender.required'=>'conductor gender is required!',
      'company_id.required'=>'company id is required!'
      ]);
     if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
   
  $checkvalidity=Conductor::where('company_id',$req['company_id'])->where('conductor_citizenship_no',$req['conductor_citizenship_no'])->where('conductor_citizenship_no','!=',null)->where('active_status','yes')->first();
  if(is_null($checkvalidity)){
     $checkifactive=Conductor::where('company_id',$req['company_id'])->where('conductor_citizenship_no',$req['conductor_citizenship_no'])->where('active_status','!=','yes')->first();
   // return $this->sendResponse($checkactivedriver,'the datas are now stored successfully!!'); 
 if(is_null($checkifactive)){
     if($req->hasFile('conductor_image')&&$req->hasFile('conductor_citizenship_image')){
     //store image in the storage
     $all=companyList::where('company_id',$req['company_id'])->first();
     $getcompany=$all->company_name;
     $req->conductor_image->store($getcompany, 'local');
     $req->conductor_citizenship_image->store($getcompany, 'local');
     $input_the_data['created_at']=Carbon::now();
     $input_the_data['updated_at']=Carbon::now();
     $input_the_data['conductor_image']=$req->conductor_image->hashName();
     $input_the_data['conductor_citizenship_image']=$req->conductor_citizenship_image->hashName();
    $createvalues=Conductor::create($input_the_data);
    return $this->SendResponse($createvalues,'Conductor registered successfully!!');
     }
     if($req->hasFile('conductor_image')){
        //store image in the storage
       $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->conductor_image->store($getcompany, 'local');
       $input_the_data['created_at']=Carbon::now();
       $input_the_data['updated_at']=Carbon::now();
       $input_the_data['conductor_image']=$req->conductor_image->hashName();
       $createvalues=Conductor::create($input_the_data);
       return $this->SendResponse($createvalues,'Conductor registered successfully!!');
     }
     
 }
    else{
        
    $updatedetails=Conductor::where('conductor_citizenship_no',$req['conductor_citizenship_no'])->update([
      //  'conductor_id'=>$checkifactive->conductor_id,
        'bus_no'=>$req->input('bus_no'),
        'conductor_pin'=>$req->conductor_pin,
        'updated_at'=>Carbon::now(),
    //    'conductor_citizenship_no'=>$checkifactive->conductor_citizenship_no,
        'active_status'=>'yes',
        'updated_by'=>$req->input('added_by'),
        ]);    
    $getdata=Conductor::where('conductor_id',$req->input('conductor_id'))->first();
     return $this->sendResponse($getdata,'conductor registered successfully!!'); 
  
    }  
   }
  else{
      return $this->sendError('Given Conductor details are already registered in the system!');  
  }  
  }
  
  function showConductor($companyid){
      $show=Conductor::where('company_id',$companyid)->where('active_status','yes')->get();
      return $this->SendResponse($show,'Conductors of the company shown successfully!!');
  }
    
    
    
    
}