<?php

namespace App\Http\Controllers\Api\Users\Transactions;
use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Admin\Transactions\FiscalYear;
use App\Models\Admin\Transactions\AccountPriceDeclaration;
use App\Models\Admin\Transactions\VehicleFixedAccount;
use App\Models\Admin\Transactions\JournalEntryOfGeneralShulka;
use App\Models\Admin\Transactions\DurghatanaFund;
use App\Models\Admin\Transactions\DurghatanaKharcha;
use App\Models\Admin\Transactions\DurghatanaKharchaDummy;
use App\Models\Admin\Transactions\JournalEntryOfDurghatanaFund;
use App\Models\Users\busOwner;
use App\Models\Users\DummyBusOwner;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\DummyBusInfo;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Models\Notification as Mynotification;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Admin\adminInfo as admininfoResource;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
//use App\Models\Admin\LoginModel;
use App\Models\Admin\Transactions\GeneralShulka;
class DurghatanaFundControllerAndroid extends BaseController
{
    //create bill from owner payment of durghatana fund in android and prepare journal
    public function payDurghatanaFundandStore(Request $req){
        
         $fiscalyear=FiscalYear::OrderBy('id','DESC')->first();
        $fenddate=$fiscalyear->fiscal_year_end_date;
        $endadateformat=Carbon::createFromFormat('Y-m-d H:i:s', $fenddate);
       // $fenddatecorrectformat=$fenddate->toDateTimeString();
        $getpaymentdate=Carbon::now();
        $paymentformat=Carbon::createFromFormat('Y-m-d H:i:s', $getpaymentdate);
        $paymentformatendofday=Carbon::parse($paymentformat)->endOfDay();
     //   return $this->sendResponse($paymentformat,'general shulka stored successfully!!'); 
       $getfyear=$fiscalyear->fiscal_year;
        
      
        //check for fiscal year
        if($endadateformat->gt($paymentformatendofday)){
        $sumdurghatanajokhimshulka=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $summembershipshulka=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumnibedanshulka=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumanudaanshulka=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumsattadartashulka=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumdurghatananirixedshulka=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumpeskifarchyotshulka=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumanyaamdani=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumreserveshulka=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumjagedakoshshulka=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        $sumayamemershipshulka=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
    
        $getbillno=DurghatanaFund::orderBy('bill_no', 'DESC')->first('bill_no');
        $toarray=json_decode($getbillno);
        $getvalue=$toarray->bill_no;
        $incrementbillbyone=$getvalue+1;
     $input=$req->all();
     $validator =Validator::make($input,[
  	       // 'bill_payment_date' => 'required',
           'bus_no' => 'required',
            'member_name'=>'required',
            'durghatana_jokhim_shulka'=>'required',
            'membership_shulka'=>'required',
            'nibedan_shulka'=>'required',
            'anudaan_shulka'=>'required',
            'sattadarta_shulka'=>'required',
            'durghatana_nirichhyad_and_nyunikarad_shulka'=>'required',
            'peski_farchyut'=>'required',
            'anya_aamdani'=>'required',
            'reserve_shulka'=>'required',
            'jageda_kosh'=>'required',
            'naya_membership_shulka'=>'required',
            'bill_no'=>'unique:durghatana_fund'
            
  ]);
  if($validator->fails()){
              DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
         //checking if all input values are zero
         if($req->input('durghatana_jokhim_shulka')==0&&$req->input('membership_shulka')==0&&$req->input('nibedan_shulka')==0&&$req->input('anudaan_shulka')==0&&$req->input('sattadarta_shulka')==0&&$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')==0
        &&$req->input('peski_farchyut')==0&&$req->input('anya_aamdani')==0&&$req->input('reserve_shulka')==0&&$req->input('jageda_kosh')==0&&$req->input('naya_membership_shulka')==0)
        {
             DB::rollback();
             return $this->sendError('please pay atleast on a topic!');
        }

        else{
        $models=new DurghatanaFund();
        $models->bill_no=$incrementbillbyone;
        $models->bill_payment_date=$paymentformat;
        $models->bus_no=$req->input('bus_no');
        $models->member_name=$req->input('member_name');
      //  number_format((float)$foo, 2, '.', '');
     if($sumdurghatanajokhimshulka<$req->input('durghatana_jokhim_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumdurghatanajokhimshulka)&&$sumdurghatanajokhimshulka>=$req->input('durghatana_jokhim_shulka')){
        $models->durghatana_jokhim_shulka=number_format((float)$req->input('durghatana_jokhim_shulka'), 2, '.', '');
               }
     if($summembershipshulka<$req->input('membership_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($summembershipshulka)&&$summembershipshulka>=$req->input('membership_shulka')){
        $models->membership_shulka=number_format((float)$req->input('membership_shulka'), 2, '.', '');
               }
     if($sumnibedanshulka<$req->input('nibedan_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumnibedanshulka)&&$sumnibedanshulka>=$req->input('nibedan_shulka')){
        $models->nibedan_shulka=number_format((float)$req->input('nibedan_shulka'), 2, '.', '');
               }           
     if($sumanudaanshulka<$req->input('anudaan_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumanudaanshulka)&&$sumanudaanshulka>=$req->input('anudaan_shulka')){
        $models->anudaan_shulka=number_format((float)$req->input('anudaan_shulka'), 2, '.', '');
               }            
     if($sumsattadartashulka<$req->input('sattadarta_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumsattadartashulka)&&$sumsattadartashulka>=$req->input('sattadarta_shulka')){
        $models->sattadarta_shulka=number_format((float)$req->input('sattadarta_shulka'), 2, '.', '');
               }            
     if($sumdurghatananirixedshulka<$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumdurghatananirixedshulka)&&$sumdurghatananirixedshulka>=$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
        $models->durghatana_nirichhyad_and_nyunikarad_shulka=number_format((float)$req->input('durghatana_nirichhyad_and_nyunikarad_shulka'), 2, '.', '');
               }        
     if($sumpeskifarchyotshulka<$req->input('peski_farchyut')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumpeskifarchyotshulka)&&$sumpeskifarchyotshulka>=$req->input('peski_farchyut')){
        $models->peski_farchyut=number_format((float)$req->input('peski_farchyut'), 2, '.', '');               }        
     if($sumanyaamdani<$req->input('anya_aamdani')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumanyaamdani)&&$sumanyaamdani>=$req->input('anya_aamdani')){
        $models->anya_aamdani=number_format((float)$req->input('anya_aamdani'), 2, '.', '');
        }        
     if($sumreserveshulka<$req->input('reserve_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumreserveshulka)&&$sumreserveshulka>=$req->input('reserve_shulka')){
        $models->reserve_shulka=number_format((float)$req->input('reserve_shulka'), 2, '.', '');
        }                    
     if($sumjagedakoshshulka<$req->input('jageda_kosh')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumjagedakoshshulka)&&$sumjagedakoshshulka>=$req->input('jageda_kosh')){
        $models->jageda_kosh=number_format((float)$req->input('jageda_kosh'), 2, '.', '');
        }                    
     if($sumayamemershipshulka<$req->input('naya_membership_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumayamemershipshulka)&&$sumayamemershipshulka>=$req->input('naya_membership_shulka')){
        $models->naya_membership_shulka=number_format((float)$req->input('naya_membership_shulka'), 2, '.', '');
        } 
        $models->durghatana_jokhim_shulka_remarks=$req->input('durghatana_jokhim_shulka_remarks');
        $models->membership_shulka_remarks=$req->input('membership_shulka_remarks');
        $models->nibedan_shulka_remarks=$req->input('nibedan_shulka_remarks');
        $models->anudaan_shulka_remarks=$req->input('anudaan_shulka_remarks');
        $models->sattadarta_shulka_remarks=$req->input('sattadarta_shulka_remarks');
        $models->durghatana_nirichhyad_and_nyunikarad_shulka_remarks=$req->input('durghatana_nirichhyad_and_nyunikarad_shulka_remarks');
        $models->peski_farchyut_remarks=$req->input('peski_farchyut_remarks');
        $models->anya_aamdani_remarks=$req->input('anya_aamdani_remarks');
        $models->reserve_shulka_remarks=$req->input('reserve_shulka_remarks');
        $models->jageda_kosh_remarks=$req->input('jageda_kosh_remarks');
        $models->naya_membership_shulka_remarks=$req->input('naya_membership_shulka_remarks');
        $models->added_by=$req->input('added_by');
        $models->company_id=$req->input('company_id');
        $models->updated_by=$req->input('updated_by');
        $models->created_at=Carbon::now();
        $models->updated_at=Carbon::now();
        $models->total_amount=$req->input('total_amount');
        $models->payment_type='digital';
        $models->fiscal_year=$getfyear;
        $models->save();
        }
        //-------------------for sending notification immediately to Company Admin::::
// ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
        //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
        //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',];
     
        //notification contents:
            $notify=array('title'=>'Durghatana Fund Payment','body'=>'Vehicle Owner '.$models->member_name.' '.'has paid total of '.$models->total_amount.' rupees.');
        //get admin datas
        $getadmindata=DurghatanaFund::where('durghatana_fund.bus_no',$req->input('bus_no'))->join('bus_info','durghatana_fund.bus_no','=','bus_info.bus_no')->join('admin_info', 'bus_info.company_id', '=', 'admin_info.company_id')->select('bus_info.owner_phone','admin_info.*')->first();
        //get admin token
    $adminFcmToken=$getadmindata->device_token;
    //saving in notification table as well::
            $store=new Mynotification();
             $store->type='Durghatana Fund Payment';
            $store->notifiable_id=$getadmindata->admin_id;
            $store->notifiable_type=LoginModel::class;
            $store->title='Owner Durghatana Shulka Paid';
            $store->body='Vehicle Owner '.$models->member_name.' '.'has paid total of '.$models->total_amount.' rupees.';
            $store->added_by=$req['added_by'];
            $store->created_at=Carbon::now();
            $store->company_id=$getadmindata->company_id;
            $store->save();
    //sending request field:for whom(reciever) and for what(data):::for owner
        $fields= json_encode(array( 'to'=> $adminFcmToken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
//          return $this->sendResponse($result,'NOtification successfully sent!!');       

     //   return $this->sendResponse($model,'general shulka stored successfully!!');
 
        //again store to journal entry of general shulka
try{
     $validator =Validator::make($input,[
  	       // 'bill_payment_date' => 'required',
          //  'date' => 'required',
         //   'bill_no'=>'required',
          //  'debit_particular'=>'required',
        //    'credit_particular'=>'required',
         //  'debit_amount'=>'required',
          //  'credit_amount'=>'required',
          //  'total_amount'=>'required',
            'added_by'=>'required',
            'company_id'=>'required',
  ]);
  if($validator->fails()){
            DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
    try{
    if($req->input('durghatana_jokhim_shulka')>0&&!is_null($sumdurghatanajokhimshulka)&&$sumdurghatanajokhimshulka<$req->input('durghatana_jokhim_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('durghatana_jokhim_shulka')>0&&!is_null($sumdurghatanajokhimshulka)&&$sumdurghatanajokhimshulka>=$req->input('durghatana_jokhim_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumdurghatanajokhimshulka-$req->input('durghatana_jokhim_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('bus_no',$req['bus_no'])->where('payment_type','durghatana_shulka')->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumdurghatanajokhimshulka&&$sumdurghatanajokhimshulka==$req->input('durghatana_jokhim_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumdurghatanajokhimshulka&&$sumdurghatanajokhimshulka>$req->input('durghatana_jokhim_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('durghatana_jokhim_shulka');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$req->input('durghatana_jokhim_shulka');
                $model->credit_amount=$req->input('durghatana_jokhim_shulka');
                $model->total_amount=$req->input('durghatana_jokhim_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding durghatana jokhim shulka a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='durghatana_jokhim_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumdurghatanajokhimshulka==$req->input('durghatana_jokhim_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumdurghatanajokhimshulka>$req->input('durghatana_jokhim_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('durghatana_jokhim_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding durghatana jokhim shulka a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding durghatana jokhim shulka a/c';
                $model->credit_particular='durghatana jokhim shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','durghatana_jokhim_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='durghatana_jokhim_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{
    if($req->input('membership_shulka')>0&&!is_null($summembershipshulka)&&$summembershipshulka<$req->input('membership_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('membership_shulka')>0&&!is_null($summembershipshulka)&&$summembershipshulka>=$req->input('membership_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$summembershipshulka-$req->input('membership_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$summembershipshulka&&$summembershipshulka==$req->input('membership_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$summembershipshulka&&$summembershipshulka>$req->input('membership_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('membership_shulka');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$req->input('membership_shulka');
                $model->credit_amount=$req->input('membership_shulka');
                $model->total_amount=$req->input('membership_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding membership shulka a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='membership_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($summembershipshulka==$req->input('membership_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($summembershipshulka>$req->input('membership_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('membership_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding membership shulka a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding membership shulka a/c';
                $model->credit_particular='membership shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='membership_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
      }
       catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{  
    if($req->input('nibedan_shulka')>0&&!is_null($sumnibedanshulka)&&$sumnibedanshulka<$req->input('nibedan_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('nibedan_shulka')>0&&!is_null($sumnibedanshulka)&&$sumnibedanshulka>=$req->input('nibedan_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumnibedanshulka-$req->input('nibedan_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumnibedanshulka&&$sumnibedanshulka==$req->input('nibedan_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumnibedanshulka&&$sumnibedanshulka>$req->input('nibedan_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('nibedan_shulka');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$req->input('nibedan_shulka');
                $model->credit_amount=$req->input('nibedan_shulka');
                $model->total_amount=$req->input('nibedan_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding nibedan shulka a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='nibedan_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumnibedanshulka==$req->input('nibedan_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumnibedanshulka>$req->input('nibedan_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('nibedan_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding nibedan shulka a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding nibedan shulka a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='nibedan_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
      }
        catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{  
    if($req->input('anudaan_shulka')>0&&!is_null($sumanudaanshulka)&&$sumanudaanshulka<$req->input('anudaan_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('anudaan_shulka')>0&&!is_null($sumanudaanshulka)&&$sumanudaanshulka>=$req->input('anudaan_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumanudaanshulka-$req->input('anudaan_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumanudaanshulka&&$sumanudaanshulka==$req->input('anudaan_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumanudaanshulka&&$sumanudaanshulka>$req->input('anudaan_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('anudaan_shulka');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$req->input('anudaan_shulka');
                $model->credit_amount=$req->input('anudaan_shulka');
                $model->total_amount=$req->input('anudaan_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding anudaan shulka a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='anudaan_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumanudaanshulka==$req->input('anudaan_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumanudaanshulka>$req->input('anudaan_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('anudaan_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding anudaan shulka a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding anudaan shulka a/c';
                $model->credit_particular='anudaan shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','anudaan_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='anudaan_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
      }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
     try{ 
    if($req->input('sattadarta_shulka')>0&&!is_null($sumsattadartashulka)&&$sumsattadartashulka<$req->input('sattadarta_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('sattadarta_shulka')>0&&!is_null($sumsattadartashulka)&&$sumsattadartashulka>=$req->input('sattadarta_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumsattadartashulka-$req->input('sattadarta_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumsattadartashulka&&$sumsattadartashulka==$req->input('sattadarta_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumsattadartashulka&&$sumsattadartashulka>$req->input('sattadarta_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('sattadarta_shulka');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$req->input('sattadarta_shulka');
                $model->credit_amount=$req->input('sattadarta_shulka');
                $model->total_amount=$req->input('sattadarta_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding satta darta shulka a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='sattadarta_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumsattadartashulka==$req->input('sattadarta_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumsattadartashulka>$req->input('sattadarta_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('sattadarta_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding satta darta shulka a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding satta darta shulka a/c';
                $model->credit_particular='satta darta shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sattadarta_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='sattadarta_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }     
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
   try{
    if($req->input('durghatana_nirichhyad_and_nyunikarad_shulka')>0&&!is_null($sumdurghatananirixedshulka)&&$sumdurghatananirixedshulka<$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('durghatana_nirichhyad_and_nyunikarad_shulka')>0&&!is_null($sumdurghatananirixedshulka)&&$sumdurghatananirixedshulka>=$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumdurghatananirixedshulka-$req->input('durghatana_nirichhyad_and_nyunikarad_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumdurghatananirixedshulka&&$sumdurghatananirixedshulka==$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumdurghatananirixedshulka&&$sumdurghatananirixedshulka>$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('durghatana_nirichhyad_and_nyunikarad_shulka');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$req->input('durghatana_nirichhyad_and_nyunikarad_shulka');
                $model->credit_amount=$req->input('durghatana_nirichhyad_and_nyunikarad_shulka');
                $model->total_amount=$req->input('durghatana_nirichhyad_and_nyunikarad_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding durghatana nirichhyad and nyunikarad shulka a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='durghatana_nirichhyad_and_nyunikarad_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumdurghatananirixedshulka==$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumdurghatananirixedshulka>$req->input('durghatana_nirichhyad_and_nyunikarad_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('durghatana_nirichhyad_and_nyunikarad_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding durghatana nirichhyad and nyunikarad shulka a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding durghatana nirichhyad and nyunikarad shulka a/c';
                $model->credit_particular='durghatana nirichhyad and nyunikarad shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','durghatana_nirichhyad_and_nyunikarad_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='durghatana_nirichhyad_and_nyunikarad_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
   }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('peski_farchyut')>0&&!is_null($sumpeskifarchyotshulka)&&$sumpeskifarchyotshulka<$req->input('peski_farchyut')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('peski_farchyut')>0&&!is_null($sumpeskifarchyotshulka)&&$sumpeskifarchyotshulka>=$req->input('peski_farchyut')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumpeskifarchyotshulka-$req->input('peski_farchyut');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumpeskifarchyotshulka&&$sumpeskifarchyotshulka==$req->input('peski_farchyut')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumpeskifarchyotshulka&&$sumpeskifarchyotshulka>$req->input('peski_farchyut')){
       $subtractedvalue=$getpaymentprice-$req->input('peski_farchyut');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$req->input('peski_farchyut');
                $model->credit_amount=$req->input('peski_farchyut');
                $model->total_amount=$req->input('peski_farchyut');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding peski farchyut a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='peski_farchyut';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumpeskifarchyotshulka==$req->input('peski_farchyut')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumpeskifarchyotshulka>$req->input('peski_farchyut')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('peski_farchyut');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding peski farchyut a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding peski farchyut a/c';
                $model->credit_particular='peski farchyut a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','peski_farchyut')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='peski_farchyut';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }

      }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('anya_aamdani')>0&&!is_null($sumanyaamdani)&&$sumanyaamdani<$req->input('anya_aamdani')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('anya_aamdani')>0&&!is_null($sumanyaamdani)&&$sumanyaamdani>=$req->input('anya_aamdani')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumanyaamdani-$req->input('anya_aamdani');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumanyaamdani&&$sumanyaamdani==$req->input('anya_aamdani')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumanyaamdani&&$sumanyaamdani>$req->input('anya_aamdani')){
       $subtractedvalue=$getpaymentprice-$req->input('anya_aamdani');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$req->input('anya_aamdani');
                $model->credit_amount=$req->input('anya_aamdani');
                $model->total_amount=$req->input('anya_aamdani');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding anya aamdani a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='anya_aamdani';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumanyaamdani==$req->input('anya_aamdani')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumanyaamdani>$req->input('anya_aamdani')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('anya_aamdani');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding anya aamdani a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding anya aamdani a/c';
                $model->credit_particular='anya aamdani a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','anya_aamdani')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='anya_aamdani';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
       catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('reserve_shulka')>0&&!is_null($sumreserveshulka)&&$sumreserveshulka<$req->input('reserve_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('reserve_shulka')>0&&!is_null($sumreserveshulka)&&$sumreserveshulka>=$req->input('reserve_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumreserveshulka-$req->input('reserve_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumreserveshulka&&$sumreserveshulka==$req->input('reserve_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumreserveshulka&&$sumreserveshulka>$req->input('reserve_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('reserve_shulka');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$req->input('reserve_shulka');
                $model->credit_amount=$req->input('reserve_shulka');
                $model->total_amount=$req->input('reserve_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding reserve shulka a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='reserve_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumreserveshulka==$req->input('reserve_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumreserveshulka>$req->input('reserve_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('reserve_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding reserve shulka a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding reserve shulka a/c';
                $model->credit_particular='reserve shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','reserve_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='reserve_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('jageda_kosh')>0&&!is_null($sumjagedakoshshulka)&&$sumjagedakoshshulka<$req->input('jageda_kosh')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('jageda_kosh')>0&&!is_null($sumjagedakoshshulka)&&$sumjagedakoshshulka>=$req->input('jageda_kosh')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumjagedakoshshulka-$req->input('jageda_kosh');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumjagedakoshshulka&&$sumjagedakoshshulka==$req->input('jageda_kosh')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumjagedakoshshulka&&$sumjagedakoshshulka>$req->input('jageda_kosh')){
       $subtractedvalue=$getpaymentprice-$req->input('jageda_kosh');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$req->input('jageda_kosh');
                $model->credit_amount=$req->input('jageda_kosh');
                $model->total_amount=$req->input('jageda_kosh');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding jageda kosh a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='jageda_kosh';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumjagedakoshshulka==$req->input('jageda_kosh')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumjagedakoshshulka>$req->input('jageda_kosh')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('jageda_kosh');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding jageda kosh a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding jageda kosh a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','jageda_kosh')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='jageda_kosh';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }    
    }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
      if($req->input('naya_membership_shulka')>0&&!is_null($sumayamemershipshulka)&&$sumayamemershipshulka<$req->input('naya_membership_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('naya_membership_shulka')>0&&!is_null($sumayamemershipshulka)&&$sumayamemershipshulka>=$req->input('naya_membership_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumayamemershipshulka-$req->input('naya_membership_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumayamemershipshulka&&$sumayamemershipshulka==$req->input('naya_membership_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumayamemershipshulka&&$sumayamemershipshulka>$req->input('naya_membership_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('naya_membership_shulka');
         //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$req->input('naya_membership_shulka');
                $model->credit_amount=$req->input('naya_membership_shulka');
                $model->total_amount=$req->input('naya_membership_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding naya membership shulka a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='naya_membership_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumayamemershipshulka==$req->input('naya_membership_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumayamemershipshulka>$req->input('naya_membership_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('naya_membership_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding naya membership shulka a/c';
                $model->credit_particular='jageda kosh a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular=$req->input('payment_type').' a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfDurghatanaFund();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding naya membership shulka a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','durghatana_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='naya_membership_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }    
        
    }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }    
}
catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
   
   //again,preparing ledger entry of the same transaction
   
      }

             else{
            return $this->sendError('New fiscal year has not been setup yet!!');
        }
        
 // If we reach here, then
// data is valid and working.
//now Commit the queries!
DB::commit();


 return $this->sendResponse($models,'durghatana fund stored successfully!!');


      
    }
    
    public function durghatanaFundBillPaymentHistory($ownerid){
         $get=[];
         $getadminname=[];
        $getdata=DurghatanaFund::join('bus_info','durghatana_fund.bus_no','=','bus_info.bus_no')->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->where('bus_owner.owner_id',$ownerid)->select('bus_info.bus_no','durghatana_fund.*')->orderBy('durghatana_fund.id', 'desc')->get();
       foreach($getdata as $data){
       $getaddedsingle=$data->added_by;
       $durghatana_jokhim_shulka=number_format((float)$data->durghatana_jokhim_shulka, 2, '.', '');
       $membership_shulka=number_format((float)$data->membership_shulka, 2, '.', '');
       $nibedan_shulka=number_format((float)$data->nibedan_shulka, 2, '.', '');
       $anudaan_shulka=number_format((float)$data->anudaan_shulka, 2, '.', '');
       $sattadarta_shulka=number_format((float)$data->sattadarta_shulka, 2, '.', '');
       $durghatana_nirichhyad_and_nyunikarad_shulka=number_format((float)$data->durghatana_nirichhyad_and_nyunikarad_shulka, 2, '.', '');
       $peski_farchyut=number_format((float)$data->peski_farchyut, 2, '.', '');
       $anya_aamdani=number_format((float)$data->anya_aamdani, 2, '.', '');
       $reserve_shulka=number_format((float)$data->reserve_shulka, 2, '.', '');
       $jageda_kosh=number_format((float)$data->jageda_kosh, 2, '.', '');
       $naya_membership_shulka=number_format((float)$data->naya_membership_shulka, 2, '.', '');
       $total_amount=number_format((float)$data->total_amount, 2, '.', '');
      
        if(ctype_digit(strval($getaddedsingle))){
    $getadminname=DurghatanaFund::join('bus_info','durghatana_fund.bus_no','=','bus_info.bus_no')->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->where('bus_owner.owner_id',$ownerid)->join('admin_info','durghatana_fund.added_by','admin_info.admin_id')->select('admin_info.admin_name')->first();    
    $minimizetoname=json_decode($getadminname);
     $getvalue=$minimizetoname->admin_name;
      // return $this->sendResponse($getadminname,'Transaction history of a bus owner is shown successfully!!');
        $data['durghatana_jokhim_shulka']=$durghatana_jokhim_shulka;
       $data['membership_shulka']=$membership_shulka;
       $data['nibedan_shulka']=$nibedan_shulka;
       $data['anudaan_shulka']=$anudaan_shulka;
       $data['sattadarta_shulka']=$sattadarta_shulka;
       $data['durghatana_nirichhyad_and_nyunikarad_shulka']=$durghatana_nirichhyad_and_nyunikarad_shulka;
       $data['peski_farchyut']=$peski_farchyut;
       $data['anya_aamdani']=$anya_aamdani;
       $data['reserve_shulka']=$reserve_shulka;
       $data['jageda_kosh']=$jageda_kosh;
       $data['naya_membership_shulka']=$naya_membership_shulka;
       $data['total_amount']=$total_amount;
       $data['added_by']=$getvalue;
       
      $get[]=[$data
          ];
        } 
        else{
         //  $data['admin']='hey man';
   
       $data['durghatana_jokhim_shulka']=$durghatana_jokhim_shulka;
       $data['membership_shulka']=$membership_shulka;
       $data['nibedan_shulka']=$nibedan_shulka;
       $data['anudaan_shulka']=$anudaan_shulka;
       $data['sattadarta_shulka']=$sattadarta_shulka;
       $data['durghatana_nirichhyad_and_nyunikarad_shulka']=$durghatana_nirichhyad_and_nyunikarad_shulka;
       $data['peski_farchyut']=$peski_farchyut;
       $data['anya_aamdani']=$anya_aamdani;
       $data['reserve_shulka']=$reserve_shulka;
       $data['jageda_kosh']=$jageda_kosh;
       $data['naya_membership_shulka']=$naya_membership_shulka;
       $data['total_amount']=$total_amount;
       $get[]=[$data,
                ];
        }
   }
     return $this->sendResponse($get,'Transaction history of durghatana payment of a bus owner is shown successfully!!');
    }
    
//************durghatana khracha parts now::
 //verify requests from admin and provide to admin:
    public function VerifyDurghatanaKharchaPaymentRequest(Request $req,$id){
         // Start transaction!
          DB::beginTransaction();
        $input=$req->all();
        $fiscalyear=FiscalYear::OrderBy('id','DESC')->first();
        $fenddate=$fiscalyear->fiscal_year_end_date;
        $endadateformat=Carbon::createFromFormat('Y-m-d H:i:s', $fenddate);
        $getpaymentdate=Carbon::now();
        $paymentformat=Carbon::createFromFormat('Y-m-d H:i:s', $getpaymentdate);
        $paymentformatendofday=Carbon::parse($paymentformat)->endOfDay();
        $getfyear=$fiscalyear->fiscal_year;
  if($endadateformat->gt($paymentformatendofday)){
        if($req['pending_status']){
    try{
         $validator =Validator::make($input,[ 'staff_id'=>'required',
         ]);
         if($validator->fails()){
            DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);
         }
  	     //now store it to general kharcha as well by shifting datas
       $getdata=DurghatanaKharchaDummy::where('id',$id)->where('verification_status',$req['pending_status'])->first();
       $bill_payment_date=$getdata->bill_payment_date;
       $kharcha_received_by=$getdata->kharcha_received_by;
       $kharcha_recipient_address=$getdata->kharcha_recipient_address;
       $peski_bibarad_kharcha=$getdata->peski_bibarad_kharcha;
       $masalanda_kharcha=$getdata->masalanda_kharcha;
       $chhapai_kharcha=$getdata->chhapai_kharcha;
       $khaja_kharcha=$getdata->khaja_kharcha;
       $aarthik_sahayog_kharcha=$getdata->aarthik_sahayog_kharcha;
       $durghatana_aarthik_sahayog_kharcha=$getdata->durghatana_aarthik_sahayog_kharcha;
       $baithak_kharcha=$getdata->baithak_kharcha;
       $durghatana_nirixed_kharcha=$getdata->durghatana_nirixed_kharcha;
       $sanyojak_maasik_lekha_nirixed_kharcha=$getdata->sanyojak_maasik_lekha_nirixed_kharcha;
       $staff_maasik_lekha_bhatta=$getdata->staff_maasik_lekha_bhatta;
       $sanchaar_kharcha=$getdata->sanchaar_kharcha;
       $added_by=$getdata->added_by;
       $company_id=$getdata->company_id;
       $updated_by=$getdata->updated_by;
       $created_at=$getdata->created_at;
       $updated_at=$getdata->updated_at;
       $total_amount=$getdata->total_amount;
       $payment_type=$getdata->payment_type;
       $fiscal_year=$getdata->fiscal_year;
       $cheque_no=$getdata->cheque_no;
       $verification_status=$getdata->verification_status;
       //increment billno
         $getbillno=DurghatanaKharcha::orderBy('bill_no', 'DESC')->first('bill_no');
        $toarray=json_decode($getbillno);
        $getvalue=$toarray->bill_no;
        $incrementbillbyone=$getvalue+1;
        $model=new DurghatanaKharcha();
        $model->bill_no=$incrementbillbyone;
        $model->bill_payment_date=$paymentformat;
        $model->kharcha_received_by=$kharcha_received_by;
        $model->kharcha_recipient_address=$kharcha_recipient_address;
        $model->peski_bibarad_kharcha=$peski_bibarad_kharcha;
        $model->masalanda_kharcha=$masalanda_kharcha;
        $model->chhapai_kharcha=$chhapai_kharcha;
        $model->khaja_kharcha=$khaja_kharcha;
        $model->aarthik_sahayog_kharcha=$aarthik_sahayog_kharcha;
        $model->durghatana_aarthik_sahayog_kharcha=$durghatana_aarthik_sahayog_kharcha;
        $model->baithak_kharcha=$baithak_kharcha;
        $model->durghatana_nirixed_kharcha=$durghatana_nirixed_kharcha;
        $model->sanyojak_maasik_lekha_nirixed_kharcha=$sanyojak_maasik_lekha_nirixed_kharcha;
        $model->staff_maasik_lekha_bhatta=$staff_maasik_lekha_bhatta;
        $model->sanchaar_kharcha=$sanchaar_kharcha;
        $model->added_by=$added_by;
        $model->company_id=$company_id;
        $model->updated_by=$updated_by;
        $model->created_at=$created_at;
        $model->updated_at=$updated_at;
        $model->total_amount=$total_amount;
        $model->payment_type=$payment_type;
        $model->fiscal_year=$fiscal_year;
        $model->cheque_no=$cheque_no;
        $getstaffname=StaffInfo::where('staff_id',$req['staff_id'])->select('staff_name')->first();
        $model->verified_by=$getstaffname->staff_name;
        $model->save();
        $getdata->delete();
   //-------------------for sending notification immediately to Company Admin::::
        //get admin datas
        $getadmindata=DurghatanaKharcha::where('durghatana_fund_kharcha.company_id',$company_id)->join('company_info','durghatana_fund_kharcha.company_id','=','company_info.company_id')->join('admin_info', 'company_info.company_id', '=', 'admin_info.company_id')->select('admin_info.*')->first();
    //saving in notification table as well::
            $store=new Mynotification();
            $store->type='Durghatana Kharcha Payment Verification';
            $store->notifiable_id=$getadmindata->admin_id;
            $store->notifiable_type=LoginModel::class;
            $store->title='Durghatana kharcha payment verified';
            $store->body='Durghatana kharcha payment of rupees'.$total_amount.' '.'has been verified by '.$model->verified_by;
            $store->added_by=$added_by;
            $store->created_at=Carbon::now();
            $store->company_id=$getadmindata->company_id;
            $store->save();
//again store to journal entry of durghatana kharcha
try{
     $validator =Validator::make($input,[
  	     ]);
  if($validator->fails()){
            DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
      try{
    if($peski_bibarad_kharcha>0) {
    $model=new JournalEntryOfDurghatanaFund();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='peski bibarad kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$peski_bibarad_kharcha;
    $model->credit_amount=$peski_bibarad_kharcha;
    $model->total_amount=$peski_bibarad_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }
     try{
    if($masalanda_kharcha>0) {
    $model=new JournalEntryOfDurghatanaFund();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='masalanda_kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$masalanda_kharcha;
    $model->credit_amount=$masalanda_kharcha;
    $model->total_amount=$masalanda_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }
     try{
    if($chhapai_kharcha>0) {
    $model=new JournalEntryOfDurghatanaFund();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='chhapai_kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$chhapai_kharcha;
    $model->credit_amount=$chhapai_kharcha;
    $model->total_amount=$chhapai_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }
     try{
    if($khaja_kharcha>0) {
    $model=new JournalEntryOfDurghatanaFund();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='khaja kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$khaja_kharcha;
    $model->credit_amount=$khaja_kharcha;
    $model->total_amount=$khaja_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }
     try{
    if($aarthik_sahayog_kharcha>0) {
    $model=new JournalEntryOfDurghatanaFund();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='aarthik_sahayog_kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$aarthik_sahayog_kharcha;
    $model->credit_amount=$aarthik_sahayog_kharcha;
    $model->total_amount=$aarthik_sahayog_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }
     try{
    if($durghatana_aarthik_sahayog_kharcha>0) {
    $model=new JournalEntryOfDurghatanaFund();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='durghatana aarthik sahayog kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$durghatana_aarthik_sahayog_kharcha;
    $model->credit_amount=$durghatana_aarthik_sahayog_kharcha;
    $model->total_amount=$durghatana_aarthik_sahayog_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }
     try{
    if($baithak_kharcha>0) {
    $model=new JournalEntryOfDurghatanaFund();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='baithak kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$baithak_kharcha;
    $model->credit_amount=$baithak_kharcha;
    $model->total_amount=$baithak_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }
     try{
    if($durghatana_nirixed_kharcha>0) {
    $model=new JournalEntryOfDurghatanaFund();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='durghatana_nirixed_kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$durghatana_nirixed_kharcha;
    $model->credit_amount=$durghatana_nirixed_kharcha;
    $model->total_amount=$durghatana_nirixed_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }
     try{
    if($sanyojak_maasik_lekha_nirixed_kharcha>0) {
    $model=new JournalEntryOfDurghatanaFund();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='$sanyojak maasik lekha nirixed kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$sanyojak_maasik_lekha_nirixed_kharcha;
    $model->credit_amount=$sanyojak_maasik_lekha_nirixed_kharcha;
    $model->total_amount=$sanyojak_maasik_lekha_nirixed_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }
     try{
    if($staff_maasik_lekha_bhatta>0) {
    $model=new JournalEntryOfDurghatanaFund();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='$staff maasik lekha bhatta a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$staff_maasik_lekha_bhatta;
    $model->credit_amount=$staff_maasik_lekha_bhatta;
    $model->total_amount=$staff_maasik_lekha_bhatta;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }
     try{
    if($sanchaar_kharcha>0) {
    $model=new JournalEntryOfDurghatanaFund();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='sanchaar kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$sanchaar_kharcha;
    $model->credit_amount=$sanchaar_kharcha;
    $model->total_amount=$sanchaar_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }
 
}
 catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   } 
    }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }
  }
      if($req['cancel_status']){
          $updatestatus=DurghatanaKharchaDummy::where('id',$id)->update(['verification_status' => $req['cancel_status']]);
         // $getall=DurghatanaKharchaDummy::where('id',$id)->first();
           return $this->sendResponse('done!','durghatana kharcha payment status changed to cancelled!!');
          }

      
}
      //if not the fiscal year
 else{
            return $this->sendError('New fiscal year has not been setup yet!!');
        }
// If we reach here, then
// data is valid and working.
// Commit the queries!
DB::commit();
    return $this->sendResponse('done','durghatana kharcha verified successfully!!');
        
        
    }
  //show general kharcha payment request made by admin
    public function ShowDummyDShulka($companyid){
     $getdata=DurghatanaKharchaDummy::where('company_id',$companyid)->get();
     return $this->sendResponse($getdata,'Payment request by admin is shown successfully!');    
    }    
  //show general shulka verified records of today
   public function showVerifiedDShulkaToday($staffid){
        $getstaffname=StaffInfo::where('staff_id',$staffid)->select('staff_name')->first();
        $getname=$getstaffname->staff_name;
        $gettodaydata=DurghatanaKharcha::where('verified_by',$getname)->whereDate('bill_payment_date', Carbon::today())->get();
        return $this->sendResponse($gettodaydata,'verified records of durghatana kharcha of today shown successfully!!');

   }
 //show total due of durghatana shulka of vehicle owner
  public function showDurghatanaShulkaDueOwner($ownerphone){
      \DB::statement("SET SQL_MODE=''");
     $getdues=AccountPriceDeclaration::join('bus_info','acount_price_declaration.bus_no','bus_info.bus_no')->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')
         ->where('bus_owner.owner_phone',$ownerphone)->where('acount_price_declaration.payment_type','durghatana_shulka')
         ->select('acount_price_declaration.*','bus_info.*','bus_owner.*')//,DB::raw('group_concat(acount_price_declaration.payment_topic) as a'),DB::raw('group_concat(acount_price_declaration.payment_type) as b'))
        ->groupBy('bus_info.bus_no')->groupBy('acount_price_declaration.payment_topic')->groupBy('acount_price_declaration.payment_type')
         ->get();//select('bus_owner.owner_name','bus_owner.owner_phone','bus_info.bus_no')->get();
         //   return $this->sendResponse($getdues,'Sum of Due of indivisual payment topic of bus owner is shown successfully!!'); 
     $getarray=[];
     foreach($getdues as $dues){
         $paymenttopic=$dues->payment_topic;
         $busno=$dues->bus_no;
         $paymentprice=$dues->payment_price;
         $ownername=$dues->owner_name;
         $ownerphone=$dues->owner_phone;
         $getsum=$dues::where('payment_topic',$paymenttopic)->where('payment_type','durghatana_shulka')->where('bus_no',$busno)->sum('payment_price');
         $getarray[]=[
                 'owner_name'=>$ownername,
                 'owner_phone'=>$ownerphone,
                 'bus_no'=>$busno,
                 'payment_topic'=>$paymenttopic,
                 'rate'=>$getsum
                 ];
     }
         return $this->sendResponse($getarray,'Sum of Due of indivisual payment topic of bus owner is shown successfully!!');
  }      
    
    
    
    
    
    
    
    
    
    
}