<?php

namespace App\Http\Controllers\Api\Users\Transactions;
use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users\busOwner;
use App\Models\Users\DummyBusOwner;
use App\Models\Admin\Transactions\FiscalYear;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\DummyBusInfo;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Admin\adminInfo as admininfoResource;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\Admin\LoginModel;
use App\Models\Admin\Transactions\GeneralShulka;
class GeneralShulkaControllerAndroid extends BaseController
{
    //create bill from owner payment in android
    public function payBillandStore(Request $req){
        try{
     $input=$req->all();
     $validator =Validator::make($input,[
  	       // 'bill_payment_date' => 'required',
            'bus_no' => 'required',
            'member_name'=>'required',
            'maasik_shulka'=>'required',
            'bill_no'=>'required|unique:general_shulka'
  ]);
  if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
        $fiscalyear=FiscalYear::OrderBy('id','DESC')->first();
        $fenddate=$fiscalyear->fiscal_year_end_date;
        $endadateformat=Carbon::createFromFormat('Y-m-d H:i:s', $fenddate);
       // $fenddatecorrectformat=$fenddate->toDateTimeString();
        $getpaymentdate=Carbon::now();
        $paymentformat=Carbon::createFromFormat('Y-m-d H:i:s', $getpaymentdate);
        $paymentformatendofday=Carbon::parse($paymentformat)->endOfDay();
     //   return $this->sendResponse($paymentformat,'general shulka stored successfully!!'); 
       $getfyear=$fiscalyear->fiscal_year;
        //  $result = $date1->gt($date2);
        if($endadateformat->gt($paymentformatendofday)){
        $model=new GeneralShulka();
        $model->bill_no=$req->input('bill_no');
        $model->bill_payment_date=$paymentformat;
        $model->bus_no=$req->input('bus_no');
        $model->member_name=$req->input('member_name');
      //  number_format((float)$foo, 2, '.', '');
        $model->maasik_shulka=number_format((float)$req->input('maasik_shulka'), 2, '.', '');
        $model->kharid_bikri_darta_sulka=number_format((float)$req->input('kharid_bikri_darta_sulka'), 2, '.', '');
        $model->nibedan_shulka=number_format((float)$req->input('nibedan_shulka'), 2, '.', '');
        $model->membership_nabikarad_shulka=number_format((float)$req->input('membership_nabikarad_shulka'), 2, '.', '');
        $model->naya_membership_shulka=number_format((float)$req->input('naya_membership_shulka'), 2, '.', '');
        $model->naya_bus_register_shulka=number_format((float)$req->input('naya_bus_register_shulka'), 2, '.', '');
        $model->jariwana_shulka=number_format((float)$req->input('jariwana_shulka'), 2, '.', '');
        $model->queue_shatta_patta_shulka=number_format((float)$req->input('queue_shatta_patta_shulka'), 2, '.', '');
        $model->identitycard_shulka=number_format((float)$req->input('identitycard_shulka'), 2, '.', '');
        $model->sifaarish_shulka=number_format((float)$req->input('sifaarish_shulka'), 2, '.', '');
        $model->vawan_build_fund=number_format((float)$req->input('vawan_build_fund'), 2, '.', '');
        $model->staff_upadaan_fund=number_format((float)$req->input('staff_upadaan_fund'), 2, '.', '');
        $model->share_saving_shulka=number_format((float)$req->input('share_saving_shulka'), 2, '.', '');
        $model->sticker_shulka=number_format((float)$req->input('sticker_shulka'), 2, '.', '');
        $model->other_shulka=number_format((float)$req->input('other_shulka'), 2, '.', '');
        $model->photocopy_fee=number_format((float)$req->input('photocopy_fee'), 2, '.', '');
        $model->peski_farchyot_shulka=number_format((float)$req->input('peski_farchyot_shulka'), 2, '.', '');
        $model->shatta_darta_shulka=number_format((float)$req->input('shatta_darta_shulka'), 2, '.', '');
        $model->maasik_shulka_remarks=$req->input('maasik_shulka_remarks');
        $model->kharid_bikri_darta_sulka_remarks=$req->input('kharid_bikri_darta_sulka_remarks');
        $model->nibedan_shulka_remarks=$req->input('nibedan_shulka_remarks');
        $model->membership_nabikarad_shulka_remarks=$req->input('membership_nabikarad_shulka_remarks');
        $model->naya_membership_shulka_remarks=$req->input('naya_membership_shulka_remarks');
        $model->naya_bus_register_shulka_remarks=$req->input('naya_bus_register_shulka_remarks');
        $model->jariwana_shulka_remarks=$req->input('jariwana_shulka_remarks');
        $model->queue_shatta_patta_shulka_remarks=$req->input('queue_shatta_patta_shulka_remarks');
        $model->identitycard_shulka_remarks=$req->input('identitycard_shulka_remarks');
        $model->sifaarish_shulka_remarks=$req->input('sifaarish_shulka_remarks');
        $model->vawan_build_fund_remarks=$req->input('vawan_build_fund_remarks');
        $model->staff_upadaan_fund_remarks=$req->input('staff_upadaan_fund_remarks');
        $model->share_saving_shulka_remarks=$req->input('share_saving_shulka_remarks');
        $model->sticker_shulka_remarks=$req->input('sticker_shulka_remarks');
        $model->other_shulka_remarks=$req->input('other_shulka_remarks');
        $model->photocopy_fee_remarks=$req->input('photocopy_fee_remarks');
        $model->peski_farchyot_shulka_remarks=$req->input('peski_farchyot_shulka_remarks');
        $model->shatta_darta_shulka_remarks=$req->input('shatta_darta_shulka_remarks');
        $model->added_by=$req->input('added_by');
        $model->company_id=$req->input('company_id');
        $model->updated_by=$req->input('updated_by');
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->total_amount=$req->input('total_amount');
        $model->payment_type='cash';
        $model->fiscal_year=$getfyear;
        $model->save();
        return $this->sendResponse($model,'general shulka stored successfully!!');
        }
         else{
            return $this->sendError('New fiscal year has not been setup yet!!');
        }
 }
            
   catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
   

        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}