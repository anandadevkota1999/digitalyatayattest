<?php

namespace App\Http\Controllers\Api\Users\Transactions;
use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Admin\Transactions\GeneralShulkaKharcha;
use App\Models\Admin\Transactions\GeneralKharchaDummy;
use App\Models\Admin\Transactions\FiscalYear;
use App\Models\Admin\Transactions\AccountPriceDeclaration;
use App\Models\Admin\Transactions\VehicleFixedAccount;
use App\Models\Admin\Transactions\JournalEntryOfGeneralShulka;
use App\Models\Users\busOwner;
use App\Models\Users\DummyBusOwner;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\DummyBusInfo;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Models\Notification as Mynotification;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Admin\adminInfo as admininfoResource;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
//use App\Models\Admin\LoginModel;
use App\Models\Admin\Transactions\GeneralShulka;
class GeneralShulkaControllerAndroid extends BaseController
{
    //create bill from owner payment in android and prepare journal
    public function payBillandStore(Request $req){
           // Start transaction!
          DB::beginTransaction();
        $fiscalyear=FiscalYear::OrderBy('id','DESC')->first();
        $fenddate=$fiscalyear->fiscal_year_end_date;
        $endadateformat=Carbon::createFromFormat('Y-m-d H:i:s', $fenddate);
       // $fenddatecorrectformat=$fenddate->toDateTimeString();
        $getpaymentdate=Carbon::now();
        $paymentformat=Carbon::createFromFormat('Y-m-d H:i:s', $getpaymentdate);
        $paymentformatendofday=Carbon::parse($paymentformat)->endOfDay();
     //   return $this->sendResponse($paymentformat,'general shulka stored successfully!!'); 
       $getfyear=$fiscalyear->fiscal_year;
        
      
        //check for fiscal year
        if($endadateformat->gt($paymentformatendofday)){
            $getbillno=GeneralShulka::orderBy('bill_no', 'DESC')->first('bill_no');
        $toarray=json_decode($getbillno);
        $getvalue=$toarray->bill_no;
        $incrementbillbyone=$getvalue+1;
       $summaasikshulka=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('bus_no',$req['bus_no'])->where('payment_type','general_shulka')->sum('payment_price');
        //  return $this->sendResponse($summaasikshulka,'shulka stored successfully!!'); 
         $sumkharidbikrishulka=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price'); 
         $sumnibedanshulka=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
        //   return $this->sendResponse($sumnibedanshulka,'shulka stored successfully!!'); 
         $summmemberrenewshulka=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumnewmembershulka=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumnewbusregistershulka=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumjariwanashulka=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumquesattapattashulka=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumidentitycardshulka=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumsifaarishshulka=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumvawanbuildshulka=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumstaffupadaanshulka=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumsharesavingshulka=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumstickershulka=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumothershulka=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumphotocopyshulka=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumpeskifarchyotshulka=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');
         $sumsattadartashulka=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->sum('payment_price');

        
     $input=$req->all();
     $validator =Validator::make($input,[
  	       // 'bill_payment_date' => 'required',
            'bus_no' => 'required',
            'member_name'=>'required',
            'maasik_shulka'=>'required',
            'kharid_bikri_darta_sulka'=>'required',
            'nibedan_shulka'=>'required',
            'membership_nabikarad_shulka'=>'required',
            'naya_membership_shulka'=>'required',
            'naya_bus_register_shulka'=>'required',
            'jariwana_shulka'=>'required',
            'queue_shatta_patta_shulka'=>'required',
            'identitycard_shulka'=>'required',
            'sifaarish_shulka'=>'required',
            'kharid_bikri_darta_sulka'=>'required',
            'vawan_build_fund'=>'required',
            'staff_upadaan_fund'=>'required',
            'share_saving_shulka'=>'required',
            'sticker_shulka'=>'required',
            'other_shulka'=>'required',
            'photocopy_fee'=>'required',
            'peski_farchyot_shulka'=>'required',
            'shatta_darta_shulka'=>'required',
          //  'bill_no'=>'unique:general_shulka'
            
  ]);
  if($validator->fails()){
              DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
         //checking if all input values are zero
        if($req->input('maasik_shulka')==0&&$req->input('kharid_bikri_darta_sulka')==0&&$req->input('naya_membership_shulka')==0&&$req->input('naya_bus_register_shulka')==0&&$req->input('jariwana_shulka')==0&&$req->input('queue_shatta_patta_shulka')==0
        &&$req->input('identitycard_shulka')==0&&$req->input('sifaarish_shulka')==0&&$req->input('vawan_build_fund')==0&&$req->input('staff_upadaan_fund')==0&&$req->input('share_saving_shulka')==0&&$req->input('sticker_shulka')==0&&$req->input('other_shulka')==0
        &&$req->input('photocopy_fee')==0&&$req->input('peski_farchyot_shulka')==0&&$req->input('shatta_darta_shulka')==0)
        {
             DB::rollback();
             return $this->sendError('please pay atleast on a topic!');
        }
        else{
        $model=new GeneralShulka();
        $model->bill_no=$incrementbillbyone;
        $model->bill_payment_date=Carbon::now()->toDateTimeString();
        $model->bus_no=$req->input('bus_no');
        $model->member_name=$req->input('member_name');
        if($summaasikshulka<$req->input('maasik_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($summaasikshulka)&&$summaasikshulka>=$req->input('maasik_shulka')){
        $model->maasik_shulka=number_format((float)$req->input('maasik_shulka'), 2, '.', ''); 
               }
     if($sumkharidbikrishulka<$req->input('kharid_bikri_darta_sulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumkharidbikrishulka)&&$sumkharidbikrishulka>=$req->input('kharid_bikri_darta_sulka')){
        $model->kharid_bikri_darta_sulka=number_format((float)$req->input('kharid_bikri_darta_sulka'), 2, '.', '');
      }
     if($sumnibedanshulka<$req->input('nibedan_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumnibedanshulka)&&$sumnibedanshulka>=$req->input('nibedan_shulka')){
      $model->nibedan_shulka=number_format((float)$req->input('nibedan_shulka'), 2, '.', '');
      }
     if($summmemberrenewshulka<$req->input('membership_nabikarad_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($summmemberrenewshulka)&&$summmemberrenewshulka>=$req->input('membership_nabikarad_shulka')){
      $model->membership_nabikarad_shulka=number_format((float)$req->input('membership_nabikarad_shulka'), 2, '.', '');
      }
     if($sumnewmembershulka<$req->input('naya_membership_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumnewmembershulka)&&$sumnewmembershulka>=$req->input('naya_membership_shulka')){
      $model->naya_membership_shulka=number_format((float)$req->input('naya_membership_shulka'), 2, '.', '');
      }
     if($sumnewbusregistershulka<$req->input('naya_bus_register_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumnewbusregistershulka)&&$sumnewbusregistershulka>=$req->input('naya_bus_register_shulka')){
      $model->naya_bus_register_shulka=number_format((float)$req->input('naya_bus_register_shulka'), 2, '.', '');
      }
     if($sumjariwanashulka<$req->input('jariwana_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumjariwanashulka)&&$sumjariwanashulka>=$req->input('jariwana_shulka')){
      $model->jariwana_shulka=number_format((float)$req->input('jariwana_shulka'), 2, '.', '');
      }
     if($sumquesattapattashulka<$req->input('queue_shatta_patta_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumquesattapattashulka)&&$sumquesattapattashulka>=$req->input('queue_shatta_patta_shulka')){
      $model->queue_shatta_patta_shulka=number_format((float)$req->input('queue_shatta_patta_shulka'), 2, '.', '');
      }
     if($sumidentitycardshulka<$req->input('identitycard_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumidentitycardshulka)&&$sumidentitycardshulka>=$req->input('identitycard_shulka')){
      $model->identitycard_shulka=number_format((float)$req->input('identitycard_shulka'), 2, '.', '');
      }
     if($sumsifaarishshulka<$req->input('sifaarish_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumsifaarishshulka)&&$sumsifaarishshulka>=$req->input('sifaarish_shulka')){
      $model->sifaarish_shulka=number_format((float)$req->input('sifaarish_shulka'), 2, '.', '');
      }
     if($sumvawanbuildshulka<$req->input('vawan_build_fund')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumvawanbuildshulka)&&$sumvawanbuildshulka>=$req->input('vawan_build_fund')){
      $model->vawan_build_fund=number_format((float)$req->input('vawan_build_fund'), 2, '.', '');
      }
     if($sumstaffupadaanshulka<$req->input('staff_upadaan_fund')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumstaffupadaanshulka)&&$sumstaffupadaanshulka>=$req->input('staff_upadaan_fund')){
      $model->staff_upadaan_fund=number_format((float)$req->input('staff_upadaan_fund'), 2, '.', '');
      }
     if($sumsharesavingshulka<$req->input('share_saving_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumsharesavingshulka)&&$sumsharesavingshulka>=$req->input('share_saving_shulka')){
      $model->share_saving_shulka=number_format((float)$req->input('share_saving_shulka'), 2, '.', '');
      }
     if($sumstickershulka<$req->input('sticker_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumstickershulka)&&$sumstickershulka>=$req->input('sticker_shulka')){
      $model->sticker_shulka=number_format((float)$req->input('sticker_shulka'), 2, '.', '');
      }
     if($sumothershulka<$req->input('other_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     } 
     if(!is_null($sumothershulka)&&$sumothershulka>=$req->input('other_shulka')){
      $model->other_shulka=number_format((float)$req->input('other_shulka'), 2, '.', '');
      }
     if($sumphotocopyshulka<$req->input('photocopy_fee')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumphotocopyshulka)&&$sumphotocopyshulka>=$req->input('photocopy_fee')){
      $model->photocopy_fee=number_format((float)$req->input('photocopy_fee'), 2, '.', '');
      }
     if($sumpeskifarchyotshulka<$req->input('peski_farchyot_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumpeskifarchyotshulka)&&$sumpeskifarchyotshulka>=$req->input('peski_farchyot_shulka')){
      $model->peski_farchyot_shulka=number_format((float)$req->input('peski_farchyot_shulka'), 2, '.', '');
      }
     if($sumsattadartashulka<$req->input('shatta_darta_shulka')){
       DB::rollback();
         return $this->sendError('you are paying higher than required!!');
     }
     if(!is_null($sumsattadartashulka)&&$sumsattadartashulka>=$req->input('shatta_darta_shulka')){
      $model->shatta_darta_shulka=number_format((float)$req->input('shatta_darta_shulka'), 2, '.', '');
      }
        $model->maasik_shulka_remarks=$req->input('maasik_shulka_remarks');
        $model->kharid_bikri_darta_sulka_remarks=$req->input('kharid_bikri_darta_sulka_remarks');
        $model->nibedan_shulka_remarks=$req->input('nibedan_shulka_remarks');
        $model->membership_nabikarad_shulka_remarks=$req->input('membership_nabikarad_shulka_remarks');
        $model->naya_membership_shulka_remarks=$req->input('naya_membership_shulka_remarks');
        $model->naya_bus_register_shulka_remarks=$req->input('naya_bus_register_shulka_remarks');
        $model->jariwana_shulka_remarks=$req->input('jariwana_shulka_remarks');
        $model->queue_shatta_patta_shulka_remarks=$req->input('queue_shatta_patta_shulka_remarks');
        $model->identitycard_shulka_remarks=$req->input('identitycard_shulka_remarks');
        $model->sifaarish_shulka_remarks=$req->input('sifaarish_shulka_remarks');
        $model->vawan_build_fund_remarks=$req->input('vawan_build_fund_remarks');
        $model->staff_upadaan_fund_remarks=$req->input('staff_upadaan_fund_remarks');
        $model->share_saving_shulka_remarks=$req->input('share_saving_shulka_remarks');
        $model->sticker_shulka_remarks=$req->input('sticker_shulka_remarks');
        $model->other_shulka_remarks=$req->input('other_shulka_remarks');
        $model->photocopy_fee_remarks=$req->input('photocopy_fee_remarks');
        $model->peski_farchyot_shulka_remarks=$req->input('peski_farchyot_shulka_remarks');
        $model->shatta_darta_shulka_remarks=$req->input('shatta_darta_shulka_remarks');
        $model->added_by=$req->input('added_by');
        $model->company_id=$req->input('company_id');
        $model->updated_by=$req->input('updated_by');
        $model->created_at=Carbon::now();
        $model->updated_at=Carbon::now();
        $model->total_amount=$req->input('total_amount');
        $model->payment_type='digital';
        $model->fiscal_year=$getfyear;
        $model->save();
        }
        //-------------------for sending notification immediately to Company Admin::::
// ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
        //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
        //for curl headers
            $headers = array();
            $headers = [
            'Authorization:key=' . GOOGLE_API_KEY,
            'Content-Type: application/json',];
     
        //notification contents:
            $notify=array('title'=>'General Shulka Payment','body'=>'Vehicle Owner '.$model->member_name.' '.'has paid total of '.$model->total_amount.' rupees.');
        //get admin datas
        $getadmindata=GeneralShulka::where('general_shulka.bus_no',$req->input('bus_no'))->join('bus_info','general_shulka.bus_no','=','bus_info.bus_no')->join('admin_info', 'bus_info.company_id', '=', 'admin_info.company_id')->select('bus_info.owner_phone','admin_info.*')->first();
        //get admin token
    $adminFcmToken=$getadmindata->device_token;
    //saving in notification table as well::
            $store=new Mynotification();
             $store->type='General Shulka Payment';
            $store->notifiable_id=$getadmindata->admin_id;
            $store->notifiable_type=LoginModel::class;
            $store->title='Owner Shulka Paid';
            $store->body='Vehicle Owner '.$model->member_name.' '.'has paid total of '.$model->total_amount.' rupees.';
            $store->added_by=$req['added_by'];
            $store->created_at=Carbon::now();
            $store->company_id=$getadmindata->company_id;
            $store->save();
    //sending request field:for whom(reciever) and for what(data):::for owner
        $fields= json_encode(array( 'to'=> $adminFcmToken,
        'notification'=>$notify));  
          $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        

        // Close connection
        curl_close($ch);

        // FCM response
//          return $this->sendResponse($result,'NOtification successfully sent!!');       

     //   return $this->sendResponse($model,'general shulka stored successfully!!');
 
        //again store to journal entry of general shulka
try{
      $validator =Validator::make($input,[
  	       // 'bill_payment_date' => 'required',
          //  'date' => 'required',
          //  'bill_no'=>'required',
          //  'debit_particular'=>'required',
        //    'credit_particular'=>'required',
         //  'debit_amount'=>'required',
          //  'credit_amount'=>'required',
          //  'total_amount'=>'required',
            'added_by'=>'required',
            'company_id'=>'required',
  ]);
  if($validator->fails()){
            DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
   try{
       if($req->input('maasik_shulka')>0&&!is_null($summaasikshulka)&&$summaasikshulka<$req->input('maasik_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }
    if($req->input('maasik_shulka')>0&&!is_null($summaasikshulka)&&$summaasikshulka>=$req->input('maasik_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$summaasikshulka-$req->input('maasik_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$summaasikshulka&&$summaasikshulka==$req->input('maasik_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$summaasikshulka&&$summaasikshulka>$req->input('maasik_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('maasik_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$req->input('maasik_shulka');
                $model->credit_amount=$req->input('maasik_shulka');
                $model->total_amount=$req->input('maasik_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding maasik shulka a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='maasik_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($summaasikshulka==$req->input('maasik_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($summaasikshulka>$req->input('maasik_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('maasik_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding maasik shulka a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding maasik shulka a/c';
                $model->credit_particular='maasik shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','maasik_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='maasik_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{
       if($req->input('kharid_bikri_darta_sulka')>0&&!is_null($sumkharidbikrishulka)&&$sumkharidbikrishulka<$req->input('kharid_bikri_darta_sulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('kharid_bikri_darta_sulka')>0&&!is_null($sumkharidbikrishulka)&&$sumkharidbikrishulka>=$req->input('kharid_bikri_darta_sulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumkharidbikrishulka-$req->input('kharid_bikri_darta_sulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumkharidbikrishulka&&$sumkharidbikrishulka==$req->input('kharid_bikri_darta_sulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumkharidbikrishulka&&$sumkharidbikrishulka>$req->input('kharid_bikri_darta_sulka')){
       $subtractedvalue=$getpaymentprice-$req->input('kharid_bikri_darta_sulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$req->input('kharid_bikri_darta_sulka');
                $model->credit_amount=$req->input('kharid_bikri_darta_sulka');
                $model->total_amount=$req->input('kharid_bikri_darta_sulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding kharid bikri darta shulka a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='kharid_bikri_darta_sulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumkharidbikrishulka==$req->input('kharid_bikri_darta_sulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumkharidbikrishulka>$req->input('kharid_bikri_darta_sulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('kharid_bikri_darta_sulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price);
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding kharid bikri darta shulka a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding kharid bikri darta shulka a/c';
                $model->credit_particular='kharid bikri darta shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','kharid_bikri_darta_sulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='kharid_bikri_darta_sulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
    }
       catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{  
      if($req->input('nibedan_shulka')>0&&!is_null($sumnibedanshulka)&&$sumnibedanshulka<$req->input('nibedan_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('nibedan_shulka')>0&&!is_null($sumnibedanshulka)&&$sumnibedanshulka>=$req->input('nibedan_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumnibedanshulka-$req->input('nibedan_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumnibedanshulka&&$sumnibedanshulka==$req->input('nibedan_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumnibedanshulka&&$sumnibedanshulka>$req->input('nibedan_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('nibedan_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$req->input('nibedan_shulka');
                $model->credit_amount=$req->input('nibedan_shulka');
                $model->total_amount=$req->input('nibedan_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding nibedan shulka a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='nibedan_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumnibedanshulka==$req->input('nibedan_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumnibedanshulka>$req->input('nibedan_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('nibedan_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding nibedan shulka a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding nibedan shulka a/c';
                $model->credit_particular='nibedan shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','nibedan_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='nibedan_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }

      }
        catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{  
     if($req->input('membership_nabikarad_shulka')>0&&!is_null($summmemberrenewshulka)&&$summmemberrenewshulka<$req->input('membership_nabikarad_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('membership_nabikarad_shulka')>0&&!is_null($summmemberrenewshulka)&&$summmemberrenewshulka>=$req->input('membership_nabikarad_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$summmemberrenewshulka-$req->input('membership_nabikarad_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$summmemberrenewshulka&&$summmemberrenewshulka==$req->input('membership_nabikarad_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$summmemberrenewshulka&&$summmemberrenewshulka>$req->input('membership_nabikarad_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('membership_nabikarad_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$req->input('membership_nabikarad_shulka');
                $model->credit_amount=$req->input('membership_nabikarad_shulka');
                $model->total_amount=$req->input('membership_nabikarad_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding membership nabikarad shulka a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='membership_nabikarad_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($summmemberrenewshulka==$req->input('membership_nabikarad_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($summmemberrenewshulka>$req->input('membership_nabikarad_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('membership_nabikarad_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding membership nabikarad shulka a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding membership nabikarad shulka a/c';
                $model->credit_particular='membership nabikarad shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','membership_nabikarad_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='membership_nabikarad_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
      }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
     try{ 
    if($req->input('naya_membership_shulka')>0&&!is_null($sumnewmembershulka)&&$sumnewmembershulka<$req->input('naya_membership_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('naya_membership_shulka')>0&&!is_null($sumnewmembershulka)&&$sumnewmembershulka>=$req->input('naya_membership_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumnewmembershulka-$req->input('naya_membership_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumnewmembershulka&&$sumnewmembershulka==$req->input('naya_membership_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumnewmembershulka&&$sumnewmembershulka>$req->input('naya_membership_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('naya_membership_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$req->input('naya_membership_shulka');
                $model->credit_amount=$req->input('naya_membership_shulka');
                $model->total_amount=$req->input('naya_membership_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding naya membership shulka a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='naya_membership_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumnewmembershulka==$req->input('naya_membership_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumnewmembershulka>$req->input('naya_membership_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('naya_membership_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding naya membership shulka a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding naya membership shulka a/c';
                $model->credit_particular='naya membership shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','naya_membership_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='naya_membership_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }     
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
   try{
  if($req->input('naya_bus_register_shulka')>0&&!is_null($sumnewbusregistershulka)&&$sumnewbusregistershulka<$req->input('naya_bus_register_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('naya_bus_register_shulka')>0&&!is_null($sumnewbusregistershulka)&&$sumnewbusregistershulka>=$req->input('naya_bus_register_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumnewbusregistershulka-$req->input('naya_bus_register_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumnewbusregistershulka&&$sumnewbusregistershulka==$req->input('naya_bus_register_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumnewbusregistershulka&&$sumnewbusregistershulka>$req->input('naya_bus_register_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('naya_bus_register_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$req->input('naya_bus_register_shulka');
                $model->credit_amount=$req->input('naya_bus_register_shulka');
                $model->total_amount=$req->input('naya_bus_register_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding naya bus register shulka a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='naya_bus_register_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumnewbusregistershulka==$req->input('naya_bus_register_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumnewbusregistershulka>$req->input('naya_bus_register_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('naya_bus_register_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding naya bus register shulka a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding naya bus register shulka a/c';
                $model->credit_particular='naya bus register shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','naya_bus_register_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='naya_bus_register_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
   }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('jariwana_shulka')>0&&!is_null($sumjariwanashulka)&&$sumjariwanashulka<$req->input('jariwana_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('jariwana_shulka')>0&&!is_null($sumjariwanashulka)&&$sumjariwanashulka>=$req->input('jariwana_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumjariwanashulka-$req->input('jariwana_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumjariwanashulka&&$sumjariwanashulka==$req->input('jariwana_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumjariwanashulka&&$sumjariwanashulka>$req->input('jariwana_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('jariwana_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$req->input('jariwana_shulka');
                $model->credit_amount=$req->input('jariwana_shulka');
                $model->total_amount=$req->input('jariwana_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding jariwana shulka a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='jariwana_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumjariwanashulka==$req->input('jariwana_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumjariwanashulka>$req->input('jariwana_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('jariwana_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding jariwana shulka a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding jariwana shulka a/c';
                $model->credit_particular='jariwana shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','jariwana_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='jariwana_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
      }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
     if($req->input('queue_shatta_patta_shulka')>0&&!is_null($sumquesattapattashulka)&&$sumquesattapattashulka<$req->input('queue_shatta_patta_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('queue_shatta_patta_shulka')>0&&!is_null($sumquesattapattashulka)&&$sumquesattapattashulka>=$req->input('queue_shatta_patta_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumquesattapattashulka-$req->input('queue_shatta_patta_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumquesattapattashulka&&$sumquesattapattashulka==$req->input('queue_shatta_patta_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumquesattapattashulka&&$sumquesattapattashulka>$req->input('queue_shatta_patta_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('queue_shatta_patta_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$req->input('queue_shatta_patta_shulka');
                $model->credit_amount=$req->input('queue_shatta_patta_shulka');
                $model->total_amount=$req->input('queue_shatta_patta_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding queue shatta patta shulka a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='queue_shatta_patta_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumquesattapattashulka==$req->input('queue_shatta_patta_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumquesattapattashulka>$req->input('queue_shatta_patta_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('queue_shatta_patta_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding queue shatta patta shulka a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding queue shatta patta shulka a/c';
                $model->credit_particular='queue shatta patta shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','queue_shatta_patta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='queue_shatta_patta_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
       catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('identitycard_shulka')>0&&!is_null($sumidentitycardshulka)&&$sumidentitycardshulka<$req->input('identitycard_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('identitycard_shulka')>0&&!is_null($sumidentitycardshulka)&&$sumidentitycardshulka>=$req->input('identitycard_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumidentitycardshulka-$req->input('identitycard_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumidentitycardshulka&&$sumidentitycardshulka==$req->input('identitycard_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumidentitycardshulka&&$sumidentitycardshulka>$req->input('identitycard_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('identitycard_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$req->input('identitycard_shulka');
                $model->credit_amount=$req->input('identitycard_shulka');
                $model->total_amount=$req->input('identitycard_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding identitycard shulka a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='identitycard_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumidentitycardshulka==$req->input('identitycard_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumidentitycardshulka>$req->input('identitycard_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('identitycard_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding identitycard shulka a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding identitycard shulka a/c';
                $model->credit_particular='identitycard shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','identitycard_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='identitycard_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
   if($req->input('sifaarish_shulka')>0&&!is_null($sumsifaarishshulka)&&$sumsifaarishshulka<$req->input('sifaarish_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('sifaarish_shulka')>0&&!is_null($sumsifaarishshulka)&&$sumsifaarishshulka>=$req->input('sifaarish_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumsifaarishshulka-$req->input('sifaarish_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumsifaarishshulka&&$sumsifaarishshulka==$req->input('sifaarish_shulka')){
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumsifaarishshulka&&$sumsifaarishshulka>$req->input('sifaarish_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('sifaarish_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$req->input('sifaarish_shulka');
                $model->credit_amount=$req->input('sifaarish_shulka');
                $model->total_amount=$req->input('sifaarish_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding sifaarish shulka a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='sifaarish_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumsifaarishshulka==$req->input('sifaarish_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumsifaarishshulka>$req->input('sifaarish_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('sifaarish_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding sifaarish shulka a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding sifaarish shulka a/c';
                $model->credit_particular='sifaarish shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sifaarish_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='sifaarish_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }     
    }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('vawan_build_fund')>0&&!is_null($sumvawanbuildshulka)&&$sumvawanbuildshulka<$req->input('vawan_build_fund')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
     if($req->input('vawan_build_fund')>0&&!is_null($sumvawanbuildshulka)&&$sumvawanbuildshulka>=$req->input('vawan_build_fund')){
  //  return $this->SendResponse('ok','done');
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumvawanbuildshulka-$req->input('vawan_build_fund');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumvawanbuildshulka&&$sumvawanbuildshulka==$req->input('vawan_build_fund')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumvawanbuildshulka&&$sumvawanbuildshulka>$req->input('vawan_build_fund')){
       $subtractedvalue=$getpaymentprice-$req->input('vawan_build_fund');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$req->input('vawan_build_fund');
                $model->credit_amount=$req->input('vawan_build_fund');
                $model->total_amount=$req->input('vawan_build_fund');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding vawan build fund a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='vawan_build_fund';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumvawanbuildshulka==$req->input('vawan_build_fund')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumvawanbuildshulka>$req->input('vawan_build_fund')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('vawan_build_fund');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding vawan build fund a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding vawan build fund a/c';
                $model->credit_particular='vawan build fund a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','vawan_build_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='vawan_build_fund';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }     
    }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('staff_upadaan_fund')>0&&!is_null($sumstaffupadaanshulka)&&$sumstaffupadaanshulka<$req->input('staff_upadaan_fund')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
    if($req->input('staff_upadaan_fund')>0&&!is_null($sumstaffupadaanshulka)&&$sumstaffupadaanshulka>=$req->input('staff_upadaan_fund')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumstaffupadaanshulka-$req->input('staff_upadaan_fund');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumstaffupadaanshulka&&$sumstaffupadaanshulka==$req->input('staff_upadaan_fund')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumstaffupadaanshulka&&$sumstaffupadaanshulka>$req->input('staff_upadaan_fund')){
       $subtractedvalue=$getpaymentprice-$req->input('staff_upadaan_fund');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$req->input('staff_upadaan_fund');
                $model->credit_amount=$req->input('staff_upadaan_fund');
                $model->total_amount=$req->input('staff_upadaan_fund');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding staff upadaan fund a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='staff_upadaan_fund';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumstaffupadaanshulka==$req->input('staff_upadaan_fund')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumstaffupadaanshulka>$req->input('staff_upadaan_fund')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('staff_upadaan_fund');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding staff upadaan fund a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding staff upadaan fund a/c';
                $model->credit_particular='staff upadaan fund a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','staff_upadaan_fund')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='staff_upadaan_fund';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }     
    }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('share_saving_shulka')>0&&!is_null($sumsharesavingshulka)&&$sumsharesavingshulka<$req->input('share_saving_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
    if($req->input('share_saving_shulka')>0&&!is_null($sumsharesavingshulka)&&$sumsharesavingshulka>=$req->input('share_saving_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumsharesavingshulka-$req->input('share_saving_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumsharesavingshulka&&$sumsharesavingshulka==$req->input('share_saving_shulka')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumsharesavingshulka&&$sumsharesavingshulka>$req->input('share_saving_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('share_saving_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$req->input('share_saving_shulka');
                $model->credit_amount=$req->input('share_saving_shulka');
                $model->total_amount=$req->input('share_saving_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding share saving shulka a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='share_saving_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumsharesavingshulka==$req->input('share_saving_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumsharesavingshulka>$req->input('share_saving_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('share_saving_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding share saving shulka a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding share saving shulka a/c';
                $model->credit_particular='share saving shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','share_saving_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='share_saving_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('sticker_shulka')>0&&!is_null($sumstickershulka)&&$sumstickershulka<$req->input('sticker_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
    if($req->input('sticker_shulka')>0&&!is_null($sumstickershulka)&&$sumstickershulka>=$req->input('sticker_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumstickershulka-$req->input('sticker_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumstickershulka&&$sumstickershulka==$req->input('sticker_shulka')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumstickershulka&&$sumstickershulka>$req->input('sticker_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('sticker_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$req->input('sticker_shulka');
                $model->credit_amount=$req->input('sticker_shulka');
                $model->total_amount=$req->input('sticker_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding sticker shulka a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='sticker_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumstickershulka==$req->input('sticker_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumstickershulka>$req->input('sticker_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('sticker_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding sticker shulka a/c';
                $model->credit_particular='sticker shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','sticker_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='sticker_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
     if($req->input('other_shulka')>0&&!is_null($sumothershulka)&&$sumothershulka<$req->input('other_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
    if($req->input('other_shulka')>0&&!is_null($sumothershulka)&&$sumothershulka>=$req->input('other_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumothershulka-$req->input('other_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumothershulka&&$sumothershulka==$req->input('other_shulka')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumothershulka&&$sumothershulka>$req->input('other_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('other_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$req->input('other_shulka');
                $model->credit_amount=$req->input('other_shulka');
                $model->total_amount=$req->input('other_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding other shulka a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='other_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumothershulka==$req->input('other_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumothershulka>$req->input('other_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('other_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding other shulka a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding other shulka a/c';
                $model->credit_particular='other shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','other_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='other_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
        
    }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('photocopy_fee')>0&&!is_null($sumphotocopyshulka)&&$sumphotocopyshulka<$req->input('photocopy_fee')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
    if($req->input('photocopy_fee')>0&&!is_null($sumphotocopyshulka)&&$sumphotocopyshulka>=$req->input('photocopy_fee')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumphotocopyshulka-$req->input('photocopy_fee');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumphotocopyshulka&&$sumphotocopyshulka==$req->input('photocopy_fee')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumphotocopyshulka&&$sumphotocopyshulka>$req->input('photocopy_fee')){
       $subtractedvalue=$getpaymentprice-$req->input('photocopy_fee');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$req->input('photocopy_fee');
                $model->credit_amount=$req->input('photocopy_fee');
                $model->total_amount=$req->input('photocopy_fee');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding photocopy shulka a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='photocopy_fee';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumphotocopyshulka==$req->input('photocopy_fee')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumphotocopyshulka>$req->input('photocopy_fee')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('photocopy_fee');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding photocopy shulka a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding photocopy shulka a/c';
                $model->credit_particular='photocopy shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','photocopy_fee')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='photocopy_fee';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('peski_farchyot_shulka')>0&&!is_null($sumpeskifarchyotshulka)&&$sumpeskifarchyotshulka<$req->input('peski_farchyot_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
    if($req->input('peski_farchyot_shulka')>0&&!is_null($sumpeskifarchyotshulka)&&$sumpeskifarchyotshulka>=$req->input('peski_farchyot_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumpeskifarchyotshulka-$req->input('peski_farchyot_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumpeskifarchyotshulka&&$sumpeskifarchyotshulka==$req->input('peski_farchyot_shulka')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumpeskifarchyotshulka&&$sumpeskifarchyotshulka>$req->input('peski_farchyot_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('peski_farchyot_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$req->input('peski_farchyot_shulka');
                $model->credit_amount=$req->input('peski_farchyot_shulka');
                $model->total_amount=$req->input('peski_farchyot_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding peski farchyot shulka a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='peski_farchyot_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumpeskifarchyotshulka==$req->input('peski_farchyot_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumpeskifarchyotshulka>$req->input('peski_farchyot_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('peski_farchyot_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding peski farchyot shulka a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding peski farchyot shulka a/c';
                $model->credit_particular='peski farchyot shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','peski_farchyot_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='peski_farchyot_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
    }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($req->input('shatta_darta_shulka')>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='bank a/c';
    $model->credit_particular='shatta darta shulka a/c';
    $model->debit_amount=$req->input('shatta_darta_shulka');
    $model->credit_amount=$req->input('shatta_darta_shulka');
    $model->total_amount=$req->input('shatta_darta_shulka');
    $model->added_by=$req->input('added_by');
    $model->company_id=$req->input('company_id');
    $model->created_at=Carbon::now();
    $model->updated_at=Carbon::now();
    $model->fiscal_year=$getfyear;    
    $model->save();
    }
    if($req->input('shatta_darta_shulka')>0&&!is_null($sumsattadartashulka)&&$sumsattadartashulka<$req->input('shatta_darta_shulka')){
           DB::rollback();
           return $this->sendError('you are paying higher than required!!');
       }  
    if($req->input('shatta_darta_shulka')>0&&!is_null($sumsattadartashulka)&&$sumsattadartashulka>=$req->input('shatta_darta_shulka')){
    //1st check approtiate condition
      //reduce paid balance from aacount on this topic
        $reducedvalue=$sumsattadartashulka-$req->input('shatta_darta_shulka');
       //if there is single data of single month and full payment is done
       $getsinglemonthvaluecount=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->count('id');
       $getvalue=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
       $getpaymentprice=$getvalue->payment_price;
       //***********single data in the records ******
      // return $this->SendResponse('ok','done');
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount==1){
        
        //if there is single data and full payment is done   
        if($getpaymentprice==$sumsattadartashulka&&$sumsattadartashulka==$req->input('shatta_darta_shulka')){
          
           //1st prepare journal
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$getpaymentprice;
                $model->credit_amount=$getpaymentprice;
                $model->total_amount=$getpaymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $getinarray[]=['1'=>$model];
                //then,delete as well from account
                $getvalue->delete();     
        }
        
        //if there is single month data and partial payment is done
        if($getpaymentprice==$sumsattadartashulka&&$sumsattadartashulka>$req->input('shatta_darta_shulka')){
       $subtractedvalue=$getpaymentprice-$req->input('shatta_darta_shulka');
         //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$req->input('shatta_darta_shulka');
                $model->credit_amount=$req->input('shatta_darta_shulka');
                $model->total_amount=$req->input('shatta_darta_shulka');
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();  
                 $getinarray[]=['2'=>$model];
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding shatta darta shulka a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$subtractedvalue;
                $model->credit_amount=$subtractedvalue;
                $model->total_amount=$subtractedvalue;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinarray[]=['2'=>$model]; 
            $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();    
                 $getvalue->delete();
            //save the remaining price in the account as well    
            $paymentoption=$getvehicleaccountdetail->payment_option; 
            $paymenttype=$getvehicleaccountdetail->payment_type;
            $expiry=$getvehicleaccountdetail->expiry_date;
            $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='shatta_darta_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$subtractedvalue;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save();  
                $getinarray[]=['2'=>$model];
        }
       }
       
     //**********multiple data in the record***********  
    
       if(!is_null($getsinglemonthvaluecount)&&$getsinglemonthvaluecount>1){
   //now, for multiple month,and when full amount is paid
      if($sumsattadartashulka==$req->input('shatta_darta_shulka')){
       $getallvalues=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
       foreach($getallvalues as $value){
           $paymentprice=$value->payment_price;
        //now,prepare journal
          $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                 $getinarray[]=['3'=>$model];
                //now delete the 1st item
                $value->delete();
       }
       unset($value);
      }
    //again,for multiple month and partial amount is paid!!
    if($sumsattadartashulka>$req->input('shatta_darta_shulka')){
    $getallvalues=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->get();
    $getinput=$req->input('shatta_darta_shulka');
    $totaloutstanding=0;
    $getvehicleaccountdetail;
    foreach($getallvalues as $value){
         $paymentprice=number_format((float)$value->payment_price, 2, '.', '');
     if($getinput<=0){
         $totaloutstanding=$totaloutstanding+$paymentprice;
          //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding shatta darta shulka a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                $value->delete(); 
                $getinarray[]=['4'=>$model];
     }    
     elseif($getinput>$paymentprice){
          //prepare journal
               $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$paymentprice;
                $model->credit_amount=$paymentprice;
                $model->total_amount=$paymentprice;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save();
                $value->delete();
               $getinput=$getinput-$paymentprice; 
                 $getinarray[]=['4'=>$model];
     }
     elseif($getinput<$paymentprice){
           //cash or bank portion
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='bank a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$getinput;
                $model->credit_amount=$getinput;
                $model->total_amount=$getinput;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $outstanding=$paymentprice-$getinput;
                $getinarray[]=['4'=>$model];    
            //outstanding portion    
                $model=new JournalEntryOfGeneralShulka();   
                $model->date=Carbon::now();
                $model->bill_no=$incrementbillbyone;
                $model->debit_particular='outstanding shatta darta shulka a/c';
                $model->credit_particular='shatta darta shulka a/c';
                $model->debit_amount=$outstanding;
                $model->credit_amount=$outstanding;
                $model->total_amount=$outstanding;
                $model->added_by=$req->input('added_by');
                $model->company_id=$req->input('company_id');
                $model->created_at=Carbon::now();
                $model->updated_at=Carbon::now();
                $model->fiscal_year=$getfyear;
                $model->save(); 
                $getinput=$getinput-$paymentprice;
                $getvehicleaccountdetail=AccountPriceDeclaration::where('payment_topic','shatta_darta_shulka')->where('payment_type','general_shulka')->where('bus_no',$req['bus_no'])->first();
                
                 $value->delete(); 
              $totaloutstanding=$totaloutstanding+$outstanding;   
                $getinarray[]=['4'=>$model];     
     }
    }
    //total outstanding entry in new row
     if($totaloutstanding>0){
        $paymentoption=$getvehicleaccountdetail->payment_option; 
        $paymenttype=$getvehicleaccountdetail->payment_type;
        $expiry=$getvehicleaccountdetail->expiry_date;
        $company=$getvehicleaccountdetail->company_id;
            $model=new AccountPriceDeclaration();
            $model->bus_no=$req['bus_no'];
            $model->payment_topic='shatta_darta_shulka';
            $model->payment_option=$paymentoption;
            $model->payment_price=$totaloutstanding;
            $model->payment_type=$paymenttype;
            $model->expiry_date=$expiry;
            $model->added_by=$req->input('added_by');
            $model->company_id=$req->input('company_id');
            $model->created_at=Carbon::now();
            $model->updated_at=Carbon::now();
            $model->fiscal_year=$getfyear;
            $model->save(); 
          $getinarray[]=['4'=>$model];
     }
  //  unset($value); 
    }
       }
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }

  }

   catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   } 
   
   //again,preparing ledger entry of the same transaction
   
   
   
      }

             else{
            return $this->sendError('New fiscal year has not been setup yet!!');
        }
        
 // If we reach here, then
// data is valid and working.
//now Commit the queries!
DB::commit();


 return $this->sendResponse($model,'general shulka stored successfully!!');


      
    }
    
    public function billPaymentHistory($ownerid){
         $get=[];
         $getadminname=[];
        $getdata=GeneralShulka::join('bus_info','general_shulka.bus_no','=','bus_info.bus_no')->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->where('bus_owner.owner_id',$ownerid)->select('bus_owner.owner_name','bus_owner.owner_phone','bus_info.bus_no','general_shulka.*')->orderBy('general_shulka.bill_id', 'desc')->get();
       foreach($getdata as $data){
       $getaddedsingle=$data->added_by;
       $maasik_shulka=number_format((float)$data->maasik_ghulka, 2, '.', '');
       $kharid_bikri_darta_sulka=number_format((float)$data->kharid_bikri_darta_sulka, 2, '.', '');
       $nibedan_shulka=number_format((float)$data->nibedan_shulka, 2, '.', '');
       $membership_nabikarad_shulka=number_format((float)$data->membership_nabikarad_shulka, 2, '.', '');
       $naya_membership_shulka=number_format((float)$data->naya_membership_shulka, 2, '.', '');
       $naya_bus_register_shulka=number_format((float)$data->naya_bus_register_shulka, 2, '.', '');
       $jariwana_shulka=number_format((float)$data->jariwana_shulka, 2, '.', '');
       $queue_shatta_patta_shulka=number_format((float)$data->queue_shatta_patta_shulka, 2, '.', '');
       $identitycard_shulka=number_format((float)$data->identitycard_shulka, 2, '.', '');
       $sifaarish_shulka=number_format((float)$data->sifaarish_shulka, 2, '.', '');
       $vawan_build_fund=number_format((float)$data->vawan_build_fund, 2, '.', '');
       $staff_upadaan_fund=number_format((float)$data->staff_upadaan_fund, 2, '.', '');
       $share_saving_shulka=number_format((float)$data->share_saving_shulka, 2, '.', '');
       $sticker_shulka=number_format((float)$data->sticker_shulka, 2, '.', '');
       $other_shulka=number_format((float)$data->other_shulka, 2, '.', '');
       $photocopy_fee=number_format((float)$data->photocopy_fee, 2, '.', '');
       $peski_farchyot_shulka=number_format((float)$data->peski_farchyot_shulka, 2, '.', '');
       $shatta_darta_shulka=number_format((float)$data->shatta_darta_shulka, 2, '.', '');
       $total_amount=number_format((float)$data->total_amount, 2, '.', '');
        if(ctype_digit(strval($getaddedsingle))){
    $getadminname=GeneralShulka::join('bus_info','general_shulka.bus_no','=','bus_info.bus_no')->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->where('bus_owner.owner_id',$ownerid)->join('admin_info','general_shulka.added_by','admin_info.admin_id')->select('admin_info.admin_name')->first();    
    $minimizetoname=json_decode($getadminname);
     $getvalue=$minimizetoname->admin_name;
      // return $this->sendResponse($getadminname,'Transaction history of a bus owner is shown successfully!!');
        $data['maasik_shulka']=$maasik_shulka;
       $data['kharid_bikri_darta_sulka']=$kharid_bikri_darta_sulka;
       $data['nibedan_shulka']=$nibedan_shulka;
       $data['membership_nabikarad_shulka']=$membership_nabikarad_shulka;
       $data['naya_membership_shulka']=$naya_membership_shulka;
       $data['naya_bus_register_shulka']=$naya_bus_register_shulka;
       $data['jariwana_shulka']=$jariwana_shulka;
       $data['queue_shatta_patta_shulka']=$queue_shatta_patta_shulka;
       $data['identitycard_shulka']=$identitycard_shulka;
       $data['sifaarish_shulka']=$sifaarish_shulka;
       $data['vawan_build_fund']=$vawan_build_fund;
       $data['staff_upadaan_fund']=$staff_upadaan_fund;
       $data['share_saving_shulka']=$share_saving_shulka;
       $data['sticker_shulka']=$sticker_shulka;
       $data['other_shulka']=$other_shulka;
       $data['photocopy_fee']=$photocopy_fee;
       $data['peski_farchyot_shulka']=$peski_farchyot_shulka;
       $data['total_amount']=$total_amount;
       $data['added_by']=$getvalue;
       
      $get[]=[$data
          ];
        } 
        else{
         //  $data['admin']='hey man';
   
       $data['maasik_shulka']=$maasik_shulka;
       $data['kharid_bikri_darta_sulka']=$kharid_bikri_darta_sulka;
       $data['nibedan_shulka']=$nibedan_shulka;
       $data['membership_nabikarad_shulka']=$membership_nabikarad_shulka;
       $data['naya_membership_shulka']=$naya_membership_shulka;
       $data['naya_bus_register_shulka']=$naya_bus_register_shulka;
       $data['jariwana_shulka']=$jariwana_shulka;
       $data['queue_shatta_patta_shulka']=$queue_shatta_patta_shulka;
       $data['identitycard_shulka']=$identitycard_shulka;
       $data['sifaarish_shulka']=$sifaarish_shulka;
       $data['vawan_build_fund']=$vawan_build_fund;
       $data['staff_upadaan_fund']=$staff_upadaan_fund;
       $data['share_saving_shulka']=$share_saving_shulka;
       $data['sticker_shulka']=$sticker_shulka;
       $data['other_shulka']=$other_shulka;
       $data['photocopy_fee']=$photocopy_fee;
       $data['peski_farchyot_shulka']=$peski_farchyot_shulka;
       $data['total_amount']=$total_amount;
       $get[]=[$data,
                ];
        }
   }
     return $this->sendResponse($get,'Transaction history of a bus owner is shown successfully!!');
    }
    //show general kharcha payment request made by admin
    public function ShowDummyGShulka($companyid){
     $getdata=GeneralKharchaDummy::where('company_id',$companyid)->get();
     return $this->sendResponse($getdata,'Payment request by admin is shown successfully!');    
    }
    //verify requests from admin and provide to admin:
    public function VerifyGeneralKharchaPaymentRequest(Request $req,$id){
         // Start transaction!
        DB::beginTransaction();
        $input=$req->all();
        $fiscalyear=FiscalYear::OrderBy('id','DESC')->first();
        $fenddate=$fiscalyear->fiscal_year_end_date;
        $endadateformat=Carbon::createFromFormat('Y-m-d H:i:s', $fenddate);
        $getpaymentdate=Carbon::now();
        $paymentformat=Carbon::createFromFormat('Y-m-d H:i:s', $getpaymentdate);
        $paymentformatendofday=Carbon::parse($paymentformat)->endOfDay();
        $getfyear=$fiscalyear->fiscal_year;
  if($endadateformat->gt($paymentformatendofday)){
      if($req['pending_status']){
    try{
         $validator =Validator::make($input,[ 'staff_id'=>'required',
         ]);
         if($validator->fails()){
            DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);
         }
  	     //now store it to general kharcha as well by shifting datas
         $getdata=GeneralKharchaDummy::where('id',$id)->where('verification_status',$req['pending_status'])->first();
       //   return $this->sendResponse($getdata,'general shulka updated successfully!!');
       $bill_payment_date=$getdata->bill_payment_date;
       $kharcha_received_by=$getdata->kharcha_received_by;
       $kharcha_recipient_address=$getdata->kharcha_recipient_address;
       $masalanda_kharcha=$getdata->masalanda_kharcha;
       $chhapai_kharcha=$getdata->chhapai_kharcha;
       $bidhyut_kharcha=$getdata->bidhyut_kharcha;
       $telephonebill_kharcha=$getdata->telephonebill_kharcha;
       $dailytour_bhatta_kharcha=$getdata->dailytour_bhatta_kharcha;
       $marmatsambhaar_kharcha=$getdata->marmatsambhaar_kharcha;
       $khaja_office_kharcha=$getdata->khaja_office_kharcha;
       $talab_kharcha=$getdata->talab_kharcha;
       $bahiri_khaja_kharcha=$getdata->bahiri_khaja_kharcha;
       $staff_maasik_bhatta_kharcha=$getdata->staff_maasik_bhatta_kharcha;
       $kosadaxya_maasik_bhatta_kharcha=$getdata->kosadaxya_maasik_bhatta_kharcha;
       $staff_sanchayakosh_kharcha=$getdata->staff_sanchayakosh_kharcha;
       $housefare_kharcha=$getdata->housefare_kharcha;
       $baithak_bhatta_kharcha=$getdata->baithak_bhatta_kharcha;
       $aarthiksahayog_kharcha=$getdata->aarthiksahayog_kharcha;
       $bibidh_kharcha=$getdata->bibidh_kharcha;
       $sanchaar_bhatta=$getdata->sanchaar_bhatta;
       $bank_nagad_jamma=$getdata->bank_nagad_jamma;
       $indhan_kharcha=$getdata->indhan_kharcha;
       $added_by=$getdata->added_by;
       $company_id=$getdata->company_id;
       $updated_by=$getdata->updated_by;
       $created_at=$getdata->created_at;
       $updated_at=$getdata->updated_at;
       $total_amount=$getdata->total_amount;
       $payment_type=$getdata->payment_type;
       $fiscal_year=$getdata->fiscal_year;
       $cheque_no=$getdata->cheque_no;
       $verification_status=$getdata->verification_status;
    //  return $this->sendResponse($verification_status,'general shulka updated successfully!!');
        
       //increment billno
         $getbillno=GeneralShulkaKharcha::orderBy('bill_no', 'DESC')->first('bill_no');
        $toarray=json_decode($getbillno);
        $getvalue=$toarray->bill_no;
        $incrementbillbyone=$getvalue+1;
      $model=new GeneralShulkaKharcha();
        $model->bill_no=$incrementbillbyone;
       // return $this->sendResponse($model->bill_no,'general shulka updated successfully!!');
        $model->bill_payment_date=$paymentformat;
        $model->kharcha_received_by=$kharcha_received_by;
        $model->kharcha_recipient_address=$kharcha_recipient_address;
        $model->masalanda_kharcha=$masalanda_kharcha;
        $model->chhapai_kharcha=$chhapai_kharcha;
        $model->bidhyut_kharcha=$bidhyut_kharcha;
        $model->telephonebill_kharcha=$telephonebill_kharcha;
        $model->dailytour_bhatta_kharcha=$dailytour_bhatta_kharcha;
        $model->marmatsambhaar_kharcha=$marmatsambhaar_kharcha;
        $model->khaja_office_kharcha=$khaja_office_kharcha;
        $model->talab_kharcha=$talab_kharcha;
        $model->bahiri_khaja_kharcha=$bahiri_khaja_kharcha;
        $model->staff_maasik_bhatta_kharcha=$staff_maasik_bhatta_kharcha;
        $model->kosadaxya_maasik_bhatta_kharcha=$kosadaxya_maasik_bhatta_kharcha;
        $model->staff_sanchayakosh_kharcha=$staff_sanchayakosh_kharcha;
        $model->housefare_kharcha=$housefare_kharcha;
        $model->baithak_bhatta_kharcha=$baithak_bhatta_kharcha;
        $model->aarthiksahayog_kharcha=$aarthiksahayog_kharcha;
        $model->bibidh_kharcha=$bibidh_kharcha;
        $model->sanchaar_bhatta=$sanchaar_bhatta;
        $model->bank_nagad_jamma=$bank_nagad_jamma;
        $model->indhan_kharcha=$indhan_kharcha;
        $model->added_by=$added_by;
        $model->company_id=$company_id;
        $model->updated_by=$updated_by;
        $model->created_at=$created_at;
        $model->updated_at=$updated_at;
        $model->total_amount=$total_amount;
        $model->payment_type=$payment_type;
        $model->fiscal_year=$fiscal_year;
        $model->cheque_no=$cheque_no;
        $getstaffname=StaffInfo::where('staff_id',$req['staff_id'])->select('staff_name')->first();
     //   return $this->sendResponse($getstaffname->staff_name,'general shulka updated successfully!!');
        $model->verified_by=$getstaffname->staff_name;
        $model->save();
       //   return $this->sendResponse($model,'general shulka updated successfully!!');
        $getdata->delete();
          //-------------------for sending notification immediately to Company Admin::::
        //get admin datas
        $getadmindata=GeneralShulkaKharcha::where('general_shulka_kharcha.company_id',$company_id)->join('company_info','general_shulka_kharcha.company_id','=','company_info.company_id')->join('admin_info', 'company_info.company_id', '=', 'admin_info.company_id')->select('admin_info.*')->first();
    //saving in notification table as well::
            $store=new Mynotification();
            $store->type='General Kharcha Payment Verification';
            $store->notifiable_id=$getadmindata->admin_id;
            $store->notifiable_type=LoginModel::class;
            $store->title='General kharcha payment verified';
            $store->body='General kharcha payment of rupees'.$total_amount.' '.'has been verified by '.$model->verified_by;
            $store->added_by=$added_by;
            $store->created_at=Carbon::now();
            $store->company_id=$getadmindata->company_id;
            $store->save();
//again store to journal entry of general shulka
try{
     $validator =Validator::make($input,[
  	     ]);
  if($validator->fails()){
            DB::rollback();
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
        try{
    if($masalanda_kharcha>0) {
    $model=new JournalEntryOfGeneralShulka();
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='masalanda kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$masalanda_kharcha;
    $model->credit_amount=$masalanda_kharcha;
    $model->total_amount=$masalanda_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
        }
         catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{
    if($chhapai_kharcha>0)
    {
    $model=new JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='chhapai kharcha a/c'; 
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$chhapai_kharcha;
    $model->credit_amount=$chhapai_kharcha;
    $model->total_amount=$chhapai_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
      }
       catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{  
    if($bidhyut_kharcha>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='bidhyut kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$bidhyut_kharcha;
    $model->credit_amount=$bidhyut_kharcha;
    $model->total_amount=$bidhyut_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
      }
        catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
    try{  
    if($telephonebill_kharcha>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='telephonebill kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$telephonebill_kharcha;
    $model->credit_amount=$telephonebill_kharcha;
    $model->total_amount=$telephonebill_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
      }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
     try{ 
    if($dailytour_bhatta_kharcha>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='dailytour bhatta kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$dailytour_bhatta_kharcha;
    $model->credit_amount=$dailytour_bhatta_kharcha;
    $model->total_amount=$dailytour_bhatta_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();

    }
         
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
   }
   try{
    if($marmatsambhaar_kharcha>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='marmatsambhaar kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$marmatsambhaar_kharcha;
    $model->credit_amount=$marmatsambhaar_kharcha;
    $model->total_amount=$marmatsambhaar_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
   }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($khaja_office_kharcha>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='khaja office kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$khaja_office_kharcha;
    $model->credit_amount=$khaja_office_kharcha;
    $model->total_amount=$khaja_office_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save(); 
    }
      }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
     if($talab_kharcha>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='talab kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$talab_kharcha;
    $model->credit_amount=$talab_kharcha;
    $model->total_amount=$talab_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save(); 
    }
     }
       catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   // return Redirect::to('/form')
   //     ->withErrors( $e->getErrors() )
     //   ->withInput();
    }
    try{
    if($bahiri_khaja_kharcha>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='bahiri khaja kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$bahiri_khaja_kharcha;
    $model->credit_amount=$bahiri_khaja_kharcha;
    $model->total_amount=$bahiri_khaja_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save(); 
    }
     }
      catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
    }
    try{
    if($staff_maasik_bhatta_kharcha>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='staff maasik bhatta kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$staff_maasik_bhatta_kharcha;
    $model->credit_amount=$staff_maasik_bhatta_kharcha;
    $model->total_amount=$staff_maasik_bhatta_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save(); 
    }
        
    }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
     }
    try{
     if($kosadaxya_maasik_bhatta_kharcha>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='kosadaxya maasik bhatta kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$kosadaxya_maasik_bhatta_kharcha;
    $model->credit_amount=$kosadaxya_maasik_bhatta_kharcha;
    $model->total_amount=$kosadaxya_maasik_bhatta_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save(); 
    }
    }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
     }
    try{
     if($staff_sanchayakosh_kharcha>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='staff sanchayakosh kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$staff_sanchayakosh_kharcha;
    $model->credit_amount=$staff_sanchayakosh_kharcha;
    $model->total_amount=$staff_sanchayakosh_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save(); 
    }
    }
    catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
     }
    try{
    if($housefare_kharcha>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='housefare kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$housefare_kharcha;
    $model->credit_amount=$housefare_kharcha;
    $model->total_amount=$housefare_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save(); 
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
     }
    try{
     if($baithak_bhatta_kharcha>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='baithak kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$baithak_bhatta_kharcha;
    $model->credit_amount=$baithak_bhatta_kharcha;
    $model->total_amount=$baithak_bhatta_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save(); 
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
      }
    try{
     if($aarthiksahayog_kharcha>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='aarthiksahayog kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$aarthiksahayog_kharcha;
    $model->credit_amount=$aarthiksahayog_kharcha;
    $model->total_amount=$aarthiksahayog_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save(); 
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
      }
    try{
      if($bibidh_kharcha>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='bibidh kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$bibidh_kharcha;
    $model->credit_amount=$bibidh_kharcha;
    $model->total_amount=$bibidh_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save(); 
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
    }
    try{
     if($sanchaar_bhatta>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='sanchaar bhatta a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$sanchaar_bhatta;
    $model->credit_amount=$sanchaar_bhatta;
    $model->total_amount=$sanchaar_bhatta;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save(); 
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
    }
    try{
    if($bank_nagad_jamma>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='bank nagad jamma a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$bank_nagad_jamma;
    $model->credit_amount=$bank_nagad_jamma;
    $model->total_amount=$bank_nagad_jamma;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
    }
   try{
    if($indhan_kharcha>0) {
    $model=new  JournalEntryOfGeneralShulka();   
    $model->date=Carbon::now();
    $model->bill_no=$incrementbillbyone;
    $model->debit_particular='indhan kharcha a/c';
    $model->credit_particular=$payment_type.' a/c';
    $model->debit_amount=$indhan_kharcha;
    $model->credit_amount=$indhan_kharcha;
    $model->total_amount=$indhan_kharcha;
    $model->added_by=$added_by;
    $model->company_id=$company_id;
    $model->created_at=$created_at;
    $model->updated_at=$updated_at;
    $model->fiscal_year=$fiscal_year;
    $model->save();
    }
     }
     catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
      }
   

}
 catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   } 
            
         }
  catch ( \Exception $ex ){ 
        DB::rollback();
      return $this->sendError(($ex->getMessage())); 
    }
    catch(ValidationException $ex){
    DB::rollback();
    return $this->sendError(($ex->getMessage()));
   }
   
      }
    //  return $this->sendResponse('done','general kharcha verified successfully!!');
      if($req['cancel_status']){
          $updatestatus=GeneralKharchaDummy::where('id',$id)->update(['verification_status' => $req['cancel_status']]);
       //   $getall=GeneralKharchaDummy::where('id',$id)->first();
           return $this->sendResponse('done','general kharcha payment status changed to cancelled!!');
          }
      
  }
       //if not the fiscal year
 else{
            return $this->sendError('New fiscal year has not been setup yet!!');
        }
// If we reach here, then
// data is valid and working.
// Commit the queries!
DB::commit();
    return $this->sendResponse('done','general kharcha verified successfully!!');
        
     }
   //show general shulka verified records of today
   public function showVerifiedGShulkaToday($staffid){
        $getstaffname=StaffInfo::where('staff_id',$staffid)->select('staff_name')->first();
        $getname=$getstaffname->staff_name;
        $gettodaydata=GeneralShulkaKharcha::where('verified_by',$getname)->whereDate('bill_payment_date', Carbon::today())->get();
         return $this->sendResponse($gettodaydata,'verified records of today shown successfully!!');

   }
  //show total due of general shulka of vehicle owner
  public function showGeneralShulkaDueOwner($ownerphone){
      \DB::statement("SET SQL_MODE=''");
     $getdues=AccountPriceDeclaration::join('bus_info','acount_price_declaration.bus_no','bus_info.bus_no')->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')
         ->where('bus_owner.owner_phone',$ownerphone)->where('acount_price_declaration.payment_type','general_shulka')
         ->select('acount_price_declaration.*','bus_info.*','bus_owner.*')//,DB::raw('group_concat(acount_price_declaration.payment_topic) as a'),DB::raw('group_concat(acount_price_declaration.payment_type) as b'))
        ->groupBy('bus_info.bus_no')->groupBy('acount_price_declaration.payment_topic')->groupBy('acount_price_declaration.payment_type')
         ->get();//select('bus_owner.owner_name','bus_owner.owner_phone','bus_info.bus_no')->get();
         //   return $this->sendResponse($getdues,'Sum of Due of indivisual payment topic of bus owner is shown successfully!!'); 
     $getarray=[];
     foreach($getdues as $dues){
         $paymenttopic=$dues->payment_topic;
         $busno=$dues->bus_no;
         $paymentprice=$dues->payment_price;
         $ownername=$dues->owner_name;
         $ownerphone=$dues->owner_phone;
         $getsum=$dues::where('payment_topic',$paymenttopic)->where('payment_type','general_shulka')->where('bus_no',$busno)->sum('payment_price');
         $getarray[]=[
                 'owner_name'=>$ownername,
                 'owner_phone'=>$ownerphone,
                 'bus_no'=>$busno,
                 'payment_topic'=>$paymenttopic,
                 'rate'=>$getsum
                 ];
     }
         return $this->sendResponse($getarray,'Sum of Due of indivisual payment topic of bus owner is shown successfully!!');
  }
    
    
    
    
    
    
    
}