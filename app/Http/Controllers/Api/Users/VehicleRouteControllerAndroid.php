<?php
namespace App\Http\Controllers\Api\Users;

use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Admin\Sms;
use App\Models\Admin\Route;
use App\Models\Admin\VehicleRoute;
use App\Models\Users\busOwner;
use App\Models\Users\DummyBusOwner;
use App\Models\Users\Driver;
use App\Models\Users\businfo;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\DummyBusInfo;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Admin\adminInfo as admininfoResource;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
Use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class VehicleRouteControllerAndroid extends BaseController
{
    function getAllRoute($companyid){
        $getall=Route::where('company_id',$companyid)->orderBy('route_info.route_number','ASC')->get();
        return $this->sendResponse($getall,'all routes is shown successfully!!');
    }
    
    function getVehiclesOfParticularRoute($busid){
   // $getdetails=businfo::where('bus_info.bus_id',$busid)->join('vehicle_route_info','vehicle_route_info.bus_id','bus_info.bus_id')->select('bus_info.bus_id','vehicle_route_info.*')->get();    
      $getrouteno=VehicleRoute::where('bus_info.bus_id',$busid)->join('route_info','vehicle_route_info.route_id','route_info.id')->join('bus_info','vehicle_route_info.bus_id','bus_info.bus_id')->first('route_info.route_number');
     if(is_null($getrouteno)){
          return $this->sendError('no route found of this vehicle!!');   
     }
     else{
      $getbusofroute=VehicleRoute::where('route_info.route_number',$getrouteno->route_number)->join('route_info','vehicle_route_info.route_id','route_info.id')->join('bus_info','vehicle_route_info.bus_id','bus_info.bus_id')->select('vehicle_route_info.*','bus_info.bus_id','bus_info.bus_no','route_info.*')->get();
    //  $getdetails= $getdetails=VehicleRoute::where('bus_info.bus_id',$busid)->join('route_info','vehicle_route_info.route_id','route_info.id')->join('bus_info','vehicle_route_info.bus_id','bus_info.bus_id')->select('vehicle_route_info.*','route_info.*')->get();
   if(is_null($getbusofroute)){
      return $this->sendError('no data found in this route!!'); 
   }
    else{
        return $this->sendResponse($getbusofroute,'vehicles of different routes shown successfully!!');
    }
     }
    }
    function VehiclesOfParticularRouteOfDriver($driverid){
      $getrouteno=VehicleRoute::join('route_info','vehicle_route_info.route_id','route_info.id')->join('bus_info','vehicle_route_info.bus_id','bus_info.bus_id')->join('driver_info','bus_info.bus_no','driver_info.bus_no')->where('driver_info.driver_id',$driverid)->first('route_info.route_number');
     //return $this->sendResponse($getrouteno,'vehicles of different routes shown successfully!!');
     if(is_null($getrouteno)){
          return $this->sendError('no route found of this vehicle!!');   
     }
     else{
      $getbusofroute=VehicleRoute::where('route_info.route_number',$getrouteno->route_number)->join('route_info','vehicle_route_info.route_id','route_info.id')->join('bus_info','vehicle_route_info.bus_id','bus_info.bus_id')->join('driver_info','bus_info.bus_no','driver_info.bus_no')->select('vehicle_route_info.*','bus_info.bus_id','bus_info.bus_no','driver_info.driver_id','route_info.*')->get();
   if(is_null($getbusofroute)){
      return $this->sendError('no data found in this route!!'); 
   }
    else{
        return $this->sendResponse($getbusofroute,'vehicles of different routes shown successfully!!');
    }
     }
    }
}