<?php

namespace App\Http\Controllers\Api\Users;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users\businfo;
use App\Models\Users\Driver;
use App\Models\Users\busOwner;
//use App\Models\Users\Driver;
use App\Models\Users\DummyBusInfo;
use App\Models\Users\DummyDriver;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Api\baseController as BaseController;
//use Hash;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class busInfoController extends BaseController
{
    //for registering Admin users in the System
  public function busRegister(Request $request){

         $validator = Validator::make($request->all(), [
            'bus_id' => 'required',
             'bus_no' => 'required',
             'owner_phone'=>'required|numeric',
             'bus_billbook_no'=>'required',
             'bus_billbook_enddate'=>'required',
            'bus_routepermit_enddate'=>'required',
            'bus_checkpass_enddate'=>'required',
            'bus_insurance_enddate'=>'required',
            'bus_insurance_enddate'=>'required',
            'company_id' => 'required',
        ]);
   
        if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
   
       $input = $request->all();
        //$admin_user = LoginModel::create($input);
     //   $success['token'] =  $admin_user->createToken('MyApp')->plainTextToken;
        //$success['name'] =  $admin_user->name;
   
         $createvalues=businfo::create($input);
    return $this->sendResponse(new $createvalues,'Congrats!, User has been registered successfully!!');  

  }
  
 //delete businfos as well as driver records of the bus matched by:bus no.
 public function deleteBusinfoAswellasDriver(Request $request,$id){
     $getbyid=businfo::where('bus_id',$id)->where('active_status','yes')->first();
     $setno='no';
         //also set driver in-active
         $finddriver=Driver::join('bus_info','driver_info.bus_no','bus_info.bus_no')->where('bus_info.bus_id',$id)->select('driver_info.*')->first();
         if(is_null($finddriver)){
           //for bus
          $getbyid->active_status=$setno;
         $getbyid->save();
         return $this->sendResponse('done', 'bus deleted successfully!!');
         }
         else{
      //for driver
         $finddriver->active_status=$setno;
          $finddriver->save();
      //for bus
          $getbyid->active_status=$setno;
         $getbyid->save();
         return $this->sendResponse('done', 'bus deleted successfully!!');
         }
    /*  try{
  
   //now also connecting  bus detail with driver linked by bus no.
    $joinwithdriver=Driver::join('bus_info', 'driver_info.bus_no', '=', 'bus_info.bus_no')->where('bus_id',$id)->first('driver_info.*');
    //if there is no connection of driver with selected bus,then....
    if(is_null($joinwithdriver)){
         //saving records row wise
    $getbus=businfo::where('bus_id','=',$id)->first();
    $bus_no=$getbus->bus_no;
    $owner_phone=$getbus->owner_phone;
    $bus_billbook_no=$getbus->bus_billbook_no;
    $bus_billbook_enddate=$getbus->bus_billbook_enddate;
    $bus_routepermit_enddate=$getbus->bus_routepermit_enddate; 
    $bus_checkpass_enddate=$getbus->bus_checkpass_enddate;
    $bus_insurance_enddate=$getbus->bus_insurance_enddate;
    $bus_billbook_image=$getbus->bus_billbook_image;
    $bus_routepermit_image=$getbus->bus_routepermit_image;
    $bus_checkpass_image=$getbus->bus_checkpass_image;
    $bus_insurance_image=$getbus->bus_insurance_image;
    $company_id=$getbus->company_id;
    $created_at=$getbus->created_at;
    $updated_at=$getbus->updated_at;
       
//now using dummy table to insert deleted records    
    $getdummy=new DummyBusInfo();
    $getdummy->bus_id=$id;
    $getdummy->bus_no=$bus_no;
    $getdummy->owner_phone=$owner_phone;
    $getdummy->bus_billbook_no=$bus_billbook_no;
    $getdummy->bus_billbook_enddate=$bus_billbook_enddate;
    $getdummy->bus_routepermit_enddate=$bus_routepermit_enddate;
    $getdummy->bus_checkpass_enddate=$bus_checkpass_enddate;
    $getdummy->bus_insurance_enddate=$bus_insurance_enddate;
    $getdummy->bus_billbook_image=$bus_billbook_image;
    $getdummy->bus_routepermit_image=$bus_routepermit_image;
    $getdummy->bus_checkpass_image=$bus_checkpass_image;
    $getdummy->bus_insurance_image=$bus_insurance_image;
    $getdummy->company_id=$company_id;
    $getdummy->created_at=$created_at;
    $getdummy->updated_at=$updated_at;
    $getdummy->save();
      //now delete  businfo row 
 $getbus->delete();  
        return $this->sendResponse($getdummy, 'bus owner records moved to dummy owner table successfully!!');
    }
    //if there is bus connected with driver then,....
    else{
         //saving records row wise
    $getbus=businfo::where('bus_id','=',$id)->first();
    $bus_no=$getbus->bus_no;
    $owner_phone=$getbus->owner_phone;
    $bus_billbook_no=$getbus->bus_billbook_no;
    $bus_billbook_enddate=$getbus->bus_billbook_enddate;
    $bus_routepermit_enddate=$getbus->bus_routepermit_enddate; 
    $bus_checkpass_enddate=$getbus->bus_checkpass_enddate;
    $bus_insurance_enddate=$getbus->bus_insurance_enddate;
    $bus_billbook_image=$getbus->bus_billbook_image;
    $bus_routepermit_image=$getbus->bus_routepermit_image;
    $bus_checkpass_image=$getbus->bus_checkpass_image;
    $bus_insurance_image=$getbus->bus_insurance_image;
    $company_id=$getbus->company_id;
    $created_at=$getbus->created_at;
    $updated_at=$getbus->updated_at;
       
//now using dummy table to insert deleted records    
    $getdummy=new DummyBusInfo();
    $getdummy->bus_id=$id;
    $getdummy->bus_no=$bus_no;
    $getdummy->owner_phone=$owner_phone;
    $getdummy->bus_billbook_no=$bus_billbook_no;
    $getdummy->bus_billbook_enddate=$bus_billbook_enddate;
    $getdummy->bus_routepermit_enddate=$bus_routepermit_enddate;
    $getdummy->bus_checkpass_enddate=$bus_checkpass_enddate;
    $getdummy->bus_insurance_enddate=$bus_insurance_enddate;
    $getdummy->bus_billbook_image=$bus_billbook_image;
    $getdummy->bus_routepermit_image=$bus_routepermit_image;
    $getdummy->bus_checkpass_image=$bus_checkpass_image;
    $getdummy->bus_insurance_image=$bus_insurance_image;
    $getdummy->company_id=$company_id;
    $getdummy->created_at=$created_at;
    $getdummy->updated_at=$updated_at;
    $getdummy->save();
    
       $id=$joinwithdriver->driver_id;
  $bus_no=$joinwithdriver->bus_no;
  $driver_name=$joinwithdriver->driver_name;
  $driver_phone=$joinwithdriver->driver_phone;
    $driver_address=$joinwithdriver->driver_address;
    $driver_pin=$joinwithdriver->driver_pin;
   $driver_dob=$joinwithdriver->driver_dob;
    $driver_gender=$joinwithdriver->driver_gender;
   $driver_image=$joinwithdriver->driver_image;
   $driver_citizenship_image=$joinwithdriver->driver_citizenship_image;
      $driver_license_image=$joinwithdriver->driver_license_image;
         $company_id=$joinwithdriver->company_id;
       $device_token=$joinwithdriver->device_token;
           $device_name=$joinwithdriver->device_name;
      $created_at=$joinwithdriver->created_at;
    $updated_at=$joinwithdriver->updated_at;
 
  //shifting to driver dummy table
   $getdumm=new DummyDriver();
    $getdumm->driver_id=$id;
    $getdumm->bus_no=$bus_no;
    $getdumm->driver_name=$driver_name;
    $getdumm->driver_phone=$driver_phone;
    $getdumm->driver_address=$driver_address;
    $getdumm->driver_pin=$driver_pin;
    $getdumm->driver_dob=$driver_dob;
    $getdumm->driver_gender=$driver_gender;
    $getdumm->driver_image=$driver_image;
    $getdumm->driver_citizenship_image=$driver_citizenship_image;
    $getdumm->driver_license_image=$driver_license_image;
    $getdumm->company_id=$company_id;
      $getdumm->device_token=$device_token;
      $getdumm->device_name=$device_name;
    $getdumm->created_at=$created_at;
    $getdumm->updated_at=$updated_at;
    $getdumm->save();
    //then delete original driver record
     $joinwithdriver->delete();
  //now delete parent businfo row 
 $getbus->delete();  
       
 return $this->sendResponse($getdumm, 'bus owner records moved to dummy owner table as well as deleted from related driver table successfully!!');
    }
  //$get=$joinwithdriver->driver_name;
    return $this->sendResponse($joinwithdriver,'joinde!!');
      }
     catch ( \Exception $ex ){ 
         $errorCode = $ex->errorInfo[1];
          if($errorCode == '1062'){
              return $this->sendError('The Given Id is duplicate, please undo your process!!');
          }
      return $this->sendError(($ex->getMessage())); 
    } */
 }
 
 public function showBusOfOwnerForAdmin($ownerphone){
   
   try{
       $checkroute=businfo::where('owner_phone',$ownerphone)->where('active_status','yes')->get();
       $get=businfo::where('owner_phone',$ownerphone)->where('active_status','yes')->leftJoin('vehicle_route_info','bus_info.bus_id','vehicle_route_info.bus_id')->leftJoin('route_info','vehicle_route_info.route_id','route_info.id')->select('bus_info.*','bus_info.bus_no','route_info.id','route_info.route_name','route_info.route_number','route_from','route_info.route_to','route_info.route_type')->get(); 
       return $this->sendResponse($get, 'bus of specific owner is shown successfully!!');        
       }
   catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
 }
 public function showBusOfOwnerForOwner($ownerphone){
     try{
       $checkroute=businfo::where('owner_phone',$ownerphone)->where('active_status','yes')->get();
       $get=businfo::where('owner_phone',$ownerphone)->where('active_status','yes')->leftJoin('vehicle_route_info','bus_info.bus_id','vehicle_route_info.bus_id')->leftJoin('route_info','vehicle_route_info.route_id','route_info.id')->select('bus_info.*','bus_info.bus_no','route_info.id','route_info.route_name','route_info.route_number','route_from','route_info.route_to','route_info.route_type')->get(); 
       return $this->sendResponse($get, 'bus of specific owner is shown successfully!!');        
       }
   catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
 }
 //**************for  showing bus of a driver ********************
public function ShowBusOfDriver($ids){
  $combidedquery=DB::table('driver_info')->where('driver_id',$ids)->join('bus_info', 'driver_info.bus_no', '=', 'bus_info.bus_no')->where('bus_info.active_status','yes')->select('bus_info.*')->first();
  if(is_null($combidedquery)){
      return $this->sendError('there are no bus of such ID!!');
  }
  else{
   return $this->sendResponse($combidedquery, 'Driver listed successfully!!'); 
}
}
 
 
}
