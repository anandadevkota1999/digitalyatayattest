<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\News;
use App\Models\NewsImage;
use App\Models\Event;
use App\Models\EventImage;
use App\Models\Users\busOwner;
use App\Models\Users\businfo;
use App\Models\Product;
use App\Models\Users\StaffInfo;
use App\Models\BeforeLogin\companyList;
use App\Models\Users\Driver;
use App\Models\Notification as Mynotification;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth as Authenticate;
use App\Http\Controllers\Api\baseController as BaseController;
//use Hash;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Guzzle\client;

//use Auth;
use App\Http\Resources\Users\busOwnerinfo as busOwnerinfoResource;


class busOwnerController extends BaseController
{
  
 /*public function __construct()
    {
        $this->middleware(
            [
                'auth:busOwner-api', 
                'scopes:create driver'
            ]); 
    } */
    
public function storevideotostorage(Request $req){
   $input_the_data=$req->all();
   $validator =Validator::make($input_the_data,[
   'sample_video'=>'required|mimes:mp4,mov,mkv,ogg,qt | max:200000000000',
  ]);
  if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
 if($req->hasFile('sample_video')) {
       $getcompany='videos';
       $req->sample_video->store('videos', 'local');
 return $this->sendResponse('done', 'video stored successfully!!');        

   }
}
    
//********************for registering Bus owners in the System****************
   public function del($id){
        $get=Product::where('id',$id)->first();
        return $this->sendResponse($get->delete(),'deleted');
    }
  
  
  public function busOwnerRegister(Request $req){

       try{
     $input_the_data= $req->all();
       $validator =Validator::make($input_the_data,[
  	        'owner_id' => 'required',
            'owner_name' => 'required',
            'owner_address'=>'required',
            'owner_phone'=>'required',
            'owner_pin'=>'required|numeric|min:5',
            'owner_dob'=>'required',
            'owner_gender'=>'required',
            'company_id'=>'required',
           // 'owner_citizenship_image'=>'required|image|mimes:jpeg,png,jpg,gif,svg,jfif',
            'owner_citizenship_image'=>'required|mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
           // 'owner_image'=>'required|image|mimes:jpeg,png,jpg,gif,svg,jfif',
            'owner_image'=>'required|mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
  ]);
  if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
   if($req->hasFile('owner_citizenship_image')&&$req->hasFile('owner_citizenship_image')&&$req->hasFile('owner_image')&&$req->hasFile('owner_image')){
        $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->owner_citizenship_image->store($getcompany, 'local');
        $req->owner_image->store($getcompany, 'local');
        $added_by=$req['added_by'];
        $updated_by=$req['updated_by'];
         
     $model=new busOwner();
     $model->owner_id=$req->input('owner_id');
     $model->owner_name=$req->input('owner_name');
     $model->owner_address=$req->input('owner_address');
     $model->owner_phone=$req->input('owner_phone');
     $model->owner_pin=$req->input('owner_pin');
     $model->owner_dob=$req->input('owner_dob');
     $model->owner_gender=$req->input('owner_gender');
     $model->company_id=$req->input('company_id');
     $model->owner_citizenship_image=$req->owner_citizenship_image->hashName();
     $model->owner_image=$req->owner_image->hashName();
     $model->device_token=$req->input('device_token');
     $model->device_name=$req->input('device_name');
     $model->added_by=$added_by;
     $model->updated_by=$updated_by;
     $model->save();
    return $this->sendResponse($model, 'stored successfully!!'); 
    
     } 
    }
      catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }


      //  return $this->sendResponse($success, 'Congrats!, User has been registered successfully!!');

  }

	//**************for bus owner login after proper validation***************


  //for choice login either admin or owner or staff or driver
 
 public function choiceLogin(Request $request,$ids){
    switch ($ids) {
        //login for admin
  case 'admin':
      try{
        	//for Admin login after validation
         $validator = Validator::make($request->all(), [
            'admin_email' => 'required|email|exists:admin_info',
            'password'=>'required|min:6',
            'company_id'=>'required',
        ]);
        if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);
        } 
     
     if (Auth::guard('admin')->attempt(['admin_email'=> $request->admin_email, 'password' => $request->password, 'company_id'=>$request->company_id])) {
       config(['auth.guards.api.provider' => 'admin_info']);  
       $admin=Auth::guard('admin')->user();
   //  $success['admin_id']=$admin->admin_id;
       $admin['token'] =  $admin->createToken('MyApp')->accessToken;
   return $this->sendResponse($admin,'token created successfully!!'); 
  
}
     else{
      return $this->sendError('provided credentials are wrong!');
     }
      }
      catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
        break;
        
    //login for staffs    
  case 'staff':
      try{
      
             $validator = Validator::make($request->all(), [
            'staff_phone' => 'required|numeric|min:10',
            'staff_pin' => 'required|min:4',
            'company_id' => 'required']);
 if($validator->fails()){
     $geterror=$validator->errors()->all();
     $string=implode(",",$geterror);
    return $this->sendError($string); 
 }
$staff_phone=$request['staff_phone'];
$staff_pin=$request['staff_pin'];
$company_id=$request['company_id'];
$device_token=$request['device_token'];
$device_name=$request['device_name'];
$user = StaffInfo::where('staff_phone' , $staff_phone)->where( 'staff_pin' , $staff_pin)->where('company_id' ,$company_id)->first();
$checkpin= StaffInfo::where('staff_phone' , $staff_phone)->first();
if($checkpin){
if($checkpin->staff_pin!=$staff_pin){
    return $this->sendError('Provided Credentials are wrong!');
}    
}
 
  if(is_null($user)){ 
       return $this->sendError('Provided Number is not registered!');
   
  }
  else{
      if($staff_pin==$user->staff_pin){
       $seetoken=$user->device_token;
      if(is_null($seetoken)){
      $user->device_token=$device_token;
      $user->device_name=$device_name;
      $user->save();
      $user['token'] =  $user->createToken('MyApp')->accessToken;
    return $this->sendResponse($user,'token created successfully!!');
    }
    else{
        $getdevicename=$user->device_name;
        return $this->sendError('You have already logged in: '.$getdevicename); 
    }
   }
   else{
        return $this->sendError('Provided Credentials are wrong!');
   }
  } 
      }
      catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
       break;
       
       //login for bus owner
case 'bus owner':
        try{
       $validator = Validator::make($request->all(), [
            'owner_phone' => 'required|numeric|min:10',
            'owner_pin' => 'required|min:4',
            'company_id' => 'required'],
[
     'owner_phone.required'=>'owner phone is neccessary',
     'owner_phone.exists:bus_owner'=>'owner does not exists',
     'owner_phone.exists'=>'owner already exists',
     'company_id.required'=>'company id is required!'
    ]);
 if($validator->fails()){
     $geterror=$validator->errors()->all();
     $string=implode(",",$geterror);
    return $this->sendError($string); 
 }
  
$owner_phone=$request['owner_phone'];
$owner_pin=$request['owner_pin'];
$company_id=$request['company_id'];
$device_token=$request['device_token'];
$device_name=$request['device_name'];
$user= busOwner::where('owner_phone' , $owner_phone)->where( 'owner_pin',$owner_pin)->where('company_id' ,$company_id)->first();
$checkpin= busOwner::where('owner_phone' , $owner_phone)->first();
if($checkpin){
if($checkpin->owner_pin!=$owner_pin){
    return $this->sendError('Provided Credentials are wrong!');
}    
}

  if(is_null($user)){
       return $this->sendError('Provided Number is not registered!');
  }
  
  else{
      $seetoken=$user->device_token;
      if(is_null($seetoken)){
      $user->device_token=$device_token;
      $user->device_name=$device_name;
      $user->save();
      $user['token'] =  $user->createToken('MyApp')->accessToken;
    return $this->sendResponse($user,'token created successfully!!');
    }
    else{
        $getdevicename=$user->device_name;
        return $this->sendError('You have already logged in: '.$getdevicename); 
    }
  
  
  }     
        }
  catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
        break;
        
        //login for driver
 case 'driver':
     try{
           $validator = Validator::make($request->all(), [
            'driver_phone' => 'required|numeric|min:10',
            'driver_pin' => 'required|min:4',
            'company_id' => 'required']);
 if($validator->fails()){
     $geterror=$validator->errors()->all();
     $string=implode(",",$geterror);
    return $this->sendError($string); 
 }
  
$driver_phone=$request['driver_phone'];
$driver_pin=$request['driver_pin'];
$company_id=$request['company_id'];
$device_token=$request['device_token'];
$device_name=$request['device_name'];
$user = Driver::where('driver_phone' , $driver_phone)->where( 'driver_pin' , $driver_pin)->where('company_id' ,$company_id)->first();
$checkpin= Driver::where('driver_phone' , $driver_phone)->first();
if($checkpin){
if($checkpin->driver_pin!=$driver_pin){
    return $this->sendError('Provided Credentials are wrong!');
}}
if(is_null($user)){ 
       return $this->sendError('Provided Number is not registered!');
  }
  else{
      $seetoken=$user->device_token;
      if(is_null($seetoken)){
      $user->device_token=$device_token;
      $user->device_name=$device_name;
      $user->save();
      $user['token'] =  $user->createToken('MyApp')->accessToken;
    return $this->sendResponse($user,'token created successfully!!');
    }
    else{
        $getdevicename=$user->device_name;
        return $this->sendError('You have already logged in: '.$getdevicename); 
    }
      
  }
         
     }
  catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
        break;
}
    
     
 }
 
 // for changing busowner password::
 
 public function changeOwnerPassword(Request $request,$id){
  $validator = Validator::make($request->all(), [
            'old_pin' => 'required|min:6',
            'new_pin'=>'required|min:6|different:old_pin',
            'confirm_pin'=>'required|same:new_pin',
        ],
        [
        'new_pin.different'=>'New Pin code should be different than old one!!' ,
        'old_pin.min'=>'old pin must be of 6 numbers',
        'new_pin.min'=>'new pin must be of 6 numbers',
        'confirm_pin.min'=>'confirm pin must be of 6 numbers',
        
            ]);
        if($validator->fails()){
           $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string); 
        }
         $showall=busOwner::find($id);
  if(is_null($showall)){
  return $this->sendError('there is no owner to show!!');
  }
  else{
    $getpin=$showall->owner_pin;
    if($request->old_pin==$getpin){
        $showall->owner_pin=$request->new_pin;
        $query=busOwner::where( 'owner_id' , $showall->owner_id)->update( array( 'owner_pin' =>  $showall->owner_pin));
         return $this->sendResponse($query,'owner password is successfully changed!');
    }
          else{
         return $this->sendError('old password doesnt match'); 
   }
  } 
 }
 
 
    //***********for retreiving stored information from the logged in Bus Owner datas::  

    public function index(){
   $get_all = busOwner::all();
    if (Auth::guard('busOwner-api')->check()) {
         if(is_null($get_all)){
          return sendError('there are no values to retrieve!!');
         }
        return $this->sendResponse($get_all,'bus owners are successfully retrieved!');

    }
                //return $this->sendResponse(admininfoResource::collection($get_all), 'Authorized Admins retrieved successfully!!'); 
    }

    //************for showing or displaying datas from the database through api  

public function showall($id){
 if (Auth::guard('busOwner-api')->check()) {
  $showall=busOwner::find($id);
  if(is_null($showall)){
  return $this->sendError('no values to show!!');
  }
  else{
  return $this->sendResponse($showall,'the values are successfully shown!');
  }
 }
}
//**************for getting counted values from owner datas********************
public function countowner(){
     $total['owners'] = busOwner::count('owner_id');
 
    if(is_null($total)){
      return $this->sendResponse('there are no any owners yet!');
    }
    else{
            return $this->sendResponse($total, 'Admins counted successfully!!'); 
    }
    
}
//**************for getting showing drivers of a owner ********************
public function combinedDatas($ids){
  $combidedquery=DB::table('bus_owner')->where('owner_id',$ids)->join('bus_info', 'bus_owner.owner_phone', '=', 'bus_info.owner_phone')
  ->join('driver_info', 'bus_info.bus_no', '=', 'driver_info.bus_no')->where('driver_info.active_status','yes')->select('driver_info.*')->get();
   return $this->sendResponse($combidedquery, 'Driver listed successfully!!'); 
}
//**************for updating values in database through api********************

//Update bus owner records
public function updateBusOwner(Request $req, $id){
   try{ 
    $validator =Validator::make($req->all(),[
 'owner_phone'=>['required', 'digits:10'],
   'owner_name'=>'required',
   'company_id'=>'required',
   //'bus_billbook_image'=>'image|mimes:jpeg,png,jpg,gif,svg,jfif|max:2048',
   //'bus_routepermit_image'=>'image|mimes:jpeg,png,jpg,gif,svg,jfif|max:2048',
   //'bus_insurance_image'=>'image|mimes:jpeg,png,jpg,gif,svg,jfif|max:2048','bus_checkpass_image'=>'image|mimes:jpeg,png,jpg,gif,svg,jfif|max:2048',
  
  ]);
   if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        } 
        
       if($req->hasFile('owner_citizenship_image')&&$req->hasFile('owner_image')){
       $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->owner_citizenship_image->store($getcompany, 'local');
       $req->owner_image->store($getcompany, 'local');
   //***********saving the required values to the database!!**************
$owner=busOwner::findorFail($id);
 if($owner){
 $owner->owner_name=$req['owner_name'];
 $owner->owner_address=$req['owner_address'];
 $owner->owner_phone =$req['owner_phone'];
 $owner->owner_dob=$req['owner_dob'];
// $owner->owner_pin=$req['owner_pin'];
 $owner->owner_gender=$req['owner_gender'];
 $owner->owner_citizenship_image=$req->owner_citizenship_image->hashName();
 $owner->owner_image=$req->owner_image->hashName();
 //$owner->device_token=$req->input('device_token');
 //$owner->device_name=$req->input('device_name');
 $owner->updated_at=Carbon::now();
  $owner->updated_by=$req->input('updated_by');
 $owner->save();
  //now also connecting  bus detail with driver linked by bus no.
       $query=businfo::where('owner_phone' , $id)->update( array( 'owner_phone' =>  $req->owner_phone));
    
  return $this->sendResponse($query,'Bus Records are successfully updated!');
      }
      else{
  return $this->sendError('There are no Bus records to update!');
}
        }
        
        // update::for owner_citizenship_image only
        else if($req->hasFile('owner_citizenship_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->owner_citizenship_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
$owner=busOwner::findorFail($id);
 if($owner){
 $owner->owner_name=$req['owner_name'];
 $owner->owner_address=$req['owner_address'];
 $owner->owner_phone =$req['owner_phone'];
 $owner->owner_dob=$req['owner_dob'];
// $owner->owner_pin=$req['owner_pin'];
 $owner->owner_gender=$req['owner_gender'];
 $owner->owner_citizenship_image=$req->owner_citizenship_image->hashName();
// $owner->device_token=$req->input('device_token');
 //$owner->device_name=$req->input('device_name');
  $owner->updated_at=Carbon::now();
 $owner->updated_by=$req->input('updated_by');
 $owner->save();
  //now also connecting  bus detail with driver linked by bus no.
      $query=businfo::where('owner_phone' , $id)->update( array( 'owner_phone' =>  $req->owner_phone));   
  return $this->sendResponse($query,'Bus Records are successfully updated!');
      }
      else{
  return $this->sendError('There are no Bus records to update!');
}
       
        }
        
          // update::for owner_citizenship_image only
        else if($req->hasFile('owner_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->owner_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 
$owner=busOwner::findorFail($id);
 if($owner){
 $owner->owner_name=$req['owner_name'];
 $owner->owner_address=$req['owner_address'];
 $owner->owner_phone =$req['owner_phone'];
 $owner->owner_dob=$req['owner_dob'];
 //$owner->owner_pin=$req['owner_pin'];
 $owner->owner_gender=$req['owner_gender'];
 $owner->owner_image=$req->owner_image->hashName();
 //$owner->device_token=$req->input('device_token');
 //$owner->device_name=$req->input('device_name');
  $owner->updated_at=Carbon::now();
  $owner->updated_by=$req->input('updated_by');
 $owner->save();
  //now also connecting  bus detail with driver linked by bus no.
      $query=businfo::where('owner_phone' , $id)->update( array( 'owner_phone' =>  $req->owner_phone));
    
  return $this->sendResponse($query,'Bus Records are successfully updated!');
      }
      else{
  return $this->sendError('There are no Bus records to update!');
}
       
        }
    
          //if the owner etries : No need to input images so,
     else{
         
        //updating in db table
        $owner=busOwner::findorFail($id);
  $owner->owner_name=$req['owner_name'];
 $owner->owner_address=$req['owner_address'];
 $owner->owner_phone =$req['owner_phone'];
 $owner->owner_dob=$req['owner_dob'];
 //$owner->owner_pin=$req['owner_pin'];
 $owner->owner_gender=$req['owner_gender'];
 //$owner->device_token=$req->input('device_token');
 //$owner->device_name=$req->input('device_name');
  $owner->updated_by=$req->input('updated_by');
   $owner->updated_at=Carbon::now();
 $owner->save();
     //now also connecting  bus detail with driver linked by bus no.
      $query=businfo::where('owner_phone' , $id)->update( array( 'owner_phone' =>  $req->owner_phone));
  return $this->sendResponse($query,'Bus Records are successfully updated!');
//    return $this->sendResponse( $bus,'the datas are now stored successfully!!');
           
     } 
       
   }
   catch ( \Exception $ex ){ 
      $errorCode = $ex->errorInfo[1];
          if($errorCode == '1062'){
              return $this->sendError('Phone no. is already registered, please undo your process!!');
          }
    }
 
}

//******for deleting the values from the table and storing it to dummy table
public function delete(busOwner $values, $id){
  $values=busOwner::find($id);
if(is_null($values)){
  return $this->sendError('there is no any data of such id!');
}
else {
    $storevalues=busDetailDummy::create($values);
  $values->delete();
  return $this->sendResponse([], 'values deleted successfully!');
}
}
public function updateProfileOnly(Request $req, $id){
     $validator =Validator::make($req->all(),[
 'owner_image'=>'required', 'image|mimes:jpeg,png,jpg,gif,svg,jfif|max:2048',
   'company_id'=>'required',
  ]);
   if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        } 
       if($req->hasFile('owner_image')){
       $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->owner_image->store($getcompany, 'local');
 $owner=busOwner::findorFail($id);
 $owner->owner_image=$req->owner_image->hashName();;
 $owner->save();
   return $this->sendResponse('well done!', 'Profile picture changed successfully!!');
        }
} 
//showing specific owners informations of a company
public function showOwnerDetail($id){
    try{
 $showall=busOwner::find($id);
 if(is_null($showall)){
     return $this->sendError('There is no owner of provided ID!!'); 
 }
 else{
return $this->sendResponse($showall,'owner details are successfully shown!');   
 }
    }
     catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }

}
//logout owner by id:
public function logoutOwner($id){
    $getowner=busOwner::where('owner_id',$id)->first();
     $getowner->update(['device_token' => null]);    
    return $this->sendResponse($getowner,'successfully is logged out!');   

}
//check login or not
function checkLoginStatusOwner($id){
    $getdriver=busOwner::where('owner_id',$id)->first('device_token');
    if(is_null($getdriver->device_token)){
         return $this->sendError('Owner is currently logged out');
    }
    else{
       
        return $this->sendResponse($getdriver,'Owner is in logged in state'); 
    }
}
//remove driver by bus owner
public function removeDriverByOwner($driverid){
    $getdriver=busOwner::join('bus_info','bus_owner.owner_phone','bus_info.owner_phone')->join('driver_info','bus_info.bus_no','driver_info.bus_no')->where('driver_info.driver_id',$driverid)->first();
 
  if($getdriver->bus_no){
     $removedriverbusno=Driver::where('driver_id',$driverid)->update(['active_status'=>'no']); 
     //notify admin as well
     //saving in notification table as well::
      $getadmindata=Driver::where('driver_info.driver_id',$driverid)->join('admin_info', 'driver_info.company_id', '=', 'admin_info.company_id')->select('driver_info.driver_phone','admin_info.*')->first();
            $store=new Mynotification();
            $store->type='driver removed';
            $store->notifiable_id=$getadmindata->admin_id;
            $store->notifiable_type=LoginModel::class;
            $store->title='Driver removed by vehicle owner';
            $store->body='Driver '.$getdriver->driver_name.' of bus no '.$getdriver->bus_no.' has been removed by vehicle owner '.$getdriver->owner_name;
            $store->added_by=$getdriver->owner_name;
            $store->created_at=Carbon::now();
            $store->company_id=$getadmindata->company_id;
            $store->save();
     
     
     return $this->sendResponse($removedriverbusno,'driver removed successfully!!');
  }
  //    return $this->sendResponse($getdriver,'drive removed successfully!!');
    
}

//********** for news related tasks ******
function showOwnerNewsWithImage($ownerid){
 $a=News::with('images')->where('news_info.news_for','owner')
    ->orWhere('news_info.news_for','all')
    ->orderBy('news_info.id','desc')->get();
     return $this->SendResponse($a,'news with image shown successfully!!');
  /*   \DB::statement("SET SQL_MODE=''");
     $getdata=News::leftJoin('news_image','news_info.id','news_image.news_id')->where('news_info.news_for','owner')
    ->orWhere('news_info.news_for','all')
    ->orderBy('news_info.id','desc')
    //->groupBy('news_info.id')
    ->select('news_info.id as news_id','news_info.topic','news_info.text_content','news_info.priority','news_image.*')
    //->selectRaw('group_concat(news_image.image) as images') */
  
}
function countNewsViews($newsid){
    $getdata=News::where('id',$newsid)->first();
    $views=$getdata->view_count;
    $incrementview=$views+1;
    $updateviews=$getdata->update([
        'view_count'=>$incrementview
        ]);
    $getdata=News::where('id',$newsid)->first();
    return $this->SendResponse($getdata,'News views counted successfully!!');
}
function showNewsByCategory($category){
  $getdata=News::with('images')->where('news_info.news_for',$category)->get();
  return $this->SendResponse($getdata,'News filtered successfully!!');
}
//********** for Events related tasks ******
function showOwnerEventsWithImage($ownerid){
    $a=Event::with('images')->where('event_info.event_for','owner')
    ->orWhere('event_info.event_for','all')
    ->orderBy('event_info.id','desc')->get();
     return $this->SendResponse($a,'Events with images shown successfully!!');
 
}
function countEventViews($eventid){
    $getdata=Event::where('id',$eventid)->first();
    $views=$getdata->view_count;
    $incrementview=$views+1;
    $updateviews=$getdata->update([
        'view_count'=>$incrementview
        ]);
    $getdata=Event::where('id',$eventid)->first();
    return $this->SendResponse($getdata,'Event views counted successfully!!');
}
function showEventByCategory($category){
  $getdata=Event::with('images')->where('event_info.event_for',$category)->get();
  return $this->SendResponse($getdata,'Event filtered successfully!!');
}
/*public function authenticate(Request $request) { 
    $owner_phone = $request->input('owner_phone');
    $user = busOwner::where('owner_phone', '=', $owner_phone)->first();
    try { 
        // verify the credentials and create a token for the user
        if (! $token = JWTAuth::fromUser($user)) { 
            return response()->json(['error' => 'invalid_credentials'], 401);
        } 
    } catch (JWTException $e) { 
        // something went wrong 
        return response()->json(['error' => 'could_not_create_token'], 500); 
    } 
    // if no errors are encountered we can return a JWT 
    return response()->json(compact('token')); 
}
*/

 


}
