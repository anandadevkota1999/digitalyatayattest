<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Users\Driver;
use App\Models\Users\businfo as VehicleInfo;
use App\Models\Notification as Mynotification;
use App\Models\BeforeLogin\companyList;
use App\Models\News;
use App\Models\NewsImage;
use App\Models\Event;
use App\Models\EventImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Http\Controllers\Api\baseController as BaseController;
use Carbon\Carbon;
//use Hash;
use Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;


class driverController extends BaseController
{
  //for registering drivers in the System
  public function driverRegister(Request $req){
  $input_the_data= $req->all();
     $validator =Validator::make($input_the_data,[
  	        'driver_id' => 'required',
            'bus_no' => 'required',
            'driver_name'=>'required',
            'driver_phone'=>'required|numeric',
            'driver_address'=>'required',
            'driver_pin'=>'required',
           // 'driver_dob'=>'required',
            'driver_gender'=>'required',
            'driver_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
            'driver_citizenship_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
           'driver_license_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
              'company_id' => 'required',
              
            
  ]);
     if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
   
  $checkvalidity=Driver::where('company_id',$req['company_id'])->where('driver_phone',$req['driver_phone'])->where('active_status','yes')->first();
 //return $this->sendResponse($checkvalidity,'the datas are now stored successfully!!'); 
  if(is_null($checkvalidity)){
     $checkactivedriver=Driver::where('company_id',$req['company_id'])->where('driver_phone',$req['driver_phone'])->where('active_status','!=','yes')->first();
   // return $this->sendResponse($checkactivedriver,'the datas are now stored successfully!!'); 
 if(is_null($checkactivedriver)){
     $input_the_data['driver_id']=$req['driver_id'];
    $createvalues=Driver::create($input_the_data);
     //notify admin as well
     //saving in notification table as well::
      $getadmindata=Driver::where('driver_info.driver_id',$req->input('driver_id'))->join('admin_info', 'driver_info.company_id', '=', 'admin_info.company_id')->select('driver_info.driver_phone','admin_info.*')->first();
            $store=new Mynotification();
            $store->type='driver register';
            $store->notifiable_id=$getadmindata->admin_id;
            $store->notifiable_type=LoginModel::class;
            $store->title='Driver registered by Vehicle owner';
            $store->body='Driver: '.$req->input('driver_name').' has been added by vehicle owner: '.$req->input('added_by').' to the Vehicle: '.$req->input('bus_no');
            $store->added_by=$req->input('added_by');
            $store->created_at=Carbon::now();
            $store->company_id=$req->input('company_id');
            $store->save(); 
    return $this->sendResponse($createvalues,'the datas are now stored successfully!!');
     }
    else{
    $updatedetails=Driver::where('driver_phone',$req['driver_phone'])->update([
        'driver_id'=>$checkactivedriver->driver_id,
        'bus_no'=>$req->input('bus_no'),
        'driver_pin'=>$req->input('driver_pin'),
        'updated_at'=>Carbon::now(),
        'active_status'=>'yes',
        'updated_by'=>$req->input('added_by')
        ]);    
    $getdata=Driver::where('driver_id',$req->input('driver_id'))->where('driver_phone',$req['driver_phone'])->first();
     //notify admin as well
     //saving in notification table as well::
      $getadmindata=Driver::where('driver_info.driver_id',$checkactivedriver->driver_id)->join('admin_info', 'driver_info.company_id', '=', 'admin_info.company_id')->select('driver_info.driver_phone','admin_info.*')->first();
            $store=new Mynotification();
            $store->type='driver register';
            $store->notifiable_id=$getadmindata->admin_id;
            $store->notifiable_type=LoginModel::class;
            $store->title='Driver registered by Vehicle owner';
            $store->body='Driver: '.$getdata->driver_name.' has been added by vehicle owner: '.$req->input('added_by').' to the Vehicle: '.$req->input('bus_no');
            $store->added_by=$req->input('added_by');
            $store->created_at=Carbon::now();
            $store->company_id=$req->input('company_id');
            $store->save(); 
     return $this->sendResponse($getdata,'the datas are now stored successfully!!'); 
  
    }  
  }
  else{
      return $this->sendError('Given Driver details are already registered in the system!');  
  }  
    /*  catch ( \Exception $ex ){ 
     $errorCode = $ex->errorInfo[1];
          if($errorCode == '1062'){
              return $this->sendError('Driver of given details already exists, please undo your process!!');
          }
      return $this->sendError(($ex->getMessage()));  
    } */
  }

public function driverLogin(Request $request) {
        $validator = Validator::make($request->all(), [
            'driver_phone' => 'required|numeric|min:10|exists:driver_info',
            'driver_pin' => 'required|numeric|min:6',
            'company_id' => 'required',

        ]);

        if($validator->fails()){
            return response()->json(['error' => $validator->errors()->all()]);
        } 
     

  if(Auth::guard('driver-api')->attempt(['driver_phone' => request('driver_phone'), 'password' => request('password'),'company_id' => request('company_id')])){

            config(['auth.guards.api.provider' => 'driver_info']);
            $driver=Auth::guard('driver-api')->user();
            $success =  $driver;
            $showsuccess['token'] =$driver->createToken('MyApp','owner')->accessToken; 
      //  return $this->sendResponse(admininfoResource::collection($success), 'Admin is logged in succesfully!!');

           return response()->json($showsuccess, 200);
        }
        else{ 
            return response()->json(['We got error!' => ['Provided credentials are Wrong!']], 200);
        }
    
}


//Update bus driver records by owners
public function updateDriverByOwner(Request $req, $id){
    try{
    $validator =Validator::make($req->all(),[
   'driver_phone'=>['required', 'digits:10'],
   'company_id'=>'required',
   'driver_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
   'driver_citizenship_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
   'driver_license_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
   
  ]);
   if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        } 
        
       if($req->hasFile('driver_image')&&$req->hasFile('driver_citizenship_image')&&$req->hasFile('driver_license_image')){
       $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_image->store($getcompany, 'local');
       $req->driver_citizenship_image->store($getcompany, 'local');
       $req->driver_license_image->store($getcompany, 'local');
   //***********saving the required values to the database!!**************
$driver=Driver::findorFail($id);
 if($driver){
  $driver->driver_name=$req['driver_name'];
 $driver->driver_phone=$req['driver_phone'];
 $driver->driver_address =$req['driver_address'];
// $driver->driver_dob=$req['driver_dob'];
 $driver->driver_gender=$req['driver_gender'];
 $driver->driver_image=$req->driver_image->hashName();
 $driver->driver_citizenship_image=$req->driver_citizenship_image->hashName();
  $driver->driver_license_image=$req->driver_license_image->hashName();
  $driver->updated_at=Carbon::now();
 $driver->updated_by=$req['updated_by'];
 $driver->save();
  return $this->sendResponse('done!','driver values are successfully updated!');
    }
      else{
  return $this->sendError('There are no staff records to update!');
}
        }
        
          // update::for driver_license_image  and driver_citizenship_image only
        else if($req->hasFile('driver_license_image')&&$req->hasFile('driver_citizenship_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_citizenship_image->store($getcompany, 'local');
       $req->driver_license_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 
$driver=Driver::findorFail($id);
 if($driver){
  $driver->driver_name=$req['driver_name'];
 $driver->driver_phone=$req['driver_phone'];
 $driver->driver_address =$req['driver_address'];
 //$driver->driver_dob=$req['driver_dob'];
 $driver->driver_gender=$req['driver_gender'];
  $driver->driver_citizenship_image=$req->driver_citizenship_image->hashName();
  $driver->driver_license_image=$req->driver_license_image->hashName();
   $driver->updated_at=Carbon::now();
 $driver->updated_by=$req['updated_by'];
 $driver->save();
  return $this->sendResponse('done!','driver values are successfully updated!');
    }
      else{
  return $this->sendError('There are no staff records to update!');
}   
        }
        
        // update::for driver_citizenship_image only
        else if($req->hasFile('driver_citizenship_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_citizenship_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
$driver=Driver::findorFail($id);
 if($driver){
  $driver->driver_name=$req['driver_name'];
 $driver->driver_phone=$req['driver_phone'];
 $driver->driver_address =$req['driver_address'];
// $driver->driver_dob=$req['driver_dob'];
 $driver->driver_gender=$req['driver_gender'];
  $driver->driver_citizenship_image=$req->driver_citizenship_image->hashName();
   $driver->updated_at=Carbon::now();
 $driver->updated_by=$req['updated_by'];
 $driver->save();
  return $this->sendResponse('done!','driver values are successfully updated!');
    }
      else{
  return $this->sendError('There are no staff records to update!');
}
       
        }
        
          // update::for driver_image only
        else if($req->hasFile('driver_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 
$driver=Driver::findorFail($id);
 if($driver){
  $driver->driver_name=$req['driver_name'];
 $driver->driver_phone=$req['driver_phone'];
 $driver->driver_address =$req['driver_address'];
// $driver->driver_dob=$req['driver_dob'];
 $driver->driver_gender=$req['driver_gender'];
  $driver->driver_image=$req->driver_image->hashName();
   $driver->updated_at=Carbon::now();
 $driver->updated_by=$req['updated_by'];
 $driver->save();
  return $this->sendResponse('done!','driver values are successfully updated!');
    }
      else{
  return $this->sendError('There are no staff records to update!');
}   
        }

    // update::for driver_license_image only
        else if($req->hasFile('driver_license_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_license_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 
$driver=Driver::findorFail($id);
 if($driver){
  $driver->driver_name=$req['driver_name'];
 $driver->driver_phone=$req['driver_phone'];
 $driver->driver_address =$req['driver_address'];
// $driver->driver_dob=$req['driver_dob'];
 $driver->driver_gender=$req['driver_gender'];
  $driver->driver_license_image=$req->driver_license_image->hashName();
   $driver->updated_at=Carbon::now();
 $driver->updated_by=$req['updated_by'];
 $driver->save();
  return $this->sendResponse('done!','driver values are successfully updated!');
    }
      else{
  return $this->sendError('There are no staff records to update!');
}   
        }
      
    
          //if the owner etries : No need to input images so,
     else{
         
        //updating in db table
$driver=Driver::findorFail($id);
   $driver->driver_name=$req['driver_name'];
 $driver->driver_phone=$req['driver_phone'];
 $driver->driver_address =$req['driver_address'];
// $driver->driver_dob=$req['driver_dob'];
 $driver->driver_gender=$req['driver_gender'];
  $driver->updated_at=Carbon::now();
 $driver->updated_by=$req['updated_by'];
 $driver->save();
 return $this->sendResponse('done!','Bus Records are successfully updated!');
       
     } 
    }
       catch ( \Exception $ex ){ 
      $errorCode = $ex->errorInfo[1];
          if($errorCode == '1062'){
              return $this->sendError('Given phone is already taken, please undo your process!!');
          }
      return $this->sendError(($ex->getMessage())); 
    } 
    
 
}
//update drivers
public function updateDriver(Request $req, $id){
         try{
    $validator =Validator::make($req->all(),[
   'driver_phone'=>'required|digits:10',
   'driver_name'=>'required',
   'driver_address'=>'required',
   'driver_dob'=>'required',
   'driver_gender'=>'required',
   'bus_no'=>'required',
   'company_id'=>'required',
   'driver_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
   'driver_citizenship_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
   'driver_license_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
   'updated_by'=>'required',
  ]);
   if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        } 
        
       if($req->hasFile('driver_image')&&$req->hasFile('driver_citizenship_image')&&$req->hasFile('driver_license_image')){
       $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_image->store($getcompany, 'local');
       $req->driver_citizenship_image->store($getcompany, 'local');
       $req->driver_license_image->store($getcompany, 'local');
   //***********saving the required values to the database!!**************
 $driver=Driver::findorFail($id);
 if($driver){
     $driver->update([
         'bus_no'=>$req['bus_no'],
         'driver_name'=>$req['driver_name'],
         'driver_phone'=>$req['driver_phone'],
         'driver_address'=>$req['driver_address'],
         'driver_dob'=>$req['driver_dob'],
         'driver_gender'=>$req['driver_gender'],
         'driver_image'=>$req->driver_image->hashName(),
         'driver_citizenship_image'=>$req->driver_citizenship_image->hashName(),
         'driver_license_image'=>$req->driver_license_image->hashName(),
         'updated_at'=>Carbon::now(),
         'updated_by'=>$req['updated_by']
         ]);
$updatedvalue=Driver::where('driver_id',$id)->first();
 return $this->sendResponse($updatedvalue,'Driver records are successfully updated!');
    }
      else{
  return $this->sendError('There are no driver records of such id!');
}
        }
        
          // update::for driver_license_image  and driver_citizenship_image only
        else if($req->hasFile('driver_license_image')&&$req->hasFile('driver_citizenship_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_citizenship_image->store($getcompany, 'local');
       $req->driver_license_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 
$driver=Driver::findorFail($id);
 if($driver){
       $driver->update([
         'bus_no'=>$req['bus_no'],
         'driver_name'=>$req['driver_name'],
         'driver_phone'=>$req['driver_phone'],
         'driver_address'=>$req['driver_address'],
         'driver_dob'=>$req['driver_dob'],
         'driver_gender'=>$req['driver_gender'],
         'driver_citizenship_image'=>$req->driver_citizenship_image->hashName(),
         'driver_license_image'=>$req->driver_license_image->hashName(),
         'updated_at'=>Carbon::now(),
         'updated_by'=>$req['updated_by']
         ]);
$updatedvalue=Driver::where('driver_id',$id)->first();
return $this->sendResponse($updatedvalue,'Driver records are successfully updated!');
    }
      else{
  return $this->sendError('There are no driver records to update!');
}   
        }
        
          // update::for driver_image  and driver_citizenship_image only
        else if($req->hasFile('driver_image')&&$req->hasFile('driver_citizenship_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_citizenship_image->store($getcompany, 'local');
       $req->driver_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 
$driver=Driver::findorFail($id);
 if($driver){
       $driver->update([
         'bus_no'=>$req['bus_no'],  
         'driver_name'=>$req['driver_name'],
         'driver_phone'=>$req['driver_phone'],
         'driver_address'=>$req['driver_address'],
         'driver_dob'=>$req['driver_dob'],
         'driver_gender'=>$req['driver_gender'],
         'driver_citizenship_image'=>$req->driver_citizenship_image->hashName(),
         'driver_image'=>$req->driver_image->hashName(),
         'updated_at'=>Carbon::now(),
         'updated_by'=>$req['updated_by']
         ]);
$updatedvalue=Driver::where('driver_id',$id)->first();
return $this->sendResponse($updatedvalue,'Driver records are successfully updated!');
    }
      else{
  return $this->sendError('There are no driver records to update!');
}   
        }
        
        // update::for driver_image  and driver_license_image only
        else if($req->hasFile('driver_image')&&$req->hasFile('driver_license_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_citizenship_image->store($getcompany, 'local');
       $req->driver_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 
$driver=Driver::findorFail($id);
 if($driver){
       $driver->update([
         'bus_no'=>$req['bus_no'],  
         'driver_name'=>$req['driver_name'],
         'driver_phone'=>$req['driver_phone'],
         'driver_address'=>$req['driver_address'],
         'driver_dob'=>$req['driver_dob'],
         'driver_gender'=>$req['driver_gender'],
         'driver_license_image'=>$req->driver_license_image->hashName(),
         'driver_image'=>$req->driver_image->hashName(),
         'updated_at'=>Carbon::now(),
         'updated_by'=>$req['updated_by']
         ]);
$updatedvalue=Driver::where('driver_id',$id)->first();
return $this->sendResponse($updatedvalue,'Driver records are successfully updated!');
    }
      else{
  return $this->sendError('There are no driver records to update!');
}   
        }
        
        // update::for driver_citizenship_image only
      else if($req->hasFile('driver_citizenship_image')){
        $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_citizenship_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
$driver=Driver::findorFail($id);
 if($driver){
 $driver->update([
         'bus_no'=>$req['bus_no'],
         'driver_name'=>$req['driver_name'],
         'driver_phone'=>$req['driver_phone'],
         'driver_address'=>$req['driver_address'],
         'driver_dob'=>$req['driver_dob'],
         'driver_gender'=>$req['driver_gender'],
         'driver_citizenship_image'=>$req->driver_citizenship_image->hashName(),
         'updated_at'=>Carbon::now(),
         'updated_by'=>$req['updated_by']
         ]);
 $updatedvalue=Driver::where('driver_id',$id)->first();
 return $this->sendResponse($updatedvalue,'Driver records are successfully updated!'); 
    }
      else{
  return $this->sendError('There are no driver records to update!');
}
       
        }
        
          // update::for driver_image only
        else if($req->hasFile('driver_image')){
       $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 
$driver=Driver::findorFail($id);
 if($driver){
     $driver->update([
         'bus_no'=>$req['bus_no'],
         'driver_name'=>$req['driver_name'],
         'driver_phone'=>$req['driver_phone'],
         'driver_address'=>$req['driver_address'],
         'driver_dob'=>$req['driver_dob'],
         'driver_gender'=>$req['driver_gender'],
         'driver_image'=>$req->driver_image->hashName(),
         'updated_at'=>Carbon::now(),
         'updated_by'=>$req['updated_by']
         ]);
 $updatedvalue=Driver::where('driver_id',$id)->first();
 return $this->sendResponse($updatedvalue,'Driver records are successfully updated!'); 

    }
      else{
  return $this->sendError('There are no driver records to update!');
}   
        }

    // update::for driver_license_image only
        else if($req->hasFile('driver_license_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_license_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 
$driver=Driver::findorFail($id);
 if($driver){
       $driver->update([
         'bus_no'=>$req['bus_no'],  
         'driver_name'=>$req['driver_name'],
         'driver_phone'=>$req['driver_phone'],
         'driver_address'=>$req['driver_address'],
         'driver_dob'=>$req['driver_dob'],
         'driver_gender'=>$req['driver_gender'],
         'driver_license_image'=>$req->driver_license_image->hashName(),
         'updated_at'=>Carbon::now(),
         'updated_by'=>$req['updated_by']
         ]);
 $updatedvalue=Driver::where('driver_id',$id)->first();
 return $this->sendResponse($updatedvalue,'Driver records are successfully updated!');

    }
      else{
  return $this->sendError('There are no driver records to update!');
}   
        }
      
    
          //if the owner etries : No need to input images so,
     else{
         
        //updating in db table
$driver=Driver::findorFail($id);
  $driver->update([
         'bus_no'=>$req['bus_no'],
         'driver_name'=>$req['driver_name'],
         'driver_phone'=>$req['driver_phone'],
         'driver_address'=>$req['driver_address'],
         'driver_dob'=>$req['driver_dob'],
         'driver_gender'=>$req['driver_gender'],
         'updated_at'=>Carbon::now(),
         'updated_by'=>$req['updated_by']
         ]);
 $updatedvalue=Driver::where('driver_id',$id)->first();
 return $this->sendResponse($updatedvalue,'Driver records are successfully updated!');
     } 
    }
      catch ( \Exception $ex ){ 
      $errorCode = $ex->errorInfo[1];
          if($errorCode == '1062'){
              return $this->sendError('The Given Id is duplicate, please undo your process!!');
          }
      return $this->sendError(($ex->getMessage())); 
    } 
    
}

public function updateProfileOnly(Request $req, $id){
     $validator =Validator::make($req->all(),[
 'driver_image'=>'required', 'image|mimes:jpeg,png,jpg,gif,svg,jfif|max:2048',
   'company_id'=>'required',
  ]);
   if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        } 
       if($req->hasFile('driver_image')){
       $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_image->store($getcompany, 'local');
 $driver=Driver::findorFail($id);
 $driver->driver_image=$req->driver_image->hashName();
 $driver->save();
   return $this->sendResponse('well done!', 'Profile picture changed successfully!!');
        }
} 

public function changeDriverPassword(Request $request,$id){
    $validator = Validator::make($request->all(), [
            'old_pin' => 'required|min:6',
            'new_pin'=>'required|min:6|different:old_pin',
            'confirm_pin'=>'required|same:new_pin',
        ],
         [
        'new_pin.different'=>'New Pin code should be different than old one!!' ,   
           'old_pin.min'=>'old pin must be of 6 numbers',
        'new_pin.min'=>'new pin must be of 6 numbers',
        'confirm_pin.min'=>'confirm pin must be of 6 numbers',
 
            ]
        );
        if($validator->fails()){
           $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string); 
        }
         $showall=Driver::find($id);
  if(is_null($showall)){
  return $this->sendError('there is no Driver of such ID!!');
  }
  else{
    $getpin=$showall->driver_pin;
     $showall->driver_pin=$request->new_pin;
    //if password input is same to previous!
     
  
    if($request->old_pin==$getpin){
        $query=Driver::where( 'driver_id' , $showall->driver_id)->update( array( 'driver_pin' =>  $showall->driver_pin));
         return $this->sendResponse($query,'Driver password is successfully changed!');
    }
   
          else{
         return $this->sendError('old password doesnt match'); 
   }
  } 
}

//**************for  showing drivers of a bus ********************
public function ShowDriverOfBus($ids){
  $combidedquery=DB::table('bus_info')->where('bus_id',$ids)->join('driver_info', 'bus_info.bus_no', '=', 'driver_info.bus_no')->select('driver_info.*')->first();
  if(is_null($combidedquery)){
      return $this->sendError('there are no bus of such ID!!');
  }
  else{
   return $this->sendResponse($combidedquery, 'Driver listed successfully!!'); 
}
}
//get bus ownerlist of driver
 public function getOwnerListofDriver($id){
     $getOwners=Driver::where('driver_id',$id)->join('bus_info','driver_info.bus_no','bus_info.bus_no')->join('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->select('bus_owner.*')->first();
     
     return $this->sendResponse($getOwners, 'Driver listed successfully,done!!');
 }
//logout driver 
public function logoutDriver($id){
    $getdriver=Driver::where('driver_id',$id)->first();
     $getdriver->update(['device_token' => null]);    
    return $this->sendResponse($getdriver,'successfully logged out!');   

}

//check login or not
function checkLoginStatusDriver($id){
    $getdriver=Driver::where('driver_id',$id)->first('device_token');
   //  return $this->sendResponse($getdriver,'Driver e'); 
    if(is_null($getdriver->device_token)){
         return $this->sendError('Driver is currently logged out');
    }
    else{
       
        return $this->sendResponse($getdriver,'Driver is in logged in state'); 
    }
}
//for showing driver details by id
public function showDriverDetail($id){
     try{
 $showall=Driver::where('driver_id',$id)->first();
  if(is_null($showall)){
     return $this->sendError('There is no driver of such ID!!'); 
 }
 else{
return $this->sendResponse($showall,'driver details are successfully shown!');   
 }
    }
     catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
}
//show drivers of owner for admin:
function showDriverofOwnerForAdmin($ownerphone){
    try{
       $get=Driver::leftJoin('bus_info','bus_info.bus_no','driver_info.bus_no')->leftJoin('bus_owner','bus_info.owner_phone','bus_owner.owner_phone')->where('bus_owner.owner_phone',$ownerphone)->where('bus_owner.active_status','yes')->where('bus_info.active_status','yes')->where('driver_info.active_status','yes')->select('driver_info.*')->get(); 
       return $this->sendResponse($get, 'drivers of specific owner is shown successfully!!');        
       }
   catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
}

//create driver by admin
 public function DriverRegisterByAdmin(Request $req){

       try{
     $input_the_data= $req->all();
       $validator =Validator::make($input_the_data,[
  	        'driver_id' => 'required',
            'bus_no' => 'required',
            'driver_name'=>'required',
            'driver_phone'=>'required|unique:driver_info',
            'driver_address'=>'required',
            'driver_pin'=>'required|numeric|min:5',
            'driver_dob'=>'required',
            'driver_gender'=>'required',
            'driver_licence_no'=>'required|unique:driver_info',
            'driver_licence_category'=>'required',
            'company_id'=>'required',
            'driver_citizenship_image'=>'required|image|mimes:jpeg,png,jpg,gif,svg,jfif',
            'driver_license_image'=>'required|mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
            'driver_image'=>'required|mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
            'driver_identity_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000'
  ],[
      'driver_licence_no.unique'=>'Driver with given license number is already registered!!',
      'driver_phone.unique'=>'Driver with given phone number is already registered!!',
      'driver_citizenship_image.required'=>'Driver citizenship image is required!',
      'driver_license_image.required'=>'Driver license image is required!',
      'driver_image.required'=>'Driver image is required!',
      'driver_licence_category.required'=>'Driver licence category is required!',
      ]);
  if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
   if($req->hasFile('driver_citizenship_image')&&$req->hasFile('driver_license_image')&&$req->hasFile('driver_image')&&$req->hasFile('driver_identity_image')){
      //$checkifdriverexists=VehicleInfo::join('driver_info','bus_info.bus_no','driver_info.bus_no')->where('driver_info.active_status','yes')->where('bus_info.active_status','yes')->get();
      // return $this->sendResponse($checkifdriverexists, 'Driver registered successfully!!'); 
        $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_citizenship_image->store($getcompany, 'local');
       $req->driver_license_image->store($getcompany, 'local');
       $req->driver_image->store($getcompany, 'local');
       $req->driver_identity_image->store($getcompany, 'local');
       $added_by=$req['added_by'];
    //   $updated_by=$req['updated_by'];
     $model=new Driver();
     $model->driver_id=$req->input('driver_id');
     $model->bus_no=$req->input('bus_no');
     $model->driver_name=$req->input('driver_name');
     $model->driver_phone=$req->input('driver_phone');
     $model->driver_address=$req->input('driver_address');
     $model->driver_pin=$req->input('driver_pin');
     $model->driver_dob=$req->input('driver_dob');
     $model->driver_gender=$req->input('driver_gender');
     $model->driver_licence_no=$req->input('driver_licence_no');
     $model->driver_licence_category=$req->input('driver_licence_category');
     $model->driver_licence_issuedate=$req->input('driver_licence_issuedate');
     $model->driver_licence_expirydate=$req->input('driver_licence_expirydate');
     $model->driver_image=$req->driver_image->hashName();
     $model->driver_citizenship_image=$req->driver_citizenship_image->hashName();
     $model->driver_license_image=$req->driver_license_image->hashName();
     $model->driver_identity_image=$req->driver_identity_image->hashName();
     $model->company_id=$req->input('company_id');
     //$model->device_token=$req->input('device_token');
     //$model->device_name=$req->input('device_name');
     $model->created_at=Carbon::now();
     $model->updated_at=Carbon::now();
     $model->added_by=$added_by;
     $model->updated_by=null;
     $model->driver_identity_no=$req->input('driver_identity_no');
     $model->driver_identity_issuedate=$req->input('driver_identity_issuedate');
     $model->driver_identity_enddate=$req->input('driver_identity_enddate');
     $model->driver_identity_image=$req->driver_identity_image->hashName();
     $model->save();
    return $this->sendResponse($model, 'Driver registered successfully!!'); 
    
     } 
   else if($req->hasFile('driver_citizenship_image')&&$req->hasFile('driver_license_image')&&$req->hasFile('driver_image')){
     $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_citizenship_image->store($getcompany, 'local');
       $req->driver_license_image->store($getcompany, 'local');
       $req->driver_image->store($getcompany, 'local');
       $added_by=$req['added_by'];
    //   $updated_by=$req['updated_by'];
     $model=new Driver();
     $model->driver_id=$req->input('driver_id');
     $model->bus_no=$req->input('bus_no');
     $model->driver_name=$req->input('driver_name');
     $model->driver_phone=$req->input('driver_phone');
     $model->driver_address=$req->input('driver_address');
     $model->driver_pin=$req->input('driver_pin');
     $model->driver_dob=$req->input('driver_dob');
     $model->driver_gender=$req->input('driver_gender');
     $model->driver_licence_no=$req->input('driver_licence_no');
     $model->driver_licence_category=$req->input('driver_licence_category');
     $model->driver_licence_issuedate=$req->input('driver_licence_issuedate');
     $model->driver_licence_expirydate=$req->input('driver_licence_expirydate');
     $model->driver_image=$req->driver_image->hashName();
     $model->driver_citizenship_image=$req->driver_citizenship_image->hashName();
     $model->driver_license_image=$req->driver_license_image->hashName();
     $model->company_id=$req->input('company_id');
     //$model->device_token=$req->input('device_token');
     //$model->device_name=$req->input('device_name');
     $model->created_at=Carbon::now();
     $model->updated_at=Carbon::now();
     $model->added_by=$added_by;
     $model->updated_by=null;
     $model->driver_identity_no=$req->input('driver_identity_no');
     $model->driver_identity_issuedate=$req->input('driver_identity_issuedate');
     $model->driver_identity_enddate=$req->input('driver_identity_enddate');
     $model->save();
    return $this->sendResponse($model, 'Driver registered successfully!!'); 
   
   }
    }
      catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }


      //  return $this->sendResponse($success, 'Congrats!, User has been registered successfully!!');

  }
//update driver by admin
public function updateDriverByAdmin(Request $req, $id){
       try{
    $validator =Validator::make($req->all(),[
   'driver_phone'=>'required|digits:10',
   'driver_name'=>'required',
   'driver_address'=>'required',
   'driver_dob'=>'required',
   'driver_gender'=>'required',
   'bus_no'=>'required',
   'company_id'=>'required',
   'driver_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
   'driver_citizenship_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
   'driver_license_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
   'driver_identity_image'=>'mimes:pdf,jpeg,png,jpg,gif,svg,jfif|max:25000',
   'updated_by'=>'required',
  ]);
   if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        } 
        
        // update::for driver_citizenship_image only
       if($req->hasFile('driver_citizenship_image')){
        $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_citizenship_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
$driver=Driver::findorFail($id);
 if($driver){
  $driver->update([
         'bus_no'=>$req['bus_no'],
         'driver_name'=>$req['driver_name'],
         'driver_phone'=>$req['driver_phone'],
         'driver_address'=>$req['driver_address'],
         'driver_dob'=>$req['driver_dob'],
         'driver_gender'=>$req['driver_gender'],
         'driver_licence_no'=>$req['driver_licence_no'],
         'driver_licence_category'=>$req['driver_licence_category'],
         'driver_licence_issuedate'=>$req['driver_licence_issuedate'],
         'driver_licence_expirydate'=>$req['driver_licence_expirydate'],
         'driver_citizenship_image'=>$req->driver_citizenship_image->hashName(),
         'updated_at'=>Carbon::now(),
         'updated_by'=>$req['updated_by'],
         'driver_identity_no'=>$req['driver_identity_no'],
         'driver_identity_issuedate'=>$req['driver_identity_issuedate'],
         'driver_identity_enddate'=>$req['driver_identity_enddate'],
         ]);
 $updatedvalue=Driver::where('driver_id',$id)->first();
// return $this->sendResponse($updatedvalue,'Driver records are successfully updated!'); 
    }
      else{
  return $this->sendError('There are no driver records to update!');
}
       
        }
        
          // update::for driver_image only
         if($req->hasFile('driver_image')){
       $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
  
$driver=Driver::findorFail($id);
 if($driver){
     $driver->update([
         'bus_no'=>$req['bus_no'],
         'driver_name'=>$req['driver_name'],
         'driver_phone'=>$req['driver_phone'],
         'driver_address'=>$req['driver_address'],
         'driver_dob'=>$req['driver_dob'],
         'driver_gender'=>$req['driver_gender'],
         'driver_licence_no'=>$req['driver_licence_no'],
         'driver_licence_category'=>$req['driver_licence_category'],
         'driver_licence_issuedate'=>$req['driver_licence_issuedate'],
         'driver_licence_expirydate'=>$req['driver_licence_expirydate'],
         'driver_image'=>$req->driver_image->hashName(),
         'updated_at'=>Carbon::now(),
         'updated_by'=>$req['updated_by'],
         'driver_identity_no'=>$req['driver_identity_no'],
         'driver_identity_issuedate'=>$req['driver_identity_issuedate'],
         'driver_identity_enddate'=>$req['driver_identity_enddate'],
         ]);
 $updatedvalue=Driver::where('driver_id',$id)->first();
// return $this->sendResponse($updatedvalue,'Driver records are successfully updated!'); 

    }
      else{
  return $this->sendError('There are no driver records to update!');
}   
        }

    // update::for driver_license_image only
         if($req->hasFile('driver_license_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_license_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 
$driver=Driver::findorFail($id);
 if($driver){
       $driver->update([
         'bus_no'=>$req['bus_no'],
         'driver_name'=>$req['driver_name'],
         'driver_phone'=>$req['driver_phone'],
         'driver_address'=>$req['driver_address'],
         'driver_dob'=>$req['driver_dob'],
         'driver_gender'=>$req['driver_gender'],
         'driver_licence_no'=>$req['driver_licence_no'],
         'driver_licence_category'=>$req['driver_licence_category'],
         'driver_licence_issuedate'=>$req['driver_licence_issuedate'],
         'driver_licence_expirydate'=>$req['driver_licence_expirydate'],
         'driver_license_image'=>$req->driver_license_image->hashName(),
         'updated_at'=>Carbon::now(),
         'updated_by'=>$req['updated_by'],
         'driver_identity_no'=>$req['driver_identity_no'],
         'driver_identity_issuedate'=>$req['driver_identity_issuedate'],
         'driver_identity_enddate'=>$req['driver_identity_enddate'],
         ]);
 $updatedvalue=Driver::where('driver_id',$id)->first();
// return $this->sendResponse($updatedvalue,'Driver records are successfully updated!');

    }
      else{
  return $this->sendError('There are no driver records to update!');
}   
        }
      
// update::for identity image only
         if($req->hasFile('driver_identity_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->driver_identity_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 
$driver=Driver::findorFail($id);
 if($driver){
       $driver->update([
         'bus_no'=>$req['bus_no'],
         'driver_name'=>$req['driver_name'],
         'driver_phone'=>$req['driver_phone'],
         'driver_address'=>$req['driver_address'],
         'driver_dob'=>$req['driver_dob'],
         'driver_gender'=>$req['driver_gender'],
         'driver_licence_no'=>$req['driver_licence_no'],
         'driver_licence_category'=>$req['driver_licence_category'],
         'driver_licence_issuedate'=>$req['driver_licence_issuedate'],
         'driver_licence_expirydate'=>$req['driver_licence_expirydate'],
         'driver_identity_image'=>$req->driver_identity_image->hashName(),
         'updated_at'=>Carbon::now(),
         'updated_by'=>$req['updated_by'],
         'driver_identity_no'=>$req['driver_identity_no'],
         'driver_identity_issuedate'=>$req['driver_identity_issuedate'],
         'driver_identity_enddate'=>$req['driver_identity_enddate'],
         ]);
 $updatedvalue=Driver::where('driver_id',$id)->first();
 return $this->sendResponse($updatedvalue,'Driver records are successfully updated!');

    }
      else{
  return $this->sendError('There are no driver records to update!');
}   
        }
        //if the owner etries : No need to input images so,
     else{
         
        //updating in db table
$driver=Driver::findorFail($id);
  $driver->update([
         'bus_no'=>$req['bus_no'],
         'driver_name'=>$req['driver_name'],
         'driver_phone'=>$req['driver_phone'],
         'driver_address'=>$req['driver_address'],
         'driver_dob'=>$req['driver_dob'],
         'driver_gender'=>$req['driver_gender'],
         'driver_licence_no'=>$req['driver_licence_no'],
         'driver_licence_category'=>$req['driver_licence_category'],
         'driver_licence_issuedate'=>$req['driver_licence_issuedate'],
         'driver_licence_expirydate'=>$req['driver_licence_expirydate'],
         'updated_at'=>Carbon::now(),
         'updated_by'=>$req['updated_by'],
         'driver_identity_no'=>$req['driver_identity_no'],
         'driver_identity_issuedate'=>$req['driver_identity_issuedate'],
         'driver_identity_enddate'=>$req['driver_identity_enddate'],
         ]);
 $updatedvalue=Driver::where('driver_id',$id)->first();
 return $this->sendResponse($updatedvalue,'Driver records are successfully updated!');
     } 
    }
      catch ( \Exception $ex ){ 
     
     $errorCode = $ex->errorInfo[1];
          if($errorCode == '1062'){
              return $this->sendError('The Given Id is duplicate, please undo your process!!');
          } 
      return $this->sendError(($ex->getMessage())); 
    } 
    
}  

//***************news portion starts here***************
function showDriverNewsWithImage($ownerid){
    $getnewswithimage=News::with('images')->where('news_info.news_for','driver')
    ->orWhere('news_info.news_for','all')
    ->orderBy('news_info.id','desc')->get();
     return $this->SendResponse($getnewswithimage,'News with image shown successfully!!');
}
function countNewsViews($newsid){
    $getdata=News::where('id',$newsid)->first();
    $views=$getdata->view_count;
    $incrementview=$views+1;
    $updateviews=$getdata->update([
        'view_count'=>$incrementview
        ]);
    $getdata=News::where('id',$newsid)->first();
    return $this->SendResponse($getdata,'News views counted successfully!!');
}
function showNewsByCategory($category){
  $getdata=News::with('images')->where('news_info.news_for',$category)->get();
  return $this->SendResponse($getdata,'News filtered successfully!!');
}
//***************Event portion starts here***************
function showDriverEventWithImage($ownerid){
    $getnewswithimage=Event::with('images')->where('event_info.event_for','driver')
    ->orWhere('event_info.event_for','all')
    ->orderBy('event_info.id','desc')->get();
     return $this->SendResponse($getnewswithimage,'Events with images shown successfully!!');
}
function countEventViews($eventid){
    $getdata=Event::where('id',$eventid)->first();
    $views=$getdata->view_count;
    $incrementview=$views+1;
    $updateviews=$getdata->update([
        'view_count'=>$incrementview
        ]);
    $getdata=Event::where('id',$eventid)->first();
    return $this->SendResponse($getdata,'Event views counted successfully!!');
}
function showEventByCategory($category){
  $getdata=Event::with('images')->where('event_info.event_for',$category)->get();
  return $this->SendResponse($getdata,'Event filtered successfully!!');
}


}
