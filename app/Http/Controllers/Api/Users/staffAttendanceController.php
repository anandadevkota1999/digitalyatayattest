<?php 
namespace App\Http\Controllers\Api\Users;
use Illuminate\Routing\Middleware\ThrottleRequests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\LoginModel;
use App\Models\Admin\StaffAttendance;
use App\Models\Users\StaffInfo;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\BeforeLogin\companyList;
//use Illuminate\Support\Facades\Session;
//use Cache;
use App\Http\Controllers\Api\baseController as BaseController;
//use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Validator;
//use Illuminate\Contracts\Auth\UserProvider;
//use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
Use Exception;
use Illuminate\Database\QueryException;

class staffAttendanceController extends BaseController{
   //get staffs attendance history report  
     public function getAttendenceHistory($ids){
             $get=StaffInfo::where('staff_attendance.staff_id',$ids)->join('staff_attendance', 'staff_info.staff_id', '=', 'staff_attendance.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_attendance.*')->orderBy('id', 'desc')->get();
        return $this->sendResponse($get,'values are updated successfully into the company!!');
    }
//count staff attendance of specific duration
  public function countAttendaceSpecificDuration(Request $req,$staffid){
       $validator = Validator::make($req->all(), [
             'from' => 'required|date',
             'to'=>'required|date',
        ]);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
     $from = $req['from'];
     $to = $req['to'];
     $comparewith=Carbon::parse($to)->endOfDay();
     //count present status only
     $countpresent=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.staff_id',$staffid)->where('staff_attendance.attendance_status','P')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->count();
     //count absent status only
     $countabsent=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.staff_id',$staffid)->where('staff_attendance.attendance_status','A')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->count();
          //count leave
     $countleave=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.staff_id',$staffid)->where('staff_attendance.attendance_status','L')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->count();
   $carbonfrom=Carbon::parse($from);
   $getdifferenceofdays = $comparewith->diffInDays($carbonfrom);
   $totalactivitydays=$countpresent+$countabsent+$countleave;
   $getholidays=$getdifferenceofdays-$totalactivitydays;
    //make array
    $getall=['present days'=>$countpresent,'absent days'=>$countabsent,'leave days'=>$countleave,'holidays'=>$getholidays];
    return $this->sendResponse($getall, 'attendance status counted successfully!!'); 
     //checking for present
     
     
     
     
     
         if($req['attendance_status']=='P'){
              $filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.staff_id',$staffid)->where('staff_attendance.attendance_status','P')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->count();
             if(is_null($filter)){
          return $this->sendResponse('No datas found', 'Present status filtered successfully!!'); 
     }
              else{
                   return $this->sendResponse($filter, 'staff on present filtered successfully!!');
               } 
     
         }
         //checking for absentees
         else  if($req['attendance_status']=='A'){
              $filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.company_id',$company_id)->where('staff_attendance.attendance_status','A')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->count();
                 if(is_null($filter)){
          return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
     }
              else{
                   return $this->sendResponse($filter, 'staff on absent filtered successfully!!');
               }     
         
          }
          //checking for:on leave staffs
         else  if($req['attendance_status']=='L'){
              $filter=StaffAttendance::whereBetween('staff_attendance.arrival_time', [$from, $comparewith])->where('staff_attendance.company_id',$company_id)->where('staff_attendance.attendance_status','L')->join('staff_info', 'staff_attendance.staff_id', '=', 'staff_info.staff_id')->select('staff_info.staff_id','staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_attendance.*')->count();
              if(is_null($filter)){
          return $this->sendResponse('No datas found', 'Absent status filtered successfully!!'); 
     }
              else{
                   return $this->sendResponse($filter, 'staff on Leave filtered successfully!!');
               } 
     
         }
         else{
              return $this->sendError('undefinable Entry!!!'); 
         }
  }



}