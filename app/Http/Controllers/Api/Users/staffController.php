<?php

namespace App\Http\Controllers\Api\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users\StaffInfo;
use App\Models\Users\StaffRole;
use App\Models\Users\DummyStaffRole;
use App\Models\Admin\Role;
use App\Models\Users\DummyStaffInfo;
use App\Models\BeforeLogin\companyList;
use App\Models\News;
use App\Models\NewsImage;
use App\Models\Event;
use App\Models\EventImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Str;
use App\Http\Controllers\Api\baseController as BaseController;
//use Hash;
use Validator;
use Illuminate\Validation\Rule;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;


class staffController extends BaseController
{
      private static $otp;
   function __construct(Request $request){
       self::$otp=rand(100000, 999999);
   }
  //for registering Admin users in the System
  public function staffRegister(Request $req){

try{
         $validator = Validator::make($req->all(), [
            'staff_id' => 'required',
             'staff_name' => 'required',
             'staff_address'=>'required',
             'staff_phone'=>'required|numeric',
             'staff_pin'=>'required|numeric|min:6',
            'staff_dob'=>'required|date',
             'staff_gender'=>'required',
              'staff_post'=>'required',
               'staff_image'=>'required|image|mimes:jpeg,png,jpg,gif,svg,jfif',
            'staff_citizenship_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,jfif',
          //  'staff_salary' => 'required',
            'company_id' => 'required',
            
        ]);
   
        if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
   
       if($req->hasFile('staff_citizenship_image')&&$req->hasFile('staff_citizenship_image')&&$req->hasFile('staff_image')&&$req->hasFile('staff_image')){
             $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->staff_citizenship_image->store($getcompany, 'local');
       $req->staff_image->store($getcompany, 'local');
    
      $model=new StaffInfo();
     $model->staff_id=$req->input('staff_id');
     $model->staff_name=$req->input('staff_name');
     $model->staff_address=$req->input('staff_address');
     $model->staff_phone=$req->input('staff_phone');
     $model->staff_pin=$req->input('staff_pin');
     $model->staff_dob=$req->input('staff_dob');
     $model->staff_gender=$req->input('staff_gender');
     $model->staff_post=$req->input('staff_post');
     $model->staff_image=$req->staff_image->hashName();
     $model->staff_citizenship_image=$req->staff_citizenship_image->hashName();
     $model->staff_salary=$req->input('staff_salary');
     $model->company_id=$req->input('company_id');
     $model->device_token=$req->input('device_token');
     $model->device_name=$req->input('device_name');
     $model->added_by=$req->input('added_by');
     $model->updated_by=$req->input('updated_by');
     $model->save();
    return $this->sendResponse($model, 'stored successfully!!');
    
     } 
    } 
  
    catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
  }
public function staffLogin(Request $request) {
     $validator = Validator::make($request->all(), [
            'staff_phone' => 'required|numeric|min:10',
            'staff_pin' => 'required|min:4',
            'company_id' => 'required'],
[
     'staff_phone.required'=>'staff phone is neccessary!',
     'company_id.required'=>'company id is required!'
    ]);

 if($validator->fails()){
     $geterror=$validator->errors()->all();
     $string=implode(",",$geterror);
    // $tojson=json_encode($geterror);
    return $this->sendError($string); 
  //return response()->json(['error' => $validator->errors()->all()]);
 }
  
$staff_phone=$request['staff_phone'];
$staff_pin=$request['staff_pin'];
$company_id=$request['company_id'];


  $user = StaffInfo::where('staff_phone' , $staff_phone)->where( 'staff_pin' , $staff_pin)->where('company_id' ,$company_id)->first();

  if($user){ 
   // $user=Auth::guard('busOwner-api')->user();
    $success['token'] =  $user->createToken('MyApp')->accessToken;
   // return response()->json(['success' => $success], $this->successStatus);
   // $token = $gettoken->createToken('Bus owner login Grant Client')->accessToken;   
    return $this->sendResponse($success,'token created successfully!!');
  }
  else{
    return $this->sendError('provided credentials are wrong!');
  }
  

}
public function deleteStaffInfo(Request $request,$id){
     try{
    //get owners record of a row by ID
    $getbus=StaffInfo::where('staff_id',$id)->first();
  //saving records row wise
    $staff_name=$getbus->staff_name;
    $staff_address=$getbus->staff_address;
    $staff_phone=$getbus->staff_phone;
    $staff_pin=$getbus->staff_pin;
    $staff_dob=$getbus->staff_dob; 
    $staff_gender=$getbus->staff_gender;
    $staff_post=$getbus->staff_post;
    $staff_image=$getbus->staff_image;
    $staff_citizenship_image=$getbus->staff_citizenship_image;
    $staff_salary=$getbus->staff_salary;
    $company_id=$getbus->company_id;
    $device_token=$getbus->device_token;
    $created_at=$getbus->created_at;
    $updated_at=$getbus->updated_at;
        
//now using dummy table to insert deleted records    
    $getdummy=new DummyStaffInfo();
    $getdummy->staff_id=$id;
    $getdummy->staff_name=$staff_name;
    $getdummy->staff_address=$staff_address;
    $getdummy->staff_phone=$staff_phone;
    $getdummy->staff_pin=$staff_pin;
    $getdummy->staff_dob=$staff_dob;
    $getdummy->staff_gender=$staff_gender;
    $getdummy->staff_post=$staff_post;
    $getdummy->staff_image=$staff_image;
    $getdummy->staff_citizenship_image=$staff_citizenship_image;
    $getdummy->staff_salary=$staff_salary;
    $getdummy->company_id=$company_id;
    $getdummy->device_token=$device_token;
    $getdummy->created_at=$created_at;
    $getdummy->updated_at=$updated_at;
    $getdummy->save();
    $getbus->delete();
   
    return $this->sendResponse($getdummy, 'staff records moved to dummy table successfully!!');   
   }
     catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
    
}
//Update staff records
public function updateStaff(Request $req, $id){
    try{
    $validator =Validator::make($req->all(),[
    'staff_phone'=>['required', 'digits:10'],
   'company_id'=>'required',
  
  ]);
   if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        } 
        
       if($req->hasFile('staff_image')&&$req->hasFile('staff_citizenship_image')){
       $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->staff_image->store($getcompany, 'local');
       $req->staff_citizenship_image->store($getcompany, 'local');
   //***********saving the required values to the database!!**************
$staff=StaffInfo::findorFail($id);
 if($staff){
  $staff->staff_name=$req['staff_name'];
 $staff->staff_address=$req['staff_address'];
 $staff->staff_phone =$req['staff_phone'];
 $staff->staff_dob=$req['staff_dob'];
 $staff->staff_gender=$req['staff_gender'];
 $staff->staff_post=$req['staff_post'];
$staff->staff_image=$req->staff_image->hashName();
 $staff->staff_citizenship_image=$req->staff_citizenship_image->hashName();
 //$staff->device_token=$req->input('device_token');
 //$staff->device_name=$req->input('device_name');
 $staff->updated_at=Carbon::now();
  $staff->updated_by=$req->input('updated_by');
  $staff->staff_salary=$req['staff_salary'];
 $staff->save();
  //now also connecting  bus detail with driver linked by bus no.
  //     $query=businfo::where('owner_phone' , $id)->update( array( 'owner_phone' =>  $req->owner_phone));
    
  return $this->sendResponse($staff,'Bus Records are successfully updated!');
      }
      else{
  return $this->sendError('There are no staff records to update!');
}
        }
        
        // update::for staff_citizenship_image only
        else if($req->hasFile('staff_citizenship_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->staff_citizenship_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
$staff=StaffInfo::findorFail($id);
 if($staff){
  $staff->staff_name=$req['staff_name'];
 $staff->staff_address=$req['staff_address'];
 $staff->staff_phone =$req['staff_phone'];
 $staff->staff_dob=$req['staff_dob'];
 $staff->staff_gender=$req['staff_gender'];
 $staff->staff_post=$req['staff_post'];
 $staff->staff_citizenship_image=$req->staff_citizenship_image->hashName();
// $staff->device_token=$req->input('device_token');
 //$staff->device_name=$req->input('device_name');
  $staff->updated_at=Carbon::now();
 $staff->updated_by=$req->input('updated_by');
 $staff->staff_salary=$req['staff_salary'];
 $staff->save();
  //now also connecting  bus detail with driver linked by bus no.
  //     $query=businfo::where('owner_phone' , $id)->update( array( 'owner_phone' =>  $req->owner_phone));
    
  return $this->sendResponse($staff,'Bus Records are successfully updated!');
      }
      else{
  return $this->sendError('There are no staff records to update!');
}
       
        }
        
          // update::for owner_citizenship_image only
        else if($req->hasFile('staff_image')){
              $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->staff_image->store($getcompany, 'local');
       //***********saving the required values to the database!!**************
 
$staff=StaffInfo::findorFail($id);
 if($staff){
  $staff->staff_name=$req['staff_name'];
 $staff->staff_address=$req['staff_address'];
 $staff->staff_phone =$req['staff_phone'];
 $staff->staff_dob=$req['staff_dob'];
 $staff->staff_gender=$req['staff_gender'];
 $staff->staff_post=$req['staff_post'];
 $staff->staff_image=$req->staff_image->hashName();
// $staff->device_token=$req->input('device_token');
// $staff->device_name=$req->input('device_name');
 $staff->updated_at=Carbon::now();
  $staff->updated_by=$req->input('updated_by');
  $staff->staff_salary=$req['staff_salary'];
 $staff->save();
  //now also connecting  bus detail with driver linked by bus no.
  //     $query=businfo::where('owner_phone' , $id)->update( array( 'owner_phone' =>  $req->owner_phone));
    
  return $this->sendResponse($staff,'Bus Records are successfully updated!');
      }
      else{
  return $this->sendError('There are no staff records to update!');
}
       
        }
    
          //if the owner etries : No need to input images so,
     else{
         
        //updating in db table
        $staff=StaffInfo::findorFail($id);
   $staff->staff_name=$req['staff_name'];
 $staff->staff_address=$req['staff_address'];
 $staff->staff_phone =$req['staff_phone'];
 $staff->staff_dob=$req['staff_dob'];
 $staff->staff_gender=$req['staff_gender'];
 $staff->staff_post=$req['staff_post'];
// $staff->device_token=$req->input('device_token');
 //$staff->device_name=$req->input('device_name');
  $staff->updated_at=Carbon::now();
  $staff->updated_by=$req->input('updated_by');
  $staff->staff_salary=$req['staff_salary'];
 $staff->save();
  return $this->sendResponse($staff,'Bus Records are successfully updated!');
       
     } 
    }
     catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
    
}
//for changing staff password
public function changeStaffPassword(Request $request,$id){
    $validator = Validator::make($request->all(), [
            'old_pin' => 'required|min:6',
            'new_pin'=>'required|min:6|different:old_pin',
            'confirm_pin'=>'required|same:new_pin',
        ],
        [
        'new_pin.different'=>'New Pin code should be different than old one!!',
        'old_pin.min'=>'old pin must be of 6 numbers',
        'new_pin.min'=>'new pin must be of 6 numbers',
        'confirm_pin.min'=>'confirm pin must be of 6 numbers',
            ]
        );
        if($validator->fails()){
           $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string); 
        }
         $showall=StaffInfo::find($id);
  if(is_null($showall)){
  return $this->sendError('there is no staff  of such ID!!');
  }
  else{
    $getpin=$showall->staff_pin;
    if($request->old_pin==$getpin){
        $showall->staff_pin=$request->new_pin;
        $query=StaffInfo::where( 'staff_id' , $showall->staff_id)->update( array( 'staff_pin' =>  $showall->staff_pin));
         return $this->sendResponse($query,'Staff password is successfully changed!');
    }
   
          else{
         return $this->sendError('old password doesnt match'); 
   }
  } 
}
public function otpForgetStaffPin(Request $request){
    $validator = Validator::make($request->all(), [
        'staff_phone' => ['required', 'digits:10','exists:admin_info'],
        ]);
        if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
        
         $say='your new pin is:';
        $getotp=self::$otp;
        $concat=$say." ".$getotp;
}
//logout staff 
public function logoutStaff($id){
    $getstaff=StaffInfo::where('staff_id',$id)->first();
    $getstaff->update(['device_token' => null]);    
    return $this->sendResponse($getstaff,'successfully logged out!');   
}
public function updateStaffProfileOnly(Request $req, $id){
     $validator =Validator::make($req->all(),[
 'staff_image'=>'required', 'image|mimes:jpeg,png,jpg,gif,svg,jfif|max:4048',
   'company_id'=>'required',
  ]);
   if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        } 
       if($req->hasFile('staff_image')){
       $all=companyList::where('company_id',$req['company_id'])->first();
       $getcompany=$all->company_name;
       $req->staff_image->store($getcompany, 'local');
 $staff=StaffInfo::findorFail($id);
 $staff->staff_image=$req->staff_image->hashName();
 $staff->save();
   return $this->sendResponse($staff, 'Profile picture changed successfully!!');
        }
} 
//check login or not
function checkLoginStatusStaff($id){
    $getdriver=StaffInfo::where('staff_id',$id)->first('device_token');
   //  return $this->sendResponse($getdriver,'Driver e'); 
    if(is_null($getdriver->device_token)){
         return $this->sendError('Staff is currently logged out');
    }
    else{
       
        return $this->sendResponse($getdriver,'Driver is in logged in state'); 
    }
}
//staff detail by staff id
function staffDetail($id){
    $get=StaffInfo::where('staff_id',$id)->first();
     return $this->sendResponse($get,'staff records shown successfully!!');
}

//-----------------------------------------staff with roles section starts from here:::
//for creating roles of staff:::::::

 public function createRole(Request $req){
         try{
             $input=$req->all();
         $validator = Validator::make($input, [
             'role_type' => 'required|unique:roles',
             'role_description' => 'required',
             'company_id'=>'required',
        ]);
   
        if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
        $input['created_at']=Carbon::now();
         $createvalues=Role::create($input);
         
         return $this->sendResponse($createvalues,'staff role has been registered!');
    }
    catch ( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
 }

//creating role for staffs:
public function createRoleForStaff(Request $req){
     \DB::statement("SET SQL_MODE=''");
     try{
         
            $input=$req->all();
            $getstaff=$input['staff_id'];
            $getrole=$input['role_id'];
           $company_id=$input['company_id'];
         $validator = Validator::make($input, [
             'staff_id' => 'required',
             'role_id' => 'required',
          'company_id'=>'required',
        ]);
         if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
        if(is_null($getstaff)){
            return $this->sendError('staff_id has not beeen selected yet!!');
        }
   else if($getstaff){
     $checkrole=StaffRole::where( 'role_id' , $getrole)->where( 'staff_id' , $getstaff)->where( 'company_id' , $company_id)->groupBy('staff_id')->first();
     
     if(is_null($checkrole)){
         $input['created_at']=Carbon::now();
         $input['added_by'];
         $createvalues=StaffRole::create($input);
         $input['added_by']=$createvalues->added_by;
         return $this->sendResponse($createvalues,'staff role has been registered!'); 
     }
     else{
          return $this->sendError('role has been already declared!!');
     }
     //return $this->sendResponse($checkrole,'staff role has been registered!');
         
    }
    else{
        return $this->sendError('staff_id has not beeen selected yet!!');
    }
  
    }
    catch( \Exception $ex ){ 
      return $this->sendError(($ex->getMessage())); 
    }
}
//show roll of company
public function showRoleBycompany($company_id){
    $query=Role::where('company_id',$company_id)->get();
     return $this->sendResponse($query,'staff role has been registered!');
}
//show staff roles by company:
public function showStaffRoleBycompany(Request $req,$company_id){
    \DB::statement("SET SQL_MODE=''");
  $validator =Validator::make($req->all(),[
   Rule::unique('staff_roles', 'role_id'),
  
  ]);
   if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        } 
  $query=StaffRole::where('staff_roles.company_id',$company_id)->join('staff_info', 'staff_roles.staff_id', '=', 'staff_info.staff_id')->join('roles', 'staff_roles.role_id', '=', 'roles.role_id')->select('staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_roles.staff_id','staff_roles.*', DB::raw('group_concat(staff_roles.role_id) as roles'), DB::raw('group_concat(roles.role_type) as role_type'))->groupBy('staff_roles.staff_id')->get();
     
     //select group_concat(role_id separator ', ') as myList from staff_roles
//GROUP BY  staff_id
     
    // $c=explode(',',$role);
      return $this->sendResponse($query,'staff role has been registered!');
}
//update staff roles by first deleting previously assigned role
public function updateStaffRole(Request $req,$staff_id){
     \DB::statement("SET SQL_MODE=''");
    $input=$req->all();
 $query=StaffRole::where('staff_id',$staff_id)->get();
  //return $this->sendResponse($query,'data shipped!');
     foreach($query as $queries) { 
           $getfromstaffrole=  [
	           'staff_role_id' => $queries->staff_role_id,
	            'staff_id' => $staff_id,
	            'role_id' =>$queries->role_id,
	            'company_id' =>$queries->company_id,
	            're_asigned_by' => $queries->updated_by
	            ];
	         //store in dummy table
	         DummyStaffRole::create($getfromstaffrole);
    	}
   //  return $this->sendResponse('bulk','staff role has been registered!'); 
    	//delete from staffroles after shipping then,
    	$query=StaffRole::where('staff_id',$staff_id)->delete();
  //then, re-insert roles for given staff
    
    $getcompany=$req->input('company_id');
    $rolesfromreq  = explode(',', $req->input('role_id'));
    $updated_by=$req->input('updated_by');
    //loops until all the values are inserted
    foreach($rolesfromreq as $roles) {
    StaffRole::insert( 
        array( 'staff_id'=>$staff_id,
                 'role_id'=>$roles,
	            'company_id' =>$getcompany,
	            'updated_at'=>Carbon::now(),
	            'updated_by' =>$updated_by) 
	            );
        }
    
    return $this->sendResponse('done','staff role has been registered!');
}
//delete staff roles
public function deleteStaffRoles(Request $req,$staff_id){
      
      $validator = Validator::make($req->all(), [
        'company_id'=>'required',
        ]);
        if($validator->fails()){
            $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        }
      $getstaffrole=StaffRole::where('staff_id',$staff_id)->where('company_id',$req['company_id'])->get();
      foreach($getstaffrole as $queries) { 
           $getfromstaffrole=  [
	           'staff_role_id' => $queries->staff_role_id,
	            'staff_id' => $staff_id,
	            'role_id' =>$queries->role_id,
	            'company_id' =>$queries->company_id,
	            'created_at' => Carbon::now()
	            ];
	         //store in dummy table
	         DummyStaffRole::create($getfromstaffrole);
    	}
    	//now deleting from staffroles
    $deletestaff=StaffRole::where('staff_id',$staff_id)->where('company_id',$req['company_id'])->delete();
    if(empty($deletestaff)){
        return $this->sendError('staff role has already been deleted!');
    }
     return $this->sendResponse('done','staff role has been deleted!'); 
  }
  
  //-------------start of staff roles task for android users(staff)
  public function showIndivisualRole(Request $req,$staffid){
        \DB::statement("SET SQL_MODE=''");
  $validator =Validator::make($req->all(),[
   Rule::unique('staff_roles', 'role_id'),
  
  ]);
   if($validator->fails()){
             $geterror=$validator->errors()->all();
            $string=implode(",",$geterror);
            return $this->sendError($string);       
        } 
  $query=StaffRole::where('staff_roles.staff_id',$staffid)->join('staff_info', 'staff_roles.staff_id', '=', 'staff_info.staff_id')->join('roles', 'staff_roles.role_id', '=', 'roles.role_id')->select('staff_info.staff_name','staff_info.staff_address','staff_info.staff_phone','staff_roles.staff_id','staff_roles.*', DB::raw('group_concat(staff_roles.role_id) as roles'), DB::raw('group_concat(roles.role_type) as role_type'))->groupBy('staff_roles.staff_id')->first();

return $this->sendResponse($query,'staff role has been deleted!'); 
  }
  
//-----------------end of staff roles section
//_______________ News Portion Starts here ________________
function showStaffNewsWithImage($ownerid){
    $getnewswithimage=News::with('images')->where('news_info.news_for','staff')
    ->orWhere('news_info.news_for','all')
    ->orderBy('news_info.id','desc')->get();
     return $this->SendResponse($getnewswithimage,'News with image shown successfully!!');
}
function countNewsViews($newsid){
    $getdata=News::where('id',$newsid)->first();
    $views=$getdata->view_count;
    $incrementview=$views+1;
    $updateviews=$getdata->update([
        'view_count'=>$incrementview
        ]);
    $getdata=News::where('id',$newsid)->first();
    return $this->SendResponse($getdata,'News views counted successfully!!');
} 
function showNewsByCategory($category){
  $getdata=News::with('images')->where('news_info.news_for',$category)->get();
  return $this->SendResponse($getdata,'News filtered successfully!!');
}
//_______________ Event Portion Starts here ________________
function showStaffEventWithImage($ownerid){
    $getnewswithimage=Event::with('images')->where('event_info.event_for','staff')
    ->orWhere('event_info.event_for','all')
    ->orderBy('event_info.id','desc')->get();
     return $this->SendResponse($getnewswithimage,'Events with images shown successfully!!');
}
function countEventViews($eventid){
    $getdata=Event::where('id',$eventid)->first();
    $views=$getdata->view_count;
    $incrementview=$views+1;
    $updateviews=$getdata->update([
        'view_count'=>$incrementview
        ]);
    $getdata=Event::where('id',$eventid)->first();
    return $this->SendResponse($getdata,'Event views counted successfully!!');
}
function showEventByCategory($category){
  $getdata=Event::with('images')->where('event_info.event_for',$category)->get();
  return $this->SendResponse($getdata,'Event filtered successfully!!');
}



}
