<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users\StaffInfo;
//use App\Models\sample;
use App\Models\sample;
use App\Models\MapTest;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Admin\adminInfo as admininfoResource;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\QueryException;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Crypt;
use App\Traits\EncryptData;
class sampleController extends BaseController
{
 use EncryptData;  
   public function mapstore(Request $req){   
       $model=new MapTest();
       $model->user_id=$req['user_id'];
       $model->latitude=$req['latitude'];
       $model->longitude=$req['longitude'];
       $model->save();
       return $this->SendResponse($model,'map data stored successfully!!!');
   }
   
   function getMapLocation($userid){
       $get=MapTest::where('user_id',$userid)->get();
       return $this->SendResponse($get,'map datas!!');
   }
   function getAllMapLocation(){
     $get=MapTest::get();
       return $this->SendResponse($get,'map datas!!');
       
   }
   
   
    public function createdb(Request $r){
          DB::statement('CREATE DATABASE sachema');
       /* 
         $schemaName = $this->argument('name') ?: config("database.connections.mysql.database");
        $charset = config("database.connections.mysql.charset",'utf8mb4');
        $collation = config("database.connections.mysql.collation",'utf8mb4_unicode_ci');

        config(["database.connections.mysql.database" => null]);

        $query = "CREATE DATABASE IF NOT EXISTS $schemaName CHARACTER SET $charset COLLATE $collation;";

        DB::statement($query);

        config(["database.connections.mysql.database" => $schemaName]); */
         return $this->sendResponse('a','db created!');
        
        $i=$r->all();
        //join between two different databases!
        $a=sample::where('id',$id)->join('digital3_catering.package','sample.id','=','package.package_id')->get();
      //$a=sample::all();
      //$a=sample::create($i);
      return $this->sendResponse($a,'db created!');
   /* $new_db=DB::statement('CREATE DATABASE `digital3_sample`');
    DB::statement("CREATE USER 'digital3_ananda'@'localhost' IDENTIFIED BY 'devkaal@10' ");
    DB::statement("GRANT ALL PRIVILEGES ON 'digital3_sample'.* TO 'digital3_ananda'@'localhost'");
       // DB::statement("GRANT ALL PRIVILEGES ON *.* TO 'digital3_ananda'@'localhost'");
    //$a=DB::getConnection()->statement('CREATE DATABASE :schema', array('schema' => 'mydatabasename'),);
    //DB::getConnection()->statement('CREATE USER :user@'localhost' IDENTIFIED BY :password array('user' => 'username', 'password' => 'h543j5kh43jk5'));
    //DB::statement('CREATE DATABASE myNewDB');
    if ($new_db) {

            $config = Config::get('database.connections.main');

            $config['database'] = env('DB_DATABASE', $dbName);
            $config['username'] = env('DB_USERNAME', 'aqee_lroot');
            $config['password'] = env('DB_PASSWORD', 'aqeelroot');

            Config::set('database.connections.main', $config); 
            Config::set('database.default', 'main'); 
            Artisan::call('migrate');
             return $this->sendResponse('done','db created!');
   
        }  */
      
    }
    public function del($id){
        $get=Product::where('id',$id)->first();
        return $this->sendResponse($get->delete(),'deleted');
    }
    
    function POL(Request $req){
       $a=new sample();
       $a->name=$req['name'];
       $a->file_path=$req['file_path'];
       $a->save();
    return $this->SendResponse($a,'a');
    }
    function getencrptdata(){
        $as=sample::get();
         return $this->SendResponse($as,'a');
         $a=[];
         foreach($as as $b){
             //$a[]=$b->name;
             $a[]=Crypt::decryptString($b->name);
         } 
       return $this->SendResponse($a,'a');
    }
public function Store(Request $request)
    {
        // Validate the inputs
        $request->validate([
            'name' => 'required',
        ]);

        // ensure the request has a file before we attempt anything else.
        if ($request->hasFile('file')) {

            $request->validate([
                'image' => 'mimes:jpeg,bmp,png' // Only allow .jpg, .bmp and .png file types.
            ]);
try{
            // Save the file locally in the storage/public/ folder under a new folder named /product
            $request->file->store('product', 'public');

            // Store the record, using the new file hashname which will be it's new filename identity.
            $product = new sample();
            $product->name=Crypt::encryptString($request['name']);
            $product->file_path=$request->file->hashName();
            $product->save(); // Finally, save the record.
            //also save to another db
            
        return $this->sendResponse($product, 'stored successfully!!');
    
}
        catch (DecryptException $e) {
    return $this->SendResponse($e);
}
        }
    }
    public function displayImage($filename)

{

  

    $path = public_path('product/' . $filename);

   

    if (!File::exists($path)) {

        abort(404);

    }

  

    $file = File::get($path);

    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);

return $this->sendResponse($response,'driver values are successfully updated!');

}

    public function index(){
      return view('welcome');
    }



}