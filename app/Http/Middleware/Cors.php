<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
  public function handle($request, Closure $next)
    {
         $allowedOrigins = ['http://testfront.digitalyatayat.com','https://www.digitalyatayat.com','http://localhost','http://localhost:8080','https://yatayatbackend.digitalyatayat.com'];
  //  $origin = $_SERVER['HTTP_ORIGIN'];
 // $allowedOrigins = ['example.com', 'example1.com', 'example2.com'];
    //$origin = $_SERVER['HTTP_ORIGIN'];

    if (in_array('HTTP_ORIGIN', $allowedOrigins)) {
        return $next($request)
            ->header('Access-Control-Allow-Origin', $origin)
            ->header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
            ->header('Access-Control-Allow-Headers', 'Content-Type');
    }

    return $next($request);
  
    /*    if ($request->isMethod('OPTIONS'))
        {
            return response()->json('{"method":"OPTIONS"}', 200, $headers);
        }

        $response = $next($request);
        $response->headers->set('Access-Control-Expose-Headers', 'Content-Disposition');
        $response->headers->set('Access-Control-Allow-Origin','http://testfront.digitalyatayat.com','https://www.digitalyatayat.com');
        $response->headers->set('Access-Control-Allow-Origin','http://localhost:4200');
        $response->headers->set('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE, OPTIONS');

        //return $response->withHeaders($headers);
        return $response; */
    }
}