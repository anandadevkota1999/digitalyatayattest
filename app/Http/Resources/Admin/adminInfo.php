<?php

namespace App\Http\Resources\Admin;

use Illuminate\Http\Resources\Json\JsonResource;

class adminInfo extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

return [
            'admin_id' => $this->admin_id,
            'admin_name' => $this->admin_name,
            'admin_phone' => $this->admin_phone,
            'admin_address' => $this->admin_address,
            'admin_email'=>$this->admin_email,
            'password' => $this->password,
            'admin_image' => $this->admin_image,
            'company_id' => $this->company_id,
            
        ];
    }
}
