<?php

namespace App\Http\Resources\BeforeLogin;

use Illuminate\Http\Resources\Json\JsonResource;

class companylist extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
 return [
            'company_id' => $this->company_id,
            'company_name' => $this->company_name,
            'company_address' => $this->company_address,
            'company_phone' => $this->company_phone,
      ];
    }
}
