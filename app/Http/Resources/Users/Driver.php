<?php

namespace App\Http\Resources\Users;

use Illuminate\Http\Resources\Json\JsonResource;

class Driver extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
 return [
            'owner_id' => $this->owner_id,
            'owner_name' => $this->owner_name,
            'owner_address' => $this->owner_address,
            'owner_phone' => $this->owner_phone,
            'owner_email' => $this->owner_email,
            'owner_pin' => $this->owner_pin,
            'owner_dob' => $this->owner_dob,
            'owner_gender' => $this->owner_gender,
            'company_id' => $this->company_id,
            'owner_citizenship_image' => $this->owner_citizenship_image,
            'device_token' => $this->device_token,

            
        ]; 
    }
}
