<?php

namespace App\Listeners;
use App\Events\NewsCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\Users\busOwner;
use App\Models\Users\Driver;
use App\Models\Users\StaffInfo;
use Carbon\Carbon;
use App\Http\Controllers\Api\baseController as BaseController;
use Illuminate\Http\Request;
use App\Models\Notification as Mynotification;
use App\Models\News;

class NotifyNewsCreated extends BaseController
{
    /**
     * Create the event listener.
     *
     * @return void
     */
  public $news; 
  public function __construct(News $news)
    {
        $this->news=$news;
       // $this->req = $req;
    }

    /**
     * Handle the event.
     *
     * @param  NewsCreated  $event
     * @return void
     */
    public function handle(NewsCreated $event)
    {
        //notify owner 
        if($event->news->news_for=='owner'){
         $ownerstoken = busOwner::where('active_status','yes')->where('device_token','!=','null')->pluck('device_token')->all();
        // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
        //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
        //for curl headers
            $headers = array();
            $headers = ['Authorization:key=' . GOOGLE_API_KEY,'Content-Type: application/json',];
        //notification contents:
        $title=$event->news->topic;
        $body=$event->news->text_content;
        $addedby=$event->news->added_by;
        $companyid=$event->news->company_id;
         $data = ["registration_ids" => $ownerstoken,"notification" => ["title" => $title,"body" =>$body]];
        $dataString = json_encode($data);
        $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
        // Close connection
        curl_close($ch);
  // FCM response
  //saving in notification table as well::
            $store=new Mynotification();
            $store->type=busOwner::class;
            $store->notifiable_id='bulknews';
            $store->notifiable_type=busOwner::class;
            $store->title=$title;
            $store->body=$body;
            $store->added_by=$addedby;
            $store->created_at=Carbon::now();
            $store->company_id=$companyid;
            $store->save();
          return $this->sendResponse($store,'NOtification successfully sent!!');

    }
       //notify drivers
        if($event->news->news_for=='driver'){
         $driverstoken = Driver::where('active_status','yes')->where('device_token','!=','null')->pluck('device_token')->all();
        // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
        //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
        //for curl headers
            $headers = array();
            $headers = ['Authorization:key=' . GOOGLE_API_KEY,'Content-Type: application/json',];
        //notification contents:
        $title=$event->news->topic;
        $body=$event->news->text_content;
        $addedby=$event->news->added_by;
        $companyid=$event->news->company_id;
         $data = ["registration_ids" => $driverstoken,"notification" => ["title" => $title,"body" =>$body]];
        $dataString = json_encode($data);
        $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
        // Close connection
        curl_close($ch);
  // FCM response
  //saving in notification table as well::
            $store=new Mynotification();
            $store->type=Driver::class;
            $store->notifiable_id='bulknews';
            $store->notifiable_type=Driver::class;
            $store->title=$title;
            $store->body=$body;
            $store->added_by=$addedby;
            $store->created_at=Carbon::now();
            $store->company_id=$companyid;
            $store->save();
          return $this->sendResponse($store,'NOtification successfully sent!!');

    }
       //notify staffs
        if($event->news->news_for=='staff'){
         $driverstoken = StaffInfo::where('device_token','!=','null')->pluck('device_token')->all();
        // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
        //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
        //for curl headers
            $headers = array();
            $headers = ['Authorization:key=' . GOOGLE_API_KEY,'Content-Type: application/json',];
        //notification contents:
        $title=$event->news->topic;
        $body=$event->news->text_content;
        $addedby=$event->news->added_by;
        $companyid=$event->news->company_id;
         $data = ["registration_ids" => $driverstoken,"notification" => ["title" => $title,"body" =>$body]];
        $dataString = json_encode($data);
        $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
        // Close connection
        curl_close($ch);
  // FCM response
  //saving in notification table as well::
            $store=new Mynotification();
            $store->type=StaffInfo::class;
            $store->notifiable_id='bulknews';
            $store->notifiable_type=StaffInfo::class;
            $store->title=$title;
            $store->body=$body;
            $store->added_by=$addedby;
            $store->created_at=Carbon::now();
            $store->company_id=$companyid;
            $store->save();
          return $this->sendResponse($store,'NOtification successfully sent!!');

    }
       //notify all
        if($event->news->news_for=='all'){
         $ownerstoken = busOwner::where('active_status','yes')->where('device_token','!=','null')->pluck('device_token')->all();
         $driverstoken = Driver::where('active_status','yes')->where('device_token','!=','null')->pluck('device_token')->all();
         $staffstoken = StaffInfo::where('device_token','!=','null')->pluck('device_token')->all();
         $alltoken=array_merge($ownerstoken,$driverstoken,$staffstoken);
        // ::defining server api key!!!
         define("GOOGLE_API_KEY", "AAAAoyz78jY:APA91bHtPOBlHHEEsedOkLbsILQ6wBUwj6P6AR3IIJOghhyxpm7EkSX5EuzEdIRHbZbS39PEY1hx6J5jOl7JDCPNymKN-7H5JJDBQfLEh6vIecxgxxPK3t91qrcjpM1ZD1dpuWthe3ql");
        //fcm url
         $url = 'https://fcm.googleapis.com/fcm/send';
        //for curl headers
            $headers = array();
            $headers = ['Authorization:key=' . GOOGLE_API_KEY,'Content-Type: application/json',];
        //notification contents:
        $title=$event->news->topic;
        $body=$event->news->text_content;
        $addedby=$event->news->added_by;
        $companyid=$event->news->company_id;
         $data = ["registration_ids" => $alltoken,"notification" => ["title" => $title,"body" =>$body]];
        $dataString = json_encode($data);
        $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }        
        // Close connection
        curl_close($ch);
  // FCM response
  //saving in notification table as well::
            $store=new Mynotification();
            $store->type='all';
            $store->notifiable_id='bulknews';
            $store->notifiable_type='all';
            $store->title=$title;
            $store->body=$body;
            $store->added_by=$addedby;
            $store->created_at=Carbon::now();
            $store->company_id=$companyid;
            $store->save();
    return $this->sendResponse($store,'NOtification successfully sent!!');
    }
    
    }
}