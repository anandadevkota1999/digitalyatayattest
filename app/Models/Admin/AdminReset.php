<?php

namespace App\Models\Admin;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class AdminReset extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
    protected $guarded = ['role'];
    protected $table = 'admin_password_reset';
   // protected $primaryKey = 'admin_id';
    protected $keyType = 'string';
   
    public $timestamps = false;
    protected $fillable = [
          'admin_email',
          'token',
         'created_at',
            
    ];

   
}