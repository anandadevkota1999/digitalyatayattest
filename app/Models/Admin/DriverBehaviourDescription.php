<?php

namespace App\Models\Admin;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class DriverBehaviourDescription extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
    protected $table = 'driver_behaviour_description';
   // protected $primaryKey = 'role_id';
    public $timestamps = false;
    protected $fillable = [
        'driver_id',
        'behaviour_description',
        'added_by',
        'company_id',
        'created_at',
        'updated_at',
        'behaviour_status',
        'behaviour_rate'
    ];

    
//protected $casts = ['created_at','updated_at'];


    
    
}