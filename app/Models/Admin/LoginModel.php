<?php

namespace App\Models\Admin;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class LoginModel extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
    protected $guarded = ['role'];
    protected $table = 'admin_info';
    protected $primaryKey = 'admin_id';
    protected $keyType = 'string';
    public $incrementing = false;

    public $timestamps = false;
    protected $fillable = [
        'admin_id',
        'admin_name',
        'admin_phone',
        'admin_address',
        'admin_email',
        'password',
        'admin_image',
        'company_id',
        'created_at',
        'updated_at',
        'admin_validity_period',
        'device_token',
        'admin_type',
        
    ];

    protected $hidden = [
        'password',
            ];
protected $casts = ['admin_id' => 'string' ,'created_at',
        'updated_at',];

}
