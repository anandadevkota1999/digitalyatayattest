<?php

namespace App\Models\Admin;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class Role extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
    protected $guarded = ['role'];
    protected $table = 'roles';
    protected $primaryKey = 'role_id';
 //   protected $keyType = 'string';
   //  public $incrementing = false;

    public $timestamps = false;
    protected $fillable = [
        'role_id',
        'role_type',
        'role_description',
        'company_id',
        'created_at',
        'updated_at',
    ];

    
protected $casts = ['created_at','updated_at'];

}
