<?php

namespace App\Models\Admin;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class Route extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
 //   protected $guarded = ['role'];
    protected $table = 'route_info';
  //  protected $primaryKey = 'role_id';
 //   protected $keyType = 'string';
   //  public $incrementing = false;

    public $timestamps = false;
    protected $fillable = [
        'route_from',
        'route_to',
        'route_name',
        'route_number',
        'route_type',
        'company_id',
        'added_by',
        'created_at',
        'updated_at',
    ];

    
protected $casts = ['created_at','updated_at'];

}