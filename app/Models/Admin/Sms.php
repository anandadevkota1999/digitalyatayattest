<?php

namespace App\Models\Admin;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class Sms extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
 //   protected $guarded = ['role'];
    protected $table = 'sms_info';
  //  protected $primaryKey = 'role_id';
 //   protected $keyType = 'string';
   //  public $incrementing = false;

    public $timestamps = false;
    protected $fillable = [
        'sms_to',
        'receiver_name',
        'sms_message',
        'created_at',
        'updated_at',
        'company_id',
        'added_by','sms_type','initial_balance','remaining_balance'
        
    ];

    
protected $casts = ['created_at','updated_at'];

}