<?php

namespace App\Models\Admin;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class StaffAttendance extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
    protected $guarded = ['role'];
    protected $table = 'staff_attendance';
    public $timestamps = false;
    public $incrementing = false;
    protected $fillable = [
          'id',
          'staff_id',
         'arrival_time',
         'departure_time',
         'attendance_status',
         'attendance_remarks',
         'company_id',
          ];

 
}