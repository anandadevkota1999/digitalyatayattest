<?php

namespace App\Models\Admin\Transactions;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class AccountPriceDeclaration extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
   // protected $guarded = ['role'];
    protected $table = 'acount_price_declaration';
    protected $primaryKey = 'id';
  //  protected $keyType = 'string';
   // public $incrementing = false;

    public $timestamps = false;
    protected $fillable = [
        'bus_no','payment_topic','payment_option','payment_price','payment_type','expiry_date','company_id','added_by','created_at','updated_at','fiscal_year'
];

    
}