<?php

namespace App\Models\Admin\Transactions;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class DurghatanaFund extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
   // protected $guarded = ['role'];
    protected $table = 'durghatana_fund';
   // protected $primaryKey = 'bill_id';
  //  protected $keyType = 'string';
   // public $incrementing = false;

    public $timestamps = false;
    protected $fillable = [
        'bill_no',
        'bill_payment_date',
        'member_name',
        'bus_no',
        'durghatana_jokhim_shulka',
        'membership_shulka',
        'nibedan_shulka',
        'anudaan_shulka',
        'sattadarta_shulka',
        'durghatana_nirichhyad_and_nyunikarad_shulka',
        'peski_farchyut',
        'anya_aamdani',
        'reserve_shulka',
        'jageda_kosh',
        'naya_membership_shulka',
        'durghatana_jokhim_shulka_remarks',
        'membership_shulka_remarks',
        'nibedan_shulka_remarks',
        'anudaan_shulka_remarks',
        'sattadarta_shulka_remarks',
        'durghatana_nirichhyad_and_nyunikarad_shulka_remarks',
        'peski_farchyut_remarks',
        'anya_aamdani_remarks',
        'reserve_shulka_remarks',
        'jageda_kosh_remarks',
        'naya_membership_shulka_remarks',
        'added_by',
        'company_id',
        'updated_by',
        'created_at',
        'updated_at',
        'total_amount',
        'payment_type',
        'fiscal_year'
        
    ];

   
protected $casts = ['bill_payment_date','created_at','updated_at'];

}