<?php
namespace App\Models\Admin\Transactions;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class DurghatanaKharcha extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
   // protected $guarded = ['role'];
    protected $table = 'durghatana_fund_kharcha';
   // protected $primaryKey = 'bill_id';
  //  protected $keyType = 'string';
   // public $incrementing = false;

    public $timestamps = false;
    protected $fillable = [
        'bill_no',
        'bill_payment_date',
        'kharcha_received_by',
        'kharcha_recipient_address',
        'peski_bibarad_kharcha',
        'masalanda_kharcha',
        'chhapai_kharcha',
        'khaja_kharcha',
        'aarthik_sahayog_kharcha',
        'durghatana_aarthik_sahayog_kharcha',
        'baithak_kharcha',
        'durghatana_nirixed_kharcha',
        'sanyojak_maasik_lekha_nirixed_kharcha',
        'staff_maasik_lekha_bhatta',
        'sanchaar_kharcha',
        'added_by',
        'company_id',
        'updated_by',
        'created_at',
        'updated_at',
        'total_amount',
        'payment_type',
        'fiscal_year',
        'cheque_no',
        'verified_by'
        
    ];

   
protected $casts = ['bill_payment_date','created_at','updated_at'];

}