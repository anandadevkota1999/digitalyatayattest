<?php

namespace App\Models\Admin\Transactions;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class FiscalYear extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
   // protected $guarded = ['role'];
    protected $table = 'fiscal_year';
    protected $primaryKey = 'id';
  //  protected $keyType = 'string';
   // public $incrementing = false;

    public $timestamps = false;
    protected $fillable = [
        'fiscal_year','fiscal_year_start_date','fiscal_year_end_date'
];

    
}