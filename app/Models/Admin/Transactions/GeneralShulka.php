<?php

namespace App\Models\Admin\Transactions;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class GeneralShulka extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
   // protected $guarded = ['role'];
    protected $table = 'general_shulka';
    protected $primaryKey = 'bill_id';
  //  protected $keyType = 'string';
   // public $incrementing = false;

    public $timestamps = false;
    protected $fillable = [
        'bill_no',
        'bill_payment_date',
        'bus_no',
        'member_name',
        'maasik_shulka',
        'kharid_bikri_darta_sulka',
        'nibedan_shulka',
        'membership_nabikarad_shulka',
        'naya_membership_shulka',
        'naya_bus_register_shulka',
        'jariwana_shulka',
        'queue_shatta_patta_shulka',
        'identitycard_shulka',
        'sifaarish_shulka',
        'vawan_build_fund',
        'staff_upadaan_fund',
        'share_saving_shulka',
        'sticker_shulka',
        'other_shulka',
        'photocopy_fee',
        'peski_farchyot_shulka',
        'shatta_darta_shulka',
        'maasik_shulka_remarks',
        'kharid_bikri_darta_sulka_remarks',
        'nibedan_shulka_remarks',
        'membership_nabikarad_shulka_remarks',
        'naya_membership_shulka_remarks',
        'naya_bus_register_shulka_remarks',
        'jariwana_shulka_remarks',
        'queue_shatta_patta_shulka_remarks',
        'identitycard_shulka_remarks',
        'sifaarish_shulka_remarks',
        'vawan_build_fund_remarks',
        'staff_upadaan_fund_remarks',
        'share_saving_shulka_remarks',
        'sticker_shulka_remarks',
        'other_shulka_remarks',
        'photocopy_fee_remarks',
        'peski_farchyot_shulka_remarks',
        'shatta_darta_shulka_remarks',
        'added_by',
        'company_id',
        'updated_by',
        'created_at',
        'updated_at',
        'total_amount',
        'payment_type',
        'fiscal_year'
        
    ];

   
protected $casts = ['bill_payment_date'];

}