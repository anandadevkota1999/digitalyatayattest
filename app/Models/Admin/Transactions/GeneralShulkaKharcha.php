<?php

namespace App\Models\Admin\Transactions;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class GeneralShulkaKharcha extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
   // protected $guarded = ['role'];
    protected $table = 'general_shulka_kharcha';
  //  protected $primaryKey = 'bill_id';
  //  protected $keyType = 'string';
   // public $incrementing = false;

    public $timestamps = false;
    protected $fillable = [
        'bill_no',
        'bill_payment_date',
        'kharcha_received_by',
        'kharcha_recipient_address',
        'masalanda_kharcha',
        'chhapai_kharcha',
        'bidhyut_kharcha',
        'telephonebill_kharcha',
        'dailytour_bhatta_kharcha',
        'marmatsambhaar_kharcha',
        'khaja_office_kharcha',
        'talab_kharcha',
        'bahiri_khaja_kharcha',
        'staff_maasik_bhatta_kharcha',
        'kosadaxya_maasik_bhatta_kharcha',
        'staff_sanchayakosh_kharcha',
        'housefare_kharcha',
        'baithak_bhatta_kharcha',
        'aarthiksahayog_kharcha',
        'bibidh_kharcha',
        'sanchaar_bhatta',
        'bank_nagad_jamma',
        'indhan_kharcha',
        'added_by',
        'company_id',
        'updated_by',
        'created_at',
        'updated_at',
        'total_amount',
        'payment_type',
        'fiscal_year',
        'cheque_no',
        'verified_by',
        
    ];

   
protected $casts = ['bill_payment_date','created_at','updated_at'];

}