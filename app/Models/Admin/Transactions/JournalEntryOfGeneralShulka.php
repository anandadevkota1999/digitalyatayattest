<?php

namespace App\Models\Admin\Transactions;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class JournalEntryOfGeneralShulka extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
   // protected $guarded = ['role'];
    protected $table = 'journalentry_general_shulka';
    protected $primaryKey = 'id';
  //  protected $keyType = 'string';
   // public $incrementing = false;

    public $timestamps = false;
    protected $fillable = [
        'date',
        'bill_no',
        'bus_no',
        'debit_particular',
        'credit_particular',
        'debit_amount',
        'credit_amount',
        'total_amount',
        'added_by',
        'company_id',
        'created_at',
        'updated_at',
        'fiscal_year',
        ];

protected $casts = ['created_at','updated_at'
];
    
    
    
}