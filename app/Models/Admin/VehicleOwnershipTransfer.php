<?php

namespace App\Models\Admin;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class VehicleOwnershipTransfer extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
 //   protected $guarded = ['role'];
    protected $table = 'vehicle_ownership_transfer';
  //  protected $primaryKey = 'role_id';
 //   protected $keyType = 'string';
   //  public $incrementing = false;

    public $timestamps = false;
    protected $fillable = [
        'old_owner_id',
        'new_owner_id',
        'bus_id',
        'company_id',
        'trasfer_date',
        'added_by',
        'created_at',
        'updated_at',
    ];

    
protected $casts = ['created_at','updated_at'];

}