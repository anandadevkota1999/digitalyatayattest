<?php

namespace App\Models\BeforeLogin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DummyCompanyList extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $table = 'dummy_company_info';
    // protected $primaryKey = 'company_id';
     //protected $keyType = 'string';
     //public $incrementing = false;
     
     protected $fillable = [
        'company_id', 'company_name', 'company_address', 'company_phone','company_email','company_image','arrival_time','departure_time','created_at',
        ];

}
