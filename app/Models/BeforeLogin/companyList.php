<?php

namespace App\Models\BeforeLogin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class companyList extends Model
{
    use HasFactory;
    public $timestamps = false;
     protected $table = 'company_info';
     protected $primaryKey = 'company_id';
     protected $keyType = 'string';
     public $incrementing = false;
     
     protected $fillable = [
        'company_id', 'company_name', 'company_address', 'company_phone','company_email','company_image','company_pan_no','compny_english_establish_date','company_nepali_establish_date','company_nepali_address','company_nepali_phone_no','company_nepali_name','company_pan_image','arrival_time','departure_time','created_at','updated_at'
    ];

}
