<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;
use Illuminate\Auth\Authenticatable;
use App\Events\NewsCreated;
//use App\Models\News;
use App\Models\NewsImage;


class News extends Model implements AuthContract
{
    use  HasFactory, Notifiable, HasApiTokens, Authenticatable;

    protected $table = 'news_info';
    public $timestamps = false;
    protected $guarded = ['role'];
  

 protected $fillable = ['topic','text_content','priority','news_for','added_by','updated_by','created_at','updated_at','company_id','view_count'];
 protected $dispatchesEvents=[
     "NewsCreated"=>'NewsCreated'
     ];
    public function images()
    {
        return $this->hasMany(NewsImage::class,'news_id');
    } 
}