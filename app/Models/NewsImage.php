<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;
use Illuminate\Auth\Authenticatable;
use App\Models\News;

class NewsImage extends Model implements AuthContract
{
    use  HasFactory, Notifiable, HasApiTokens, Authenticatable;

    protected $table = 'news_image';
    public $timestamps = false;
    protected $guarded = ['role'];

 protected $fillable = ['news_id','image','added_by','created_at','updated_at','company_id'];
public function news()
    {
        return $this->belongsTo(News::class,'id','news_id');
    }
}