<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;
use Illuminate\Auth\Authenticatable;

class Product extends Model implements AuthContract
{
    use  HasFactory, Notifiable, HasApiTokens, Authenticatable;

    protected $table = 'product';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $guarded = ['role'];
    protected $keyType = 'string';
    public $incrementing = false;

 protected $fillable = ['id','file_path'];

}