<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Auth\Authenticatable;
//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;
class Conductor extends Model implements AuthContract
{
    use HasFactory, Notifiable, HasApiTokens, Authenticatable;


 protected $table = 'conductor_info';
    protected $primaryKey = 'conductor_id';
    public $timestamps = false;
    protected $guarded = ['role'];
    protected $keyType = 'string';
    public $incrementing = false; 
    protected $fillable = [
        'conductor_id',
        'conductor_name',
        'conductor_phone',
        'conductor_address',
        'conductor_pin',
        'conductor_dob',
        'conductor_gender',
        'conductor_citizenship_no',
        'conductor_citizenship_image',
        'conductor_image',
        'conductor_bus_no',
        'company_id',
        'created_at',
        'updated_at',
        'added_by',
        'updated_by',
        'active_status',
    ];

 protected $hidden = [
        'condctor_pin',
      //  'device_token',
    ];

}