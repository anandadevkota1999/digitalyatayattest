<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Auth\Authenticatable;
//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;
class Driver extends Model implements AuthContract
{
    use HasFactory, Notifiable, HasApiTokens, Authenticatable;


 protected $table = 'driver_info';
    protected $primaryKey = 'driver_id';
    public $timestamps = false;
    protected $guarded = ['role'];
    protected $keyType = 'string';
    public $incrementing = false; 
    protected $fillable = [
        'driver_id',
        'bus_no',
        'driver_name',
        'driver_phone',
        'driver_address',
        'driver_pin',
        'driver_dob',
        'driver_gender',
        'driver_licence_no',
        'driver_licence_category',
        'driver_licence_issuedate',
        'driver_licence_expirydate',
        'driver_image',
        'driver_citizenship_image',
        'driver_license_image',
        'company_id',
        'device_token',
        'device_name',
        'created_at',
        'updated_at',
        'added_by',
        'updated_by',
        'active_status',
        'driver_identity_no',
        'driver_identity_issuedate',
        'driver_identity_enddate',
        'driver_identity_image',

    ];

 protected $hidden = [
    //    'driver_pin',
      //  'device_token',
    ];

}
