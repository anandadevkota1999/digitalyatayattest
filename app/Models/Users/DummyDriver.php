<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Auth\Authenticatable;
//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;
class DummyDriver extends Model implements AuthContract
{
    use HasFactory, Notifiable, HasApiTokens, Authenticatable;


 protected $table = 'dummy_driver_info';
    protected $primaryKey = 'driver_id';
    public $timestamps = false;
    protected $guarded = ['role'];
    protected $keyType = 'string';
    public $incrementing = false; 
    protected $fillable = [
        'driver_id',
        'bus_no',
        'driver_name',
        'driver_phone',
        'driver_address',
        'driver_pin',
        'driver_dob',
        'driver_gender',
        'driver_image',
        'driver_citizenship_image',
        'driver_license_image',
        'company_id',
        'device_token',
        'device_name',

    ];

 protected $hidden = [
        'driver_pin',
        'device_token',
    ];

}
