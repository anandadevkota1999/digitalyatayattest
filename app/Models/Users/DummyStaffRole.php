<?php

namespace App\Models\Users;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;


class DummyStaffRole extends Model implements AuthContract {
    use   HasFactory, Notifiable, HasApiTokens, Authenticatable;
 
    protected $guarded = ['role'];
    protected $table = 'dummy_staff_roles';
    protected $primaryKey = 'dummy_staffrole_id';
 //   protected $keyType = 'string';
 ////    public $incrementing = false;

  //  public $timestamps = false;
    protected $fillable = [
       'staff_role_id',
        'staff_id',
        'role_id',
        'company_id',
        'created_at',
        'updated_at',
        're_asigned_by',
    ];

    
protected $casts = ['created_at','updated_at'];

}