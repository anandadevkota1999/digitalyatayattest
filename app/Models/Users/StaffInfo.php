<?php

namespace App\Models\Users;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;
class StaffInfo extends Model implements AuthContract
{
    use  HasFactory, Notifiable, HasApiTokens, Authenticatable;

    protected $table = 'staff_info';
    protected $primaryKey = 'staff_id';
    public $timestamps = false;
    protected $guarded = ['role'];
    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        'staff_id',
        'staff_name',
        'staff_address',
        'staff_phone',
        'staff_pin',
        'staff_dob',
        'staff_gender',
        'staff_post',
        'staff_image',
        'staff_citizenship_image',
        'staff_salary',
        'company_id',
        'device_token',
        'device_name',
        'created_at',
        'updated_at',
        'added_by',
        'updated_by',
        

    ];

 protected $hidden = [
        'staff_pin',
        //'device_token',
    ];

  
public function getAuthPassword()
{
    return $this->staff_pin;
} 



}
