<?php

namespace App\Models\Users;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;
class busOwner extends Model implements AuthContract
{
    use  HasFactory, Notifiable, HasApiTokens, Authenticatable;

    protected $table = 'bus_owner';
    protected $primaryKey = 'owner_id';
   // public $timestamps = false;
    protected $guarded = ['role'];
    protected $keyType = 'string';
    public $incrementing = false;

    protected $fillable = [
        'owner_id',
        'owner_name',
        'owner_address',
        'owner_phone',
        'owner_pin',
        'owner_dob',
        'owner_gender',
        'company_id',
        'owner_citizenship_image',
        'owner_image',
        'device_token',
        'device_name',
        'created_at',
        'updated_at',
        'added_by',
        'updated_by',

    ];

 protected $hidden = [
        'owner_pin',
      //  'device_token',
    ];

  
public function getAuthPassword()
{
    return $this->owner_pin;
} 



}
