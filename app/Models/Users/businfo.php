<?php

namespace App\Models\Users;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\Authenticatable as AuthContract;
use Illuminate\Auth\Authenticatable;

class businfo extends Model implements AuthContract
{
    use  HasFactory, Notifiable, HasApiTokens, Authenticatable;

    protected $table = 'bus_info';
    protected $primaryKey = 'bus_id';
   // public $timestamps = false;
    protected $guarded = ['role'];
    protected $keyType = 'string';
    public $incrementing = false;


 protected $fillable = [
        'bus_id',
        'bus_no',
        'bus_type',
        'owner_phone',
        'bus_billbook_no',
        'bus_billbook_enddate',
        'bus_routepermit_enddate',
        'bus_checkpass_enddate',
        'bus_insurance_enddate',
        'bus_billbook_image',
        'bus_routepermit_image',
        'bus_checkpass_image',
        'bus_insurance_image',
        'company_id',
        'created_at',
        'updated_at',
        'added_by',
        'updated_by',
        'active_status'
    ];


}
