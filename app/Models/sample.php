<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\EncryptData;
class sample extends Authenticatable
{
    use HasFactory, Notifiable, EncryptData;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $connection = 'mysql2';
     protected $table = 'product';
     public $timestamps=false;
     protected $fillable = [
        'id',
        'name',
        'filepath',
      
    ];
     protected $encryptable = ['name',];
    
}