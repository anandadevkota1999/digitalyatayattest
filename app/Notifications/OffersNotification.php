<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OffersNotification extends Notification
{
    use Queueable;
    private $offerData;
      private  $driverData;
    private $drivername; 
    private $company_id;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($offerData,$driverData,$company_id)
    {
        $this->offerData = $offerData;
        $this->driverData = $driverData;
        $this->company_id = $company_id;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail','database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        /* return (new MailMessage)            
            ->name($this->offerData['name'])
            ->line($this->offerData['body'])
            ->action($this->offerData['offerText'], $this->offerData['offerUrl'])
            ->line($this->offerData['thanks']); */
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'khae'=>$this->offerData['bhat'],
            'names' => $this->offerData['name'],
             'staff_id' => $this->offerData['staff_id'],
             'driverTitle'=>$this->driverData['title'],
             'explain'=>$this->driverData['body'],
             'getname'=>$this->driverData['only'],
             'phone'=>$this->driverData['phone'],
             'company'=>$this->company_id,
        ];
    }
}
