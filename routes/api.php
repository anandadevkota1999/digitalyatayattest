<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\BeforeLoginController;
use App\Http\Controllers\Api\Admin\AdminLoginController;
//use App\Http\Controllers\Api\BeforeLoginController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//*********general Apis goes here for general purpose::::
//******************before login api routes************************

Route::get('companylist', [beforeLoginController::class, 'retriever']);
Route::post('companylist', [beforeLoginController::class, 'store']);
Route::get('companylist/{id}', [beforeLoginController::class, 'show']);
Route::put('companylist/{id}', [beforeLoginController::class, 'update']);
Route::delete('companylist/{id}', [beforeLoginController::class, 'delete']);

Route::post('admin/login',[AdminLoginController::class, 'adminLogin'])->name('adminLogin');
Route::post('admin/register',[AdminLoginController::class, 'adminRegister'])->name('adminRegister');



Route::middleware('auth:api')->get('/user', function (Request $request) {
   return $request->user();
});
