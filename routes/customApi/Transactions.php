<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Admin\Transactions\GeneralShulkaController;
use App\Http\Controllers\Api\Admin\Transactions\DurghatanaFundController;
use App\Http\Controllers\Api\Admin\Transactions\VehicleAccountController;
use App\Http\Controllers\Api\Users\Transactions\GeneralShulkaControllerAndroid;
use App\Http\Controllers\Api\Users\Transactions\DurghatanaFundControllerAndroid;
use App\Http\Controllers\Api\Admin\AdminLoginController;
use App\Http\Controllers\Api\BeforeLoginController;
use App\Http\Controllers\Api\Admin\AdminController;
use App\Http\Controllers\Api\Admin\staffAttendanceController;
use App\Http\Controllers\Api\Admin\AllNotificationController;
use App\Http\Controllers\Api\Users\staffController;
use App\Http\Controllers\Api\Users\busInfoController;
use App\Http\Controllers\Api\Users\driverController;
use App\Http\Controllers\Api\Users\busOwnerController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//       All the Routes that goes through the Admin Login(Route Group)
//             !!!!!!!!!!!!!! Here We GO !!!!!!!!!!!!
// ___________________________________________________________________________

// Samples.....
  
   Route::delete('delete/generalshulka/{bus_no}',[GeneralShulkaController::class, 'deleteGShulka']);
   Route::put('update/generalshulka/{bus_no}',[GeneralShulkaController::class, 'UpdateGShulka']);
   
// end of sample testing   
//----------------------------for admin group routes----------------------
Route::group( ['prefix' => 'admin','middleware' => ['auth:admin-api'] ],function(){
    //get bus and owner detail
 Route::get('getbusandownerdetail/bybusno/{busno}',[GeneralShulkaController::class, 'getBusandOwnerdetailbyBusno']);
    // ***************general shulka portion ******************
    //set billno by admin for general shulka
    Route::get('makebill/{company}',[GeneralShulkaController::class, 'makeBillno']);
 //pay general shulka by admin and notify user
  Route::post('create/generalshulka',[GeneralShulkaController::class, 'storeGeneralShulkaAndJournal']);   
//show general payment history of a company
 Route::get('showpaymenthistory/ofcompany/{companyid}',[GeneralShulkaController::class, 'showPaymentHistory']);
//show total general shulka due amount of a vehicle
Route::get('showduegeneralshulka/ofbus/{busno}',[GeneralShulkaController::class, 'showGeneralShulkaDuePaymentofBus']);
  // ***************general shulka Kharcha portion ******************
  
  //set billno by admin for general shulka kharcha
    Route::get('makebillofkharcha/{company}',[GeneralShulkaController::class, 'makeBillGeneralShulkaKharcha']);
//store general kharcha in dummy at first to insure verification process
  Route::post('create/dummygeneralkharcha',[GeneralShulkaController::class, 'DummyStoreGeneralKharcha']);
//show general kharcha request mady by admin
Route::get('showgeneralkharcharequest/{companyid}',[GeneralShulkaController::class, 'showDummyGShulka']);

 //pay general shulka by admin and notify user
  Route::post('create/generalshulkakharcha',[GeneralShulkaController::class, 'storeGeneralShulkaKharchaAndJournal']);   
//show general shulka Kharcha payment history of a company
 Route::get('showgeneralkharchapaymenthistory/ofcompany/{companyid}',[GeneralShulkaController::class, 'showGeneralshulkaKharchaPaymentHistory']);
//delete general kharcha request made by admin
 Route::delete('deletegeneralkharcharequest/{id}',[GeneralShulkaController::class, 'deleteDummyGShulka']);
 //show today's general kharcha of a company
  Route::get('showgeneralkharchahistory/oftoday/{companyid}',[GeneralShulkaController::class, 'getGKharchaToday']);

  // ***************Journal of General shulka portion ******************
 //show journal history of Durghatana (shulka and kharcha) of current fiscal year of a company
 Route::get('journalhistoryofdurghatanafund/{companyid}',[DurghatanaFundController::class, 'journalHistoryofDurghatanafund']);

 //show journal history of general (shulka and kharcha) of current fiscal year of a company
 Route::get('journalhistoryofgeneralshulka/{companyid}',[GeneralShulkaController::class, 'journalHistoryofGeneralshulka']);
 
 
  // ***************Durghatana fund portion ******************
 //set billno by admin for general shulka
    Route::get('makebilldurghatana/{company}',[DurghatanaFundController::class, 'makeBillnoDurghatana']);
//pay durghatana fund by admin and notify user
  Route::post('create/durghatanafund',[DurghatanaFundController::class, 'storeDurghatanaFundAndJournal']);   
//show durghatana fund payment history of a company
 Route::get('showdurghatanapaymenthistory/ofcompany/{companyid}',[DurghatanaFundController::class, 'showDurghatanaFundPaymentHistory']);
//show total durghatana shulka due amount of a vehicle
Route::get('showduedurghatanashulka/ofbus/{busno}',[DurghatanaFundController::class, 'showDurghatanaShulkaDuePaymentofBus']);

 // ***************Durghatana Kharcha portion ******************
 //set billno by admin for general kharcha
    Route::get('makebilldurghatanakharcha/{company}',[DurghatanaFundController::class, 'makeBillnoDurghatanaKharcha']);
//store Durghatana kharcha in dummy at first to insure verification process
  Route::post('create/dummydurghatanakharcha',[DurghatanaFundController::class, 'DummyStoreDurghatanaKharcha']);
//show Durghatana kharcha request mady by admin:
Route::get('showdurghatanakharcharequest/{companyid}',[DurghatanaFundController::class, 'showDummyDShulka']);  
//pay durghatana kharcha by admin:prepare journal in the process
  Route::post('create/durghatanakharcha',[DurghatanaFundController::class, 'storeDurghatanaKharchaAndJournal']);   
//show durghatana kharcha payment history of a company
 Route::get('showdurghatanapaymenthistory/ofcompany/{companyid}',[DurghatanaFundController::class, 'showDurghatanaKharchaPaymentHistory']);
//delete durghatana kharcha request made by admin
 Route::delete('deletedurghatanakharcharequest/{id}',[DurghatanaFundController::class, 'deleteDummyDShulka']);
//show today's durghatana kharcha of a company
  Route::get('showdurghatanakharchahistory/oftoday/{companyid}',[DurghatanaFundController::class, 'getDKharchaToday']);

//*************** Vehicle account section****************************
  //set vehicle pricing first for further entries
 Route::post('setvehiclepricing',[VehicleAccountController::class, 'SetVehiclePricing']);
 //update fixed account's expired payment date on monthly basis
 Route::put('updateexpiredfixedaccount',[VehicleAccountController::class, 'updateFixedaccountperiodically']);
//add payment price monthly in the topics having fixed price
 Route::post('vehicleaccount/addfixedpricemonthly',[VehicleAccountController::class, 'addFixedAmountOnSpecificTopicPayment']);
});




//--------------------------Android Users:

//...............routegroup for bus owners......
Route::group( ['prefix' => 'busowner','middleware' => ['auth:busOwner-api'] ],function(){
    //bill payment by bus owner
Route::post('paybill',[GeneralShulkaControllerAndroid::class, 'payBillandStore']);    
    //general payment hisotry of a bus owner
Route::get('paymenthistory/{ownerid}',[GeneralShulkaControllerAndroid::class, 'billPaymentHistory']);    
    //Durghatana Fund bill payment by bus owner
Route::post('paydurghatanabill',[DurghatanaFundControllerAndroid::class, 'payDurghatanaFundandStore']);    
   //durghatana fund payment hisotry of a bus owner
Route::get('durghatanapaymenthistory/{ownerid}',[DurghatanaFundControllerAndroid::class, 'durghatanaFundBillPaymentHistory']);    
//show total due of busowner of general shulka of indivisual payment topics
Route::get('showvehicleduegeneralshulka/ofowner/{ownerphone}',[GeneralShulkaControllerAndroid::class, 'showGeneralShulkaDueOwner']); 
//show total due of busowner of durghatana shulka of indivisual payment topics
Route::get('showvehicleduedurghatanashulka/ofowner/{ownerphone}',[DurghatanaFundControllerAndroid::class, 'showDurghatanaShulkaDueOwner']); 

});

//<<<<<<<...............routegroup for bus drivers......>>>>
Route::group( ['prefix' => 'driver','middleware' => ['auth:busDriver-api'] ],function(){
    
    
});

//<<<<<<<...............routegroup for Staffs......>>>>
Route::group( ['prefix' => 'staff','middleware' => ['auth:staff-api'] ],function(){
//show general kharcha requset mady by admin
Route::get('showgeneralkharcharequest/{companyid}',[GeneralShulkaControllerAndroid::class, 'showDummyGShulka']);
//verify the indivusal general kharcha request then update to admin
Route::get('verify/generalkharcharequest/{id}',[GeneralShulkaControllerAndroid::class, 'VerifyGeneralKharchaPaymentRequest']);
//show verified general kharcha of today
Route::get('showverifiedgeneralkharchatoday/{staffid}',[GeneralShulkaControllerAndroid::class, 'showVerifiedGShulkaToday']);


//----------durghatana Parts now----:
//verify the indivusal durghatana kharcha request then update to admin
Route::get('verify/durghatanakharcharequest/{id}',[DurghatanaFundControllerAndroid::class, 'VerifyDurghatanaKharchaPaymentRequest']);
//show durghatana kharcha request mady by admin
Route::get('showdurghatanakharcharequest/{companyid}',[DurghatanaFundControllerAndroid::class, 'showDummyDShulka']);
//show verified durghatana kharcha of today
Route::get('showverifieddurghatanakharchatoday/{staffid}',[DurghatanaFundControllerAndroid::class, 'showVerifiedDShulkaToday']);


});

