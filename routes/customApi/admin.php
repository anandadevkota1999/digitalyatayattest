<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Admin\AdminLoginController;
use App\Http\Controllers\Api\BeforeLoginController;
use App\Http\Controllers\Api\Admin\AdminController;
use App\Http\Controllers\Api\Admin\smsController;
use App\Http\Controllers\Api\Admin\VehicleRouteController;
use App\Http\Controllers\Api\Admin\staffAttendanceController;
use App\Http\Controllers\Api\Admin\AllNotificationController;
use App\Http\Controllers\Api\Admin\VehicleOwnershipTransferController;
use App\Http\Controllers\Api\Admin\DriverBehaviourController;
use App\Http\Controllers\Api\Admin\ConductorBehaviourController;
use App\Http\Controllers\Api\Users\staffController;
use App\Http\Controllers\Api\Users\busInfoController;
use App\Http\Controllers\Api\Users\busOwnerController;
use App\Http\Controllers\Api\Users\driverController;
use App\Http\Controllers\Api\Users\ConductorController;
use App\Http\Controllers\Api\Admin\NewsController;
use App\Http\Controllers\Api\Admin\EventController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//*********general Apis goes here for general purpose::::
//******************before login api routes************************

Route::get('companylist', [beforeLoginController::class, 'retriever']);
Route::post('companylist', [beforeLoginController::class, 'store']);
Route::get('companylist/{id}', [beforeLoginController::class, 'show']);
Route::put('companylist/{id}', [beforeLoginController::class, 'updateCompany']);
Route::delete('companylist/{id}', [beforeLoginController::class, 'deleteDemoCompany']);


//******************after login api routes************************

 //for admin login
Route::post('admin/login',[AdminLoginController::class, 'adminLogin'])->name('adminLogin');
//for admin register
Route::post('admin/register',[AdminLoginController::class, 'adminRegister'])->name('adminRegister');
//for changing password
Route::post('admin/changepassword/{id}',[AdminLoginController::class, 'changePassword']);
 // sending token  mail for forget password
   Route::post('admin/otplink',[AdminLoginController::class, 'mailForgetPassword']);
    Route::post('/update/otplink',[AdminLoginController::class, 'updateForgetPassword']);
  //changing password via mail token 
  Route::resource('updateForgetPassword', 'AdminLoginController');
  //  Route::Resource('/',[AdminLoginController::class, 'updateForgetPassword']);
Route::get('chitwanyatayat/images/{filename}',[AdminController::class, 'displayImage']);
  /*  Route::get('storage/app/pulic/image/{filename}', function ($filename) {
        return Image::make(storage_path() . '/storage/app/image/' . $filename)->response();
    });
}); */

//       All the Routes that goes through the Admin Login(Route Group)
//             !!!!!!!!!!!!!! Here We GO !!!!!!!!!!!!
// ___________________________________________________________________________

Route::group( ['prefix' => 'admin','middleware' => ['auth:admin-api'] ],function(){
    
   // ________________authenticated admin routes here 
   
   //get total admin information
   Route::get('authorizedadmins',[AdminLoginController::class, 'index']);
   //count total admins
   Route::get('countadmin',[AdminController::class, 'countAdmins']);
    //count total admins of specific company
   Route::get('count/specificadmin/{id}',[AdminController::class, 'countSpecificAdmins']);
 //list total admins of specific company
   Route::get('list/specificadmin/{id}',[AdminController::class, 'listSpecificAdmins']);
   
   //view admin information (specific)
   Route::get('show/{id}',[AdminLoginController::class, 'show']);
  
   //for getting token
   Route::get('/reset-password/{token}', 'AdminLoginController@getforgetPassword');
   //for updating password
 Route::post('/reset-password', 'ResetPasswordController@updateForgetPassword');
  //for updating admin records
   Route::put('update/admininfo/{id}', [AdminController::class, 'updateAdmin']);


   //___________end of admin routes
   //----------------------------------------------------
   
    //________________for Bus INFO
    //register bus info
    Route::post('bus/register',[AdminController::class, 'busStore']);
    //show bus images
    
  //  Route::get('bus/images/{filename}',[AdminController::class, 'displayImage']);
    Route::get('bus/display/images/{id}',[AdminController::class, 'getSpecificImages']);
    //count total bus of specific company
    Route::get('count/specific/bus/bycompany/{id}',[AdminController::class, 'countBusSpecificCompany']);
    //get bus infos of specific company
  Route::get('bus/filterby/company/{id}',[AdminController::class, 'listBusSpecificCompany']);
  //show specific bus details
   Route::get('businfo/{id}',[AdminController::class, 'showBusDetail']); 
//show specific bus details of specific company
   Route::get('businfo/filterbydate/{id}',[AdminController::class, 'showBusDetailByLatestDate']); 

    //update bus info records
   Route::put('update/businfo/{id}', [AdminController::class, 'updateBusInfo']);
 //delete  bus details
  // Route::get('delete/businfo/{id}',[AdminController::class, 'deleteBusInfo']); 
//delete  bus details
   Route::get('delete/busanddriver/{id}',[busInfoController::class, 'deleteBusinfoAswellasDriver']);
//for bus Info _____________________
Route::get('owner/hasbus/{ownerphone}',[busInfoController::class, 'showBusOfOwnerForAdmin']);


    //___________end of bus INFO
    
    //----------------------------------------------------
   // _________________for Bus Owner
   
   //bus owner list
    Route::get('ownerlist',[AdminController::class, 'listOwner']);
    //bus owners of specific company
    Route::get('owner/filterby/company/{id}',[AdminController::class, 'listOwnerSpecificCompany']);
    //owner detail of specific company
    Route::get('ownerinfo/{id}',[busOwnerController::class, 'showOwnerDetail']);
    //count bus owner of specific company
    Route::get('count/specific/busowner/{id}',[AdminController::class, 'countownerSpecificCompany']);
    //search owners by latest date
    Route::get('search/owners/{id}',[AdminController::class, 'searchBusownerbyLatestDate']);
     //search owners by specific date
   Route::get('search/owners/specific',[AdminController::class, 'filterbySpecificDate']);
     //count owners by specific date of speciifc company
   Route::get('count/owners/specific/{id}',[AdminController::class, 'CountOwnerBySpecificCompany']);
   //update bus owner records
   Route::put('update/busowner/{id}', [busOwnerController::class, 'updateBusOwner']);
    //delete bus owner records
   Route::get('delete/busowner/{id}', [AdminController::class, 'deleteBusOwner']);
   //reset device
    Route::post('reset/ownerdevice/{id}',[AdminController::class, 'resetOwnerByAdmin']);
  //show owner details by phone
  Route::get('showowner/{phone}', [AdminController::class, 'showOwner']);
  //send sms to indivisual person
   Route::post('indivisualsms/{phone}',[smsController::class, 'smsTosingleVehicleOwner']);
  //change vehicle ownership(shift owner from an owner to another)
  Route::post('changeownership',[VehicleOwnershipTransferController::class, 'shiftOwnership']);
   //get vehicle ownership transfer history
  Route::get('ownershiptrasferhistory/{companyid}', [VehicleOwnershipTransferController::class, 'showOwnershipRecords']);
  //update vehicle ownership only if mistaken
  Route::put('updateownership/{busid}', [VehicleOwnershipTransferController::class, 'updateOwnershipIfMistaken']);
  //____________end of bus owner 
  
  //----------------------------------------------------
  
 //_________________for Bus Driver
  
    //register driver by system admin
  Route::post('driverregister',[driverController::class, 'DriverRegisterByAdmin']);
  //count total drivers of specific company
   Route::get('count/specific/driver/{id}',[AdminController::class, 'countDriverSpecificCompany']);
   //get drivers info of specific company 
  Route::get('driver/filterby/company/{id}',[AdminController::class, 'listDriverSpecificCompany']);
  //get details of specific driver
   Route::get('driverinfo/{id}',[AdminController::class, 'showDriverDetail']);
  //count total drivers of all company
  Route::get('countdriver',[AdminController::class, 'countDriver']);
   //update bus driver records
   Route::put('updatebusdriver/{id}', [driverController::class, 'updateDriverByAdmin']);
 //reset device
    Route::post('reset/driverdevice/{id}',[AdminController::class, 'resetDriverByAdmin']);  
 //show in-active drivers:
  Route::get('showinactivedrivers/{companyid}',[AdminController::class, 'showInactiveDrivers']);
//show drivers of a owner:
 Route::get('driversofowner/{ownerphone}',[driverController::class, 'showDriverofOwnerForAdmin']);
 //set driver behaviour description
 Route::post('setdriverbehaviour',[DriverBehaviourController::class, 'setDriverBehaviour']);
 //show drivers behaviour of the company
  Route::get('showdriverbehaviour/{companyid}',[DriverBehaviourController::class, 'viewDriverBehaviour']);
  //show blacklisted drivers of the company
  Route::get('showblacklisteddriver/{companyid}',[DriverBehaviourController::class, 'showBlackListDriver']);
  //_________end of Bus Driver
   //----------------------------------------------------
 //_________________for Staff of Company
 //for registering staff
 Route::post('staff/register',[staffController::class, 'staffRegister'])->name('staffRegister');
 //get all staff of all companies
  Route::get('allstaff',[AdminController::class, 'indexStaff']);
    //count total staff of specific company
   Route::get('count/specific/staff/{id}',[AdminController::class, 'countStaffSpecificCompany']);
   //get staffs info of specific company 
  Route::get('staff/filterby/company/{id}',[AdminController::class, 'ListStaffSpecific']);
  //get details of specific staff
   Route::get('staffinfo/{id}',[AdminController::class, 'showStaffDetail']);
  //count total staffs of all company
  Route::get('countstaff',[AdminController::class, 'countStaff']);
   //update bus driver records
   Route::put('update/staff/{id}', [staffController::class, 'updateStaff']);
    //delete company staff records
   Route::get('delete/staff/{id}', [staffController::class, 'deleteStaffInfo']);
  //reset device
    Route::post('reset/staffdevice/{id}',[AdminController::class, 'resetStaffByAdmin']);  
  
   
   
   //for registering staff attendence in the system
 Route::post('staff/attendance/register',[staffAttendanceController::class, 'RegisterAttendance'])->name('staffRegister');
   //update bus company records for setting arival and depart time!!
   Route::put('update/company/arivalanddepart/{id}', [staffAttendanceController::class, 'setCompanyArAndDepTime']);
   //update all staff attendance records
  Route::put('update/company/attendance/{id}', [staffAttendanceController::class, 'updateAttendance']);
 //get all staff attendance record of company
 Route::get('getall/attendence/{id}',[staffAttendanceController::class, 'getAttendenceInfo']);
 //get  staff attendance detail record of staffs
 Route::get('get/attendence/{id}',[staffAttendanceController::class, 'showStaffAtendanceInfo']);
 
  //get all staff attendance record of specific date
 Route::get('filterby/attendencedate/{company_id}',[staffAttendanceController::class, 'filterAttendancebySpecificDate']);
 //from and to:filter attendance of staff
  Route::get('filterby/specific/attendencedate/{company_id}',[staffAttendanceController::class, 'filterbySpecificDate']);
 
 //get staff atedance detail by ther id
  Route::get('filterby/staff/attendence/{ids}',[staffAttendanceController::class, 'getAttendanceDetailOfStaff']);
 //insert attendance data from staff to atendance table
  Route::get('insertallstaff/attendenceby/{company_id}',[staffAttendanceController::class, 'StoreAtClick']);
 //filter data by today date
  Route::get('filerby/attendencedate/{company_id}',[staffAttendanceController::class, 'filterAttendanceByDate']);
 //update attendance status of staff
  Route::post('filterby/attendencestatus/{staff_id}/{created_at}',[staffAttendanceController::class, 'updateAttendaceStatus']);
 //filter attendace by status:of company of current day
   Route::get('checkby/attendencestatus/{company_id}',[staffAttendanceController::class, 'showByAttendanceStatusOfCurrentDay']);
   //filter attendace by status:of company of specific duration(from and to)
   Route::get('checkby/attendencestatus/specificduration/{company_id}',[staffAttendanceController::class, 'AttendanceStatusOfSpecificDuration']);
   //filter indivisual staff status:current day
   Route::get('checkstaff/attendencestatus/{staff_id}',[staffAttendanceController::class, 'staffAttendanceStatusOfCurrentDay']);
   //filter indivisual staff status:from specific duration(from and to)
    Route::get('checkstaff/attendencestatus/specificduration/{staff_id}',[staffAttendanceController::class, 'staffAttendanceStatusOfSpecificDuration']);
   //update staff departure time and remarks:
   Route::put('update/staffdepartandremarks/{id}', [staffAttendanceController::class, 'updateDepAndRemarks']);
   
   //count attendace by status:of company of current day
   Route::get('countby/attendencestatus/{company_id}',[staffAttendanceController::class, 'countByAttendanceStatusOfCurrentDay']);
    //count attendace by status:of company of specific duration(from and to)
   Route::get('countby/attendencestatus/specificduration/{company_id}',[staffAttendanceController::class, 'CountAttendanceStatusOfSpecificDuration']);
   //count indivisual staff status:current day
   Route::get('countstaff/attendencestatus/{staff_id}',[staffAttendanceController::class, 'CountStaffAttendanceStatusOfCurrentDay']);
   //count indivisual staff status:from specific duration(from and to)
    Route::get('countstaff/attendencestatus/specificduration/{staff_id}',[staffAttendanceController::class, 'countStaffAttendanceStatusOfSpecificDuration']);
   //_________________End of Staff routes
   
   //_________________Conductor Portion
   
     //register conductor by system admin
  Route::post('conductorregister',[ConductorController::class, 'conductorRegister']);
    //show conductors of the company
   Route::get('showallconductors/{companyid}',[ConductorController::class, 'showConductor']);  
 //set conductor behaviour description
 Route::post('setconductorbehaviour',[ConductorBehaviourController::class, 'setConductorBehaviour']);
 //show conductors behaviour of the company
  Route::get('showconductorbehaviour/{companyid}',[ConductorBehaviourController::class, 'viewConductorBehaviour']);
  //show blacklisted conductors of the company
  Route::get('showblacklistedconductor/{companyid}',[ConductorBehaviourController::class, 'showBlackListConductor']);

 
 
  
  //**********************SMS portion**********************
  //set initial balance for company
   Route::post('setinitialbalanceforcompany',[smsController::class, 'SetInitialPriceValue']);
 //send sms to indivisual person
   Route::post('indivisualsms/{phone}',[smsController::class, 'smsTosingleVehicleOwner']);
 //send bulk sms to owner,staff,driver and all members
 Route::post('choicebulksms/{id}',[smsController::class, 'bulkSmsToDifferentMembers']);
 //send bulk sms to owners having end date near by 3 days!
 Route::post('bulksmsendatesowner',[smsController::class, 'smsEndDateOwner']);
 Route::post('routewisebulksmsowner/{routeno}',[smsController::class, 'bulkSmsRouteWise']);
 Route::get('smshistory/{companyid}',[smsController::class, 'smsHistory']);
 //_________________End of SMS section
 
  //_________________Start of Notification routes:::::
  
  //notification to:owner and driver from: bus update
  Route::post('notify/owneranddriver',[AllNotificationController::class, 'sendNotificationToOwnerAndDriver']);
 
  //notification :show only admin type
    Route::get('getadminnotification/{company_id}',[AllNotificationController::class, 'getAdmintypeNotification']);
//count unread notification of admin type by company id:    
     Route::get('countadminnotification/{company_id}',[AllNotificationController::class, 'countAdmintypeUnreadNotification']);
     //notification to:owner and driver from :owner update
  Route::post('notify/owneranddriver/ownerupdate/{id}',[AllNotificationController::class, 'NotificationOwnerUpdate']);
  //mark single notifiction as read by notification id:
   Route::post('markasread/singlenotification/{id}',[AllNotificationController::class, 'MarkAsReadSingleAdminNotification']);
  //mark all notifiction as read by company id:
   Route::post('markasread/allnotification/{company_id}',[AllNotificationController::class, 'markAsReadAllAdminNotification']);
  
  //get all(of any type) admin notification of a company:
  Route::get('getalladminnotification/{company_id}',[AllNotificationController::class, 'getAllAdminNotification']);
 //notification to: staff of :staff update ,by:staff id
  Route::post('notifystaff/ofstaffupdate/{staff_id}',[AllNotificationController::class, 'staffUpdateByAdmin']);
//notification to:owner of:bus entry, by:owner phone
  Route::post('notifybusowner/ofbusentry/{owner_phone}',[AllNotificationController::class, 'notifyOwnerOfBusEntry']);
 
 //_________________End of Notification routes
 
//__________________creating Roles for the Company staffs:::

//create new role for staffs:
Route::post('rolesforstaff/create',[staffController::class, 'createRoleForStaff']);
//get all roles of company
 Route::get('getallroles/{company_id}',[staffController::class, 'showRoleBycompany']);
//get all staff roles by company:
 Route::get('getallstaffroles/{company_id}',[staffController::class, 'showStaffRoleBycompany']);
 //update staff roles:
   Route::put('update/staffroles/{staff_id}', [staffController::class, 'updateStaffRole']);
//delete  staff roles
 Route::get('deletestaffroles/{staff_id}',[staffController::class, 'deleteStaffRoles']);
//__________________End of Roles for the Company staffs:::

 //_________________Start of News routes:::::
Route::post('createnews',[NewsController::class, 'CreateNews']); 
Route::get('shownews/{companyid}',[NewsController::class, 'getNews']);
Route::get('showimageofnews/{newsid}',[NewsController::class, 'shownewsImage']);
Route::delete('deletenewsimage/{imageid}',[NewsController::class, 'deleteImageFromStorage']);
Route::get('shownewsdetail/{newsid}',[NewsController::class, 'showNewsDetail']);
Route::put('updatenewsdetail/{newsid}',[NewsController::class, 'updateNewsWithImage']);
Route::delete('deletenews/{newsid}',[NewsController::class, 'deleteNewsAndImage']);

//_________________Start of Event routes:::::
Route::post('createevent',[EventController::class, 'CreateEvent']); 
Route::get('showevent/{companyid}',[EventController::class, 'getEvents']);
Route::get('showimageofevent/{eventid}',[EventController::class, 'showEventImage']);
Route::delete('deleteeventimage/{imageid}',[EventController::class, 'deleteImageFromStorage']);
Route::get('showneventdetail/{eventid}',[EventController::class, 'showEventDetail']);
Route::put('updateeventdetail/{eventid}',[EventController::class, 'updateEventWithImage']);
Route::delete('deleteevent/{eventid}',[EventController::class, 'deleteEventAndImage']);


//_________________for General Routes
 Route::get('countall/{id}',[AdminController::class, 'countAllMembers']);
 //show everything in the dashboard of a company ::admin,staff,owner,driver,new owner,total bus,totalmember 
 Route::get('showalldashboardelements/{ids}',[AdminController::class, 'showEveryDashoboardComponent']);
//create vehicle routes for the company 
Route::post('createroutes',[VehicleRouteController::class, 'createRoutes']);
//set route for the indivisual vehicle 
Route::post('setvehicleroute',[VehicleRouteController::class, 'SetVehicleRoute']);
//get all routes of the company
Route::get('getroutes/{companyid}',[VehicleRouteController::class, 'getAllRoute']);
//get vehicles without routes
Route::get('getvehicleswithoutroute/{companyid}',[VehicleRouteController::class, 'ShowVehicleswithoutRoute']);
//get all routes of the company
Route::delete('deleteroute/{routeid}',[VehicleRouteController::class, 'deleteRoute']);
//update route by route id:
Route::put('updateroute/{id}',[VehicleRouteController::class, 'editRoute']);
//show all vehicles having routes of company
Route::get('vehicleswithroutes/{companyid}',[VehicleRouteController::class, 'getVehicleWithRouteDetail']);
//show vehicles of different routes
Route::get('vehicleofdifferentroute/{routeno}',[VehicleRouteController::class, 'getIndivisualVehicleRouteDetail']);
//delete(remove) vehicle from route
Route::delete('deletevehiclefromroute/{id}',[VehicleRouteController::class, 'deleteVehiclefromRoute']);
//show vehicles of a route and count vehicles
Route::get('showandcountvehiclesofroute/{routeno}',[VehicleRouteController::class, 'showAndCountVehiclesofRoute']);
//show vehicles of different routes of company
Route::get('vehicleroutesofcompany/{companyid}',[VehicleRouteController::class, 'showDifferentRouteOfCompany']);
});


/* 
request handler

// Allow up to 60 requests in 1 minute for that route (= 1 req/s)
Route::get('api/v1/user', 'Api\UserController@index')->middleware('throttle:60,1');

// Allow up to 60 requests in 60 minutes for that route (= 1 req/m)
Route::post('api/v1/user', 'Api\UserController@store')->middleware('throttle:60,60');

// Rate Limiting for a whole group of routes
Route::group(['middleware' => 'throttle:60,1'], function () {
    // [...]

Another way to implement auth
Route::namespace('Auth')->group(function(){
        
    //Login Routes
    Route::get('/login','LoginController@showLoginForm')->name('login');
    Route::post('/login','LoginController@login');
    Route::post('/logout','LoginController@logout')->name('logout');

    //Forgot Password Routes
    Route::get('/password/reset','ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('/password/email','ForgotPasswordController@sendResetLinkEmail')->name('password.email');

    //Reset Password Routes
    Route::get('/password/reset/{token}','ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('/password/reset','ResetPasswordController@reset')->name('password.update');

});
*/