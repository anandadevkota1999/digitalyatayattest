<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Users\busOwnerController;
use App\Http\Controllers\Api\Users\driverController;
use App\Http\Controllers\Api\Users\VehicleRouteControllerAndroid;
use App\Http\Controllers\Api\Users\busInfoController;
use App\Http\Controllers\Api\Admin\AdminController;
use App\Http\Controllers\Api\Admin\AllNotificationController;
use App\Http\Controllers\Api\Users\staffController;
use App\Http\Controllers\Api\Users\staffAttendanceController;
use App\Http\Controllers\Api\NotificationController;
use App\Http\Controllers\Api\BeforeLoginController;
use App\Http\Controllers\sampleController;
use App\Notifications\NewUserNotification;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('dbbackup',[BeforeLoginController::class, 'backupDb']);
Route::get('companydetail',[BeforeLoginController::class, 'CompanyDetail']);
//Route::post('storemap',[sampleController::class, 'mapstore']);
Route::get('getmaplocation/{userid}',[sampleController::class, 'getMapLocation']);
Route::get('getallmaplocation',[sampleController::class, 'getencrptdata']);
Route::get('getfromchitwanyatayat',[BeforeLoginController::class, 'getDataFromChitwanYatayat']);
Route::get('createdb',[sampleController::class, 'createdb']);
Route::post('sendtochitwandb', [sampleController::class, 'store']);
Route::post('tore',[sampleController::class, 'POL']);

Route::post('uploadvideo', [busOwnerController::class, 'storevideotostorage']);
/**************** for bus owner.....*****************/
Route::post('choicelogin/{id}',[busOwnerController::class, 'choiceLogin'])->name('choiceLogin');
Route::post('busowner/login',[busOwnerController::class, 'login'])->name('busOwnerLogin');
Route::post('busowner/register',[busOwnerController::class, 'busOwnerRegister'])->name('busOwnerRegister');
//sample notification

Route::get('deletepro/{id}',[busOwnerController::class, 'del']);

Route::post('notify/user',[NotificationController::class, 'sendfferNotification']);

//...............routegroup for bus owners......
Route::group( ['prefix' => 'busowner','middleware' => ['auth:busOwner-api'] ],function(){
   //---------------- authenticated bus owners routes here 
Route::get('count',[busOwnerController::class, 'countowner']);
Route::get('list',[busOwnerController::class, 'index']);
Route::get('get/driverdetail/{id}',[busOwnerController::class, 'combinedDatas']);
Route::get('combined/all',[busOwnerController::class, 'combinedDatas']);
Route::get('show/{id}',[busOwnerController::class, 'showall']);
Route::delete('delete/{id}',[busOwnerController::class, 'delete'])->name('driverDelete');
//for changing owner password
Route::post('changepassword/{id}',[busOwnerController::class, 'changeOwnerPassword']);
 //owner detail of specific company
    Route::get('ownerinfo/{id}',[busOwnerController::class, 'showOwnerDetail']);
 //update bus owner records
   Route::put('update/busowner/{id}', [busOwnerController::class, 'updateBusOwner']);
   //update owner profile picture only
     Route::put('update/busowner/image/{id}', [busOwnerController::class, 'updateProfileOnly']);
   //logout busowner
   Route::post('logout/{id}',[busOwnerController::class, 'logoutOwner']);
   //check logout status of owner
 Route::get('checkloginstatus/{id}',[busOwnerController::class, 'checkLoginStatusOwner']); 
//------------------authenticated bus info routes   
//for bus Info _____________________
Route::get('hasbus/{ownerphone}',[busInfoController::class, 'showBusOfOwnerForOwner']);
 //update bus info records
   Route::put('update/businfo/{id}', [AdminController::class, 'updateBusInfo']);
//-----------------authenticated driver routes
//for bus driver ________________________
Route::post('driver/register',[driverController::class, 'driverRegister']);
//update bus driver records by bus owner
   Route::put('update/driver/{id}', [driverController::class, 'updateDriverByOwner']);
 //update owner profile picture only
     Route::put('update/driver/image/{id}', [driverController::class, 'updateProfileOnly']); 
 //remove driver by vehicle owner
  Route::get('removedriver/{driverid}', [busOwnerController::class, 'removeDriverByOwner']);
     
//-----------------Vehicle route system---------------------
//get all routes of the company
Route::get('getroutes/{companyid}',[VehicleRouteControllerAndroid::class, 'getAllRoute']);
//get vehicle list of different bus
Route::get('getvehicleswithroute/{busid}',[VehicleRouteControllerAndroid::class, 'getVehiclesOfParticularRoute']);

//----------------News Section ------------------------
 Route::get('shownewswithimages/{ownerid}',[busOwnerController::class, 'showOwnerNewsWithImage']);
 Route::get('countviews/{newsid}',[busOwnerController::class, 'countNewsViews']);
 Route::get('filternews/{category}',[busOwnerController::class, 'showNewsByCategory']);
 //----------------Event Section ------------------------
 Route::get('showeventwithimages/{ownerid}',[busOwnerController::class, 'showOwnerEventsWithImage']);
 Route::get('counteventviews/{eventid}',[busOwnerController::class, 'countEventViews']);
 Route::get('filterevent/{category}',[busOwnerController::class, 'showEventByCategory']);
 
});


/**************** for Bus(vehicle) information.....*****************/

//<<<<<<<...............routegroup for bus drivers......>>>>
Route::group( ['prefix' => 'driver','middleware' => ['auth:busDriver-api'] ],function(){
//below routes are called after login actions...............
//for changing driver password
Route::post('changepassword/{id}',[driverController::class, 'changeDriverPassword']);
//for showing driver of bus(driver beling to bus)
Route::get('ofbus/{id}',[driverController::class, 'ShowDriverOfBus']);
//for showing bus of driver(bus belong to driver)
Route::get('hasbus/{id}',[busInfoController::class, 'ShowBusOfDriver']);
//update bus driver records by bus owner
   Route::put('update/{id}', [driverController::class, 'updateDriverByOwner']);
   //update bus driver records
   Route::put('updates/{id}', [driverController::class, 'updateDriver']);
//get details of specific driver
   Route::get('driverinfo/{id}',[driverController::class, 'showDriverDetail']);
//get owner of driver:
   Route::get('hasowner/{id}',[driverController::class, 'getOwnerListofDriver']);
   //logout driver by id
 Route::post('logout/{id}',[driverController::class, 'logoutDriver']);    
//check logout status of driver
 Route::get('checkloginstatus/{id}',[driverController::class, 'checkLoginStatusDriver']);  

//-----------------Vehicle route system---------------------
//get all routes of the company
Route::get('getroutes/{companyid}',[VehicleRouteControllerAndroid::class, 'getAllRoute']);
//get vehicle list of different bus
Route::get('vehicleswithrouteofdriver/{driverid}',[VehicleRouteControllerAndroid::class, 'VehiclesOfParticularRouteOfDriver']);

//******* News Portal ******
 Route::get('shownewswithimages/{driverid}',[driverController::class, 'showDriverNewsWithImage']);
Route::get('countviews/{newsid}',[driverController::class, 'countNewsViews']);
Route::get('filternews/{category}',[busOwnerController::class, 'showNewsByCategory']);
//******* Event Portal ******
 Route::get('showeventwithimages/{driverid}',[driverController::class, 'showDriverEventWithImage']);
 Route::get('counteventviews/{eventid}',[driverController::class, 'countEventViews']);
 Route::get('filterevent/{category}',[busOwnerController::class, 'showEventByCategory']);

});


//<<<<<<<...............routegroup for Staffs......>>>>
Route::group( ['prefix' => 'staff','middleware' => ['auth:staff-api'] ],function(){
//below routes are called after login actions...............
//for changing driver password
Route::post('changepassword/{id}',[staffController::class, 'changeStaffPassword']);
//logout staff by id
 Route::post('logout/{id}',[staffController::class, 'logoutStaff']);   
 //update staffs records
   Route::put('update/{id}', [staffController::class, 'updateStaff']);
//update staff profile picture only
  Route::put('update/profilepicture/{id}', [staffController::class, 'updateStaffProfileOnly']);
//__________________ Staff Attendance Section _______________________
 //Indivisual staff attendance history
 Route::get('getattendancehistory/{id}',[staffAttendanceController::class, 'getAttendenceHistory']);
//count attendance status
 Route::get('countattendance/{staffid}',[staffAttendanceController::class, 'countAttendaceSpecificDuration']);    
//show indivisual staff role
 Route::get('showrole/{staffid}',[staffController::class, 'showIndivisualRole']);
  //show indivisual staff detail
 Route::get('detail/{id}',[staffController::class, 'staffDetail']); 
 
 //******* News Portal ******
 Route::get('shownewswithimages/{staffid}',[staffController::class, 'showStaffNewsWithImage']);
 Route::get('countviews/{newsid}',[staffController::class, 'countNewsViews']);
 Route::get('filternews/{category}',[busOwnerController::class, 'showNewsByCategory']);
//******* Event Portal ******
 Route::get('showeventwithimages/{staffid}',[staffController::class, 'showStaffEventWithImage']);
 Route::get('counteventviews/{eventid}',[staffController::class, 'countEventViews']);
 Route::get('filterevent/{category}',[busOwnerController::class, 'showEventByCategory']);

//-----------------Vehicle route system---------------------
//get all routes of the company
Route::get('getroutes/{companyid}',[VehicleRouteControllerAndroid::class, 'getAllRoute']);

    
});

Route::group( ['prefix' => 'bus'],function(){
 Route::post('register',[busInfoController::class, 'busRegister'])->name('busRegister');
 Route::get('count',[busInfoController::class, 'countDriver']);
 Route::get('list',[busInfoController::class, 'index']);
 Route::get('show/{id}',[busInfoController::class, 'showall']);
//for showing bus record of the owner

 
});




/**************** for bus driver.....*****************/
//Route::post('driver/register',[driverController::class, 'driverRegister'])->name('driverRegister');
//Route::post('driver/login',[driverController::class, 'driverLogin'])->name('driverLogin');

//...............routegroup for bus drivers......
Route::group( ['prefix' => 'driver','middleware' => ['auth:busDriver-api'] ],function(){
   // authenticated bus drivers routes here 

Route::get('/count',[driverController::class, 'countDriver']);
Route::get('/list',[driverController::class, 'index']);
Route::get('/show/{id}',[driverController::class, 'showall']);


 
});

//__________________________route for notification in android ::
//notification :show any type from db
    Route::get('getanynotification/{notifiable_id}',[AllNotificationController::class, 'getAlltypeNotification']);
//get history of notification      
Route::get('gethistoryofanynotification/{notifiable_id}',[AllNotificationController::class, 'getHistryOfAnyNotificaion']);

//get notification by:android null status::
Route::get('getnullnotification/{notifiable_id}',[AllNotificationController::class, 'getAndroidNulltypeNotification']);
//update notification by:android null status::
Route::put('updatenullnotification/{notifiable_id}',[AllNotificationController::class, 'updateAndroidNullNotification']);
//notify:owner and Admin of driver updates
Route::post('notify/ownerandadmin/driverupdate/{id}',[AllNotificationController::class, 'driverUpdateToOwnerAndAdmin']);
//notify:owner and Admin of bus updates
  Route::post('notify/driverandadmin/busupdate/{id}',[AllNotificationController::class, 'busUpdateToDriverAndAdmin']);
//notify:driver and admin of bus owner update:
 Route::post('notify/driverandadmin/ownerupdate/{id}',[AllNotificationController::class, 'ownerUpdateToDriverAndAdmin']);
 //notify:send driver update notification to driver and admin done by driver(bus) owner:
 Route::post('notify/driverandadmin/driverupdatebyowner/{id}',[AllNotificationController::class, 'driverUpdateByOwnerToDriverAndAdmin']);
//notify:admin of staff details update:
 Route::post('notify/admin/ofstaffupdate/{staff_id}',[AllNotificationController::class, 'staffUpdateToAdmin']);
//notify:send birthday notification to driver of any company:
 Route::post('notify/birthday/todriver',[AllNotificationController::class, 'birthdayNotificationToDriver']);
 //notify:send birthday notification to driver of any company:
 Route::post('notify/birthday/toowner',[AllNotificationController::class, 'birthdayNotificationToOwner']);
 //notify:send birthday notification to driver of any company:
 Route::post('notify/birthday/tostaff',[AllNotificationController::class, 'birthdayNotificationToStaff']);
 
 
 
//notify:send warning notification to bus owner of end dates:check pass,route permit,billbook and insurance
Route::post('notify/busowner/ofenddates',[AllNotificationController::class, 'notifyOwnerAndAdminOfEndDates']);

//__________________________End of route for notification in android ::

/**************** for STAFFS .....*****************/

Route::post('staff/login',[staffController::class, 'staffLogin'])->name('staffLogin');
//
//for roles::::::::::
//create roles:
Route::post('roles/create',[staffController::class, 'createRole']);



