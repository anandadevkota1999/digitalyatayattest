<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Admin\AdminLoginController;
use App\Http\Controllers\sampleController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('/linkstorage', function () {
  //  Artisan::call('storage:link');
//});
Route::get('phone-auth', [sampleController::class, 'index']);
 Route::post('/update/otplink',[AdminLoginController::class, 'updateForgetPassword']);
Route::resource('changepassord', AdminLoginController::class); 
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();
